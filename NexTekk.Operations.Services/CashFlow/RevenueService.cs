﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.CashFlow
{
    public class RevenueService : IRevenueService
    {
        private readonly IRevenueRepository _revenueRepository;
        private readonly IExpenseRepository _expenseRepository;
        private readonly IUserContext _userContext;
        private readonly IRevenueEmailSender _revenueEmailSender;

        public RevenueService(
            IRevenueRepository revenueRepository,
            IUserContext userContext,
            IExpenseRepository expenseRepository,
            IRevenueEmailSender revenueEmailSender)
        {
            _revenueRepository = revenueRepository;
            _userContext = userContext;
            _expenseRepository = expenseRepository;
            _revenueEmailSender = revenueEmailSender;
        }

        public async Task<Revenue> GetRevenueAsync(Guid id)
        {
            try
            {
                return await _revenueRepository.Get(id);
            }
            catch (Exception ex)
            {
                throw new ObjectNotFoundException($"No revenue found for id {id}", ex);
            }
        }

        public async Task DeleteRevenueAsync(Guid id)
        {
            try
            {
                var revenue = await GetRevenueAsync(id);

                ValidateUserCanDelete(revenue);

                revenue.Deleted = true;
                revenue.Modified = DateTime.UtcNow;
                revenue.ModifiedById = _userContext.GetUserId();

                await _revenueRepository.Update(revenue);

                await _revenueEmailSender.SendRevenueDeletedEmail(revenue);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to delete revenue", ex);
            }
        }

        public async Task<IList<Revenue>> GetRevenuesAsync(RevenueSearchCriteria searchCriteria)
        {
            try
            {
                searchCriteria.PageSize = searchCriteria.PageSize ?? 100;
                searchCriteria.NumberToSkip = searchCriteria.NumberToSkip ?? 
                    (searchCriteria.PageNumber > 0 ? searchCriteria.PageNumber - 1 : 0) * 100;

                var revenues = await _revenueRepository.GetAll(searchCriteria) as List<Revenue>;
                
                if (searchCriteria.IncludeExpenses && searchCriteria.ClientIds.IsNullOrEmpty())
                {
                    var expenseSearchCriteria = new ExpenseSearchCriteria
                    {
                        IncurredDateRange = searchCriteria.PaymentDate,
                        PageNumber = searchCriteria.PageNumber,
                        DescriptionContains = searchCriteria.Description,
                        UserId  = searchCriteria.EnteredBy,
                        MinAmount = searchCriteria.MinAmount,
                        MaxAmount = searchCriteria.MaxAmount,
                        Currency = searchCriteria.Currency
                    };

                    await _expenseRepository.Search(expenseSearchCriteria)
                        .ContinueWith(task => task.Result.ForEach(expense => revenues.Add(AutoMapper.Mapper.Map<Revenue>(expense))));
                }

                revenues.Sort((a, b) => b.PaymentDate.CompareTo(a.PaymentDate));

                return revenues;
            }
            catch (Exception ex)
            {
                throw new ObjectNotFoundException("Failed to retrieve revenue based on the set search criteria", ex);
            }
        }

        public async Task<Revenue> SaveRevenueAsync(Revenue revenue)
        {
            var isNew = revenue.Id == Guid.Empty;
            var userId = _userContext.GetUserId();

            revenue.Modified = DateTime.UtcNow;
            revenue.ModifiedById = userId;
                
            var savedRevenue = default(Revenue);

            if(isNew)
            {
                savedRevenue = await CreateAsync(revenue);
            }
            else
            {
                await UpdateAsync(revenue);
                savedRevenue = revenue;
            }

            return savedRevenue;   
        }
        
        private async Task<Revenue> CreateAsync(Revenue revenue)
        {
            revenue.Created = revenue.Modified;
            revenue.CreatedById = revenue.ModifiedById;

            try
            {
                await _revenueRepository.Create(revenue);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to create revenue", ex);
            }

            await _revenueEmailSender.SendRevenueAddedEmail(revenue);

            return revenue;
        }

        private async Task UpdateAsync(Revenue revenue)
        {
            try
            {
                var dbRevenue = await GetRevenueAsync(revenue.Id);

                ValidateUserCanEdit(dbRevenue);

                dbRevenue.Amount = revenue.Amount;
                dbRevenue.ClientId = revenue.ClientId;
                dbRevenue.Currency = revenue.Currency;
                dbRevenue.Description = revenue.Description;
                dbRevenue.PaymentDate = revenue.PaymentDate;

                await _revenueRepository.Update(dbRevenue);

                await _revenueEmailSender.SendRevenueUpdatedEmail(revenue);
            }
            catch (Exception ex)
            {
                throw new OperationException("Revenue can not be edited", ex);
            }
        }

        private void ValidateUserCanDelete(Revenue revenue)
        {
            if (!(DateTime.UtcNow.Subtract(revenue.Modified).Days < 30))
            {
                throw new OperationException("Expense was last modified over 30 days");
            }
        }

        private void ValidateUserCanEdit(Revenue entity)
        {
            if (!(DateTime.Today.Subtract(entity.Created).Days < 30))
            {
                throw new OperationException("Revenue has been created for over 30 days");
            }
        }
    }
}
