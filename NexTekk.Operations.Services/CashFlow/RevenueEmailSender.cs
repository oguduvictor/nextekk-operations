﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.CashFlow
{
    public class RevenueEmailSender : IRevenueEmailSender
    {
        private const string ADDED = "Added";
        private const string DELETED = "Deleted";
        private const string UPDATED = "Updated";

        private readonly IEmailService _emailService;
        private readonly IUserLookupService _userLookupService;
        private readonly IClientLookupService _clientLookupService;

        public RevenueEmailSender(IEmailService emailService, IUserLookupService userLookupService, IClientLookupService clientLookupService)
        {
            _emailService = emailService;
            _userLookupService = userLookupService;
            _clientLookupService = clientLookupService;
        }

        public async Task SendRevenueAddedEmail(Revenue revenue)
        {
            await SendRevenueEmailAsync(ADDED, revenue);
        }

        public async Task SendRevenueUpdatedEmail(Revenue revenue)
        {
            await SendRevenueEmailAsync(UPDATED, revenue);
        }

        public async Task SendRevenueDeletedEmail(Revenue revenue)
        {
            await SendRevenueEmailAsync(DELETED, revenue);
        }
        
        private async Task SendRevenueEmailAsync(string emailType, Revenue revenue)
        {
            if (!AppSettingsProvider.Current.EnableStatusChangeEmail)
            {
                return;
            }

            var usersToMail = await GetUsersToMail(revenue.ModifiedById);
            var editor = await _userLookupService.GetUserAsync(revenue.ModifiedById);
            var client = await _clientLookupService.GetClientAsync(revenue.ClientId);

            foreach (var user in usersToMail)
            {
                var emailStringBuilder = new StringBuilder();
                emailStringBuilder.AppendFormat("Dear {0},", user.FirstName);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("The following revenue has just been {0} by {1} {2} :", 
                                                emailType.ToLower(), editor?.LastName.Trim(), editor?.FirstName.Trim());
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Amount: {0}{1}", revenue.Currency.GetCurrencySymbol(), revenue.Amount);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Payment Date: {0}", revenue.PaymentDate.ToString("MMM dd yyyy"));
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Client: {0}", client?.Name);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Description: {0}", revenue.Description);

                var email = new Email
                {
                    Subject = $"Nextekk Revenue {emailType}",
                    Body = emailStringBuilder.ToString(),
                    Sender = AppSettingsProvider.Current.SenderEmail,
                    Recipients = new List<string> { user.Email },
                };

                try
                {
                    await _emailService.SendEmail(email);
                }
                catch (Exception ex)
                {
                    throw new EmailFailedException($"{emailType} revenue mail failed to send", ex);
                }
            }
        }
        
        private async Task<IEnumerable<UserEntity>> GetUsersToMail(Guid excludeManagerId) =>
            await _userLookupService.GetManagersAsync()
                .ContinueWith(task => task.Result.Where(user => user.Id != excludeManagerId));
    }
}
