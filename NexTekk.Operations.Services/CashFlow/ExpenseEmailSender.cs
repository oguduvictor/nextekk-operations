﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.CashFlow
{
    public class ExpenseEmailSender : IExpenseEmailSender
    {
        private const string ADDED = "Added";
        private const string DELETED = "Deleted";
        private const string UPDATED = "Updated";
        private const string REIMBURSED = "Reimbursed";

        private readonly IEmailService _emailService;
        private readonly IUserLookupService _userLookupService;

        public ExpenseEmailSender(IEmailService emailService, IUserLookupService userLookupService)
        {
            _emailService = emailService;
            _userLookupService = userLookupService;
        }

        public async Task SendExpenseCreationMailAsync(Expense expense)
        {
            await SendExpenseMailAsync(ADDED, expense);
        }
        
        public async Task SendExpenseDeletionMailAsync(Expense expense)
        {
            await SendExpenseMailAsync(DELETED, expense);
        }

        public async Task SendExpenseUpdateMailAsync(Expense expense)
        {
            await SendExpenseMailAsync(UPDATED, expense);
        }

        public async Task SendExpenseReimbursedMailAsync(Expense expense)
        {
            

            await SendExpenseMailAsync(REIMBURSED, expense);
        }

        private async Task SendExpenseMailAsync(string emailType, Expense expense)
        {
            if (!AppSettingsProvider.Current.EnableStatusChangeEmail)
            {
                return;
            }

            var editor = await _userLookupService.GetUserAsync(expense.ModifiedById);

            var usersToMail = await GetUsersToMail(expense.CreatedById);

            foreach (var user in usersToMail) 
            {
                var emailStringBuilder = new StringBuilder();

                emailStringBuilder.AppendFormat("Dear {0},", user.FirstName);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("The ​following ​expense has​ just ​been {0} ​by {1} {2}:",
                    emailType.ToLower(), editor.FirstName.Trim(), editor.LastName.Trim());
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Amount: {0}", expense.Amount);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Description: {0}", expense.Description);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Date: {0}", expense.Date);
                if (expense.Reimbursed.HasValue)
                {
                    emailStringBuilder.AppendLine();
                    emailStringBuilder.AppendFormat("Reimbursed: {0}", expense.Reimbursed);
                }

                var email = new Email
                {
                    Subject = $"​NexTekk​​ ​Expense {emailType}",
                    Body = emailStringBuilder.ToString(),
                    Sender = AppSettingsProvider.Current.SenderEmail,
                    Recipients = new List<string> { user.Email }
                };

                try
                {
                    await _emailService.SendEmail(email);
                }
                catch (Exception e)
                {
                    throw new EmailFailedException($"{emailType} expense mail failed to send", e);
                }
            }
        }

        private async Task<IEnumerable<UserEntity>> GetUsersToMail(Guid excludeManagerId) =>
            await _userLookupService.GetManagersAsync()
                .ContinueWith(task => task.Result.Where(user => user.Id != excludeManagerId));
    }
}
 