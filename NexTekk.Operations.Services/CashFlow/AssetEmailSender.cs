﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.CashFlow
{
    public class AssetEmailSender : IAssetEmailSender
    {
        private readonly IEmailService _emailService;
        private readonly IUserLookupService _userLookupService;

        public AssetEmailSender(IEmailService emailService, IUserLookupService userLookupService)
        {
            _emailService = emailService;
            _userLookupService = userLookupService;
        }

        public async Task SendRequestEmail(Asset asset, Guid requesterId)
        {
            if (!AppSettingsProvider.Current.EnableStatusChangeEmail)
            {
                return;
            }

            var usersToMail = await _userLookupService.GetManagersAsync().ContinueWith(task => task.Result.Where(user => user.Id != requesterId));
            var requester = await _userLookupService.GetUserAsync(requesterId);

            foreach (var user in usersToMail)
            {
                var emailStringBuilder = new StringBuilder();
                emailStringBuilder.AppendFormat("Dear {0},", user.FirstName);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("The following asset has just been requested by {0}:", requester?.FullName.Trim());
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Item: {0}", asset.Item);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Manufacturer: {0}", asset.Manufacturer);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Model: {0}", asset.Model);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Serial Number: {0}", asset.SerialNo);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Category: {0}", asset.Category);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Condition: {0}", asset.Condition.ToString());
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Description: {0}", asset.Description);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Cost: {0}{1}", asset.Currency.GetCurrencySymbol(), Math.Round(asset.Cost, 2, MidpointRounding.AwayFromZero));

                var email = new Email
                {
                    Subject = "Asset Requested",
                    Body = emailStringBuilder.ToString(),
                    Sender = AppSettingsProvider.Current.SenderEmail,
                    Recipients = new List<string> { user.Email },
                };

                try
                {
                    await _emailService.SendEmail(email);
                }
                catch (Exception ex)
                {
                    throw new EmailFailedException("Asset request mail sending failed", ex);
                }
            }
        }
    }
}
