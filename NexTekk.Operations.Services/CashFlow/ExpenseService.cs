﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.CashFlow
{
    public class ExpenseService : IExpenseService
    {
        private readonly IExpenseRepository _expenseRepository;
        private readonly IUserContext _userContext;
        private readonly IExpenseEmailSender _expenseEmailSender;

        public ExpenseService(
            IExpenseRepository expenseRepository,
            IUserContext userContext,
            IExpenseEmailSender expenseEmailSender)
        {
            _expenseRepository = expenseRepository;
            _userContext = userContext;
            _expenseEmailSender = expenseEmailSender;
        }

        public Task<Expense> Get(Guid id)
        {
            try
            {
                return _expenseRepository.Get(id);
            }
            catch (Exception e)
            {
                throw new ObjectNotFoundException($"No expense found for id {id}", e);
            }
        }

        public async Task<IEnumerable<Expense>> GetAll(ExpenseSearchCriteria searchCriteria)
        {
            try
            {
                searchCriteria.PageSize = searchCriteria.PageSize ?? 100;

                if (!_userContext.IsUserManager())
                {
                    searchCriteria.UserId = _userContext.GetUserId();
                }

                var expenses = await _expenseRepository.Search(searchCriteria);

                return expenses;
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve expenses based on the set search criteria", ex);
            }
        }

        public async Task<Expense> SaveExpense(Expense entity)
        {
            var isNew = entity.Id == default(Guid);
                
            entity.Modified = DateTime.UtcNow;
            entity.ModifiedById = _userContext.GetUserId();

            return isNew ? await CreateExpense(entity) : await UpdateExpense(entity);
        }

        public async Task DeleteExpense(Expense expense)
        {
            var userId = _userContext.GetUserId();
            var isManager = _userContext.IsUserManager();

            try
            {
                ValidateUserCanDelete(expense, userId, isManager);

                expense.Deleted = true;
                expense.Modified = DateTime.UtcNow;
                expense.ModifiedById = userId;

                await _expenseRepository.Update(expense);

                await _expenseEmailSender.SendExpenseDeletionMailAsync(expense);
            }
            catch (Exception e)
            {
                throw new OperationException("Failed to delete expense", e);
            }
        }

        private async Task<Expense> CreateExpense(Expense expense) 
        {
            try
            {
                expense.Id = Guid.NewGuid();
                expense.Created = expense.Modified;
                expense.CreatedById = expense.ModifiedById;

                ValidateUserOrManagerCanPersist(expense, expense.ModifiedById);

                var savedExpense = await _expenseRepository.Create(expense);

                await _expenseEmailSender.SendExpenseCreationMailAsync(savedExpense);

                return savedExpense;
            }
            catch (Exception e)
            {
                throw new OperationException("Failed to create expense", e);
            }
        }

        private async Task<Expense> UpdateExpense(Expense expenseModel)
        {
            var dbExpense = await Get(expenseModel.Id);
            
            ValidateUserOrManagerCanPersist(dbExpense, expenseModel.ModifiedById, expenseModel.IncurredById);

            if (dbExpense.Currency != expenseModel.Currency ||
                dbExpense.Amount != expenseModel.Amount ||
                dbExpense.Reimbursed != expenseModel.Reimbursed)
            {
                ValidateCanModify(dbExpense, expenseModel);

                dbExpense.Currency = expenseModel.Currency;
                dbExpense.Amount = expenseModel.Amount;
                dbExpense.Reimbursed = expenseModel.Reimbursed;
            }

            dbExpense.Date = expenseModel.Date;
            dbExpense.IncurredById = expenseModel.IncurredById;
            dbExpense.Description = expenseModel.Description;
            dbExpense.Reason = expenseModel.Reason;

            try
            {
                await _expenseEmailSender.SendExpenseUpdateMailAsync(dbExpense);

                await _expenseRepository.Update(dbExpense);

                return dbExpense;
            }
            catch (Exception e)
            {
                throw new OperationException("Failed to Update Expense", e);
            }
        }

        private void ValidateCanModify(Expense existingExpense, Expense newExpense)
        {
            var today = DateTime.UtcNow;
            
            if (!(existingExpense.Reimbursed.IsNull() || existingExpense.Modified.Date == today.Date || !(existingExpense.Modified.AddDays(1) <= today)))
            {
                throw new OperationException("Expense cannot be edited as it was last modified more than 24 hours");
            }
        }

        private void ValidateUserOrManagerCanPersist(Expense expense, Guid userId, Guid? incurredBy = null)
        {
            var isManager = _userContext.IsUserManager();
            incurredBy = incurredBy.HasValue ? incurredBy : expense.IncurredById;

            if (!isManager && !(userId == incurredBy || userId == expense.CreatedById) )
            {
                throw new OperationException("Current logged-in employee cannot persist expense on behalf of another employee");
            }
        }
         
        private static void ValidateUserCanDelete(Expense expense, Guid userId, bool isManager)
        {
            var dateIsLessThanOneDay = expense.Reimbursed?.AddDays(-1) < DateTime.UtcNow; 
            var isManagerOrCreator = (userId == expense.CreatedById) || isManager;

            if (!(isManagerOrCreator || expense.Reimbursed.IsNull() || dateIsLessThanOneDay))
            {
                throw new OperationException("Record cannot be deleted");
            }
        }
    }
}
