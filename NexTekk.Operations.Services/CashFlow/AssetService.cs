﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.CashFlow
{
    public class AssetService : IAssetService
    {
        private readonly IAssetRepository _assetRepository;
        private readonly IUserContext _userContext;
        private readonly IAssetEmailSender _assetEmailSender;

        public AssetService(
            IAssetRepository assetRepository,
            IUserContext userContext,
            IAssetEmailSender assetEmailSender
            )
        {
            _assetRepository = assetRepository;
            _userContext = userContext;
            _assetEmailSender = assetEmailSender;
        }

        public async Task DeleteAsset(Guid id)
        {
            if (!_userContext.IsUserManager())
            {
                throw new OperationException("Only Admin can delete assets");
            }

            var asset = await Get(id);

            asset.Deleted = true;
            asset.Modified = DateTime.UtcNow;
            asset.ModifiedById = _userContext.GetUserId();

            try
            {
                await _assetRepository.Update(asset);
            }
            catch(Exception ex)
            {
                throw new OperationException("Failed to delete asset", ex);
            }
        }

        public async Task<Asset> Get(Guid id)
        {
            try
            {
                return await _assetRepository.Get(id);
            }
            catch (Exception ex)
            {
                throw new ObjectNotFoundException($"No Asset exists for id: {id}", ex);
            }
        }

        public async Task<IEnumerable<AssetSummary>> GetAssetSummariesAsync(AssetSearchCriteria searchCriteria)
        {
            try
            {
                if (!_userContext.IsUserManager())
                {
                    searchCriteria.UserId = _userContext.GetUserId();
                }

                searchCriteria.PageSize = searchCriteria.PageSize ?? 100;
                searchCriteria.NumberToSkip = searchCriteria.NumberToSkip ??
                    (searchCriteria.PageNumber > 0 ? searchCriteria.PageNumber - 1 : 0) * 100;

                return await _assetRepository.GetAssetSummariesAsync(searchCriteria);
            }
            catch (Exception ex)
            {
                throw new ObjectNotFoundException("Failed to retrieve assets based on the set search criteria", ex);
            }
        }

        public async Task<Asset> SaveAssetAsync(Asset assetDTO)
        {
            var isNew = assetDTO.Id == default(Guid);

            assetDTO.Modified = DateTime.UtcNow;
            assetDTO.ModifiedById = _userContext.GetUserId();

            var savedAsset = default(Asset);

            if (isNew)
            {
                savedAsset = await CreateAssetAsync(assetDTO);
            }
            else
            {
                await UpdateAssetAsync(assetDTO);
                savedAsset = assetDTO;
            }

            return savedAsset;
        }

        public async Task<IList<Asset>> GetAssetsAsync(AssetSearchCriteria searchCriteria)
        {
            try
            {
                searchCriteria.PageSize = searchCriteria.PageSize ?? 100;
                searchCriteria.NumberToSkip = searchCriteria.NumberToSkip ??
                    (searchCriteria.PageNumber > 0 ? searchCriteria.PageNumber - 1 : 0) * 100;

                return await _assetRepository.GetAssetsAsync(searchCriteria);
            }
            catch (Exception ex)
            {
                throw new ObjectNotFoundException("Failed to retrieve assets based on the set search criteria", ex);
            }
        }

        public async Task RequestAsset(Guid id)
        {
            if (id == default(Guid))
            {
                throw new ArgumentNullException("Asset Id can not be null");
            }

            var asset = await Get(id);

            if (asset.AssigneeId.HasValue)
            {
                throw new OperationException($"{asset.Item} is already assigned to another employee");
            }

            var requesterId = _userContext.GetUserId();

            await _assetEmailSender.SendRequestEmail(asset, requesterId);
        }

        private async Task UpdateAssetAsync(Asset assetDTO)
        {
            var dbAsset = await _assetRepository.Get(assetDTO.Id);
            var isManager = _userContext.IsUserManager();
            
            ValidateUserCanEdit(dbAsset, assetDTO, isManager);
                
            if (dbAsset.AssigneeId != assetDTO.AssigneeId)
            {
                dbAsset.LastAssigneeId = dbAsset.AssigneeId ?? dbAsset.LastAssigneeId;
                dbAsset.AssigneeId = assetDTO.AssigneeId;
                dbAsset.LastAssignedDate = dbAsset.AssignedDate ?? dbAsset.LastAssignedDate;
                dbAsset.AssignedDate = assetDTO.AssigneeId.HasValue ? assetDTO.Modified : default(DateTime?);
            }

            if (dbAsset.Condition != assetDTO.Condition && assetDTO.Condition == Condition.Bad)
            {
                dbAsset.Condition = Condition.Bad;
                dbAsset.MarkedBadById = assetDTO.ModifiedById;
                dbAsset.MarkedBadOn = assetDTO.Modified; 
            }

            dbAsset.SerialNo = assetDTO.SerialNo;
            dbAsset.Item = assetDTO.Item;
            dbAsset.Category = assetDTO.Category;
            dbAsset.Cost = assetDTO.Cost;
            dbAsset.Currency = assetDTO.Currency;
            dbAsset.Description = assetDTO.Description;
            dbAsset.Manufacturer = assetDTO.Manufacturer;
            dbAsset.Model = assetDTO.Model;
            dbAsset.PurchasedById = assetDTO.PurchasedById;
            dbAsset.PurchasedDate = assetDTO.PurchasedDate;
            dbAsset.ServiceCode = assetDTO.ServiceCode;
            dbAsset.ServiceTag = assetDTO.ServiceTag;

            try
            {
                await _assetRepository.Update(dbAsset);
            }
            catch (Exception ex)
            {
                throw new OperationException("Unable to Update Asset", ex);
            }
        }

        private async Task<Asset> CreateAssetAsync(Asset assetDTO)
        {
            try
            {
                assetDTO.Id = Guid.NewGuid();
                assetDTO.Created = assetDTO.Modified;
                assetDTO.CreatedById = assetDTO.ModifiedById;

                return await _assetRepository.Create(assetDTO);
            }
            catch (Exception ex)
            {
                throw new OperationException("Creating of asset failed", ex);
            }
        }

        private void ValidateUserCanEdit(Asset dbAsset, Asset assetDTO, bool isManager)
        {
            if (dbAsset.Condition != assetDTO.Condition)
            {
                if (!(isManager || dbAsset.AssigneeId == assetDTO.ModifiedById))
                {
                    throw new OperationException($"Managers {(dbAsset.AssigneeId.HasValue ? "and Current assignee " : string.Empty)}can modify {dbAsset.Item}'s condition");
                }
            }

            if (dbAsset.AssigneeId != assetDTO.AssigneeId
                || dbAsset.Cost != assetDTO.Cost
                || dbAsset.Currency != assetDTO.Currency
                || dbAsset.Description != assetDTO.Description
                || dbAsset.Item != assetDTO.Item
                || dbAsset.Manufacturer != assetDTO.Manufacturer
                || dbAsset.Model != assetDTO.Model
                || dbAsset.SerialNo != assetDTO.SerialNo
                || dbAsset.PurchasedDate != assetDTO.PurchasedDate
                || dbAsset.PurchasedById != assetDTO.PurchasedById
                || dbAsset.AssigneeId != assetDTO.AssigneeId
                || dbAsset.AssignedDate?.Date != assetDTO.AssignedDate?.Date
                || dbAsset.Category != assetDTO.Category
                || dbAsset.ServiceTag != assetDTO.ServiceTag
                || dbAsset.ServiceCode != assetDTO.ServiceCode
                )
            {
                if (!isManager)
                {
                    throw new OperationException($"Currently logged in user can not modify {dbAsset.Item}");
                }
            }
        }
    }
}
