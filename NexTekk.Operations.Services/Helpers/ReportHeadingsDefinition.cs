﻿using NexTekk.Operations.Core.Reports;
using System.Collections.Generic;

namespace NexTekk.Operations.Services.Helpers
{
    public class ReportHeadingsDefinition
    {
        public static IList<ColumnDefinitions> Assets
        {
            get
            {
                return new List<ColumnDefinitions>
                {
                    new ColumnDefinitions("Item", "Item"),
                    new ColumnDefinitions("Serial #", "SerialNo"),
                    new ColumnDefinitions("Purchased Date", "PurchasedDate"),
                    new ColumnDefinitions("Purchased By", "PurchasedBy"),
                    new ColumnDefinitions("Assignee", "Assignee"),
                    new ColumnDefinitions("Assigned Date", "AssignedDate"),
                    new ColumnDefinitions("Condition", "Condition"),
                    new ColumnDefinitions("Category", "Category"),
                    new ColumnDefinitions("Description", "Description"),
                    new ColumnDefinitions("Manufacturer", "Manufacturer"),
                    new ColumnDefinitions("Model", "Model"),
                    new ColumnDefinitions("Service Tag", "ServiceTag"),
                    new ColumnDefinitions("Service Code", "ServiceCode"),
                    new ColumnDefinitions("Cost", "Cost"),
                    new ColumnDefinitions("Currency", "Currency"),
                    new ColumnDefinitions("Last Assignee", "LastAssignee"),
                    new ColumnDefinitions("Last Assigned Date", "LastAssignedDate"),
                    new ColumnDefinitions("Marked Bad On", "MarkedBadOn"),
                    new ColumnDefinitions("Marked Bad By", "MarkedBadBy"),
                    new ColumnDefinitions("Entered By", "EnteredBy")
                };
            }
        }

        public static IList<ColumnDefinitions> Expenses
        {
            get
            {
                return new List<ColumnDefinitions>
                {
                    new ColumnDefinitions("Date", "Date"),
                    new ColumnDefinitions("Person", "Incurrer"),
                    new ColumnDefinitions("Currency", "Currency"),
                    new ColumnDefinitions("Amount", "Amount"),
                    new ColumnDefinitions("Description", "Description"),
                    new ColumnDefinitions("Reason", "Reason"),
                    new ColumnDefinitions("Reimbursed", "Reimbursed"),
                    new ColumnDefinitions("Created Date", "Created"),
                    new ColumnDefinitions("Modified Date", "Modified")
                };
            }
        }

        public static IList<ColumnDefinitions> Revenues
        {
            get
            {
                return new List<ColumnDefinitions>
                {
                    new ColumnDefinitions("Payment Date", "PaymentDate"),
                    new ColumnDefinitions("Entered By", "CreatedBy"),
                    new ColumnDefinitions("Currency", "Currency"),
                    new ColumnDefinitions("Amount", "Amount"),
                    new ColumnDefinitions("Client", "Client"),
                    new ColumnDefinitions("Description", "Description"),
                    new ColumnDefinitions("Created Date", "Created"),
                    new ColumnDefinitions("Modified Date", "Modified")
                };
            }
        }
    }
}
