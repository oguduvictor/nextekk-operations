﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Core.Reports;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NexTekk.Operations.Services.Helpers
{
    public static class ReportGenerator
    {
        const string dateFormat = "m/d/yy";
        const string numberFormat = "0.00";

        public static byte[] GenerateReport<T>(IEnumerable<T> list, FileFormat format, IList<ColumnDefinitions> headings, string fileName = null)
        {
            return format == FileFormat.Excel ? CreateExcel(list, headings, fileName) : CreateCSV(list, headings);
        }

        public static NamedMIMEType GetFileFormatAndName(string fileName, FileFormat format)
        {
            var mimeType = string.Empty;
            var filenameWithExt = string.Empty;

            switch (format)
            {
                case FileFormat.Excel:
                    mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    filenameWithExt = $"{fileName}-{DateTime.UtcNow.Ticks}.xlsx";
                    break;

                case FileFormat.CSV:
                    filenameWithExt = $"{fileName}-{DateTime.UtcNow.Ticks}.csv";
                    mimeType = "text/csv";
                    break;
            }

            return new NamedMIMEType(filenameWithExt, mimeType);
        }

        private static byte[] CreateCSV<TObject>(IEnumerable<TObject> list, IList<ColumnDefinitions> headings)
        {
            var stringBuilder = new StringBuilder();

            foreach (var heading in headings)
            {
                stringBuilder.Append(heading.Title);

                if (headings.LastOrDefault() == heading) continue;

                stringBuilder.Append(", ");
            }

            stringBuilder.AppendLine();

            foreach (var item in list)
            {
                for (int i = 0; i < headings.Count(); i++)
                {
                    var heading = headings.ElementAt(i).Property;

                    var columnValue = item.GetType().GetProperty(heading)?.GetValue(item);

                    if (columnValue.IsNull()) continue;

                    if (columnValue.GetType() == typeof(User))
                    {
                        stringBuilder.Append($"{(columnValue as User)?.FullName}");
                    }
                    else if (columnValue.GetType().IsAnyOf(typeof(Client), typeof(ClientSummary)))
                    {
                        stringBuilder.Append($"{(columnValue as ClientSummary)?.Name}");
                    }
                    else if (columnValue.GetType().IsAnyOf(typeof(TimeEntryDateRange)))
                    {
                        var hours = CalculateHours(columnValue as TimeEntryDateRange);

                        stringBuilder.Append($"{(columnValue as ClientSummary)?.Name}");
                    }
                    else
                    {
                        stringBuilder.Append($"{columnValue ?? string.Empty}");
                    }

                    if (i == headings.Count - 1) continue;

                    stringBuilder.Append(", ");
                }

                stringBuilder.AppendLine();
            }

            return Encoding.UTF8.GetBytes(stringBuilder.ToString());
        }

        private static byte[] CreateExcel<TObject>(IEnumerable<TObject> list, IList<ColumnDefinitions> headings, string workSheetName = null)
        {
            using (var package = new ExcelPackage())
            {
                var worksheetName = workSheetName ?? "Report";
                var workSheet = package.Workbook.Worksheets.Add(worksheetName);

                for (int i = 0; i < headings.Count(); i++)
                {
                    var cell = headings.ElementAt(i);
                    workSheet.Cells[1, i + 1].Value = cell.Title;
                }

                for (int i = 0, len = list.Count(); i < len; i++)
                {
                    var item = list.ElementAt(i);

                    var props = item.GetType().GetProperties();

                    foreach (var prop in props)
                    {
                        var heading = headings.FirstOrDefault(x => x.Property == prop.Name);

                        if (heading.IsNull()) continue;

                        var column = headings.IndexOf(heading) + 1;

                        if (prop.PropertyType == typeof(User))
                        {
                            var user = prop.GetValue(item) as User;

                            workSheet.Cells[i + 2, column].Value = user?.FullName.Trim();
                        }
                        else if (prop.PropertyType.IsAnyOf(typeof(Client), typeof(ClientSummary)))
                        {
                            var client = prop.GetValue(item) as ClientSummary;

                            workSheet.Cells[i + 2, column].Value = client?.Name;
                        }
                        else if (prop.PropertyType == typeof(TimeEntryDateRange))
                        {
                            var hours = CalculateHours(prop.GetValue(item) as TimeEntryDateRange);

                            workSheet.Cells[i + 2, column].Value = hours;
                        }
                        else if (prop.PropertyType == typeof(Currency))
                        {
                            workSheet.Cells[i + 2, column].Value = ((Currency)prop.GetValue(item)).GetCurrencySymbol();
                        }
                        else
                        {
                            workSheet.Cells[i + 2, column].Value = prop.GetValue(item);
                        }

                        if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                        {
                            workSheet.Cells[i + 2, column].Style.Numberformat.Format = dateFormat;
                        }
                        else if (prop.PropertyType.IsAnyOf(typeof(int), typeof(long), typeof(decimal), typeof(double), typeof(TimeEntryDateRange)))
                        {
                            workSheet.Cells[i + 2, column].Style.Numberformat.Format = numberFormat;
                        };
                    }
                }

                return package.GetAsByteArray();
            }
        }

        private static double CalculateHours(TimeEntryDateRange timeRange)
        {
            if (timeRange == null)
            {
                return 0;
            }

            TimeSpan ts = timeRange.EndDateTime - timeRange.BeginDateTime;

            return Math.Round(ts.TotalHours, 2);
        }
    }
}