﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserContext _userContext;
        private readonly IUserRepository _userRepository;
        private readonly IClientRepository _clientRepository;

        public ProjectService(
            IClientRepository clientRepository,
            IProjectRepository projectRepository,
            IUserContext userContext,
            IUserRepository userRepository)
        {
            _projectRepository = projectRepository;
            _userContext = userContext;
            _userRepository = userRepository;
            _clientRepository = clientRepository;
        }

        public async Task<IEnumerable<Project>> GetProjects(ProjectSearchCriteria searchCriteria)
        {
            try
            {
                var result = (await _projectRepository.GetProjects(searchCriteria)).ToList();

                return result.Select(x =>
                {
                    return new Project(x);
                });
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve projects based on set search criteria", ex);
            }
        }

        public async Task<ProjectDetail> GetProject(Guid id)
        {
            try
            {
                var entity = await _projectRepository.GetProject(id);

                if (entity.IsNull())
                {
                    throw new ArgumentException($"No Project exists for the given {nameof(id)}, {id}");
                }
                
                return new ProjectDetail(entity);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve project based on given Id", ex);
            }
        }

        public async Task<Project> SaveProject(Project project)
        {
            try
            {
                var isNew = project.Id.IsNull() || project.Id == Guid.Empty;
                var entity = isNew ? new ProjectEntity() : await _projectRepository.GetProject(project.Id);

                var userId = _userContext.GetUserId();
                var entities = new ProjectEntity[] { entity };

                var client = await _clientRepository.Get(project.Client.Id);

                entity.Billable = project.Billable;
                entity.Edited = DateTime.Now;
                entity.EditorId = userId;
                entity.Title = project.Title;
                
                if (!client.IsNull())
                {
                    entity.ClientId = project.Client.Id;
                }
                
                if (!isNew)
                {
                    entity.Status = project.Status;
                    await _projectRepository.Update(entities);
                }
                else
                {
                    entity.Id = Guid.NewGuid();
                    entity.CreatorId = userId;
                    entity.Created = DateTime.UtcNow;
                    entity.Status = ProjectStatus.NotStarted;

                    await _projectRepository.Create(entities);
                }
                
                entity.Client = client;

                return new Project(entity);
            }
            catch (Exception ex) when(!(ex is OperationException))
            {
                throw new OperationException($"Failed to persist project, {project.Title}", ex);
            }
        }

        public async Task DeleteProject(Guid id)
        {
            try
            {
                await _projectRepository.Delete(new Guid[] { id });
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to delete project", ex);
            }
        }

        public async Task SaveTeamMembers(IEnumerable<TeamMemberEntity> teamMembers)
        {
            try
            {
                await _projectRepository.UpdateTeamMembers(teamMembers);
            }
            catch (Exception ex)
            {
                throw new OperationException("Unable to Persist TeamMember", ex);
            }
        }
        
        public async Task<IEnumerable<TeamMember>> GetTeamMembers(Guid projectId)
        {
            try
            {
                var results = await _projectRepository.GetTeamMembers(projectId);

                return results.Select(x => new TeamMember(x));
            }
            catch (Exception ex)
            {
                throw new OperationException("Unable to get all team members", ex);
            }
        }
   }
}
