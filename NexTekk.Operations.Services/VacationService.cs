﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class VacationService : IVacationService
    {
        private const decimal DEFAULT_HOURS_PER_PERIOD = 3.33m;
        private const decimal DEFAULT_YEAR_BEGIN_BALANCE = 0;
        private readonly IUserContext _userContext;
        private readonly ITimeEntryService _timeEntryService;
        private readonly IVacationRepository _vacationRepository;

        public VacationService(
            IUserContext userContext,
            ITimeEntryService timeEntryService,
            IVacationRepository vacationRepository)
        {
            _userContext = userContext;
            _timeEntryService = timeEntryService;
            _vacationRepository = vacationRepository;
        }

        public async Task<Vacation> GetVacation(Guid employeeId, int year)
        {
            VerifyUserAccess(employeeId);

            var entity = await _vacationRepository.Get(employeeId, GetYear(year));
            if (entity == null)
            {
                throw new OperationException("No vacation that matches the given employee ID can be found");
            }

            return new Vacation(entity);
        }

        public async Task<IEnumerable<Vacation>> GetVacations(int year)
        {
            var entities = await _vacationRepository.GetAll(GetYear(year));
            return entities.Select(x => new Vacation(x));
        }

        public async Task<IEnumerable<TimeEntry>> GetVacationTimeEntries(Guid employeeId, int year)
        {
            VerifyUserAccess(employeeId);

            var vacationYear = GetYear(year);
            var beginDate = new DateTime(vacationYear, 1, 1); ////TODO: this needs to create a UTC time based on the local time of the caller
            var searchCriteria = new TimeEntrySearchCriteria
            {
                ActivitiesOnly = true,
                CreatorId = employeeId,
                BeginDateTime = beginDate,
                EndDateTime = beginDate.AddYears(1).AddSeconds(-1),
                Status = TimeEntryStatus.Approved,
                IsVacation = true
            };

            return await _timeEntryService.GetTimeEntries(searchCriteria);
        }

        public async Task<decimal> UpdateHoursPerPeriod(Guid employeeId, decimal hoursPerPeriod)
        {
            var entity = await _vacationRepository.GetSetting(employeeId);

            if (entity.IsNull())
            {
                await SaveVacation(employeeId, hoursPerPeriod, default(decimal));
                return hoursPerPeriod;
            }

            entity.HoursPerPeriod = hoursPerPeriod;
            entity.Edited = DateTime.UtcNow;

            await _vacationRepository.Update(entity);

            return hoursPerPeriod;
        }

        public async Task<decimal> UpdateYearBeginBalance(Guid employeeId, decimal yearBeginBalance)
        {
            var entity = await _vacationRepository.GetSetting(employeeId);

            if (entity.IsNull())
            {
                await SaveVacation(employeeId, default(decimal), yearBeginBalance);
                return yearBeginBalance;
            }

            entity.YearBeginBalance = yearBeginBalance;
            entity.Edited = DateTime.UtcNow;
            await _vacationRepository.Update(entity);

            return yearBeginBalance;
        }

        public async Task<VacationSetting> SaveVacation(Guid employeeId, decimal? hoursPerPeriod, decimal? yearBeginBalance)
        {
            var savedSetting = await _vacationRepository.GetSetting(employeeId);
            var userId = _userContext.GetUserId();

            if (savedSetting == null)
            {
                var vacationSetting = new VacationSetting
                {
                    UserId = employeeId,
                    HoursPerPeriod = hoursPerPeriod ?? DEFAULT_HOURS_PER_PERIOD,
                    YearBeginBalance = yearBeginBalance ?? DEFAULT_YEAR_BEGIN_BALANCE,
                    Edited = DateTime.UtcNow,
                    EditorId = userId,
                    Created = DateTime.UtcNow,
                    CreatorId = userId
                };

                await _vacationRepository.Create(vacationSetting);

                return vacationSetting;
            }

            savedSetting.HoursPerPeriod = hoursPerPeriod.Value;
            savedSetting.YearBeginBalance = yearBeginBalance.Value;
            savedSetting.Edited = DateTime.UtcNow;
            savedSetting.EditorId = userId;
            await _vacationRepository.Update(savedSetting);

            return savedSetting;
        }

        private int GetYear(int year = 0)
        {
            return year == default(int) ? DateTime.UtcNow.Year : year;
        }

        private void VerifyUserAccess(Guid employeeId)
        {
            if (!_userContext.IsUserManager() && employeeId != _userContext.GetUserId())
            {
                throw new OperationException("User does not have access to requested vacation information", null);
            }
        }
    }
}
