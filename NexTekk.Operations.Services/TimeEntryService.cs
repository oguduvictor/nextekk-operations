﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class TimeEntryService : ITimeEntryService
    {
        private readonly IUserContext _userContext;
        private readonly ITimeEntryRepository _timeEntryRepository;
        private readonly IActivityRepository _activityRepository;
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IReportService _reportService;
        private readonly ITimeTrackingEmailSender _timeTrackingEmailSender;
        private readonly ILogger _logger;

        public TimeEntryService(
            IUserContext userContext, 
            ITimeEntryRepository timeEntryRepository, 
            IActivityRepository activityRepository,
            IUserRepository userRepository,
            IProjectRepository projectRepository,
            IReportService reportService,
            ITimeTrackingEmailSender timeTrackingEmailSender,
            ILogger logger)
        {
            _userContext = userContext;
            _timeEntryRepository = timeEntryRepository;
            _activityRepository = activityRepository;
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _reportService = reportService;
            _timeTrackingEmailSender = timeTrackingEmailSender;
            _logger = logger;
        }

        public async Task<IEnumerable<TimeEntry>> GetTimeEntries(TimeEntrySearchCriteria searchCriteria)
        {
            try
            {
                var result = (await _timeEntryRepository.GetAll(searchCriteria)).ToList();

                // The previous search would have retrieved projects only
                // because only projects have project managers
                // so we need to load all the activities now
                if (searchCriteria.ProjectManagerId.HasValue)
                {
                    searchCriteria.ActivitiesOnly = true;

                    var timeEntriesWithActivity = await _timeEntryRepository.GetAll(searchCriteria);

                    result.AddRange(timeEntriesWithActivity);
                }

                return result.Select(x => new TimeEntry(x));
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve time entries based on the set search criteria", ex);
            }
        }

        public async Task<TimeEntryDetail> GetTimeEntry(Guid id)
        {
            try
            {
                var entity = await _timeEntryRepository.Get(id);

                if (entity.IsNull())
                {
                    throw new ArgumentException($"No entity exists for the given {nameof(id)}, {id}");
                }

                return new TimeEntryDetail(entity);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve time entry for the given ID.", ex);
            }
        }

        public async Task<IEnumerable<ActivitySummary>> GetAllActivities(Guid? userId = null)
        {
            try
            {
                var combinedActivities = new List<ActivitySummary>();
                var activities = await _activityRepository.GetAll();
                var projects = await _projectRepository.GetProjectSummaries(userId);

                projects = projects.Where(x => x.Status == ProjectStatus.InProgress);

                if (!activities.IsNullOrEmpty())
                {
                    combinedActivities.AddRange(activities.Select(x => new ActivitySummary(x)));
                }

                if (!projects.IsNullOrEmpty())
                {
                    combinedActivities.AddRange(projects.Select(x => 
                        new ActivitySummary
                        {
                            Id = x.Id,
                            Title = x.Title,
                            Billable = x.Billable
                        }));
                }

                return combinedActivities;
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to get all activities", ex);
            }
        }

        public async Task<TimeEntryDetail> SaveTimeEntry(TimeEntry timeEntry, Guid? id = null)
        {
            try
            {
                if (!timeEntry.IsValid())
                {
                    throw new InvalidDateRangeException(timeEntry);
                }

                var isNew = !id.HasValue || id == Guid.Empty;
                var entity = isNew ? new TimeEntryEntity() : await _timeEntryRepository.Get(id.Value);

                await CheckTimeEntryOverlaps(timeEntry, id);

                var userId = _userContext.GetUserId();
                var entities = new TimeEntryEntity[] { entity };
                var activity = await _activityRepository.Get(timeEntry.Activity.Id);
                var project = default(ProjectEntity);

                entity.Task = timeEntry.Task;
                entity.Comment = timeEntry.Comment;
                entity.Edited = DateTime.UtcNow;
                entity.EditorId = userId;
                entity.BeginDateTime = timeEntry.BeginDateTime;
                entity.EndDateTime = timeEntry.EndDateTime;

                if (activity != null)
                {
                    entity.Billable = activity.Billable;
                    entity.ActivityId = activity.Id;
                    entity.ProjectId = null;
                }
                else if ((project = (await _projectRepository.GetProjectSummariesById(timeEntry.Activity.Id)).FirstOrDefault()) != null)
                {
                    entity.Billable = project.Billable;
                    entity.ProjectId = project.Id;
                    entity.ActivityId = null;
                }
                else
                {
                    throw new InvalidOperationException($"{timeEntry.Activity.Id} is not a valid activity ID.");
                }
           
                if (!isNew)
                {
                    await _timeEntryRepository.Update(entities);
                }
                else
                {
                    entity.Id = Guid.NewGuid();
                    entity.CreatorId = userId;
                    entity.Created = DateTime.UtcNow;
                    entity.Status = TimeEntryStatus.Saved;

                    await _timeEntryRepository.Create(entities);
                }

                entity.Activity = activity;

                if (!project.IsNull())
                {
                    entity.Project = new ProjectSummary { Id = project.Id, Title = project.Title, Billable = project.Billable };
                }

                return new TimeEntryDetail(entity);
            }
            catch (Exception ex) when (!(ex is OperationException))
            {
                throw new OperationException($"Failed to persist time entry, { timeEntry.Task }", ex);
            }
        }

        public async Task UpdateTimeEntryStatus(IEnumerable<IdCommentPair> idCommentPairs, TimeEntryStatus status)
        {
            var ids = idCommentPairs.Select(x => x.Id);
            var timeEntries = default(IEnumerable<TimeEntryEntity>);

            try
            {
                var userId = _userContext.GetUserId();
                timeEntries = await _timeEntryRepository.GetAll(ids);

                foreach (var timeEntry in timeEntries)
                {
                    var comment = idCommentPairs.FirstOrDefault(x => x.Id == timeEntry.Id)?.Comment;

                    switch (status)
                    {
                        case TimeEntryStatus.Approved:
                            ValidateStatusUpdate(timeEntry.Status, status, TimeEntryStatus.Submitted);
                            timeEntry.ApproverId = userId;
                            timeEntry.Approved = DateTime.UtcNow;
                            timeEntry.ManagerComment = comment ?? timeEntry.ManagerComment;
                            break;

                        case TimeEntryStatus.Denied:
                            ValidateStatusUpdate(timeEntry.Status, status, TimeEntryStatus.Submitted);
                            timeEntry.DenierId = userId;
                            timeEntry.Denied = DateTime.UtcNow;
                            timeEntry.ManagerComment = comment ?? timeEntry.ManagerComment;
                            break;

                        case TimeEntryStatus.Submitted:
                            ValidateStatusUpdate(timeEntry.Status, status, TimeEntryStatus.Saved, TimeEntryStatus.Denied);
                            timeEntry.Edited = DateTime.UtcNow;
                            timeEntry.EditorId = userId;
                            timeEntry.Comment = comment ?? timeEntry.Comment;
                            break;

                        case TimeEntryStatus.Saved: // Unsubmit
                            if (timeEntry.Status != TimeEntryStatus.Approved && timeEntry.Status != TimeEntryStatus.Submitted && timeEntry.Status != TimeEntryStatus.Denied)
                            {
                                throw new OperationException($"Status of time entry, {timeEntry.Task}, cannot be changed to {status}", null);
                            }

                            timeEntry.Edited = DateTime.UtcNow;
                            timeEntry.EditorId = userId;
                            break;
                        default:
                            throw new OperationException($"{status} is not a valid status", null);
                    }

                    timeEntry.Status = status;
                }

                await _timeEntryRepository.Update(timeEntries);
            }
            catch (Exception ex)
            {
                throw new OperationException($"Updating status for \"{ timeEntries.First().Task }\" failed.", ex);
            }

            try
            {
                await NotifyConcernedUsers(timeEntries, status);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public async Task DeleteTimeEntry(IEnumerable<Guid> ids)
        {
            try
            {
                if (_userContext.IsUserManager())
                {
                    await _timeEntryRepository.Delete(ids);
                }
                else
                {
                    var userId = _userContext.GetUserId();
                    var timeEntries = await _timeEntryRepository.GetAll(ids);

                    if (timeEntries.Any(entry => entry.CreatorId != userId))
                    {
                        throw new UnauthorizedAccessException("Current user is not authorized to delete time entry (ies)");
                    }

                    await _timeEntryRepository.Delete(ids);
                }
            }
            catch (Exception ex)
            {
                throw new OperationException($"Failed to delete time entry", ex);
            }
        }

        public async Task<byte[]> GetTabularReport(TimeEntrySearchCriteria searchCriteria, FileFormat fileFormat)
        {
            try
            {
                var searchResult = await GetTimeEntries(searchCriteria);
                var report = _reportService.GetTabularReport(searchResult, fileFormat);

                return report;
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to generate for the specified search criteria", ex);
            }
        }

        public async Task SendReminderEmail(DateTimeRange dateRange)
        {
            try
            {
                var usersWithSubmission = await _timeEntryRepository.GetUsersIdsWithSubmission(dateRange);
                var usersWithoutSubmission = await _userRepository.GetAllNotInIds(usersWithSubmission);

                await _timeTrackingEmailSender.SendLateSubmissionReminder(usersWithoutSubmission, dateRange);
            }
            catch (Exception ex) when (!(ex is OperationException))
            {
                throw new OperationException("Failed to send reminder emails", ex);
            }
        }

        private async Task NotifyConcernedUsers(IEnumerable<TimeEntryEntity> timeEntries, TimeEntryStatus status)
        {
            if (status == TimeEntryStatus.Submitted)
            {
                var timeEntryGroups = timeEntries.GroupBy(x => x.ProjectId);

                foreach (var timeEntryGroup in timeEntryGroups)
                {
                    var timeEntry = timeEntryGroup.First();

                    if (!timeEntry.ProjectId.HasValue)
                    {
                        continue;
                    }

                    await _timeTrackingEmailSender.SendSubmittedEmails(timeEntry);
                }
            }
            else if (status == TimeEntryStatus.Denied)
            {
                var timeEntry = timeEntries.First();

                await _timeTrackingEmailSender.SendDeniedEmails(timeEntry);
            }
        }

        private void ValidateStatusUpdate(TimeEntryStatus fromStatus, TimeEntryStatus toStatus, params TimeEntryStatus[] expectedStatuses)
        {
            if (toStatus.IsAnyOf(TimeEntryStatus.Approved, TimeEntryStatus.Denied) && !_userContext.IsUserManager())
            {
                throw new UnauthorizedAccessException("User does not have the access to change time entry status");
            }

            if (!expectedStatuses.Contains(fromStatus))
            {
                throw new InvalidOperationException($"Status of the time entry cannot be changed from {fromStatus} to {toStatus}");
            }
        }

        private async Task CheckTimeEntryOverlaps(TimeEntry timeEntry, Guid? id = null)
        {
            var searchCriteria = new TimeEntrySearchCriteria();

            searchCriteria.CreatorId = _userContext.GetUserId();

            searchCriteria.BeginDateTime = timeEntry.BeginDateTime.Date;
            searchCriteria.EndDateTime = searchCriteria.BeginDateTime.Value.AddDays(1).AddSeconds(-1);

            var savedTimeEntries = await GetTimeEntries(searchCriteria);
            var overlappedTimeEntry = savedTimeEntries.FirstOrDefault(x => x.Id != id && timeEntry.Overlaps(x));

            if (overlappedTimeEntry != null)
            {
                throw new TimeEntryOverlapException(overlappedTimeEntry);
            }
        }
    }
}