﻿using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

namespace NexTekk.Operations.Services
{
    public class ReportService : IReportService
    {
        private const string TASK_HEADING = "Task";
        private const string PROJECT_HEADING = "Project";
        private const string FROM_DATE_HEADING = "From";
        private const string TO_DATE_HEADING = "To";
        private const string HOURS_HEADING = "Hours";

        public byte[] GetTabularReport(IEnumerable<TimeEntry> timeEntries, FileFormat fileFormat)
        {
            if (fileFormat == FileFormat.Excel)
            {
                return CreateExcel(timeEntries);
            }

            return CreateCsv(timeEntries);
        }

        public byte[] CreateExcel(IEnumerable<TimeEntry> timeEntries)
        {
            using (var package = new ExcelPackage())
            {
                const string worksheetName = "Time Tracking Report";
                const string dateFormat = "m/d/yy h:mm";
                const string numberFormat = "0.00";
                var workSheet = package.Workbook.Worksheets.Add(worksheetName);

                workSheet.Cells[1, 1].Value = TASK_HEADING;
                workSheet.Cells[1, 2].Value = PROJECT_HEADING;
                workSheet.Cells[1, 3].Value = FROM_DATE_HEADING;
                workSheet.Cells[1, 4].Value = TO_DATE_HEADING;
                workSheet.Cells[1, 5].Value = HOURS_HEADING;

                for (int i = 0, len = timeEntries.Count(); i < len; i++)
                {
                    var timeEntry = timeEntries.ElementAt(i);
                    var cells = workSheet.Cells;

                    cells[i + 2, 1].Value = timeEntry.Task;
                    cells[i + 2, 2].Value = timeEntry.Activity?.Title ?? string.Empty;

                    cells[i + 2, 3].Value = timeEntry.BeginDateTime;
                    cells[i + 2, 3].Style.Numberformat.Format = dateFormat;

                    cells[i + 2, 4].Value = timeEntry.EndDateTime;
                    cells[i + 2, 4].Style.Numberformat.Format = dateFormat;

                    cells[i + 2, 5].Value = CalculateHours(timeEntry);
                    cells[i + 2, 5].Style.Numberformat.Format = numberFormat;
                }

                return package.GetAsByteArray();
            }
        }

        public byte[] CreateCsv(IEnumerable<TimeEntry> timeEntries)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{TASK_HEADING},{PROJECT_HEADING},{FROM_DATE_HEADING},{TO_DATE_HEADING},{HOURS_HEADING}");

            foreach (var entry in timeEntries)
            {
                sb.AppendFormat(
                    @"""{0}"",""{1}"",""{2}"",""{3}"",""{4}""", 
                    entry.Task, 
                    entry.Activity?.Title, 
                    entry.BeginDateTime, 
                    entry.EndDateTime,
                    CalculateHours(entry));

                sb.AppendLine();
            }

            return Encoding.UTF8.GetBytes(sb.ToString());
        }

        private double CalculateHours(TimeEntryDateRange timeRange)
        {
            if (timeRange == null)
            {
                return 0;
            }

            TimeSpan ts = timeRange.EndDateTime - timeRange.BeginDateTime;

            return Math.Round(ts.TotalHours, 2);
        }
    }
}
