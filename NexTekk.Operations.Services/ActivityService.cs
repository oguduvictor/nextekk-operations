﻿using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class ActivityService : IActivityService
    {
        private readonly IActivityRepository _activityRepository;
        private readonly IUserContext _userContext;

        public ActivityService(IActivityRepository activityRepository, IUserContext userContext)
        {
            _activityRepository = activityRepository;
            _userContext = userContext;
        }

        public async Task<IEnumerable<Activity>> GetActivities()
        {
            var entities = await _activityRepository.GetAll();

            return entities.Select(x => new Activity(x));
        }

        public async Task<ActivityDetail> GetActivityById(Guid activityId)
        {
            var activity = await _activityRepository.Get(activityId);

            return new ActivityDetail(activity);
        }

        public async Task<Activity> SaveActivity(ActivityEntity activity)
        {
            return await(activity.Id == default(Guid) ? CreateActivity(activity) : UpdateActivity(activity));
        }

        public async Task DeleteActivity(Guid activityId)
        {
            await _activityRepository.Delete(activityId);
        }

        private async Task<Activity> UpdateActivity(ActivityEntity model)
        {
            var userId = _userContext.GetUserId();
            var activity = await _activityRepository.Get(model.Id);

            activity.Title = model.Title;
            activity.Billable = model.Billable;
            activity.IsVacation = model.IsVacation;
            activity.Description = model.Description;
            activity.Edited = DateTime.UtcNow;
            activity.EditorId = userId;

            await _activityRepository.Update(activity);

            return new ActivityDetail(activity);
        }

        private async Task<Activity> CreateActivity(ActivityEntity activity)
        {
            var userId = _userContext.GetUserId();

            activity.Id = Guid.NewGuid();
            activity.CreatorId = userId;
            activity.Created = DateTime.UtcNow;
            activity.Edited = DateTime.UtcNow;
            activity.EditorId = userId;

            await _activityRepository.Create(activity);

            return new Activity(activity);
        }
    }
}
