﻿using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Providers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class TimeTrackingEmailSender : ITimeTrackingEmailSender
    {
        private const string EMAIL_SIGNATURE = "NexTekk Time Tracking System";
        private readonly IEmailService _emailService;
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ILogger _logger;

        public TimeTrackingEmailSender(IEmailService emailService, 
                                       IUserRepository userRepository,
                                       IProjectRepository projectRepository,
                                       ILogger logger)
        {
            _emailService = emailService;
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _logger = logger;
        }

        public async Task SendSubmittedEmails(TimeEntryEntity timeEntry)
        {
            if (!AppSettingsProvider.Current.EnableStatusChangeEmail)
            {
                return;
            }

            var creator = await _userRepository.Get(timeEntry.CreatorId);
            var project = await _projectRepository.GetProject(timeEntry.ProjectId.Value);
            var managerId = project.TeamMembers.FirstOrDefault(x => x.Role == TeamMemberRole.ProjectManager)?.UserId;

            if (managerId == null)
            {
                _logger.Info($"Email cannot be sent to the project manager because no project manager exists for project, {project.Title}");
                return;
            }

            var manager = await _userRepository.Get(managerId.Value);
            
            var emailStringBuilder = new StringBuilder();

            emailStringBuilder.AppendFormat("Hi {0},", manager.FirstName);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("{0} just submitted Time Entries that are awaiting your approval.", creator.FirstName);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendLine();
            emailStringBuilder.Append("Thanks,");
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("{0}.", EMAIL_SIGNATURE);

            var email = new Email
            {
                Subject = $"Time Entries Submitted By {creator.FirstName}",
                Body = emailStringBuilder.ToString(),
                Sender = AppSettingsProvider.Current.SenderEmail,
                Recipients = new List<string> { manager.Email }
            };

            await _emailService.SendEmail(email);
        }

        public async Task SendDeniedEmails(TimeEntryEntity timeEntry)
        {
            if (!AppSettingsProvider.Current.EnableStatusChangeEmail)
            {
                return;
            }

            var creator = await _userRepository.Get(timeEntry.CreatorId);
            var manager = await _userRepository.Get(timeEntry.DenierId.Value);

            var emailStringBuilder = new StringBuilder();

            emailStringBuilder.AppendFormat("Hi {0},", creator.FirstName);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("Your Time Entry has been denied by {0}.", manager.FirstName);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendLine();
            emailStringBuilder.Append("The details of the time entry is as shown below:");
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("Task: {0}.", timeEntry.Task);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("Begin Date: {0}.", timeEntry.BeginDateTime);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("End Date: {0}.", timeEntry.EndDateTime);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("Manager Comment: {0}.", timeEntry.ManagerComment);
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendLine();
            emailStringBuilder.Append("Thanks,");
            emailStringBuilder.AppendLine();
            emailStringBuilder.AppendFormat("{0}.", EMAIL_SIGNATURE);

            var email = new Email
            {
                Subject = $"Time Entry Denied: { timeEntry.Task }",
                Body = emailStringBuilder.ToString(),
                Sender = AppSettingsProvider.Current.SenderEmail,
                Recipients = new List<string> { creator.Email }
            };

            await _emailService.SendEmail(email);
        }

        public async Task SendLateSubmissionReminder(IEnumerable<UserEntity> users, DateTimeRange dateRange)
        {
            if (!AppSettingsProvider.Current.EnableStatusChangeEmail)
            {
                return;
            }

            foreach (var user in users)
            {
                var emailStringBuilder = new StringBuilder();
                
                emailStringBuilder.AppendFormat("Hi {0},", user.FirstName);
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("Your Time Entries for time period {0} and {1} have not been submitted.", dateRange.BeginDateTime, dateRange.EndDateTime);
                emailStringBuilder.AppendLine();
                emailStringBuilder.Append("Please Submit your time entries as soon as possible.");
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendLine();
                emailStringBuilder.Append("Thanks,");
                emailStringBuilder.AppendLine();
                emailStringBuilder.AppendFormat("{0}.", EMAIL_SIGNATURE);

                var email = new Email
                {
                    Subject = "TimeEntry Reminder",
                    Body = emailStringBuilder.ToString(),
                    Sender = AppSettingsProvider.Current.SenderEmail,
                    Recipients = new List<string> { user.Email }
                };

                await _emailService.SendEmail(email);
            }
        }
    }
}
