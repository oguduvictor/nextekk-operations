﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IUserContext _userContext;

        public ClientService(
            IClientRepository clientRepository,
            IUserContext userContext)
        {
            _clientRepository = clientRepository;
            _userContext = userContext;
        }

        public async Task<IEnumerable<Client>> GetClients()
        {
            try
            {
                var result = await _clientRepository.GetAll();

                return result.Select(entity => new Client(entity));
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve all clients", ex);
            }
        }

        public async Task<ClientDetail> GetClient(Guid id)
        {
            try
            {
                var result = await _clientRepository.Get(id);
               
                return new ClientDetail(result);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve client based on given Id", ex);
            }
        }

        public async Task<IEnumerable<ClientSummary>> GetClientSummaries()
        {
            var result = await _clientRepository.GetClientSummaries(false);

            return result.Select(entity => new ClientSummary(entity));
        }

        public async Task<Client> SaveClient(Client client)
        {
            try
            {
                var isNew = client.Id.IsNull() || client.Id == default(Guid);
                var clientEntity = isNew ? new ClientEntity() : await _clientRepository.Get(client.Id);
                var userId = _userContext.GetUserId();

                clientEntity.Name = client.Name;
                clientEntity.EditorId = userId;
                clientEntity.Edited = DateTime.UtcNow;
                
                if (isNew)
                {
                    clientEntity.Id = Guid.NewGuid();
                    clientEntity.Created = DateTime.UtcNow;
                    clientEntity.CreatorId = userId;

                    await _clientRepository.Create(clientEntity);
                }
                else
                {
                    await _clientRepository.Update(clientEntity);
                }

                try
                {
                    var clientsContacts = new List<ClientsContactsEntity>();

                    if (!client.Contacts.IsNullOrEmpty())
                    {
                        client.Contacts.ForEach(contact =>
                        {
                            clientsContacts.Add(new ClientsContactsEntity { ContactId = contact.Id, ClientId = clientEntity.Id, Created = DateTime.UtcNow });
                        });
                    }
                    else
                    {
                        clientsContacts.Add(new ClientsContactsEntity { ContactId = default(int), ClientId = clientEntity.Id });
                    }

                    await SaveContacts(clientsContacts);
                }
                catch (Exception ex) when (!(ex is OperationException))
                {
                    throw new OperationException($"Failed to add Contact(s) to Client, {client.Name}", ex);
                }

                return new Client(clientEntity);
            }
            catch (Exception ex) when (!(ex is OperationException))
            {
                throw new OperationException($"Failed to persist Client, {client.Name}", ex);
            }
        }

        public async Task DeleteClient(Guid id)
        {
            try
            {
                if (await _clientRepository.HasProjects(id))
                {
                    throw new OperationException("Can't delete client because it has projects attached to it", null);
                }

                var entity = await _clientRepository.Get(id);

                entity.Deleted = true;

                await _clientRepository.Update(entity);
            }
            catch (Exception ex) when (!(ex is OperationException))
            {
                throw new OperationException("Failed to delete client", ex);
            }
        }

        public async Task SaveContacts(IEnumerable<ClientsContactsEntity> entities)
        {
            try
            {
                await _clientRepository.SaveContacts(entities);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to Add contacts to client", ex);
            }
        }
        
    }
}
