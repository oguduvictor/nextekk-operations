﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.Lookup
{
    public class UserLookupService : IUserLookupService
    {
        private readonly ICacheService _cacheService;
        private readonly IUserRepository _userRepository;

        private const string CacheKeyPrefix = "OperationsUser-";

        public UserLookupService(ICacheService cacheService, IUserRepository userRepository)
        {
            _cacheService = cacheService;
            _userRepository = userRepository;
        }

        public async Task<User> GetUserAsync(Guid userId)
        {
            var key = GetKey(userId.ToString());
            var user = _cacheService.Get<User>(key, true);

            if (user.IsNull())
            {
                user = new User(await _userRepository.Get(userId));

                _cacheService.AddOrUpdate(key, user);
            }

            return user;
        }

        public async Task<IEnumerable<User>> GetUsersAsync(IEnumerable<Guid> userIds)
        {
            var users = new List<User>(userIds.Count());

            // TODO: revisit logic
            foreach (var userId in userIds)
            {
                var user = await GetUserAsync(userId);

                users.Add(user);
            }

            return users;
        }

        public async Task<IEnumerable<UserEntity>> GetManagersAsync()
        {
            var key = $"{CacheKeyPrefix}Managers";
            var users = _cacheService.Get<IEnumerable<UserEntity>>(key, true);

            if (users.IsNullOrEmpty())
            {
                users = await _userRepository.GetAllManagers(); ;
            }

            return users;
        }

        private string GetKey(string userId)
        {
            return $"{CacheKeyPrefix}{userId}";
        }
    }
}
