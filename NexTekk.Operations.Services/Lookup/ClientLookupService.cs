﻿using AutoMapper;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services.Lookup
{
    public class ClientLookupService : IClientLookupService
    {
        private readonly ICacheService _cacheService;
        private readonly IClientRepository _clientRepository;

        private const string CacheKeyPrefix = "OperationsClient-";

        public ClientLookupService(ICacheService cacheService, IClientRepository clientRepository)
        {
            _cacheService = cacheService;
            _clientRepository = clientRepository;
        }

        public async Task<ClientSummary> GetClientAsync(Guid clientId)
        {
            var key = GetKey(clientId.ToString());
            var client = _cacheService.Get<ClientSummary>(key, true);

            if (client == null)
            {
                var savedClient =  await _clientRepository.GetClientEntityAsync(clientId);

                client = Mapper.Map<ClientSummary>(savedClient);

                _cacheService.AddOrUpdate(key, client);
            }

            return client;
        }

        public async Task<IEnumerable<ClientSummary>> GetClientsAsync(IEnumerable<Guid> clientIds)
        {
            var clients = new List<ClientSummary>(clientIds.Count());

            // TODO: revisit logic
            foreach (var clientId in clientIds)
            {
                var client = await GetClientAsync(clientId);

                clients.Add(client);
            }

            return clients;
        }

        private string GetKey(string clientId)
        {
            return $"{CacheKeyPrefix}{clientId}";
        }
    }
}
