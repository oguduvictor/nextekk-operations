﻿using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository _contactRepository;
        private readonly IUserContext _userContext;

        public ContactService(IContactRepository contactRepository, IUserContext userContext)
        {
            _contactRepository = contactRepository;
            _userContext = userContext;
        }

        public async Task<IEnumerable<Contact>> GetContacts()
        {
            try
            {
                var result = await _contactRepository.GetAll();
                return result.Select(entity => new Contact(entity));
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve all contacts", ex);
            }
        }

        public async Task<IEnumerable<ContactSummary>> GetContactSummaries()
        {
            var result = await _contactRepository.GetContactSummaries(false);

            return result.Select(entity => new ContactSummary(entity));
        }

        public async Task<ContactDetail> GetContact(int id)
        {
            try
            {
                var result = await _contactRepository.Get(id);
                return new ContactDetail(result);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to retrieve contact based on given Id", ex);
            }
        }

        public async Task<Contact> SaveContact(ContactEntity contact)
        {
            try
            {
                return await(contact.Id == default(int) ? CreateContact(contact) : UpdateContact(contact));
            }
            catch (Exception ex) when (!(ex is OperationException))
            {
                throw new OperationException($"Failed to persist Contact, {contact.FirstName}", ex);
            }
        }

        public async Task DeleteContact(int id)
        {
            try
            {
                await _contactRepository.Delete(id);
            }
            catch (Exception ex)
            {
                throw new OperationException("Failed to delete contact", ex);
            }
        }

        private async Task<Contact> UpdateContact(ContactEntity model)
        {
            var userId = _userContext.GetUserId();
            var contact = await _contactRepository.Get(model.Id);

            contact.FirstName = model.FirstName;
            contact.LastName = model.LastName;
            contact.Email = model.Email;
            contact.PhoneNumber1 = model.PhoneNumber1;
            contact.PhoneNumber2 = model.PhoneNumber2;

            contact.Edited = DateTime.UtcNow;
            contact.EditorId = userId;

            await _contactRepository.Update(contact);

            return new ContactDetail(contact);
        }

        private async Task<Contact> CreateContact(ContactEntity model)
        {
            var userId = _userContext.GetUserId();
            
            model.CreatorId = userId;
            model.Created = DateTime.UtcNow;
            model.Edited = DateTime.UtcNow;
            model.EditorId = userId;

            await _contactRepository.Create(model);

            return new Contact(model);
        }
    }
}
