# README #

** *Time Tracker* ** is an application that helps users log their time

### Getting Solution from Repo ###
* Run the following command to clone the repo

```
#!bash

git clone https://walexee@bitbucket.org/nextekkdev/nextekk-operations.git
```

### Creating the Database ###
* Open Management Studio and login using windows authentication
* Create a new database named ** *'TimeTrackingDB'* **
* Open the cloned solution with Visual Studio, and build the entire solution.
* Right-click on the database project (Infrastructure->NextekkTimetacking.Database), select publish and then select the database you created above.

### Running the Web API ###
* Open the cloned solution with Visual Studio 2015.
* Build the solution and make sure that it compiles without errors. (If you get an error that NexTekk.TimeTracking.Database.Sqlproject could not be found, exclude it from the solution)
* Set NexTekk.TimeTracking.WebApi as the default project and run the project to see the Swagger UI of the API.

### Running the Angular App ###
* Note that NexTekk.TimeTracking.WebUI is not listed on Visual Studio 2015, so you'll need to open it with Visual Studio Code.
* Open Visual Studio Code.
* Go to File->Open Folder, and navigate to the NexTekk.TimeTracking.WebUI folder under the TimeTracking solution folder.
* Go to View->Integrated Terminal, and a command line terminal would open at the bottom of the page.
* Type "npm start" in the terminal to run the app.
* Open your browser (preferably chrome) and type http://localhost:4200 to the address bar. 
* Note that the angular app needs the Web API mentioned above to be running, because that's where it gets it data from.

### External Javascript/Typescript Framework Management ###
* Will be accomplished using npm.
* Node Package Manager(npm) should already be installed on your computer, but if not then install it(npm https://www.npmjs.com/package/npm . Get the MSI) 
* From the command prompt Type npm help to make sure that it was installed correctly.
* You may need to install the extension, Latest TypeScript and Javascript Grammar, on Visual Studio Code to be able to get TypeScript functionalities.

### Other Tools to Download ###
* CHROME DEVELOPER TOOLS
* POSTMAN (https://www.getpostman.com/)