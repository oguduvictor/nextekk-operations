﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.WebApi.Models;
using System.Net;
using System.Threading.Tasks;

namespace NexTekk.Operations.WebApi.Middleware
{
    /// <summary>
    /// Global hander for all exceptions
    /// </summary>
    public static class GlobalExceptionHandler
    {
        private static ILogger _logger;

        /// <summary>
        /// Uses custom implemention for global exception handling
        /// </summary>
        /// <param name="builder">ApplicationBuilder instance</param>
        /// /// <param name="logger">Logger instance</param>
        public static void HandleExceptions(this IApplicationBuilder builder, ILogger logger)
        {
            _logger = logger;
            builder.Run(HandleServerError);
        }

        private static async Task HandleServerError(HttpContext context)
        {
            var error = context.Features.Get<IExceptionHandlerFeature>()?.Error;

            if (error == null)
            {
                return;
            }

            var accessControlOriginKey = "Access-Control-Allow-Origin";
            var serverErrorMsg = "Internal server error";

            context.Response.ContentType = "application/json; charset=utf-8";

            if (!context.Response.Headers.ContainsKey(accessControlOriginKey))
            {
                context.Response.Headers.Append(accessControlOriginKey, "*");
            }

            if (context.Response.StatusCode == default(int))
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            var response = new ErrorResponse();

            response.Message = (error is OperationException) ? error.Message : serverErrorMsg;
#if DEBUG
            response.Message = error.Message;
            response.StackTrace = error.StackTrace;
            response.InnerExceptionMessage = error.InnerException?.Message;
#endif

            if (!(error is OperationException) || error.InnerException != null)
            {
                response.LogId = _logger.Error(error);
            }

            await context.Response.WriteAsync(response.ToJson(true)).ConfigureAwait(false);
        }
    }
}
