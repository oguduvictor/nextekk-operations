﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// CORS Configuration to use during startup
    /// </summary>
    public static class CorsConfig
    {
        private const string CORS_POLICY_NAME = "myCorsPolicy";
        /// <summary>
        /// Method for adding CORS Configuration
        /// </summary>
        /// <param name="services"></param>
        public static void AddCorsSettings(this IServiceCollection services)
        {
            // TODO: change to a stricter policy
            services.AddCors(x =>
            {
                x.AddPolicy(CORS_POLICY_NAME, p =>
                {
                    p.AllowAnyOrigin();
                    p.AllowAnyHeader();
                    p.AllowAnyMethod();
                });
            });
        }

        /// <summary>
        /// Method to use CORS Policy
        /// </summary>
        /// <param name="app"></param>
        public static void UseCorsPolicy(this IApplicationBuilder app)
        {
            app.UseCors(CORS_POLICY_NAME);
        }
    }
}
