﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Data.Sql.DbContexts;
using NexTekk.Operations.Data.Sql.Repositories;
using NexTekk.Operations.Data.Sql.Repositories.CashFlow;
using NexTekk.Operations.Framework;
using NexTekk.Operations.Services;
using NexTekk.Operations.Services.CashFlow;
using NexTekk.Operations.Services.Lookup;
using NexTekk.Operations.Web.Services;
using NexTekk.Operations.WebApi.Providers;
using System.Net.Http;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// IoC container for Dependency Injection
    /// </summary>
    public static class IoC
    {
        private const string OPERATIONS_CONNECTION = "OperationsConnection";

        /// <summary>
        /// Method for registering dependencies
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void ConfigureAppDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUserContext, UserContext>();
            services.AddSingleton<HttpClient>();
            services.AddScoped<ILogger, Logger>();
            services.AddScoped<ITimeEntryService, TimeEntryService>();
            services.AddScoped<IActivityService, ActivityService>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ITimeEntryRepository, TimeEntryRepository>();
            services.AddScoped<IActivityRepository, ActivityRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IVacationRepository, VacationRepository>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IRestClient, RestClient>();
            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<ITimeTrackingEmailSender, TimeTrackingEmailSender>();
            services.AddScoped<IVacationService, VacationService>();
            services.AddScoped<IExpenseRepository, ExpenseRepository>();
            services.AddScoped<IExpenseService, ExpenseService>();
            services.AddScoped<IExpenseEmailSender, ExpenseEmailSender>();
            services.AddScoped<IRevenueRepository, RevenueRepository>();
            services.AddScoped<IRevenueService, RevenueService>();
            services.AddScoped<IRevenueEmailSender, RevenueEmailSender>();
            services.AddScoped<ICacheService, MemoryCacheService>();
            services.AddScoped<IUserLookupService, UserLookupService>();
            services.AddScoped<IClientLookupService, ClientLookupService>();
            services.AddScoped<IAssetRepository, AssetRepository>();
            services.AddScoped<IAssetService, AssetService>();
            services.AddScoped<IAssetEmailSender, AssetEmailSender>();
            services.AddScoped<IEmailService, EmailService>(serviceProvider =>
                new EmailService(serviceProvider.GetService<IRestClient>(), configuration.GetValue<string>("EmailApiUrl")));
            services.AddDbContext<TimeTrackingDbContext>(options =>
                        options.UseSqlServer(configuration.GetConnectionString(OPERATIONS_CONNECTION)));
            services.AddDbContext<ProjectDbContext>(options =>
                        options.UseSqlServer(configuration.GetConnectionString(OPERATIONS_CONNECTION)));
            services.AddDbContext<CashFlowDbContext>(options =>
                        options.UseSqlServer(configuration.GetConnectionString(OPERATIONS_CONNECTION)));
        }
    }
}
