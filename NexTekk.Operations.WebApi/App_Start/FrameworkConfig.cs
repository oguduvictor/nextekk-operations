﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NexTekk.Operations.WebApi.Filters;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// Framework Configuration to use at startup
    /// </summary>
    public static class FrameworkConfig
    {
        /// <summary>
        /// Method that adds framework services
        /// </summary>
        /// <param name="services"></param>
        public static void AddFrameworkServices(this IServiceCollection services)
        {
            services
              .AddMvc(options =>
              {
                    //var policy = new AuthorizationPolicyBuilder()
                    //    .RequireAuthenticatedUser()
                    //    .Build();

                    //options.Filters.Add(new AuthorizeFilter(policy));

                    options.Filters.Add(typeof(ModelValidatorFilter));
              })
              .AddJsonOptions(options =>
              {
                  options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                  options.SerializerSettings.Converters.Add(new StringEnumConverter(true));
                  options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                  options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
              });
        }
    }
}
