﻿using AutoMapper;
using NexTekk.Operations.Core.AutoMapperProfile;
using NexTekk.Operations.Data.Sql.AutoMapperProfile;
using NexTekk.Operations.WebApi.AutoMapperProfile;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// AutoMapper class for Mapping
    /// </summary>
    public class AutoMapperProfileConfig
    {
        /// <summary>
        /// Method that initializes mapped AutoMapper profiles
        /// </summary>
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CoreMapperProfile>();
                cfg.AddProfile<SqlDataMapperProfile>();
                cfg.AddProfile<WebApiMapperProfile>();
            });
        }

        /// <summary>
        /// Method for resetting AutoMapper. For Testing Only
        /// </summary>
        public static void Reset()
        {
            Mapper.Reset();
        }
    }
}
