﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.WebApi.Middleware;
using System;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// Logger and Exception Configurations to use at startup
    /// </summary>
    public static class LoggerAndExceptionConfig
    {
        /// <summary>
        /// Method that get called at runtime
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="serviceProvider"></param>
        public static void UseExceptionConfigurations(this IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var logger = serviceProvider.GetService<ILogger>();

            app.UseExceptionHandler(builder => builder.HandleExceptions(logger));
        }
    }
}
