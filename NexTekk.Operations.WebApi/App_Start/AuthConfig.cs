﻿using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IdentityModel.Tokens.Jwt;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// Authentication Configuration to use during startup
    /// </summary>
    public static class AuthConfig
    {
        /// <summary>
        /// Method for adding Authentication Configuration
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddApplicationAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication("Bearer")
               .AddIdentityServerAuthentication(
                   options =>
                   {
                       options.Authority = configuration.GetValue<string>("Authorization:Authority");
                       options.SaveToken = true;
                       options.RequireHttpsMetadata = false;
                       options.ApiSecret = configuration.GetValue<string>("Authorization:ApiSecret");
                       options.ApiName = configuration.GetValue<string>("Authorization:ApiName");
                       options.SupportedTokens = SupportedTokens.Both;
                   });
        }

        /// <summary>
        /// Method to use Added Application's Authentication
        /// </summary>
        /// <param name="app"></param>
        public static void UseApplicationAuthentication(this IApplicationBuilder app)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.UseAuthentication();
        }
    }
}
