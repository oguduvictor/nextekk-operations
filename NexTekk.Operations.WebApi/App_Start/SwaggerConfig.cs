﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.Swagger.Model;
using System.Collections.Generic;
using System.IO;

namespace NexTekk.Operations.WebApi.App_Start
{
    /// <summary>
    /// Swagger Related Configurations for use at Startup
    /// </summary>
    public static class SwaggerConfig
    {
        /// <summary>
        /// Method to add swagger generator and config
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddSwaggerConfigurations(this IServiceCollection services, IConfiguration configuration)
        {

            // Inject an implementation of ISwaggerProvider with defaulted settings applied.
            services.AddSwaggerGen();

            //// Add the detail information for the API.
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Time Tracking API",
                    Description = "A system for tracking times and tasks",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "NexTekk LLC",
                        Email = string.Empty,
                        Url = string.Empty
                    },
                    ////License = new License { Name = "Use under LICX", Url = "http://url.com" }
                });

                options.AddSecurityDefinition("auth2_security", new OAuth2Scheme
                {
                    Description = "OAuth2 client credentials flow",
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{configuration.GetValue<string>("Authorization:Authority")}Account/Login",
                    Scopes = new Dictionary<string, string> { { "Scope", "NexTekk.Operations" } }
                });

                ////Determine base path for the application.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                ////Set the comments path for the swagger json and ui.
                var xmlPath = Path.Combine(basePath, "NexTekk.Operations.WebApi.xml");
                options.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Enables middleware to use swagger
        /// </summary>
        /// <param name="app"></param>
        public static void UseSwaggerConfiguration(this IApplicationBuilder app)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();
        }
    }
}
