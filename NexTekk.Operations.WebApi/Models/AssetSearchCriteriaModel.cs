﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// search criteria model for asset
    /// </summary>
    public class AssetSearchCriteriaModel
    {
        /// <summary>
        /// Name of item to search by
        /// </summary>
        public string Item { get; set; }

        /// <summary>
        /// Serial No for model
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// Date range Asset was purchased
        /// </summary>
        public DateTimeRange PurchasedDate { get; set; }

        /// <summary>
        /// Current PageNumber
        /// </summary>
        public int PageNumber { get; set; }
        
        /// <summary>
        /// Date range Asset was assigned
        /// </summary>
        public DateTimeRange AssignedDate { get; set; }

        /// <summary>
        /// Condition to search by
        /// </summary>
        public Condition? Condition { get; set; }

        /// <summary>
        /// Assigned User's id to filter by
        /// </summary>
        public IEnumerable<Guid> AssigneeIds { get; set; }

        /// <summary>
        /// Ids of those who purchased assets
        /// </summary>
        public IEnumerable<Guid> PurchasedByIds { get; set; }
    }
}
