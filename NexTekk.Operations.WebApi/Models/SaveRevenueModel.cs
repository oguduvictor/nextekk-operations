﻿using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Model for creating/updating revenue
    /// </summary>
    public class SaveRevenueModel
    {
        /// <summary>
        /// PaymentDate of the revenue
        /// </summary>
        public DateTime PaymentDate { get; set; }
        
        /// <summary>
        /// The currency of the revenue
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// The amount of the revenue
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The description of the revenue
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The ID of the client
        /// </summary>
        public Guid ClientId { get; set; }
    }
}
