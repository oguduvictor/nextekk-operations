﻿using NexTekk.Operations.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// model for creating/updating time entry
    /// </summary>
    public class TimeEntryViewModel : TimeEntryDateRange
    {
        /// <summary>
        /// Task name of the time entry
        /// </summary>
        [Required, StringLength(256, MinimumLength = 3)]
        public string Task { get; set; }

        /// <summary>
        /// Employee's comment for time entry
        /// </summary>
        [StringLength(4000, MinimumLength = 5)]
        public string Comment { get; set; }

        /// <summary>
        /// Manager's comment for time entry
        /// </summary>
        [StringLength(4000, MinimumLength = 5)]
        public string ManagerComment { get; set; }

        /// <summary>
        /// Activity ID of time entry
        /// </summary>
        [Required]
        public Guid? ActivityId { get; set; }

        /// <summary>
        /// convert model to a time entry class
        /// </summary>
        public TimeEntry ToEntry(Guid? id = null)
        {
            var timeEntry = new TimeEntry();

            timeEntry.Id = id.HasValue ? id.Value : default(Guid);
            timeEntry.Task = Task;
            timeEntry.Comment = Comment;
            timeEntry.ManagerComment = ManagerComment;
            timeEntry.BeginDateTime = BeginDateTime;
            timeEntry.EndDateTime = EndDateTime;

            if (ActivityId != null)
            {
                timeEntry.Activity = new ActivitySummary();
                timeEntry.Activity.Id = ActivityId.Value;
            }

            return timeEntry;
        }
    }
}
