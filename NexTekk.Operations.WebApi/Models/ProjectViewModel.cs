﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// model for passing project
    /// </summary>
    public class ProjectViewModel
    {
        /// <summary>
        /// Title of the project
        /// </summary>
        [Required, StringLength(256, MinimumLength = 3)]
        public string Title { get; set; }
        
        /// <summary>
        /// ID for the client associated with project
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// Status of the project
        /// </summary>
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// Billable status of project
        /// </summary>
        public bool Billable { get; set; }

        /// <summary>
        /// Converts the model into an entry
        /// </summary>
        /// <returns></returns>
        public Project ToEntry(Guid? id = null)
        {
            var project = new Project();

            project.Id = id.HasValue ? id.Value : default(Guid);
            project.Title = Title;
            project.Status = Status;
            project.Billable = Billable;
            
            if (ClientId != null)
            {
                project.Client = new Client();
                project.Client.Id = ClientId;
            }

            return project;
        }
    }
}
