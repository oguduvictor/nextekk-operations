﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// model for creating/updating client
    /// </summary>
    public class ClientViewModel
    {
        /// <summary>
        /// Constructor for the model
        /// </summary>
        public ClientViewModel()
        {
            ContactIds = new List<int>();
        }

        /// <summary>
        /// Name of the client
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// List of contactIDs
        /// </summary>
        public IEnumerable<int> ContactIds { get; set; }
        
        /// <summary>
        /// Converts the model into an entry
        /// </summary>
        /// <returns></returns>
        public Client ToEntry(Guid? id = null)
        {
            var client = new Client();

            client.Id = id.HasValue ? id.Value : default(Guid);
            client.Name = Name;

            ContactIds.ForEach(contactId =>
            {
                var entity = new ContactSummary();
                entity.Id = contactId;

                client.Contacts.Add(entity);
            });

            return client;
        }
    }
}
