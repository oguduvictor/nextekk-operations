﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Expense model that is returned from API
    /// </summary>
    public class ExpenseViewModel
    {
        /// <summary>
        /// Expense Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Date the Expense was incurred
        /// </summary>
        public DateTime Date { get; set; }  

        /// <summary>
        /// Person who incurred the Expense
        /// </summary>
        public User IncurredBy { get; set; }

        /// <summary>
        /// Currency of the Expense
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// Amount of the Expense
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Descriptuin of the Expense
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Reason for the Expense
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Date Expense was Reinbursed
        /// </summary>
        public DateTime? Reimbursed { get; set; }
        
        /// <summary>
        /// Date Expense was Created
        /// </summary>
        public DateTime Created { get; set; }
        
        /// <summary>
        /// Date Expense was Modified
        /// </summary>
        public DateTime Modified { get; set; }
     }
}
