﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Criteria for searching expenses
    /// </summary>
    public class ExpenseSearchCriteriaModel
    {
        /// <summary>
        /// Id for the current logged in user
        /// </summary>
        public Guid? UserId { get; set; }
        
        /// <summary>
        /// Id of the incurrer
        /// </summary>
        public Guid? IncurredById { get; set; }

        /// <summary>
        /// Currency in which the expense is incured
        /// </summary>
        public Currency? Currency { get; set; }

        /// <summary>
        /// Minimum amount expensed
        /// </summary>
        public decimal? MinAmount { get; set; }

        /// <summary>
        /// Maximum amount expensed
        /// </summary>
        public decimal? MaxAmount { get; set; }

        /// <summary>
        /// The period during which expenses were incured
        /// </summary>
        public DateTimeRange IncurredDateRange { get; set; }

        /// <summary>
        /// Description of the expenses
        /// </summary>
        public string DescriptionContains { get; set; }

        /// <summary>
        /// If expense is reimbursed or not
        /// </summary>
        public bool? Reimbursed { get; set; }

        /// <summary>
        /// Current pageNumber
        /// </summary>
        public int PageNumber { get; set; }
    }
}
