﻿using NexTekk.Operations.Core.Models;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Asset model that is returned from API
    /// </summary>
    public class AssetViewModel : AssetSummaryViewModel
    {
        /// <summary>
        /// Description of asset
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Manufacturer of the asset
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Model of the asset
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Service tag for asset
        /// </summary>
        public string ServiceTag { get; set; }

        /// <summary>
        /// Service code for asset
        /// </summary>
        public string ServiceCode { get; set; }
        
        /// <summary>
        /// Date asset was marked bad
        /// </summary>
        public DateTime? MarkedBadOn { get; set; }

        /// <summary>
        /// Date which last person used asset before it went bad
        /// </summary>
        public DateTime? LastAssignedDate { get; set; }

        /// <summary>
        /// Person who marked asset as bad
        /// </summary>
        public User MarkedBadBy { get; set; }

        /// <summary>
        /// Person who entered the asset
        /// </summary>
        public User EnteredBy { get; set; }

        /// <summary>
        /// Last person that asset was assigned to before it went bad
        /// </summary>
        public User LastAssignee { get; set; }
    }
}
