﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Model for creating/updating of Items
    /// </summary>
    public class SaveAssetModel
    {
        /// <summary>
        /// Name of the Asset
        /// </summary>
        public string Item { get; set; }

        /// <summary>
        /// Serial # of the Asset
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// Purchase Date of the Asset
        /// </summary>
        public DateTime? PurchasedDate { get; set; }

        /// <summary>
        /// The Purchases Id of the asset
        /// </summary>
        public Guid? PurchasedById { get; set; }

        /// <summary>
        /// The Current Assignee Id of the asset
        /// </summary>
        public Guid? AssigneeId { get; set; }

        /// <summary>
        /// Current Assigned Date of the Asset
        /// </summary>
        public DateTime? AssignedDate { get; set; }

        /// <summary>
        /// The condition of the asset
        /// </summary>
        public Condition Condition { get; set; }

        /// <summary>
        /// Category in which the item is grouped
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Description of the item
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The name of the Manafacturer of asset
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Model of the item
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Service Tag on the item
        /// </summary>
        public string ServiceTag { get; set; }

        /// <summary>
        /// Service code on the item
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        /// Cost of the item
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Currency of Cost
        /// </summary>
        public Currency Currency { get; set; }
    }
}
