﻿using NexTekk.Operations.Core.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// model for creating/updating contact
    /// </summary>
    public class ContactViewModel
    {
        /// <summary>
        /// Firstname of the contact
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Lastname of the contact
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Email of the contact
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Primary phonenumber of the contact
        /// </summary>
        public string PhoneNumber1 { get; set; }

        /// <summary>
        /// Secondary phonenumber of the contact
        /// </summary>
        public string PhoneNumber2 { get; set; }

        /// <summary>
        /// Converts the model into an entity
        /// </summary>
        /// <returns></returns>
        public ContactEntity ToEntity(int? id = null)
        {
            var contact = new ContactEntity();

            contact.Id = id.HasValue ? id.Value : default(int);
            contact.FirstName = FirstName;
            contact.LastName = LastName;
            contact.Email = Email;
            contact.PhoneNumber1 = PhoneNumber1;
            contact.PhoneNumber2 = PhoneNumber2;

            return contact;
        }
    }
}
