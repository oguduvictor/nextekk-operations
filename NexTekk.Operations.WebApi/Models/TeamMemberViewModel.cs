﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// model for passing teamMembers
    /// </summary>
    public class TeamMemberViewModel
    {
        /// <summary>
        /// ID for the project
        /// </summary>
        public Guid ProjectId { get; set; }
        
        /// <summary>
        /// ID for the user
        /// </summary>
        public Guid UserId { get; set; }
        
        /// <summary>
        /// Role of the user
        /// </summary>
        public TeamMemberRole Role { get; set; }

        /// <summary>
        /// Converts the model into an entity
        /// </summary>
        /// <returns></returns>
        public TeamMemberEntity ToEntity()
        {
            var teamMember = new TeamMemberEntity();

            teamMember.ProjectId = ProjectId;
            teamMember.UserId = UserId;
            teamMember.Role = Role;
            
            return teamMember;
        }
    }
}
