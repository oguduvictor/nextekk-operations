﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// search criteria model for revenue
    /// </summary>
    public class RevenueSearchCriteriaModel
    {
        /// <summary>
        /// The date range for PaymentDate
        /// </summary>
        public DateTimeRange PaymentDate { get; set; }

        /// <summary>
        /// ID of person who entered the revenue
        /// </summary>
        public Guid? EnteredBy { get; set; }

        /// <summary>
        /// Currency which revenue was saved in
        /// </summary>
        public Currency? Currency { get; set; }
        
        /// <summary>
        /// Ids of clients to search from
        /// </summary>
        public IEnumerable<Guid> ClientIds { get; set; }

        /// <summary>
        /// Description of the Revenue
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Minimum amount of revenue to search for
        /// </summary>
        public int? MinAmount { get; set; }

        /// <summary>
        /// Maximum amount of revenue to search for
        /// </summary>
        public int? MaxAmount { get; set; }

        /// <summary>
        /// Current pageNumber
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Specify whether Expenses is checked from view
        /// </summary>
        public bool IncludeExpenses { get; set; }
    }
}
