﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// model for creating/updating Activity
    /// </summary>
    public class ActivityModel
    {
        /// <summary>
        /// Title of the activity
        /// </summary>
        [Required, MinLength(3)]
        public string Title { get; set; }

        /// <summary>
        /// Billable status of activity
        /// </summary>
        public bool Billable { get; set; }

        /// <summary>
        /// Vacation status of activity
        /// </summary>
        public bool IsVacation { get; set; }

        /// <summary>
        /// Description of activity
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Converts model to entity
        /// </summary>
        public ActivityEntity ToEntity(Guid? id = null)
        {
            var activity = new ActivityEntity();

            activity.Id = id.HasValue ? id.Value : default(Guid);
            activity.Title = Title;
            activity.Billable = Billable;
            activity.IsVacation = IsVacation;
            activity.Description = Description;

            return activity;
        }
    }
}
