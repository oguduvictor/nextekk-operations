﻿using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Response message in case of errors
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// ID for the log entry
        /// </summary>
        public Guid LogId { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Error message for the inner exception
        /// </summary>
        public string InnerExceptionMessage { get; set; }

        /// <summary>
        /// Stack trace for the exception
        /// </summary>
        public string StackTrace { get; set; }
    }
}
