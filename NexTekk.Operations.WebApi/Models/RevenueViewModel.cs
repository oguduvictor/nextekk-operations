﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Revenue model that is returned when API request is made
    /// </summary>
    public class RevenueViewModel
    {
        /// <summary>
        /// Id of the view model
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Date payment was made
        /// </summary>
        public DateTime PaymentDate { get; set; }

        /// <summary>
        /// Currency payment was made in
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// Amount paid
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Details about the revenue
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Client who brought revenue
        /// </summary>
        public ClientSummary Client { get; set; }

        /// <summary>
        /// Date revenue was created
        /// </summary>
        public DateTime Created { get; set; }
        
        /// <summary>
        /// Date revenue was modified
        /// </summary>
        public DateTime Modified { get; set; }
        
        /// <summary>
        /// Person who created the revenue
        /// </summary>
        public User CreatedBy { get; set; }
    }
}
