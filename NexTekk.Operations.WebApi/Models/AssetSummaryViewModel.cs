﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Asset summary model that is returned from API
    /// </summary>
    public class AssetSummaryViewModel
    {
        /// <summary>
        /// Id of the asset
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Name of asset
        /// </summary>
        public string Item { get; set; }

        /// <summary>
        /// Serial number of Asset
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// Date Asset was purchased
        /// </summary>
        public DateTime PurchasedDate { get; set; }

        /// <summary>
        /// Date asset was assigned
        /// </summary>
        public DateTime? AssignedDate { get; set; }

        /// <summary>
        /// Condition of the asset
        /// </summary>
        public Condition Condition { get; set; }

        /// <summary>
        /// Cost of asset
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Currency of the asset
        /// </summary>
        public Currency Currency { get; set; }
        
        /// <summary>
        /// Category asset belongs to
        /// </summary>
        public Category Category { get; set; }
        
        /// <summary>
        /// Person who purchased the asset
        /// </summary>
        public User PurchasedBy { get; set; }

        /// <summary>
        /// Person asset is assigned to
        /// </summary>
        public User Assignee { get; set; }
    }
}
