﻿using NexTekk.Operations.Core.Models;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Criteria for searching time entries
    /// </summary>
    public class TimeEntrySearchCriteriaModel
    {
        /// <summary>
        /// The datetime for begin
        /// </summary>
        public DateTime? BeginDateTime { get; set; }

        /// <summary>
        /// The datetime for begin
        /// </summary>
        public DateTime? EndDateTime { get; set; }

        /// <summary>
        /// ID for the user that created the time entry
        /// </summary>
        public Guid? CreatorId { get; set; }

        /// <summary>
        /// ID for the manager in charge of the project
        /// </summary>
        public Guid? ProjectManagerId { get; set; }

        /// <summary>
        /// ID for the activity/project
        /// </summary>
        public Guid? ActivityId { get; set; }

        /// <summary>
        /// Status of the time entry
        /// </summary>
        public TimeEntryStatus? Status { get; set; }

        /// <summary>
        /// Converts the model into a search object
        /// </summary>
        /// <returns></returns>
        public TimeEntrySearchCriteria ToSearch()
        {
            return new TimeEntrySearchCriteria
            {
                BeginDateTime = BeginDateTime,
                EndDateTime = EndDateTime,
                CreatorId = CreatorId,
                Status = Status,
                ProjectManagerId = ProjectManagerId,
                ProjectOrActivityId = ActivityId
            };
        }
    }
}
