﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.WebApi.Models
{
    /// <summary>
    /// Model for creating/updating expenses
    /// </summary>
    public class SaveExpenseModel
    {
        /// <summary>
        /// Date of the incurred expense
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// ID of the person that incurred the expense
        /// </summary>
        public Guid IncurredById { get; set; }

        /// <summary>
        /// The currency of the incurred expense
        /// </summary>
        public Currency Currency { get; set; }

        /// <summary>
        /// The amount of the expense
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The description of the expense
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The reason for the expense
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Date of reimbursement
        /// </summary>
        public DateTime? Reimbursed { get; set; }
    }
}
