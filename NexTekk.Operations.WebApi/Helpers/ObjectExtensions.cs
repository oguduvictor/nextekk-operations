﻿using AutoMapper;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.WebApi.Helpers
{
    /// <summary>
    /// Object extensions for WebAPI
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Maps Asset to AssetViewModel
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        public static async Task<AssetViewModel> ToAssetViewModel(this IUserLookupService userLookupService, Asset asset)
        {
            var assetViewModel = Mapper.Map<AssetViewModel>(asset);

            await userLookupService.SetAssetViewUsers(assetViewModel, asset);

            return assetViewModel;
        }

        /// <summary>
        /// Maps Assets to AssetsViewModel
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="assets"></param>
        /// <returns></returns>
        public static async Task<IList<AssetViewModel>> ToAssetsViewModel(this IUserLookupService userLookupService, IEnumerable<Asset> assets)
        {
            var assetsViewModel = Mapper.Map<IList<AssetViewModel>>(assets);

            foreach (var asset in assets)
            {
                var assetViewModel = assetsViewModel.FirstOrDefault(x => x.Id == asset.Id);

                await userLookupService.SetAssetViewUsers(assetViewModel, asset);
            }

            return assetsViewModel;
        }

        /// <summary>
        /// Maps AssetSummary to AssetSummaryViewModel
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="assetSummary"></param>
        /// <returns></returns>
        public static async Task<AssetSummaryViewModel> ToAssetSummaryViewModel(this IUserLookupService userLookupService, AssetSummary assetSummary)
        {
            var assetSummaryViewModel = Mapper.Map<AssetSummaryViewModel>(assetSummary);

            await userLookupService.SetAssetSummaryUsers(assetSummaryViewModel, assetSummary);

            return assetSummaryViewModel;
        }

        /// <summary>
        /// Maps AssetSummaries to AssetSummaryViewModel list
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="assetSummaries"></param>
        /// <returns></returns>
        public static async Task<IList<AssetSummaryViewModel>> ToAssetSummariesViewModel(this IUserLookupService userLookupService, IEnumerable<AssetSummary> assetSummaries)
        {
            var assetSummariesViewModel = Mapper.Map<IList<AssetSummaryViewModel>>(assetSummaries);

            foreach (var assetSummary in assetSummaries)
            {
                var assetSummaryViewModel = assetSummariesViewModel.FirstOrDefault(x => x.Id == assetSummary.Id);

                await userLookupService.SetAssetSummaryUsers(assetSummaryViewModel, assetSummary);
            }

            return assetSummariesViewModel;
        }

        /// <summary>
        /// Maps Revenue to RevenueViewModel
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="clientLookupService"></param>
        /// <param name="revenue"></param>
        /// <returns></returns>
        public static async Task<RevenueViewModel> ToRevenueViewModel(this IUserLookupService userLookupService, IClientLookupService clientLookupService, Revenue revenue)
        {
            var revenueViewModel = Mapper.Map<RevenueViewModel>(revenue);

            await userLookupService.SetRevenueViewUsersAndClient(clientLookupService, revenueViewModel, revenue);

            return revenueViewModel;
        }

        /// <summary>
        /// Maps revenues to revenueViewModels
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="clientLookupService"></param>
        /// <param name="revenues"></param>
        /// <returns></returns>
        public static async Task<IList<RevenueViewModel>> ToRevenuesViewModel(this IUserLookupService userLookupService, IClientLookupService clientLookupService,
            IEnumerable<Revenue> revenues)
        {
            var revenuesViewModel = Mapper.Map<IList<RevenueViewModel>>(revenues);

            foreach (var revenue in revenues)
            {
                var revenueViewModel = revenuesViewModel.FirstOrDefault(x => x.Id == revenue.Id);

                await userLookupService.SetRevenueViewUsersAndClient(clientLookupService, revenueViewModel, revenue);
            }

            return revenuesViewModel;
        }
        
        /// <summary>
        /// Maps Expenses to ExpenseViewModel
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="expenses"></param>
        /// <returns></returns>
        public static async Task<IList<ExpenseViewModel>> ToExpensesViewModel(this IUserLookupService userLookupService, IEnumerable<Expense> expenses)
        {
            var expensesViewModel = Mapper.Map<IList<ExpenseViewModel>>(expenses);

            foreach (var expense in expenses)
            {
                var expenseViewModel = expensesViewModel.FirstOrDefault(x => x.Id == expense.Id);

                expenseViewModel.IncurredBy = await userLookupService.GetUserAsync(expense.IncurredById);
            }

            return expensesViewModel;
        }
        
        /// <summary>
        /// Maps Expenses to ExpenseViewModel
        /// </summary>
        /// <param name="userLookupService"></param>
        /// <param name="expense"></param>
        /// <returns></returns>
        public static async Task<ExpenseViewModel> ToExpenseViewModel(this IUserLookupService userLookupService, Expense expense)
        {
            var expenseViewModel = Mapper.Map<ExpenseViewModel>(expense);
            
            expenseViewModel.IncurredBy = await userLookupService.GetUserAsync(expense.IncurredById);

            return expenseViewModel;
        }

        private static async Task SetAssetViewUsers(this IUserLookupService userLookupService,
            AssetViewModel assetViewModel, Asset asset)
        {
            assetViewModel.EnteredBy = await userLookupService.GetUserAsync(asset.CreatedById);
            assetViewModel.LastAssignee = asset.LastAssigneeId.IsNull() ? null : await userLookupService.GetUserAsync((Guid)asset.LastAssigneeId);
            assetViewModel.MarkedBadBy = asset.MarkedBadById.IsNull() ? null : await userLookupService.GetUserAsync((Guid)asset.MarkedBadById);
            assetViewModel.Assignee = asset.AssigneeId.IsNull() ? null : await userLookupService.GetUserAsync((Guid)asset.AssigneeId);
            assetViewModel.PurchasedBy = asset.PurchasedById == default(Guid) ? null : await userLookupService.GetUserAsync(asset.PurchasedById);
        }

        private static async Task SetAssetSummaryUsers(this IUserLookupService userLookupService,
            AssetSummaryViewModel assetSummaryViewModel, AssetSummary assetSummary)
        {
            assetSummaryViewModel.Assignee = assetSummary.AssigneeId.IsNull() ? null : await userLookupService.GetUserAsync(assetSummary.AssigneeId.Value);

            assetSummaryViewModel.PurchasedBy = await userLookupService.GetUserAsync(assetSummary.PurchasedById);
        }

        private static async Task SetRevenueViewUsersAndClient(this IUserLookupService userLookupService,
            IClientLookupService clientLookupService, RevenueViewModel revenueViewModel, Revenue revenue)
        {
            revenueViewModel.CreatedBy = await userLookupService.GetUserAsync(revenue.CreatedById);
            revenueViewModel.Client = await clientLookupService.GetClientAsync(revenue.ClientId);
        }
    }
}
