﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Core.Reports;
using NexTekk.Operations.Services.Helpers;
using NexTekk.Operations.WebApi.Helpers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing revenue in the system
    /// </summary>
    [Route("api/revenues")]
    [EnableCors("myCorsPolicy")]
    [Authorize(Roles = ManagerOnly)]
    public class RevenuesController : Controller
    {
        private readonly IRevenueService _revenueService;
        private readonly IUserLookupService _userLookupService;
        private readonly IClientLookupService _clientLookupService;

        /// <summary>
        /// RevenueController constructor
        /// </summary>
        /// <param name="revenueService"></param>
        /// <param name="userLookupService"></param>
        /// <param name="clientLookupService"></param>
        public RevenuesController(
            IRevenueService revenueService,
            IUserLookupService userLookupService,
            IClientLookupService clientLookupService)
        {
            _revenueService = revenueService;
            _userLookupService = userLookupService;
            _clientLookupService = clientLookupService;
        }

        /// <summary>
        /// Gets all revenue based on search criteria
        /// </summary>
        /// <param name="searchCriteriaModel"></param>
        /// <returns>List of all Revenue</returns>
        [HttpGet]
        public async Task<IEnumerable<RevenueViewModel>> GetAll([FromQuery]RevenueSearchCriteriaModel searchCriteriaModel)
        {
            var searchCriteria = Mapper.Map<RevenueSearchCriteria>(searchCriteriaModel);
            var revenues = await _revenueService.GetRevenuesAsync(searchCriteria);

            return await _userLookupService.ToRevenuesViewModel(_clientLookupService, revenues);
        }

        /// <summary>
        /// Create a revenue
        /// </summary>
        /// <param name="saveModel">revenue to be created</param>
        /// <returns>RevenueEntity</returns>
        [HttpPost]
        public async Task<RevenueViewModel> Create([FromBody]SaveRevenueModel saveModel)
        {
            var revenueToSave = Mapper.Map<Revenue>(saveModel);
            var revenue = await _revenueService.SaveRevenueAsync(revenueToSave);
            
            return await _userLookupService.ToRevenueViewModel(_clientLookupService, revenue);
        }

        /// <summary>
        /// Updates a revenue
        /// </summary>
        /// <param name="id">ID of revenue to be updated</param>
        /// <param name="saveModel">revenue to be updated</param>
        [HttpPut("{id:guid}")]
        public async Task Update(Guid id, [FromBody]SaveRevenueModel saveModel)
        {
            var revenueToSave = Mapper.Map<Revenue>(saveModel);
            revenueToSave.Id = id;

            await _revenueService.SaveRevenueAsync(revenueToSave);
        }

        /// <summary>
        /// Delete Revenue
        /// </summary>
        /// <param name="id">ID of revenue to be deleted</param>
        [HttpDelete("{id:guid}")]
        public async Task Delete(Guid id)
        {
            await _revenueService.DeleteRevenueAsync(id);
        }
        
        /// <summary>
        /// Returns a report file
        /// </summary>
        /// <param name="format">File format of the report. E.g. Excel, CSV etc.</param>
        /// <param name="searchCriteriaModel">Criteria that defines the scope of the report data</param>
        /// <returns>File download</returns>
        [HttpGet]
        [Route("download")]
        public async Task<FileResult> DownloadReport([FromQuery] FileFormat format, [FromQuery]RevenueSearchCriteriaModel searchCriteriaModel)
        {
            var searchCriteria = Mapper.Map<RevenueSearchCriteria>(searchCriteriaModel);
            searchCriteria.PageSize = (searchCriteria.PageNumber > 0 ? searchCriteria.PageNumber : 1) * 100;
            searchCriteria.NumberToSkip = 0;

            var revenues = await _revenueService.GetRevenuesAsync(searchCriteria);
            var revenuesViewModel = await _userLookupService.ToRevenuesViewModel(_clientLookupService, revenues);

            var report = ReportGenerator.GenerateReport(revenuesViewModel, format, ReportHeadingsDefinition.Revenues, "Revenue Report");
            var fileFormatExt = ReportGenerator.GetFileFormatAndName("revenue_report", format);

            return File(report, fileFormatExt.MIMEType, fileFormatExt.Name);
        }
    }
}