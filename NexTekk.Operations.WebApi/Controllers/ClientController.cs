﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing clients
    /// </summary>
    [Route("api/[controller]/[action]")]
    [EnableCors("myCorsPolicy")]
    [Authorize]
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;

        /// <summary>
        /// Constructor for the client controller
        /// </summary>
        /// <param name="clientService">client management service</param>
        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        /// <summary>
        /// Gets the list of clients 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Client>> GetAll()
        {
            return await _clientService.GetClients();
        }

        /// <summary>
        /// Gets the list of client summaries 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<ClientSummary>> GetClientSummaries()
        {
            return await _clientService.GetClientSummaries();
        }
        
        /// <summary>
        /// Gets client details for the given ID
        /// </summary>
        /// <param name="id">client ID</param>
        /// <returns>client detail</returns>
        [HttpGet("{id}")]
        public async Task<ClientDetail> Get(Guid id)
        {
            return await _clientService.GetClient(id);
        }
        
        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="client">Model for the client to be updated</param>
        [HttpPost]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Client> Create([FromBody] ClientViewModel client)
        {
            return await _clientService.SaveClient(client.ToEntry());
        }
        
        /// <summary>
        /// Updates an existing client
        /// </summary>
        /// <param name="id">ID of the client</param>
        /// <param name="client">Model for the client to be updated</param>
        [HttpPut("{id:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Client> Update(Guid id, [FromBody] ClientViewModel client)
        {
            return await _clientService.SaveClient(client.ToEntry(id));
        }

        /// <summary>
        /// Deletes the client from the database
        /// </summary>
        /// <param name="id">ID of the client</param>
        /// <returns>Http status code of 200</returns>
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task Delete(Guid id)
        {
            await _clientService.DeleteClient(id);
        }
    }
}
