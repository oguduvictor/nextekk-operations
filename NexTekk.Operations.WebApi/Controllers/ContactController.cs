﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.WebApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing contacts
    /// </summary>
    [Route("api/[controller]/[action]")]
    [EnableCors("myCorsPolicy")]
    [Authorize]
    public class ContactController : Controller
    {
        private readonly IContactService _contactService;

        /// <summary>
        /// Constructor for the contact controller
        /// </summary>
        /// <param name="contactService">contact management service</param>
        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        /// <summary>
        /// Gets the list of contact summaries 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<ContactSummary>> GetContactSummaries()
        {
            return await _contactService.GetContactSummaries();
        }

        /// <summary>
        /// Gets the list of contact summaries 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Contact>> GetAll()
        {
            return await _contactService.GetContacts();
        }

        /// <summary>
        /// Gets contact details for the given ID
        /// </summary>
        /// <param name="id">contact ID</param>
        /// <returns>contact detail</returns>
        [HttpGet("{id:min(1)}")]
        public async Task<ContactDetail> Get(int id)
        {
            return await _contactService.GetContact(id);
        }

        /// <summary>
        /// Creates a new contact
        /// </summary>
        /// <param name="contact">Model for the contact to be updated</param>
        [HttpPost]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Contact> Create([FromBody]ContactViewModel contact)
        {
            return await _contactService.SaveContact(contact.ToEntity());
        }

        /// <summary>
        /// Updates an existing contact
        /// </summary>
        /// <param name="id">ID of the contact</param>
        /// <param name="contact">Model for the contact to be updated</param>
        [HttpPut("{id:min(1)}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Contact> Update(int id, [FromBody] ContactViewModel contact)
        {
            return await _contactService.SaveContact(contact.ToEntity(id));
        }

        /// <summary>
        /// Deletes the contact from the database
        /// </summary>
        /// <param name="id">ID of the contact</param>
        /// <returns>Http status code of 200</returns>
        [HttpDelete("{id:min(1)}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task Delete(int id)
        {
            await _contactService.DeleteContact(id);
        }
    }
}
