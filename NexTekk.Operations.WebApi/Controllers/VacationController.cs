﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing vacations
    /// </summary>
    [Route("api/[controller]/[action]")]
    [EnableCors("myCorsPolicy")]
    public class VacationController
    {
        private readonly IVacationService _vacationService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="vacationService"></param>
        public VacationController(IVacationService vacationService)
        {
            _vacationService = vacationService;
        }

        /// <summary>
        /// Gets vacation by ID
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="year">The year of the vacation. Default is current year.</param>
        [HttpGet("{employeeId:guid}/{year:min(2000)?}")]
        [Authorize]
        public async Task<Vacation> Get(Guid employeeId, int year = 0)
        {
            return await _vacationService.GetVacation(employeeId, year);
        }

        /// <summary>
        /// Gets all the vacations available in the system for the specified year
        /// <param name="year">The year of the vacations. Default is current year.</param>
        /// </summary>
        [HttpGet("{year:min(2000)?}")]
        [Authorize]
        public async Task<IEnumerable<Vacation>> GetAll(int year = 0)
        {
            return await _vacationService.GetVacations(year);
        }

        /// <summary>
        /// Gets all time entries for the vacations taken by the specified employee in the given year
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="year">The year for which time entries are being requested. Default is current year.</param>
        [HttpGet("{employeeId:guid}/{year:min(2000)?}")]
        [Authorize]
        public async Task<IEnumerable<TimeEntry>> GetTimeEntries(Guid employeeId, int year = 0)
        {
            return await _vacationService.GetVacationTimeEntries(employeeId, year);
        }

        /// <summary>
        /// Updates the number of hours per period for the specified employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="hours">New value for the number of hours per pay period.</param>
        [HttpPut("{employeeId:guid}")]
        [Authorize]
        public async Task<decimal> UpdateHours(Guid employeeId, [FromBody]decimal hours)
        {
            return await _vacationService.UpdateHoursPerPeriod(employeeId, hours);
        }

        /// <summary>
        /// Updates the number of hours rolled over from the previous year
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="yearBeginBalance">New value for the number of hours rolled over from the previous year</param>
        [HttpPut("{employeeId:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task<decimal> UpdateYearBeginBalance(Guid employeeId, [FromBody]decimal yearBeginBalance)
        {
            return await _vacationService.UpdateYearBeginBalance(employeeId, yearBeginBalance);
        }

        /// <summary>
        /// Creates vacation settings entry for the given employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="hours">New value for the number of hours per pay period.</param>
        /// <param name="yearBeginBalance">New value for the number of hours rolled over from the previous year</param>
        [HttpPut("{employeeId:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task<VacationSetting> SaveSettings(Guid employeeId, [FromQuery]decimal hours, [FromBody]decimal yearBeginBalance)
        {
            return await _vacationService.SaveVacation(employeeId, hours, yearBeginBalance);
        }
    }
}
