﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing activities in the system
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("myCorsPolicy")]
    [Authorize]
    public class ActivityController : Controller
    {
        private readonly IActivityService _activityService;

        /// <summary>
        /// ActivityController constructor
        /// </summary>
        /// <param name="activityService">Repository for retrieving activities</param>
        public ActivityController(IActivityService activityService)
        {
            _activityService = activityService;
        }

        /// <summary>
        /// Gets the list of all activities in the system
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Activity>> Get()
        {
            return await _activityService.GetActivities();
        }

        /// <summary>
        /// Gets activity for the given ID
        /// </summary>
        /// <param name="id">ID of the activity</param>
        /// <returns>Activity</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActivityDetail> Get(Guid id)
        {
            return await _activityService.GetActivityById(id);
        }

        /// <summary>
        /// Creates a new activity in the system
        /// </summary>
        /// <param name="activityModel">Model that represents the activity to be created</param>
        /// <returns>Activity</returns>
        [HttpPost]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Activity> Post([FromBody]ActivityModel activityModel)
        {
            return await _activityService.SaveActivity(activityModel.ToEntity());
        }

        /// <summary>
        /// Updates an existing activity
        /// </summary>
        /// <param name="id">ID of the activity to be updated</param>
        /// <param name="activityModel">Model representing the activity to be updated</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Activity> Put(Guid id, [FromBody]ActivityModel activityModel)
        {
            return await _activityService.SaveActivity(activityModel.ToEntity(id));
        }

        /// <summary>
        /// Deletes the activity with the given ID
        /// </summary>
        /// <param name="id">ID of the activity to be deleted</param>
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task Delete(Guid id)
        {
            await _activityService.DeleteActivity(id);
        }
    }
}
