﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Services.Helpers;
using NexTekk.Operations.WebApi.Helpers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing assets
    /// </summary>
    [Route("api/assets")]
    [EnableCors("myCorsPolicy")]
    [Authorize(Roles = EmployeeOrManager)]
    public class AssetsController : Controller
    {
        private readonly IAssetService _assetService;
        private readonly IUserLookupService _userLookupService;

        /// <summary>
        /// Constructor that receives dependencies
        /// </summary>
        /// <param name="assetService"></param>
        /// <param name="userLookupService"></param>
        public AssetsController(IAssetService assetService, IUserLookupService userLookupService)
        {
            _assetService = assetService;
            _userLookupService = userLookupService;
        }

        /// <summary>
        /// Get all the asset based on the search criteria
        /// </summary>
        /// <return>Asset</return>
        [HttpGet, Route("search")]
        public async Task<IEnumerable<AssetSummaryViewModel>> Search([FromQuery] AssetSearchCriteriaModel searchCriteriaModel)
        {
            var searchCriteria = Mapper.Map<AssetSearchCriteria>(searchCriteriaModel);

            var assetSummaries = await _assetService.GetAssetSummariesAsync(searchCriteria);

            return await _userLookupService.ToAssetSummariesViewModel(assetSummaries);
        }

        /// <summary>
        /// Get the details of the Asset using the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<AssetViewModel> Get(Guid id)
        {
            var asset = await _assetService.Get(id);

            return await _userLookupService.ToAssetViewModel(asset);
        }

        /// <summary>
        /// Create an asset
        /// </summary>
        /// <param name="model">asset to be created</param>
        /// <returns>Asset</returns>
        [HttpPost]
        public async Task<AssetSummaryViewModel> Create([FromBody]SaveAssetModel model)
        {
            var assetDTO = Mapper.Map<Asset>(model);

            var asset = await _assetService.SaveAssetAsync(assetDTO);

            return await _userLookupService.ToAssetSummaryViewModel(asset);
        }

        /// <summary>
        /// Updates an asset
        /// </summary>
        /// <param name="id">ID of asset to be updated</param>
        /// <param name="model">model to be updated</param>
        [HttpPut("{id:guid}")]
        public async Task Update(Guid id, [FromBody]SaveAssetModel model)
        {
            var assetDTO = Mapper.Map<Asset>(model);
            assetDTO.Id = id;

            await _assetService.SaveAssetAsync(assetDTO);
        }

        /// <summary>
        /// Delete Asset
        /// </summary>
        /// <param name="id">ID of the assets to be deleted</param>
        [Authorize(Roles = ManagerOnly), HttpDelete("{id:guid}")]
        public async Task Delete(Guid id)
        {
            await _assetService.DeleteAsset(id);
        }

        /// <summary>
        /// Returns a report file
        /// </summary>
        /// <param name="format">File format of the report. E.g. Excel, CSV etc.</param>
        /// <param name="searchCriteriaModel">Criteria that defines the scope of the report data</param>
        /// <returns>File download</returns>
        [Authorize(Roles = ManagerOnly), HttpGet, Route("download")]
        public async Task<FileResult> DownloadReport([FromQuery] FileFormat format, [FromQuery]AssetSearchCriteriaModel searchCriteriaModel)
        {
            var searchCriteria = Mapper.Map<AssetSearchCriteria>(searchCriteriaModel);
            searchCriteria.PageSize = (searchCriteria.PageNumber > 0 ? searchCriteria.PageNumber : 1) * 100;
            searchCriteria.NumberToSkip = 0;

            var assets = await _assetService.GetAssetsAsync(searchCriteria);
            var assetsViewModel = await _userLookupService.ToAssetsViewModel(assets);

            var report = ReportGenerator.GenerateReport(assetsViewModel, format, ReportHeadingsDefinition.Assets, "Assets Report");
            var fileFormatExt = ReportGenerator.GetFileFormatAndName("assets_report", format);

            return File(report, fileFormatExt.MIMEType, fileFormatExt.Name);
        }

        /// <summary>
        /// Method that is called when asset is requested
        /// </summary>
        /// <param name="id">ID of the asset to be requested for</param>
        /// <returns></returns>
        [HttpGet, Route("request")]
        public async Task RequestAsset(Guid id)
        {
            await _assetService.RequestAsset(id);
        }
    }
}
