﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for accessing user information
    /// </summary>
    [Authorize]
    [Route("api/[controller]/[action]")]
    [EnableCors("myCorsPolicy")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userRepository">user repository</param>
        /// <param name="userContext">Use context</param>
        public UserController(IUserRepository userRepository, IUserContext userContext)
        {
            _userRepository = userRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Gets information for the currently logged-in user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public UserDetail GetLoggedInUser()
        {
            return _userContext.GetUserDetail();
        }

        /// <summary>
        /// Gets info for all users in the system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<UserEntity>> GetAllUsers()
        {
            return await _userRepository.GetAll();
        }

        /// <summary>
        /// Gets info for all employees and managers in the system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<UserEntity>> GetEmployeesAndManagers()
        {
            return await _userRepository.GetAll(employeesAndManagersOnly: true);
        }
    }
}
