﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing time entries
    /// </summary>
    ///
    [Route("api/[controller]/[action]")]
    [EnableCors("myCorsPolicy")]
    [Authorize]
    public class TimeEntryController : Controller
    {
        private readonly ITimeEntryService _timeEntryService;
        private readonly IUserContext _userContext;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor for the time entry controller
        /// </summary>
        /// <param name="userContext">User context management service</param>
        /// <param name="timeEntryService">Time entry management service</param>
        public TimeEntryController(
            ITimeEntryService timeEntryService, 
            IUserContext userContext,
            ILogger logger)
        {
            _timeEntryService = timeEntryService;
            _userContext = userContext;
            _logger = logger;
        }

        /// <summary>
        /// Gets the list of time entry summaries that match the search criteria
        /// </summary>
        /// <param name="searchCriteria">Criteria for searching time entries</param>
        /// <returns>List of time entries</returns>
        [HttpGet]
        public async Task<IEnumerable<TimeEntry>> Search([FromQuery]TimeEntrySearchCriteria searchCriteria)
        {
            return await _timeEntryService.GetTimeEntries(searchCriteria);
        }

        /// <summary>
        /// Gets time entry details for the given ID
        /// </summary>
        /// <param name="id">Time entry ID</param>
        /// <returns>List of time entries</returns>
        [HttpGet("{id:guid}")]
        public async Task<TimeEntryDetail> Get(Guid id)
        {
            return await _timeEntryService.GetTimeEntry(id);
        }

        /// <summary>
        /// Gets the list of activities and projects in the system
        /// </summary>
        /// <returns>List of all activities in the system</returns>
        [HttpGet]
        public async Task<IEnumerable<ActivitySummary>> AllActivities()
        {
            return await _timeEntryService.GetAllActivities();
        }

        /// <summary>
        /// Gets the list of activities and projects which logged in user is assigned to
        /// </summary>
        /// <returns>List of logged-in user projects activities in the system</returns>
        [HttpGet]
        public async Task<IEnumerable<ActivitySummary>> UserActivities()
        {
            var userId = _userContext.GetUserId();

            return await _timeEntryService.GetAllActivities(userId);
        }

        /// <summary>
        /// Creates a new time entry
        /// </summary>
        /// <param name="timeEntryViewModel">Model for the time entry to be updated</param>
        [HttpPost]
        [Authorize(Roles = EmployeeOrManager)]
        public async Task<TimeEntryDetail> Create([FromBody]TimeEntryViewModel timeEntryViewModel)
        {
            return await _timeEntryService.SaveTimeEntry(timeEntryViewModel.ToEntry());
        }

        /// <summary>
        /// Updates an existing time entry
        /// </summary>
        /// <param name="id">ID of the time entry</param>
        /// <param name="timeEntryViewModel">Model for the time entry to be updated</param>
        [HttpPut("{id:guid}")]
        [Authorize(Roles = EmployeeOrManager)]
        public async Task<TimeEntryDetail> Update(Guid id, [FromBody]TimeEntryViewModel timeEntryViewModel)
        {
            return await _timeEntryService.SaveTimeEntry(timeEntryViewModel.ToEntry(), id);
        }

        /// <summary>
        /// Sets the status of time entry to Submitted
        /// </summary>
        /// <param name="idCommentPairs">Collection of Ids and comments for the submitted time entry</param>
        [HttpPut]
        [Authorize(Roles = EmployeeOrManager)]
        public async Task Submit([FromBody]IdCommentPair[] idCommentPairs)
        {
            await ChangeStatus(TimeEntryStatus.Submitted, idCommentPairs);
        }

        /// <summary>
        /// Sets the status of time entry to Submitted
        /// </summary>
        /// <param name="idCommentPairs">Collection of Ids and comments for the unsubmitted time entry</param>
        [HttpPut]
        [Authorize(Roles = EmployeeOrManager)]
        public async Task Unsubmit([FromBody]IdCommentPair[] idCommentPairs) // TODO: allow only managers
        {
            await ChangeStatus(TimeEntryStatus.Saved, idCommentPairs);
        }

        /// <summary>
        /// Sets the status of time entry to Denied
        /// </summary>
        /// <param name="idCommentPairs">Collection of Ids and comments for the denied time entry</param>
        //// TODO: allow only managers
        [HttpPut]
        [Authorize(Roles = ManagerOnly)]
        public async Task Deny([FromBody] IdCommentPair[] idCommentPairs)
        {
            if (idCommentPairs.Any(x => x.Comment.IsNullOrEmpty()))
            {
                BadRequest("comment is required");
            }

            await ChangeStatus(TimeEntryStatus.Denied, idCommentPairs);
        }

        /// <summary>
        /// Sets the status of time entry to Approved
        /// </summary>
        /// <param name="idCommentPairs">Collection of Ids and comments for the approved time entry</param>
        [HttpPut]
        [Authorize(Roles = ManagerOnly)]
        public async Task Approve([FromBody] IdCommentPair[] idCommentPairs)
        {
            await ChangeStatus(TimeEntryStatus.Approved, idCommentPairs);
        }

        /// <summary>
        /// Permanently deletes the time entry from the database
        /// </summary>
        /// <param name="id">ID of the time entry</param>
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = EmployeeOrManager)]
        public async Task Delete(Guid id)
        {
            await _timeEntryService.DeleteTimeEntry(new Guid[] { id });
        }

        /// <summary>
        /// Returns a report file
        /// </summary>
        /// <param name="format">File format of the report. E.g. Excel, CSV etc.</param>
        /// <param name="searchCriteria">Criteria that defines the scope of the report data</param>
        /// <returns>File download</returns>
        [HttpGet]
        [Authorize(Roles = ManagerOnly)]
        public async Task<FileResult> DownloadReport([FromQuery] FileFormat format, [FromQuery]TimeEntrySearchCriteriaModel searchCriteria)
        {
            var report = await _timeEntryService.GetTabularReport(searchCriteria.ToSearch(), format);
            var fileFormatExt = GetFilenameAndFormat(format);

            return File(report, fileFormatExt.Item1, fileFormatExt.Item2);
        }

        /// <summary>
        /// Changes the status of the given set of time entries
        /// </summary>
        /// <param name="status">New status to change to</param>
        /// <param name="model">List of IdCommentPair</param>
        [HttpPut]
        [Authorize(Roles = EmployeeOrManager)]
        public async Task ChangeStatus([FromQuery]TimeEntryStatus status, [FromBody]IdCommentPair[] model)
        {
            await _timeEntryService.UpdateTimeEntryStatus(model, status);
        }

        /// <summary>
        /// Calls a Time Entry Reminder
        /// </summary>
        /// <param name="dateRange">Date range for the time entries to consider</param>
        [HttpPost]
        public async Task SendReminderEmail([FromQuery]DateTimeRange dateRange)
        {
            LogRequest();

            await _timeEntryService.SendReminderEmail(dateRange);
        }

        /// <summary>
        /// Sends reminder email to those who failed to submit the time entries for the previous week
        /// </summary>
        [HttpGet, AllowAnonymous]
        public async Task<string> SendReminderEmail()
        {
            LogRequest();

            const string successResponse = "success";
            const string failureResponse = "failure";

            var endTime = DateTime.UtcNow.Date.AddHours(7);

            // only send out reminders on Mondays
            if (endTime.DayOfWeek != DayOfWeek.Monday)
            {
                return failureResponse;
            }

            var dateRange = new DateTimeRange
            {
                BeginDateTime = endTime.AddDays(-7),
                EndDateTime = endTime
            };

            await _timeEntryService.SendReminderEmail(dateRange);
            return successResponse;
        }

        private Tuple<string, string> GetFilenameAndFormat(FileFormat fileFormat)
        {
            var format = string.Empty;
            var filenameWithExt = string.Empty;
            var filename = $"time_tracking_report-{DateTime.UtcNow.Ticks}";

            switch (fileFormat)
            {
                case FileFormat.Excel:
                    format = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    filenameWithExt = $"{filename}.xlsx";
                    break;

                case FileFormat.CSV:
                    filenameWithExt = $"{filename}.csv";
                    format = "text/csv";
                    break;

                default:
                    throw new OperationException("Invalid File Format", null);
            }

            return Tuple.Create(format, filenameWithExt);
        }

        private void LogRequest()
        {
            var nL = Environment.NewLine;
            var r = Request;
            var data = $"WEB REQUEST{nL}Path: {r.Path}{nL}Host: {r.Host.Value}{nL}Scheme: {r.Scheme}{nL}Query: {r.QueryString.Value}";

            _logger.Info(data);
        }
    }
}
