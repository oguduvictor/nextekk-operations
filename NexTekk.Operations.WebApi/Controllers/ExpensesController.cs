﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Services.Helpers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NexTekk.Operations.WebApi.Helpers;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing cashflows in the system
    /// </summary>
    [Route("api/[controller]")] 
    [EnableCors("myCorsPolicy")]
    [Authorize(Roles = EmployeeOrManager)]
    public class ExpensesController : Controller
    {
        private readonly IExpenseService _expenseService;
        private readonly IUserLookupService _userLookupService;

        /// <summary>
        /// ExpenseController constructor
        /// </summary>
        /// <param name="expenseService"></param>
        /// <param name="userLookupService"></param>
        public ExpensesController(
            IExpenseService expenseService,
            IUserLookupService userLookupService) 
        {
            _expenseService = expenseService;
            _userLookupService = userLookupService;
        }

        /// <summary>
        /// Gets all expense based on search criteria
        /// </summary>
        /// <param name="searchCriteriaModel"></param>
        /// <returns>List of all Expenses</returns>
        [HttpGet]
        public async Task<IEnumerable<ExpenseViewModel>> GetAll([FromQuery] ExpenseSearchCriteriaModel searchCriteriaModel)
        {
            var searchCriteria = Mapper.Map<ExpenseSearchCriteria>(searchCriteriaModel);

            var expenses = await _expenseService.GetAll(searchCriteria);

            return await _userLookupService.ToExpensesViewModel(expenses);
        }

        /// <summary>
        /// Create an expense
        /// </summary>
        /// <param name="saveExpenseModel">expense to be created</param>
        /// <returns>Expense</returns>
        [HttpPost]
        public async Task<ExpenseViewModel> Create([FromBody]SaveExpenseModel saveExpenseModel)
        {
            var expenseDto = Mapper.Map<SaveExpenseModel, Expense>(saveExpenseModel);

            var savedExpense = await _expenseService.SaveExpense(expenseDto);

            return await _userLookupService.ToExpenseViewModel(savedExpense);
        }

        /// <summary>
        /// Updates an expense
        /// </summary>
        /// <param name="id">ID of expense to be updated</param>
        /// <param name="saveExpense">expense to be updated</param>
        [HttpPut("{id:guid}")]
        public async Task Update(Guid id, [FromBody]SaveExpenseModel saveExpense)
        {
            var expenseDto = Mapper.Map<SaveExpenseModel, Expense>(saveExpense);
            expenseDto.Id = id;

            await _expenseService.SaveExpense(expenseDto);
        }

        /// <summary>
        /// Delete Expenses
        /// </summary>
        /// <param name="id">IDs of the expenses to be deleted</param>
        [HttpDelete("{id:guid}")]
        public async Task Delete(Guid id)
        {
            var savedExpense = await _expenseService.Get(id);

            await _expenseService.DeleteExpense(savedExpense);
        }
        
        /// <summary>
        /// Returns a report file
        /// </summary>
        /// <param name="format">File format of the report. E.g. Excel, CSV etc.</param>
        /// <param name="searchCriteria">Criteria that defines the scope of the report data</param>
        /// <returns>File download</returns>
        [Route("download")]
        [HttpGet]
        public async Task<FileResult> DownloadReport([FromQuery] FileFormat format, [FromQuery]ExpenseSearchCriteriaModel searchCriteria)
        {
            var criteria = Mapper.Map<ExpenseSearchCriteria>(searchCriteria);
            var results = await _expenseService.GetAll(criteria);
            
            var report = ReportGenerator.GenerateReport(results, format, ReportHeadingsDefinition.Expenses, "Expenses Report");
            var fileFormatExt = ReportGenerator.GetFileFormatAndName("expense_report", format);

            return File(report, fileFormatExt.MIMEType, fileFormatExt.Name);
        }

        private async Task<IEnumerable<UserEntity>> GetUsersToMail(Guid excludeManagerId) =>
            await _userLookupService.GetManagersAsync()
                .ContinueWith(task => task.Result.Where(user => user.Id != excludeManagerId));
    }
}
