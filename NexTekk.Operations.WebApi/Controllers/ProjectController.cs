﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.WebApi.Controllers
{
    /// <summary>
    /// Controller for managing projects
    /// </summary>
    [Route("api/[controller]/[action]")]
    [EnableCors("myCorsPolicy")]
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;

        /// <summary>
        /// Constructor for the project controller
        /// </summary>
        /// <param name="projectService">project management service</param>
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        /// <summary>
        /// Gets the list of project summaries that match the search criteria
        /// </summary>
        /// <param name="searchCriteria">Criteria for searching projects</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Project>> Search([FromQuery]ProjectSearchCriteria searchCriteria)
        {
            return await _projectService.GetProjects(searchCriteria);
        }

        /// <summary>
        /// Gets project details for the given ID
        /// </summary>
        /// <param name="id">project ID</param>
        /// <returns>project detail</returns>
        [HttpGet("{id:guid}")]
        public async Task<ProjectDetail> Get(Guid id)
        {
            return await _projectService.GetProject(id);
        }
        
        /// <summary>
        /// Creates a new project
        /// </summary>
        /// <param name="projectViewModel">Model for the project to be updated</param>
        [HttpPost]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Project> Create([FromBody]ProjectViewModel projectViewModel)
        {
            return await _projectService.SaveProject(projectViewModel.ToEntry());
        }

        /// <summary>
        /// Gets the list of team members for the given id
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<IEnumerable<TeamMember>> GetTeamMembers(Guid id)
        {
            return await _projectService.GetTeamMembers(id);
        }

        /// <summary>
        /// Updates an existing project
        /// </summary>
        /// <param name="id">ID of the project</param>
        /// <param name="projectViewModel">Model for the project to be updated</param>
        [HttpPut("{id:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task<Project> Update(Guid id, [FromBody]ProjectViewModel projectViewModel)
        {
            return await _projectService.SaveProject(projectViewModel.ToEntry(id));
        }

        /// <summary>
        /// Saves a new ProjectMember
        /// </summary>
        /// <param name="projectId">Model for the project</param>
        /// <param name="teamMembers">Model for the teams to be saved</param>
        [HttpPut("{projectId:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task SaveTeamMember(Guid projectId, [FromBody]IEnumerable<TeamMemberViewModel> teamMembers)
        {
            var entities = teamMembers.Select(member => member.ToEntity());

            await _projectService.SaveTeamMembers(entities);
        }
        
        /// <summary>
        /// Deletes the project from the database
        /// </summary>
        /// <param name="id">ID of the project</param>
        /// <returns>Http status code of 200</returns>
        [HttpDelete("{id:guid}")]
        [Authorize(Roles = ManagerOnly)]
        public async Task Delete(Guid id)
        {
            await _projectService.DeleteProject(id);
        }
    }
}
