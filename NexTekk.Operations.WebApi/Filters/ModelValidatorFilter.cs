﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace NexTekk.Operations.WebApi.Filters
{
    /// <summary>
    /// Model validators for all actions
    /// </summary>
    public class ModelValidatorFilter : IAsyncActionFilter
    {
        /// <summary>
        /// Validates model before action executes
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
            else
            {
                await next();
            }
        }
    }
}
