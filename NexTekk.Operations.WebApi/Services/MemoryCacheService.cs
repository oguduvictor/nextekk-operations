﻿using Microsoft.Extensions.Caching.Memory;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using System;

namespace NexTekk.Operations.Web.Services
{
    /// <summary>
    /// Application wide Memory Cache Service
    /// </summary>
    public class MemoryCacheService : ICacheService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly TimeSpan DefaultCacheDuration = TimeSpan.FromMinutes(15);

        /// <summary>
        /// Constructor for memory cache service
        /// </summary>
        /// <param name="memoryCache"></param>
        public MemoryCacheService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        
        /// <summary>
        /// Method for Persisting Cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="cacheDuration"></param>
        public void AddOrUpdate<T>(string key, T value, TimeSpan cacheDuration = default(TimeSpan))
        {
            var offset = cacheDuration == default(TimeSpan) ? DefaultCacheDuration : cacheDuration;

            var options = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTimeOffset.UtcNow + offset
            };

            _memoryCache.Set(key, value, options);
        }

        /// <summary>
        /// Method that removes item from cache
        /// </summary>
        /// <param name="key"></param>
        public void Delete(string key)
        {
            _memoryCache.Remove(key);
        }

        /// <summary>
        /// Method for getting item from cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultIfNotFound"></param>
        /// <returns></returns>
        public T Get<T>(string key, bool defaultIfNotFound = false)
        {
            T value = default(T);

            if (!_memoryCache.TryGetValue<T>(key, out value) && !defaultIfNotFound)
            {
                throw new ObjectNotFoundException($"Not entry found for key {key}");
            }

            return value;
        }
    }
}
