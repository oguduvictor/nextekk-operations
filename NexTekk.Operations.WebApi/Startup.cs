﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Providers;
using NexTekk.Operations.WebApi.App_Start;
using System;

namespace NexTekk.Operations.WebApi
{
    /// <summary>
    /// An instance of this class gets called at first run
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructor for the startup class
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Property that contains configurations from AppSettings.json
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //// Add Authentication configurations
            services.AddApplicationAuthentication(Configuration);

            //// Add framework services.
            services.AddFrameworkServices();

            //// Add Cors
            services.AddCorsSettings();

            //// Add Swagger Dependencies
            services.AddSwaggerConfigurations(Configuration);
            
            //// Add Application Dependencies
            services.ConfigureAppDependencies(Configuration);

            //// Registers AutoMapper profiles
            AutoMapperProfileConfig.Initialize();

            AppSettingsProvider.Register(new AppSettings(Configuration));
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="serviceProvider"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            //// Exception and logger related config to use
            app.UseExceptionConfigurations(env, serviceProvider);


            //// Enable Application Authentication
            app.UseApplicationAuthentication();

            app.UseMvc();

            app.UseCorsPolicy();

            //// Enable middleware to use swagger configurations
            app.UseSwaggerConfiguration();

            //// Server static files, and use wwwroot/index.html as default
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
