﻿using AutoMapper;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.WebApi.Models;

namespace NexTekk.Operations.WebApi.AutoMapperProfile
{
    /// <summary>
    /// Mapper Profile for WebApi Project
    /// </summary>
    public class WebApiMapperProfile : Profile
    {
        /// <summary>
        /// constructor for mapper profile
        /// </summary>
        public WebApiMapperProfile()
        {
            CreateMap<RevenueSearchCriteriaModel, RevenueSearchCriteria>()
                .ForMember(x => x.PageSize, y => y.Ignore())
                .ForMember(x => x.NumberToSkip, y => y.Ignore());

            CreateMap<SaveRevenueModel, Revenue>();

            CreateMap<Revenue, RevenueViewModel>();

            CreateMap<ExpenseSearchCriteriaModel, ExpenseSearchCriteria>()
                .ForMember(x => x.PageSize, y => y.Ignore())
                .ForMember(x => x.NumberToSkip, y => y.Ignore());

            CreateMap<SaveExpenseModel, Expense>().ReverseMap();

            CreateMap<AssetSearchCriteriaModel, AssetSearchCriteria>();

            CreateMap<SaveAssetModel, Asset>();

            CreateMap<Asset, AssetViewModel>();

            CreateMap<AssetSummary, AssetSummaryViewModel>();

            CreateMap<SaveExpenseModel, Expense>();

            CreateMap<Expense, ExpenseViewModel>();
        }
    }
}
