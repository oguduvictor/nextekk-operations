﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace NexTekk.Operations.WebApi
{
    /// <summary>
    /// Main Class of web api
    /// </summary>
    public class Program
    {
        /// <summary>
        /// static main method to run program
        /// </summary>
        //public static void Main(string[] args)
        //{
        //    var host = new WebHostBuilder()
        //        .UseKestrel()
        //        .UseContentRoot(Directory.GetCurrentDirectory())
        //        .UseIISIntegration()
        //        .UseStartup<Startup>()
        //        .CaptureStartupErrors(true)
        //        .UseSetting("detailedErrors", "true")
        //        .Build();

        //    host.Run();
        //}
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
