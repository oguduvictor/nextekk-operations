﻿using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Data.Sql.Entities
{
    internal class Asset
    {
        public Guid Id { get; set; }

        public string Item { get; set; }

        public string SerialNo { get; set; }

        public Condition Condition { get; set; }

        public Category Category { get; set; }

        public Currency Currency { get; set; }

        public string Description { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string ServiceTag { get; set; }

        public string ServiceCode { get; set; }

        public decimal Cost { get; set; }

        public DateTime PurchasedDate { get; set; }

        public DateTime? AssignedDate { get; set; }

        public DateTime Modified { get; set; }

        public DateTime Created { get; set; }

        public DateTime? MarkedBadOn { get; set; }

        public DateTime? LastAssignedDate { get; set; }

        public Guid? LastAssigneeId { get; set; }

        public virtual UserEntity LastAssignee { get; set; }

        public Guid PurchasedById { get; set; }

        public virtual UserEntity PurchasedBy { get; set; }

        public Guid? AssigneeId { get; set; }

        public virtual UserEntity Assignee { get; set; }

        public Guid? MarkedBadById { get; set; }

        public virtual UserEntity MarkedBadBy { get; set; }

        public Guid CreatedById { get; set; }

        public virtual UserEntity CreatedBy { get; set; }

        public Guid ModifiedById { get; set; }

        public virtual UserEntity ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
