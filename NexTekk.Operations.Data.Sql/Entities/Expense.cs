﻿using NexTekk.Operations.Core.Models.Enums;
using System;
using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Data.Sql.Entities
{
    internal class Expense
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public Guid IncurredById { get; set; }

        public virtual UserEntity IncurredBy { get; set; }  

        public Currency Currency { get; set; }

        public decimal Amount { get; set; }

        public string Description { get; set; }

        public string Reason { get; set; }

        public DateTime? Reimbursed { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatedById { get; set; }

        public virtual UserEntity CreatedBy { get; set; } 

        public DateTime? Modified { get; set; }

        public Guid ModifiedById { get; set; }

        public virtual UserEntity ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
