﻿using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Data.Sql.Entities
{
    internal class Revenue
    {
        public Guid Id { get; set; }

        public DateTime PaymentDate { get; set; }

        public Currency Currency { get; set; }

        public decimal Amount { get; set; }

        public string Description { get; set; }

        public Guid ClientId { get; set; }

        public virtual ClientEntity Client { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatedById { get; set; }

        public virtual UserEntity CreatedBy { get; set; }

        public DateTime Modified { get; set; }

        public Guid ModifiedById { get; set; }
        
        public virtual UserEntity ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
