﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Data.Sql.DbContexts
{
    public class TimeTrackingDbContext : DbContext
    {
        public TimeTrackingDbContext(DbContextOptions<TimeTrackingDbContext> options) 
            : base(options)
        {
        }

        public DbSet<TimeEntryEntity> TimeEntries { get; set; }

        public DbSet<ActivityEntity> Activities { get; set; }

        public DbSet<TeamMemberEntity> TeamMembers { get; set; }

        public DbSet<UserEntity> Users { get; set; }

        public DbSet<RoleEntity> Roles { get; set; }

        public DbSet<UserRoleEntity> UserRoles { get; set; }

        public DbSet<VacationEntity> Vacations { get; set; }

        public DbSet<VacationSetting> VacationSettings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().ToTable("vw_Users");
            modelBuilder.Entity<RoleEntity>().ToTable("vw_Roles");

            var userRoles = modelBuilder.Entity<UserRoleEntity>();
            userRoles.ToTable("vw_UserRoles");
            userRoles.HasKey(x => new { x.RoleId, x.UserId });

            var timeEntryEntity = modelBuilder.Entity<TimeEntryEntity>();
            timeEntryEntity.ToTable("TimeEntries");
            timeEntryEntity.HasOne(t => t.Creator).WithMany().HasForeignKey(t => t.CreatorId).IsRequired();
            timeEntryEntity.HasOne(t => t.Editor).WithMany().HasForeignKey(t => t.EditorId).IsRequired();
            timeEntryEntity.HasOne(t => t.Approver).WithMany().HasForeignKey(t => t.ApproverId).IsRequired(false);
            timeEntryEntity.HasOne(t => t.Denier).WithMany().HasForeignKey(t => t.DenierId).IsRequired(false);

            var activityEntity = modelBuilder.Entity<ActivityEntity>();
            activityEntity.ToTable("Activities");
            activityEntity.HasOne(t => t.Creator).WithMany().HasForeignKey(t => t.CreatorId).IsRequired();
            activityEntity.HasOne(t => t.Editor).WithMany().HasForeignKey(t => t.EditorId).IsRequired();
            
            var projectEntity = modelBuilder.Entity<ProjectSummary>();
            projectEntity.ToTable("Projects");
            projectEntity.HasMany(x => x.TeamMembers).WithOne().HasForeignKey(x => x.ProjectId);

            var teamMemberEntity = modelBuilder.Entity<TeamMemberEntity>();
            teamMemberEntity.HasKey(x => new { x.ProjectId, x.UserId });
            teamMemberEntity.ToTable("TeamMembers").Property(t => t.Role).HasColumnName("RoleId");

            var vacationEntity = modelBuilder.Entity<VacationEntity>();
            vacationEntity.ToTable("vw_Vacations");
            vacationEntity.HasKey(x => x.UserId);
            vacationEntity.HasOne(x => x.User).WithOne().HasForeignKey<UserEntity>(x => x.Id);

            var vacationSettings = modelBuilder.Entity<VacationSetting>();
            vacationSettings.ToTable("VacationSettings");
            vacationSettings.HasKey(x => x.UserId);
        }
    }
}
