﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.Entities;

namespace NexTekk.Operations.Data.Sql.DbContexts
{
    public class CashFlowDbContext : DbContext
    {
        public CashFlowDbContext(DbContextOptions<CashFlowDbContext> options)
            : base(options)
        {
        }

        internal DbSet<Asset> Assets { get; set; }

        internal DbSet<Expense> Expenses { get; set; }

        internal DbSet<UserEntity> Users { get; set; }

        internal DbSet<ClientEntity> Clients { get; set; }

        internal DbSet<Revenue> Revenues { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().ToTable("vw_Users");

            var expenseEntity = modelBuilder.Entity<Expense>();
            expenseEntity.ToTable("Expenses");
            expenseEntity.HasKey(x => x.Id);
            expenseEntity.HasOne(x => x.IncurredBy).WithMany().HasForeignKey(x => x.IncurredById).IsRequired();
            expenseEntity.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
            expenseEntity.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById).IsRequired();


            var revenueEntity = modelBuilder.Entity<Revenue>();
            revenueEntity.ToTable("Revenues");
            revenueEntity.HasKey(x => x.Id);
            revenueEntity.HasOne(x => x.Client).WithMany().HasForeignKey(x => x.ClientId);
            revenueEntity.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
            revenueEntity.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById).IsRequired();

            var clientEntity = modelBuilder.Entity<ClientEntity>();
            clientEntity.ToTable("Clients");
            clientEntity.HasOne(x => x.Creator).WithMany().HasForeignKey(x => x.CreatorId).IsRequired();
            clientEntity.HasOne(x => x.Editor).WithMany().HasForeignKey(x => x.EditorId).IsRequired();

            var clientContactEntity = modelBuilder.Entity<ClientsContactsEntity>();
            clientContactEntity.HasKey(x => new { x.ContactId, x.ClientId });
            clientContactEntity.ToTable("ClientContacts").Property(x => x.Created).HasColumnName("Created");
            clientContactEntity.HasOne(x => x.Client).WithMany(x => x.ClientsContacts).HasForeignKey(x => x.ClientId);
            clientContactEntity.HasOne(x => x.Contact).WithMany().HasForeignKey(x => x.ContactId);

            var teamMemberEntity = modelBuilder.Entity<TeamMemberEntity>();
            teamMemberEntity.HasKey(x => new { x.ProjectId, x.UserId });
            teamMemberEntity.ToTable("TeamMembers").Property(x => x.Role).HasColumnName("RoleId");
            teamMemberEntity.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId);

            var assetEntity = modelBuilder.Entity<Asset>();
            assetEntity.ToTable("Assets");
            assetEntity.HasKey(x => x.Id);
            assetEntity.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
            assetEntity.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById).IsRequired();
            assetEntity.HasOne(x => x.PurchasedBy).WithMany().HasForeignKey(x => x.PurchasedById).IsRequired();
            assetEntity.HasOne(x => x.Assignee).WithMany().HasForeignKey(x => x.AssigneeId);
            assetEntity.HasOne(x => x.LastAssignee).WithMany().HasForeignKey(x => x.LastAssigneeId);
            assetEntity.HasOne(x => x.MarkedBadBy).WithMany().HasForeignKey(x => x.MarkedBadById);
        }
    }
}
