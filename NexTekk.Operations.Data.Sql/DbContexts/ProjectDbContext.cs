﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Data.Sql.DbContexts
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) 
            : base(options)
        {
        }

        public DbSet<ProjectEntity> Projects { get; set; }

        public DbSet<ClientEntity> Clients { get; set; }

        public DbSet<ContactEntity> Contacts { get; set; }

        public DbSet<TeamMemberEntity> TeamMembers { get; set; }

        public DbSet<ClientsContactsEntity> ClientsContacts { get; set; }

        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().ToTable("vw_Users");

            var projectEntity = modelBuilder.Entity<ProjectEntity>();
            projectEntity.HasOne(x => x.Creator).WithMany().HasForeignKey(x => x.CreatorId).IsRequired();
            projectEntity.HasOne(x => x.Editor).WithMany().HasForeignKey(x => x.EditorId).IsRequired();
            projectEntity.HasOne(x => x.Client).WithMany(x => x.Projects).HasForeignKey(z => z.ClientId);
            projectEntity.HasMany(x => x.TeamMembers).WithOne().HasForeignKey(x => x.ProjectId);

            var clientEntity = modelBuilder.Entity<ClientEntity>();
            clientEntity.ToTable("Clients");
            clientEntity.HasOne(x => x.Creator).WithMany().HasForeignKey(x => x.CreatorId).IsRequired();
            clientEntity.HasOne(x => x.Editor).WithMany().HasForeignKey(x => x.EditorId).IsRequired();

            var contactEntity = modelBuilder.Entity<ContactEntity>();
            contactEntity.ToTable("Contacts");
            contactEntity.HasOne(x => x.Creator).WithMany().HasForeignKey(x => x.CreatorId).IsRequired();
            contactEntity.HasOne(x => x.Editor).WithMany().HasForeignKey(x => x.EditorId).IsRequired();

            var clientContactEntity = modelBuilder.Entity<ClientsContactsEntity>();
            clientContactEntity.HasKey(x => new { x.ContactId, x.ClientId });
            clientContactEntity.ToTable("ClientContacts").Property(x => x.Created).HasColumnName("Created");
            clientContactEntity.HasOne(x => x.Client).WithMany(x => x.ClientsContacts).HasForeignKey(x => x.ClientId);
            clientContactEntity.HasOne(x => x.Contact).WithMany().HasForeignKey(x => x.ContactId);

            var teamMemberEntity = modelBuilder.Entity<TeamMemberEntity>();
            teamMemberEntity.HasKey(x => new { x.ProjectId, x.UserId });
            teamMemberEntity.ToTable("TeamMembers").Property(x => x.Role).HasColumnName("RoleId");
            teamMemberEntity.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId);
        }
    }
}
