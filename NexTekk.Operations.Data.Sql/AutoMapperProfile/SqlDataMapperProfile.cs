﻿using AutoMapper;
using NexTekk.Operations.Data.Sql.Entities;
using Models = NexTekk.Operations.Core.Models.CashFlow;

namespace NexTekk.Operations.Data.Sql.AutoMapperProfile
{
    public class SqlDataMapperProfile : Profile
    {
        public SqlDataMapperProfile()
        {
            CreateMap<Models.Revenue, Revenue>()
                .ForMember(x => x.Client, y => y.Ignore())
                .ForMember(x => x.CreatedBy, y => y.Ignore())
                .ForMember(x => x.ModifiedBy, y => y.Ignore())
                .ReverseMap();

            CreateMap<Models.AssetSummary, Asset>()
                .ForMember(x => x.Assignee, y => y.Ignore())
                .ForMember(x => x.CreatedBy, y => y.Ignore())
                .ForMember(x => x.ModifiedBy, y => y.Ignore())
                .ForMember(x => x.LastAssignee, y => y.Ignore())
                .ForMember(x => x.MarkedBadBy, y => y.Ignore())
                .ForMember(x => x.PurchasedBy, y => y.Ignore());
            
            CreateMap<Models.Asset, Asset>()
                .ForMember(x => x.Assignee, y => y.Ignore())
                .ForMember(x => x.CreatedBy, y => y.Ignore())
                .ForMember(x => x.ModifiedBy, y => y.Ignore())
                .ForMember(x => x.LastAssignee, y => y.Ignore())
                .ForMember(x => x.MarkedBadBy, y => y.Ignore())
                .ForMember(x => x.PurchasedBy, y => y.Ignore())
                .ReverseMap();
            CreateMap<Models.Expense, Expense>()
                .ForMember(x => x.IncurredBy, y => y.Ignore())
                .ForMember(x => x.CreatedBy, y => y.Ignore())
                .ForMember(x => x.ModifiedBy, y => y.Ignore())
                .ReverseMap();
        }
    }
}
