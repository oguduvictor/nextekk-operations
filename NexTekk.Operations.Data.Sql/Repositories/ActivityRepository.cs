﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class ActivityRepository : IActivityRepository
    {
        private readonly TimeTrackingDbContext _dbContext;

        public ActivityRepository(TimeTrackingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(ActivityEntity activity)
        {
            _dbContext.Activities.Add(activity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<ActivityEntity> Get(Guid id)
        {
            return await _dbContext
                            .Activities
                            .Include(x => x.Creator)
                            .Include(x => x.Editor)
                            .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<ActivityEntity>> GetAll(bool includeDeleted = false)
        {
            return await _dbContext
                            .Activities
                            .Where(x => x.Deleted == includeDeleted)
                            .ToListAsync();
        }

        public async Task Update(ActivityEntity activity)
        {
            if (_dbContext.Entry(activity).State == EntityState.Detached)
            {
                _dbContext.Activities.Attach(activity);
            }

            _dbContext.Entry(activity).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var activity = await _dbContext.Activities.FirstOrDefaultAsync(x => x.Id == id);

            if (activity == null)
            {
                throw new InvalidOperationException($"No activity exists that has the given Id of ${id}");
            }

            activity.Deleted = true;

            await Update(activity);
        }
    }
}
