﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories.CashFlow
{
    public class AssetRepository : IAssetRepository
    {
        private readonly CashFlowDbContext _dbContext;

        public AssetRepository(CashFlowDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Asset> Create(Asset asset)
        {
            var entity = Mapper.Map<Entities.Asset>(asset);

            _dbContext.Assets.Add(entity);

            await _dbContext.SaveChangesAsync();

            return Mapper.Map<Asset>(entity);
        }

        public async Task<Asset> Get(Guid id)
        {
            return await _dbContext.Assets
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id)
                .ContinueWith(task => Mapper.Map<Asset>(task.Result));
        }

        public async Task<IList<Asset>> GetAssetsAsync(AssetSearchCriteria searchCriteria)
        {
            return await FilterQuery(searchCriteria)
                .Skip(searchCriteria.NumberToSkip.Value)
                .Take(searchCriteria.PageSize.Value)
                .OrderByDescending(x => x.PurchasedDate)
                .ToListAsync()
                .ContinueWith(task => Mapper.Map<IList<Asset>>(task.Result));
        }

        public async Task<IList<AssetSummary>> GetAssetSummariesAsync(AssetSearchCriteria searchCriteria)
        {
            return await FilterQuery(searchCriteria)
                .Select(x => new AssetSummary
                {
                    Id = x.Id,
                    Item = x.Item,
                    Condition = x.Condition,
                    SerialNo = x.SerialNo,
                    PurchasedById = x.PurchasedById,
                    PurchasedDate = x.PurchasedDate,
                    AssigneeId = x.AssigneeId,
                    AssignedDate = x.AssignedDate,
                    Cost = x.Cost,
                    Currency = x.Currency,
                    Category = x.Category
                })
                .Skip(searchCriteria.NumberToSkip.Value)
                .Take(searchCriteria.PageSize.Value)
                .OrderByDescending(x => x.PurchasedDate)
                .ToListAsync();
        }

        public async Task Update(Asset assets)
        {
            var entity = Mapper.Map<Entities.Asset>(assets);

            if(_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbContext.Assets.Attach(entity);
            }

            _dbContext.Assets.Update(entity);

            await _dbContext.SaveChangesAsync();
        }

        private IQueryable<Entities.Asset> FilterQuery(AssetSearchCriteria searchCriteria)
        {
            var query = _dbContext.Assets.Where(x => !x.Deleted);

            if (!searchCriteria.AssigneeIds.IsNullOrEmpty())
            {
                query = query.Where(x => x.AssigneeId.HasValue &&
                                   searchCriteria.AssigneeIds.Contains(x.AssigneeId.Value));
            }

            if (!searchCriteria.PurchasedByIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.PurchasedByIds.Contains(x.PurchasedById));
            }

            if (!searchCriteria.SerialNo.IsNullOrEmpty())
            {
                query = query.Where(x => x.SerialNo == searchCriteria.SerialNo);
            }

            if (!searchCriteria.AssignedDate.IsNull())
            {
                query = query.Where(x => x.AssignedDate.HasValue
                                        && x.AssignedDate >= searchCriteria.AssignedDate.BeginDateTime
                                        && x.AssignedDate <= searchCriteria.AssignedDate.EndDateTime);
            }

            if (!searchCriteria.PurchasedDate.IsNull())
            {
                query = query.Where(x => x.PurchasedDate >= searchCriteria.PurchasedDate.BeginDateTime
                    && x.PurchasedDate <= searchCriteria.PurchasedDate.EndDateTime);
            }

            if (searchCriteria.Condition.HasValue)
            {
                query = query.Where(x => x.Condition == searchCriteria.Condition);
            }

            if (!searchCriteria.Item.IsNullOrEmpty())
            {
                query = query.Where(x => x.Item.ToLower() == searchCriteria.Item.ToLower());
            }

            return query;
        }
    }
}
