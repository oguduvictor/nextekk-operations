﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories.CashFlow
{
    public class ExpenseRepository : IExpenseRepository
    {
        private readonly CashFlowDbContext _dbContext;

        public ExpenseRepository(CashFlowDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Expense> Get(Guid id)
        {
            var expense = await _dbContext.Expenses.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            return Mapper.Map<Expense>(expense);
        }

        public async Task<Expense> Create(Expense expense)
        {
            var entity = Mapper.Map<Entities.Expense>(expense);

            _dbContext.Expenses.Add(entity);

            await _dbContext.SaveChangesAsync();

            return Mapper.Map<Expense>(entity);
        }

        public async Task Update(Expense expense)
        {
            var entity = Mapper.Map<Entities.Expense>(expense);

            if (_dbContext.Entry(entity).State  == EntityState.Detached)
            {
                _dbContext.Expenses.Attach(entity);
            }

            _dbContext.Expenses.Update(entity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Expense>> Update(IEnumerable<Expense> entities)
        {
            var expenseEntities = Mapper.Map<IEnumerable<Entities.Expense>>(entities);

            _dbContext.Expenses.UpdateRange(expenseEntities);

            await _dbContext.SaveChangesAsync();

            return Mapper.Map<IEnumerable<Expense>>(expenseEntities);
        }
         
        public async Task<IEnumerable<Expense>> Search(ExpenseSearchCriteria searchCriteria) 
        {
            var query = _dbContext.Expenses.Where(x => !x.Deleted);

            if (searchCriteria.MinAmount.HasValue && searchCriteria.MaxAmount.HasValue)
            {
                query = query.Where(x => x.Amount >= searchCriteria.MinAmount);
            }

            if (searchCriteria.UserId.HasValue)
            {
                query = query.Where(x => x.CreatedById == searchCriteria.UserId || x.IncurredById == searchCriteria.UserId);
            }
            
            if(searchCriteria.Currency.HasValue)
            {
                query = query.Where(x => x.Currency == searchCriteria.Currency.Value);
            }

            if (searchCriteria.IncurredById.HasValue)
            {
                query = query.Where(x => x.IncurredById == searchCriteria.IncurredById.Value);
            }

            if (searchCriteria.Reimbursed.HasValue)
            {
                query = query.Where(x => x.Reimbursed.HasValue == searchCriteria.Reimbursed);
            }

            if (!searchCriteria.DescriptionContains.IsNullOrEmpty())
            {
                query = query.Where(x => x.Description.ToLower().Contains(searchCriteria.DescriptionContains.ToLower().Trim()));
            }

            if (!searchCriteria.IncurredDateRange.IsNull())
            {
                query = query.Where(x => x.Date >= searchCriteria.IncurredDateRange.BeginDateTime
                                        && x.Date <= searchCriteria.IncurredDateRange.EndDateTime);
            }
            
            if (searchCriteria.NumberToSkip.HasValue)
            {
                query = query.Skip(searchCriteria.NumberToSkip.Value);
            }

            if (searchCriteria.PageSize.HasValue)
            {
                query = query.Take(searchCriteria.PageSize.Value);
            }

            var searchedEntities = await query.OrderByDescending(x => x.Date).ToListAsync();

            return Mapper.Map<IEnumerable<Expense>>(searchedEntities);
        }
    }
}
