﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories.CashFlow
{
    public class RevenueRepository : IRevenueRepository
    {
        private readonly CashFlowDbContext _dbContext;

        public RevenueRepository(CashFlowDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IList<Revenue>> GetAll(RevenueSearchCriteria searchCriteria)
        {
            var query = _dbContext.Revenues.Where(x => !x.Deleted);

            if (!searchCriteria.PaymentDate.IsNull())
            {
                query = query.Where(x => x.PaymentDate >= searchCriteria.PaymentDate.BeginDateTime
                                    && x.PaymentDate <= searchCriteria.PaymentDate.EndDateTime);
            }
            
            if (searchCriteria.EnteredBy.HasValue)
            {
                query = query.Where(x => x.CreatedById == searchCriteria.EnteredBy);
            }
            
            if (searchCriteria.Currency.HasValue)
            {
                query = query.Where(x => x.Currency == searchCriteria.Currency.Value);
            }

            if (searchCriteria.MinAmount.HasValue || searchCriteria.MaxAmount.HasValue)
            {
                query = query.Where(x => x.Amount >= searchCriteria.MinAmount
                                        && x.Amount <= searchCriteria.MaxAmount);
            }

            if (!searchCriteria.ClientIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.ClientIds.Contains(x.ClientId));
            }

            if (!searchCriteria.Description.IsNullOrEmpty())
            {
                query = query.Where(x => x.Description.ToLower().Contains(searchCriteria.Description.ToLower().Trim()));
            }

            if (searchCriteria.NumberToSkip.HasValue)
            {
                query = query.Skip(searchCriteria.NumberToSkip.Value);
            }

            if (searchCriteria.PageSize.HasValue)
            {
                query = query.Take(searchCriteria.PageSize.Value);
            }
            
            var result = await query.OrderByDescending(x => x.PaymentDate).ToListAsync();

            return Mapper.Map<IList<Entities.Revenue>, IList<Revenue>>(result);
        }

        public async Task<Revenue> Get(Guid id)
        {
            var result = await _dbContext.Revenues.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            return Mapper.Map<Entities.Revenue, Revenue>(result);
        }

        public async Task<Revenue> Create(Revenue revenue)
        {
            var entity = Mapper.Map<Entities.Revenue>(revenue);

            _dbContext.Revenues.Add(entity);

            await _dbContext.SaveChangesAsync();

            return Mapper.Map<Entities.Revenue, Revenue>(entity);
        }

        public Task Update(Revenue revenue)
        {
            var entity = Mapper.Map<Entities.Revenue>(revenue);
            
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbContext.Revenues.Attach(entity);
            }

            _dbContext.Revenues.Update(entity);

            return _dbContext.SaveChangesAsync();
        }

        public Task Update(IEnumerable<Revenue> revenues)
        {
            var entities = Mapper.Map<IEnumerable<Entities.Revenue>>(revenues);

            _dbContext.Revenues.UpdateRange(entities);

            return _dbContext.SaveChangesAsync();
        }
    }
}
