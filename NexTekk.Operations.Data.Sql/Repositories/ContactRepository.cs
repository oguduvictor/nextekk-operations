﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class ContactRepository : IContactRepository
    {
        private readonly ProjectDbContext _dbContext;

        public ContactRepository(ProjectDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(ContactEntity contact)
        {
            _dbContext.Contacts.Add(contact);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<ContactEntity> Get(int id)
        {
            return await _dbContext.Contacts
                .Include(x => x.Creator)
                .Include(x => x.Editor)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<ContactEntity>> GetContactSummaries(bool includeDeleted = false)
        {
            return await _dbContext.Contacts
                .Where(x => x.Deleted == includeDeleted)
                .Select(x => new ContactEntity { Id = x.Id, FirstName = x.FirstName, LastName = x.LastName })
                .ToListAsync();
        }

        public async Task<IEnumerable<ContactEntity>> GetAll(bool includeDeleted = false)
        {
            return await _dbContext.Contacts.Where(x => x.Deleted == includeDeleted).ToListAsync();
        }

        public async Task Update(ContactEntity contact)
        {
            if (_dbContext.Entry(contact).State == EntityState.Detached)
            {
                _dbContext.Contacts.Attach(contact);
            }

            _dbContext.Entry(contact).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var contact = await Get(id);

            if (contact == null)
            {
                throw new InvalidOperationException($"No contact exists that has the given Id of ${id}");
            }

            contact.Deleted = true;

            await Update(contact);
        }
    }
}
