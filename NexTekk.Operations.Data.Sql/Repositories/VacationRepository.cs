﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class VacationRepository : IVacationRepository
    {
        private readonly TimeTrackingDbContext _dbContext;

        public VacationRepository(TimeTrackingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<VacationEntity>> GetAll(int year)
        {
            return await _dbContext.Vacations
                .Where(x => x.Year == year)
                .Include(x => x.User)
                .AsNoTracking().ToListAsync();
        }

        public async Task<VacationEntity> Get(Guid userId, int year)
        {
            return await _dbContext.Vacations
                .Where(x => x.UserId == userId && x.Year == year)
                .Include(x => x.User)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        public async Task<VacationSetting> GetSetting(Guid userId)
        {
            return await _dbContext.VacationSettings.FindAsync(userId);
        }

        public async Task Create(VacationSetting entity)
        {
            _dbContext.VacationSettings.Add(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(VacationSetting entity)
        {
            var localEntity = _dbContext.VacationSettings.Local.FirstOrDefault(x => x.UserId == entity.UserId);

            if (localEntity.IsNull())
            {
                _dbContext.VacationSettings.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
