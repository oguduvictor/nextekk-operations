﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Core.Helpers.RoleConstants;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly TimeTrackingDbContext _dbContext;
        private const string MANAGER_ROLE = "Manager";

        public UserRepository(TimeTrackingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<UserEntity> Get(Guid id)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<UserEntity>> GetAll(IEnumerable<Guid> ids  = null, bool employeesAndManagersOnly = false)
        {
            if (employeesAndManagersOnly)
            {
                return await _dbContext.UserRoles
                    .Where(x => x.Role.Name == MANAGER_ROLE || x.Role.Name == EmployeeOnly)
                    .Select(x => x.User).Distinct().ToListAsync();
            }

            if (ids.IsNullOrEmpty())
            {
                return await _dbContext.Users.ToListAsync();
            }

            return await _dbContext.Users.Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<UserEntity>> GetAllNotInIds(IEnumerable<Guid> ids)
        {
            return await _dbContext.Users.Where(x => !ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<UserEntity>> GetAllManagers()
        {
            return await _dbContext.UserRoles
                .Where(x => x.Role.Name == MANAGER_ROLE)
                .Select(x => x.User).ToListAsync();
        }
    }
}
