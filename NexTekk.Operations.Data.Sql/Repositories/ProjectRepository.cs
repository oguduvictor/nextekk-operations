﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ProjectDbContext _dbContext;

        public ProjectRepository(ProjectDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<IEnumerable<ProjectEntity>> GetProjects(ProjectSearchCriteria searchCriteria)
        {
            var query = _dbContext.Projects.Where(x => x.Deleted == false);

            if (searchCriteria.CreatorId.HasValue)
            {
                query = query.Where(x => x.CreatorId == searchCriteria.CreatorId);
            }

            if (searchCriteria.ClientId.HasValue)
            {
                query = query.Where(x => x.ClientId == searchCriteria.ClientId);
            }
            
            if (searchCriteria.Status.HasValue)
            {
                query = query.Where(x => x.Status == searchCriteria.Status);
            }

            if (searchCriteria.ProjectManagerId.HasValue)
            {
                query = query.Where(x => x.TeamMembers
                .Any(y => y.UserId == searchCriteria.ProjectManagerId && y.Role == TeamMemberRole.ProjectManager));
            }

            return await query
                .Include(x => x.Client)
                .Include(x => x.TeamMembers)
                .ThenInclude(x => x.User)
                .OrderBy(x => x.Created)
                .ToListAsync();
        }

        public async Task<ProjectEntity> GetProject(Guid id)
        {
            return await _dbContext.Projects
                .Include(x => x.Client)
                .Include(x => x.TeamMembers)
                .ThenInclude(x => x.User)
                .Include(x => x.Creator)
                .Include(x => x.Editor)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Create(IEnumerable<ProjectEntity> projects)
        {
            _dbContext.Projects.AddRange(projects);

            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(IEnumerable<ProjectEntity> projects)
        {
            foreach (var project in projects)
            {
                if (_dbContext.Entry(project).State == EntityState.Detached)
                {
                    _dbContext.Projects.Attach(project);
                }

                _dbContext.Entry(project).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<TeamMemberEntity>> GetTeamMembers(Guid projectId)
        {
            return await _dbContext.TeamMembers.Where(x => x.ProjectId == projectId).Include(x => x.User).ToListAsync();
        }

        public async Task UpdateTeamMembers(IEnumerable<TeamMemberEntity> teamMembers)
        {
            var hasChanges = false;
            var projectId = teamMembers.FirstOrDefault()?.ProjectId;
            var dbTeamMembers = await _dbContext.TeamMembers.Where(x => x.ProjectId == projectId).ToListAsync();
            
            teamMembers.ForEach(x =>
            {
                var teamMember = dbTeamMembers.FirstOrDefault(y => y.ProjectId == x.ProjectId && y.UserId == x.UserId);
                
                if (teamMember.IsNull())
                {
                    hasChanges = true;
                    _dbContext.TeamMembers.Add(x); 
                }
                else if (teamMember.Role != x.Role)
                {
                    hasChanges = true;
                    teamMember.Role = x.Role; 
                }
            });

            var deletedMembers = dbTeamMembers.Where(p => 
                !teamMembers.Any(l => l.ProjectId == p.ProjectId && l.UserId == p.UserId));
            
            if (!deletedMembers.IsNullOrEmpty())
            {
                hasChanges = true;
                _dbContext.TeamMembers.RemoveRange(deletedMembers);
            }

            if (hasChanges)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task Delete(IEnumerable<Guid> ids)
        {
            var projectEntities = await _dbContext.Projects
                                    .Where(x => ids.Contains(x.Id))
                                    .ToListAsync();

            projectEntities.ForEach(x => x.Deleted = true);

            await Update(projectEntities);
        }

        public async Task<IEnumerable<ProjectEntity>> GetProjectSummaries(Guid? userId)
        {
            if (!userId.HasValue)
            {
                return await _dbContext.Projects
                    .Where(x => x.Deleted == false)
                    .Select(t => new ProjectEntity { Id = t.Id, Title = t.Title, Billable = t.Billable })
                    .ToListAsync();
            }

            return await _dbContext.Projects
                .Where(x => x.TeamMembers.Any(y => y.UserId == userId) && x.Deleted == false)
                .Select(t => new ProjectEntity { Id = t.Id, Title = t.Title, Billable = t.Billable, Status = t.Status })
                .ToListAsync();
        }

        public async Task<IEnumerable<ProjectEntity>> GetProjectSummariesById(params Guid[] ids)
        {
            return await _dbContext.Projects
                .Where(x => ids.Contains(x.Id))
                .Select(t => new ProjectEntity { Id = t.Id, Title = t.Title, Billable = t.Billable, Status = t.Status })
                .ToListAsync();
        }
    }
}
