﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly ProjectDbContext _dbContext;

        public ClientRepository(ProjectDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(ClientEntity client)
        {
            _dbContext.Clients.Add(client);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<ClientEntity> Get(Guid id)
        {
            return await _dbContext.Clients
                .Include(x => x.Creator)
                .Include(x => x.Editor)
                .Include(x => x.ClientsContacts)
                .ThenInclude(x => x.Contact)
                .Include(x => x.Projects)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<ClientEntity> GetClientEntityAsync(Guid id)
        {
            return await _dbContext.Clients.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<ClientEntity>> GetAll(bool includeDeleted = false)
        {
            return await _dbContext.Clients
                .Include(x => x.Creator)
                .Include(x => x.ClientsContacts)
                .ThenInclude(x => x.Contact)
                .Where(x => x.Deleted == includeDeleted)
                .OrderBy(x => x.Created)
                .ToListAsync();
        }
        
        public async Task<IEnumerable<ClientEntity>> GetClientSummaries(bool includeDeleted = false)
        {
            return await _dbContext.Clients
                .Where(x => x.Deleted == includeDeleted)
                .ToListAsync();
        }

        public async Task Update(ClientEntity client)
        {
            if (_dbContext.Entry(client).State == EntityState.Detached)
            {
                _dbContext.Clients.Attach(client);
            }

            _dbContext.Entry(client).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task SaveContacts(IEnumerable<ClientsContactsEntity> clientContacts)
        {
            var hasChanges = false;
            var clientId = clientContacts.FirstOrDefault()?.ClientId;
            var dbClientContacts = await _dbContext.ClientsContacts.Where(x => x.ClientId == clientId).ToListAsync();

            clientContacts.ForEach(x =>
            {
                if (x.ContactId == default(int))
                {
                    return;
                }

                var member = dbClientContacts.FirstOrDefault(y => y.ContactId == x.ContactId);

                if (member.IsNull())
                {
                    hasChanges = true;
                    _dbContext.ClientsContacts.Add(x);
                }
            });

            var deletedMembers = dbClientContacts.Where(p =>
                !clientContacts.Any(l => l.ClientId == p.ClientId && l.ContactId == p.ContactId));

            if (!deletedMembers.IsNullOrEmpty())
            {
                hasChanges = true;
                _dbContext.ClientsContacts.RemoveRange(deletedMembers);
            }

            if (hasChanges)
            {
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task<bool> HasProjects(Guid clientId)
        {
            return await _dbContext.Projects.AnyAsync(x => x.ClientId == clientId && !x.Deleted);
        }
    }
}
