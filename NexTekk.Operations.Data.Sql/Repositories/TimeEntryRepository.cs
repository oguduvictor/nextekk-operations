﻿using Microsoft.EntityFrameworkCore;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Data.Sql.Repositories
{
    public class TimeEntryRepository : ITimeEntryRepository
    {
        private readonly TimeTrackingDbContext _dbContext;

        public TimeEntryRepository(TimeTrackingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TimeEntryEntity> Get(Guid id)
        {
            return await _dbContext
                            .TimeEntries
                            .Include(x => x.Activity)
                            .Include(x => x.Project)
                            .Include(x => x.Creator)
                            .Include(x => x.Editor)
                            .Include(x => x.Approver)
                            .Include(x => x.Denier)
                            .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<TimeEntryEntity>> GetAll(IEnumerable<Guid> ids)
        {
            return await _dbContext
                            .TimeEntries
                            .Include(x => x.Activity)
                            .Include(x => x.Project)
                            .Where(x => ids.Contains(x.Id))
                            .OrderBy(x => x.BeginDateTime)
                            .ToListAsync();
        }

        public async Task<IEnumerable<Guid>> GetUsersIdsWithSubmission(DateTimeRange dateRange)
        {
            return await _dbContext
                            .TimeEntries
                            .Where(x => 
                                ((x.Status == TimeEntryStatus.Submitted) || (x.Status == TimeEntryStatus.Approved)) && 
                                ((x.BeginDateTime >= dateRange.BeginDateTime) && (x.EndDateTime <= dateRange.EndDateTime)))
                            .Select(x => x.CreatorId)
                            .Distinct()
                            .ToListAsync();
        }

        public async Task<IEnumerable<TimeEntryEntity>> GetAll(TimeEntrySearchCriteria searchCriteria)
        {
            var query = _dbContext.TimeEntries as IQueryable<TimeEntryEntity>;
            var activityOnly = false;

            if (searchCriteria.BeginDateTime.HasValue)
            {
                query = query.Where(x => x.BeginDateTime >= searchCriteria.BeginDateTime);
            }

            if (searchCriteria.EndDateTime.HasValue)
            {
                query = query.Where(x => x.EndDateTime <= searchCriteria.EndDateTime);
            }

            if (searchCriteria.CreatorId.HasValue)
            {
                query = query.Where(x => x.CreatorId == searchCriteria.CreatorId);
            }

            if (searchCriteria.Status.HasValue)
            {
                query = query.Where(x => x.Status == searchCriteria.Status);
            }

            if (searchCriteria.ActivitiesOnly)
            {
                activityOnly = true;
                query = query.Where(x => x.ActivityId != null);
            }
            else if (searchCriteria.IsVacation.HasValue)
            {
                activityOnly = true;
                query = query.Where(x => x.ActivityId != null && x.Activity.IsVacation == searchCriteria.IsVacation.Value);
            }
            else if (searchCriteria.ProjectsOnly)
            {
                query = query.Where(x => x.ProjectId != null);
            }

            if (searchCriteria.ActivityId.HasValue)
            {
                activityOnly = true;
                query = query.Where(x => x.ActivityId == searchCriteria.ActivityId);
            }
            else if (searchCriteria.ProjectId.HasValue)
            {
                query = query.Where(x => x.Project.Id == searchCriteria.ProjectId);
            }
            else if (searchCriteria.ProjectOrActivityId.HasValue)
            {
                query = query.Where(x => x.ActivityId == searchCriteria.ProjectOrActivityId || x.ProjectId == searchCriteria.ProjectOrActivityId);
            }

            // Activities don't have project managers
            // Only projects do
            if (!activityOnly && searchCriteria.ProjectManagerId.HasValue)
            {
                query = query.Where(x => x.Project.TeamMembers
                    .Any(y => y.UserId == searchCriteria.ProjectManagerId && y.Role == TeamMemberRole.ProjectManager));
            }

            return await query
                            .Include(x => x.Activity)
                            .Include(x => x.Project)
                            .Include(x => x.Creator)
                            .OrderBy(x => x.BeginDateTime)
                            .ToListAsync();
        }

        public async Task Create(IEnumerable<TimeEntryEntity> entities)
        {
            _dbContext.TimeEntries.AddRange(entities);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(IEnumerable<TimeEntryEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (_dbContext.Entry(entity).State == EntityState.Detached)
                {
                    _dbContext.TimeEntries.Attach(entity);
                }

                _dbContext.Entry(entity).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(IEnumerable<Guid> ids)
        {
            foreach (var id in ids)
            {
                var dbEntity = _dbContext.TimeEntries.Local.FirstOrDefault(x => x.Id == id);

                if (dbEntity == null)
                {
                    dbEntity = new TimeEntryEntity();
                    dbEntity.Id = id;
                    _dbContext.TimeEntries.Attach(dbEntity);
                }

                _dbContext.TimeEntries.Remove(dbEntity);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
