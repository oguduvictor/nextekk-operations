﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Data.Sql.DbContexts;
using NexTekk.Operations.Data.Sql.Repositories;
using NexTekk.Operations.Framework;
using System;
using System.IO;
using System.Net.Http;
using System.Text;

namespace NexTekk.Operations.Tests.Helpers
{
    public static class ConfigurationHelper
    {
        private static IConfigurationRoot _configuration;
        private static string _projectBasePath;

        public static IConfigurationRoot Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    var builder = new ConfigurationBuilder()
                         .SetBasePath(ProjectBaseDirectory)
                         .AddJsonFile("appsettings.json");

                    _configuration = builder.Build();
                }

                return _configuration;
            }
        }

        public static string ProjectBaseDirectory
        {
            get
            {
                if (_projectBasePath.IsNullOrEmpty())
                {
                    var baseDir = Directory.GetCurrentDirectory();
                    var splitter = new string[] { @"\bin" };

                    _projectBasePath = baseDir.Split(splitter, StringSplitOptions.RemoveEmptyEntries)[0];
                }

                return _projectBasePath;
            }
        }

        public static TimeTrackingDbContext GetDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TimeTrackingDbContext>();

            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("OperationsConnection"));

            return new TimeTrackingDbContext(optionsBuilder.Options);
        }

        public static ProjectDbContext GetProjectDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>();

            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("OperationsConnection"));

            return new ProjectDbContext(optionsBuilder.Options);
        }

        public static CashFlowDbContext GetCashFlowDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CashFlowDbContext>();

            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("OperationsConnection"));

            return new CashFlowDbContext(optionsBuilder.Options);
        }

        public static IEmailService GetEmailService()
        {
            var restClient = new RestClient(new HttpClient());
            var apiUrl = "http://nextekk.com";
            var emailService = new EmailService(restClient, apiUrl);

            return emailService as IEmailService;
        }

        public static IUserRepository GetUserRepository()
        {
            var dbContext = GetDbContext();
            var userRepository = new UserRepository(dbContext);

            return userRepository as IUserRepository;
        }

        /// <summary>
        /// Gets the access token
        /// </summary>
        /// <param name="isManager">Manager if true, and Employee if false</param>
        public static string GetAccessToken(bool isManager = false)
        {
            // TODO: Remove static token when getting it dynamically is fixed.
            return "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI4MDA5MkQ2QTEyMUQ1RDIzOEVGMUMyRDBFQzQzRjBGNkFCQ0YzQTgiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJ1QUNTMXFFaDFkSTQ3eHd0RHNRX0QycTg4NmcifQ.eyJuYmYiOjE0OTMzNzEzNDUsImV4cCI6MTQ5MzM3Njk0NSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAwIiwiYXVkIjpbImh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC9yZXNvdXJjZXMiLCJOZXhUZWtrLk9wZXJhdGlvbnMiXSwiY2xpZW50X2lkIjoiTmV4VGVray5PcGVyYXRpb25zIiwic3ViIjoiNGQxMDk1MzktZTBkNy00ZDk2LWJjYzUtZDZkYWRiMWNhMGFhIiwiYXV0aF90aW1lIjoxNDkzMzcxMzQ1LCJpZHAiOiJsb2NhbCIsIm5hbWUiOiJheW8uYWtpbkBuZXh0ZWtrLmNvbSIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiZjU2MDI3YTMtNjU0My00MGUyLWI1MTYtOTE2MTVjOTljZDBmIiwicm9sZSI6WyJVc2VyIiwiRW1wbG95ZWUiLCJNYW5hZ2VyIl0sImVtYWlsIjoiYXlvLmFraW5AbmV4dGVray5jb20iLCJGaXJzdE5hbWUiOiJBeW8iLCJMYXN0TmFtZSI6IkFraW4iLCJzY29wZSI6WyJOZXhUZWtrLk9wZXJhdGlvbnMiXSwiYW1yIjpbInB3ZCJdfQ.bimYnSaJbxF4sCcek3KT8BKNl4ThSwaQJ_gm08faFjKm3QwnUaVrKYBWkfX8AfhZBsldwLrxi2gIvCXysu8nFvwp2zoK926zpsWY-2IijvQUjSxk7vcyYIU1tTM-_L4QEazlH3kwZpcbs12q5Noev_LwpFuUGLz2BN8EBk4cOSIsW7sMiSmYhr8EzaDYHDEIG8i-7Sddvp_lHPogHtOg86OBsTW3v8h0WhdXFpap0xY5Xm8556Qk5--sweHrqd5PX_bvumDqjidv8BVhAEvly6G0lXa6LIKUYicd3lFGiK0qzApP58sKHOgnUrjB1CF9G6FK2BowMLw3ctaXR7ywog";

            ////var username = isManager ? "ayo.akin@nextekk.com" : "tim.roger@nextekk.com";
            ////var requestData = new {
            ////    username = username,
            ////    password = "Pass@word1",
            ////    grant_type = "password",
            ////    client_id = "NexTekk.Operations",
            ////    client_secret = "Hello32ec36a5413f41f09dbe34465cfc97dbNexTekkTimeTrackingIsHere",
            ////    scope = "NexTekk.Operations" };
            ////var contentValue = requestData.ToJson();
            ////var content = new StringContent(contentValue, Encoding.UTF8, "application/json");

            ////using (var client = new HttpClient())
            ////{
            ////    client.BaseAddress = new Uri("http://localhost:5000");

            ////    var task = client.PostAsync("/connect/token", content);

            ////    task.Wait();

            ////    var response = task.Result;

            ////    response.EnsureSuccessStatusCode();

            ////    var task2 = response.Content.ReadAsStringAsync();

            ////    task2.Wait();

            ////    var jsonResult = task2.Result.FromJson<JObject>();

            ////    return jsonResult["access_token"].Value<string>();
            ////}
        }
    }
}