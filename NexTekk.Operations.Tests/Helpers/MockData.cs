﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using NexTekk.Operations.Core.Models.CashFlow;
using AutoMapper;

namespace NexTekk.Operations.Tests.Helpers
{
    public static class MockData
    {
        private static Guid _id = Guid.Parse("2A843B5E-D1A1-4D59-8C89-9081F6F1DB39");
        private static Guid _id1 = Guid.Parse("A02206CC-B7E6-4131-AA9D-318416E0DD35");
        private static Guid _id2 = Guid.Parse("C0E31CE5-6F45-4BAD-A1FC-F6025EF4DB6A");

        public static UserEntity User
        {
            get
            {
                return new UserEntity(Guid.NewGuid(), "Emeka", "Victor");
            }
        }

        public static List<ContactEntity> ContactDB
        {
            get
            {
                return new List<ContactEntity>
                {
                    new ContactEntity
                    {
                        Id = 120, FirstName = "Victor", Creator = User, Editor = User,  CreatorId = User.Id, EditorId = User.Id
                    },
                    new ContactEntity
                    {
                        Id = 310, FirstName = "Moses", Creator = User, Editor = User,  CreatorId = User.Id, EditorId = User.Id
                    },
                    new ContactEntity
                    {
                        Id = 210, FirstName = "Emeka", Creator = User, Editor = User,  CreatorId = User.Id, EditorId = User.Id
                    }
                };
            }
        }

        public static List<ClientEntity> ClientDB
        {
            get
            {
                return new List<ClientEntity>
                {
                    new ClientEntity
                    {
                        Id = _id, Creator = User, Editor = User, CreatorId = User.Id, EditorId = User.Id, Name = "Nextekk"
                    },
                    new ClientEntity
                    {
                        Id = _id1, Creator = User, Editor = User, CreatorId = User.Id, EditorId = User.Id, Name = "Microsoft"
                    }
                };
            }
        }

        public static List<ProjectEntity> ProjectDB
        {
            get
            {
                return new List<ProjectEntity>
                {
                    new ProjectEntity
                    {
                        Id = _id, Title = "NexTekk TimeTracking", Deleted = false, ClientId = ClientDB.First().Id,
                        Client = ClientDB.First(), Creator = User, Editor = User, CreatorId = User.Id, EditorId = User.Id
                    },
                    new ProjectEntity
                    {
                        Id = _id, Title = "Expense Management", Deleted = false, ClientId = ClientDB.First().Id, Client = ClientDB.First(),
                        Status = ProjectStatus.NotStarted, Creator = User, Editor = User, CreatorId = User.Id, EditorId = User.Id
                    }
                };
            }
        }

        public static List<TeamMemberEntity> TeamMemberDB
        {
            get
            {
                return new List<TeamMemberEntity>
                {
                    new TeamMemberEntity { ProjectId = _id, Role = TeamMemberRole.ProjectManager, User = User, UserId = User.Id },
                    new TeamMemberEntity { ProjectId = _id, Role = TeamMemberRole.Developer, User = User, UserId = User.Id },
                };
            }
        }

        public static List<VacationSetting> VacationSettingDB
        {
            get
            {
                return new List<VacationSetting>
                {
                    new VacationSetting { UserId = _id, HoursPerPeriod = 2, YearBeginBalance = 5, EditorId = _id },
                    new VacationSetting { UserId = User.Id, HoursPerPeriod = 3, YearBeginBalance = 2, EditorId = User.Id }
                };
            }
        }

        public static List<VacationEntity> VacationDB
        {
            get
            {
                var first_vacation = new VacationEntity
                {
                    UserId = _id,
                    User = User,
                    HoursPerPeriod = 2,
                    YearBeginBalance = 3,
                    CreatorId = User.Id,
                    EditorId = User.Id,
                    Created = new DateTime(2016 - 11 - 24)
                };

                var second_vacation = new VacationEntity
                {
                    UserId = _id,
                    User = User,
                    HoursPerPeriod = 2,
                    YearBeginBalance = 3,
                    CreatorId = User.Id,
                    EditorId = User.Id,
                    Created = new DateTime(2016 - 11 - 24)
                };

                SetPropertyValue(first_vacation, "Year", 2016);

                SetPropertyValue(second_vacation, "Year", 2017);

                return new List<VacationEntity>
                {
                   first_vacation, second_vacation
                };
            }
        }

        public static List<Expense> ExpenseDB
        {
            get
            {
                return new List<Expense>
                {
                    new Expense
                    {
                        Amount = 32, Description = "This is a test Description"
                    },
                    new Expense
                    {
                        Id = _id, Date = new DateTime(2016 - 12 - 06), Currency = Currency.Dollar, Deleted = false, Amount = 200,
                        Description = "Expense on purchase of modem", Reason = "The need pending", IncurredById = _id, Reimbursed = null,
                        Created = new DateTime(2017, 02, 08), CreatedById = _id, ModifiedById = Guid.NewGuid(), Modified = new DateTime(2017, 07, 08)
                    },
                    new Expense
                    {
                        Id = _id1, Date = new DateTime(2016, 12, 06), Currency = Currency.Naira, Deleted = false, Amount = 700,
                        Description = "Expense on purchase of fuel", Reason = "A daily need", IncurredById = _id, Reimbursed = null,
                        Created = new DateTime(2017, 12, 09), CreatedById = _id, ModifiedById = Guid.NewGuid(), Modified = new DateTime(2016, 07, 10)
                    }
                };
            }
        }

        public static List<Revenue> RevenueDB
        {
            get
            {
                return new List<Revenue>
                {
                    new Revenue
                    {
                        Id = _id, Amount = 10000, ClientId = ClientDB[0].Id, PaymentDate = new DateTime(2018, 1, 11),
                        Currency = Currency.Dollar, CreatedById = User.Id, Created = DateTime.Today.AddDays(20),
                        Modified = DateTime.UtcNow, ModifiedById = User.Id, Description = "Payment for BlockChain App", Deleted = false
                    },
                    new Revenue
                    {
                        Id = _id1, Amount = 2000000, ClientId = ClientDB[1].Id, PaymentDate = new DateTime(2018, 1, 11),
                        Currency = Currency.Naira, CreatedById = User.Id, Created = new DateTime(2018, 1, 11), Deleted = false,
                        Modified = DateTime.UtcNow, ModifiedById = User.Id, Description = "Part Payment for Unit Test"
                    },
                    new Revenue
                    {
                        Id = _id2, Amount = 2000, ClientId = ClientDB[1].Id, PaymentDate = new DateTime(2018, 1, 11),
                        Currency = Currency.Naira, CreatedById = User.Id, Created = DateTime.Today.AddDays(-31), Deleted = false,
                        Modified = DateTime.Today.AddDays(-31), ModifiedById = User.Id, Description = "User Experience Design"
                    }
                };
            }
        }

        public static List<Asset> AssetDB
        {
            get
            {
                return new List<Asset>
                {
                    new Asset
                    {
                        Id = _id, Item = "Laptop", Manufacturer = "Dell", Model = "Inspiron 15", Cost = 800, Currency = Currency.Dollar, Condition = Condition.New, Created = DateTime.UtcNow.AddDays(-1), CreatedById = User.Id
                    },
                    new Asset
                    {
                        Id = _id1, Item = "Monitor", Manufacturer = "Hp", Model = "Desktop monitor", Cost = 30, Currency = Currency.Dollar, Condition = Condition.New, Created = DateTime.UtcNow.AddDays(-1), CreatedById = User.Id
                    },
                    new Asset
                    {
                        Id = _id2, Item = "WhiteBoard", Manufacturer = "Board Makers", Model = "White", Cost = 15000, Currency = Currency.Naira, Condition = Condition.Good, Created = DateTime.UtcNow.AddDays(-1), CreatedById = User.Id, AssigneeId = _id1
                    }
                };
            }
        }

        private static void SetPropertyValue<T, S>(T obj, string propertyName, S value)
        {
            var type = obj.GetType();
            var propertyInfo = type.GetProperty(propertyName);

            propertyInfo.SetValue(obj, value);
        }
    }
}
