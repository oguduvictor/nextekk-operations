﻿namespace NexTekk.Operations.Tests.Helpers
{
    public static class Constants
    {
        public const string UnitTest = "Unit";
        public const string IntegrationTest = "Integration";
    }
}
