﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using static NexTekk.Operations.Tests.Helpers.ConfigurationHelper;

namespace NexTekk.Operations.Tests.Helpers
{
    public static class DbEntityHelper
    {
        private static Guid _userId = new Guid("4d109539-e0d7-4d96-bcc5-d6dadb1ca0aa");
        private static HashSet<Guid> _addedTimeEntryIds = new HashSet<Guid>();
        private static HashSet<Guid> _addedActivityIds = new HashSet<Guid>();

        public static Guid UserId
        {
            get { return _userId; }
        }

        public static HashSet<Guid> AddedTimeEntryIds
        {
            get { return _addedTimeEntryIds; }
        }

        public static HashSet<Guid> AddedActivityIds
        {
            get { return _addedActivityIds; }
        }

        public static TimeEntryEntity GetTimeEntryFromDB(Guid id)
        {
            using (var context = GetDbContext())
            {
                return context.TimeEntries.Find(id);
            }
        }

        public static void DeleteAddedEntries()
        {
            DeleteTimeEntries(_addedTimeEntryIds.ToArray());
        }

        public static void DeleteTimeEntries(params Guid[] ids)
        {
            if (ids.IsNullOrEmpty())
            {
                return;
            }

            using (var context = GetDbContext())
            {
                foreach (var id in ids)
                {
                    var te = context.TimeEntries.Local.FirstOrDefault(x => x.Id == id);

                    if (te == null)
                    {
                        te = new TimeEntryEntity();
                        te.Id = id;

                        context.TimeEntries.Attach(te);
                    }

                    context.TimeEntries.Remove(te);
                }

                context.SaveChanges();
            }

            AddedTimeEntryIds.RemoveWhere(x => ids.Contains(x));
        }

        public static TimeEntryEntity[] AddTimeEntries(int count = 1, TimeEntryStatus status = TimeEntryStatus.Saved)
        {
            var activity = AddActivities()[0];
            var timeEntries = new TimeEntryEntity[count];
            var date = new DateTime(2000, 11, 20);

            using (var context = GetDbContext())
            {
                for (int i = 0; i < count; i++)
                {
                    var thisDate = date.AddHours(i);
                    var te = new TimeEntryEntity
                    {
                        Id = Guid.NewGuid(),
                        CreatorId = _userId,
                        EditorId = _userId,
                        Task = $"Sample Time Entry ({i + 1})",
                        BeginDateTime = thisDate,
                        EndDateTime = thisDate.AddHours(1),
                        Created = thisDate,
                        Edited = thisDate,
                        Status = status,
                        ActivityId = activity.Id
                    };

                    timeEntries[i] = te;
                    context.TimeEntries.Add(te);
                    _addedTimeEntryIds.Add(te.Id);
                }

                context.SaveChanges();

                return timeEntries;
            }
        }

        public static ActivityEntity GetActivityFromDB(Guid id)
        {
            using (var context = GetDbContext())
            {
                return context.Activities.Find(id);
            }
        }

        public static void DeleteActivities(params Guid[] ids)
        {
            if (ids.IsNullOrEmpty())
            {
                return;
            }

            using (var context = GetDbContext())
            {
                foreach (var id in ids)
                {
                    var activity = context.Activities.Local.FirstOrDefault(x => x.Id == id);

                    if (activity == null)
                    {
                        activity = new ActivityEntity();
                        activity.Id = id;

                        context.Activities.Attach(activity);
                    }

                    context.Activities.Remove(activity);
                }

                context.SaveChanges();
            }

            AddedActivityIds.RemoveWhere(x => ids.Contains(x));
        }

        public static void DeleteAddedActivities()
        {
            DeleteActivities(AddedActivityIds.ToArray());
        }

        public static ActivityEntity[] AddActivities(int count = 1)
        {
            var activities = new ActivityEntity[count];

            using (var context = GetDbContext())
            {
                for (int i = 0; i < count; i++)
                {
                    var activity = new ActivityEntity
                    {
                        Id = Guid.NewGuid(),
                        Title = $"Test Activity {i + 1}",
                        CreatorId = _userId,
                        EditorId = _userId,
                        Created = DateTime.UtcNow,
                        Edited = DateTime.UtcNow,
                        Billable = false
                    };

                    activities[i] = activity;
                    context.Activities.Add(activity);
                    AddedActivityIds.Add(activity.Id);
                }

                context.SaveChanges();

                return activities;
            }
        }
    }
}
