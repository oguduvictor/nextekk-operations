﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Data.Sql.DbContexts;
using NexTekk.Operations.Data.Sql.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.ConfigurationHelper;
using static NexTekk.Operations.Tests.Helpers.Constants;

namespace NexTekk.Operations.Tests.Integration.Repository
{
    [TestClass]
    public class ProjectRepositoryTests
    {
        private static ProjectDbContext context;
        private static ProjectRepository _repo;
        private static IDbContextTransaction _txn;

        private static IEnumerable<ClientEntity> clients;
        private static ProjectEntity[] projectDB;

        private static Guid userId = new Guid("4d109539-e0d7-4d96-bcc5-d6dadb1ca0aa");

        [ClassInitialize]
        public static void BeforeAll(TestContext testContest)
        {
            clients = new List<ClientEntity>
            {
                new ClientEntity { Id = Guid.NewGuid(), CreatorId = userId, EditorId = userId, Name = "Microsoft" }
            };

            projectDB = new ProjectEntity[]
            {
                new ProjectEntity
                {
                    Id = Guid.NewGuid(), Title = "NexTekk TimeTracking", ClientId = clients.First().Id, CreatorId = userId, EditorId = userId
                },
                new ProjectEntity
                {
                    Id = Guid.NewGuid(), Title = "Expense Management", ClientId = clients.First().Id, CreatorId = userId, EditorId = userId
                }
            };

            context = GetProjectDbContext();
        }

        [ClassCleanup]
        public static void AfterAll()
        {
            context.Clients.Remove(clients.First());
            context.SaveChanges();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _repo = new ProjectRepository(context);
            _txn = context.Database.BeginTransaction();
            context.Clients.Add(clients.First());
            context.SaveChanges();
        }

        [TestCleanup]
        public void AfterEach()
        {
            _txn.Rollback();
            _txn.Dispose();
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Should_Be_Able_To_Add_Project_To_DB()
        {
            var project = projectDB.First();

            await _repo.Create(new ProjectEntity[] { project });

            var dbProject = context.Set<ProjectEntity>().Find(project.Id);

            Assert.IsNotNull(dbProject);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Should_Get_Projects_From_DB()
        {
            var search = new ProjectSearchCriteria();
            
            await _repo.Create(projectDB);

            var dbProject = await _repo.GetProjects(search);

            Assert.AreEqual(2, dbProject.Count());
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Should_Get_A_Project_From_DB()
        {
            var project = projectDB.First();

            context.Projects.Add(project);
            context.SaveChanges();

            var dbProject = await _repo.GetProject(project.Id);

            Assert.IsNotNull(dbProject);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Should_Be_Able_To_Soft_Delete_Project()
        {
            var projectId = projectDB.First().Id;
            await _repo.Create(projectDB);

            await _repo.Delete(new Guid[] { projectId });

            var project = await _repo.GetProject(projectId);

            Assert.IsTrue(project.Deleted);
        }
    }
}
