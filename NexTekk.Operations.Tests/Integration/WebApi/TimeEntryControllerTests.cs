﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.WebApi;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.ConfigurationHelper;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.DbEntityHelper;

namespace Tests.Integration.WebApi
{
    [TestClass]
    public class TimeEntryControllerTests
    {
        private static TestServer _server;
        private static HttpClient _client;

        public TimeEntryControllerTests()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseContentRoot(ProjectBaseDirectory)
                .UseStartup<Startup>());

            _client = _server.CreateClient();
            _client.SetBearerToken(GetAccessToken());
        }

        [ClassCleanup]
        public static void AfterAll()
        {
            _server.Dispose();
            _client.Dispose();            
        }

        [TestCleanup]
        public void AfterEach()
        {
            DeleteAddedEntries();
            DeleteAddedActivities();
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Create_TimeEntry()
        {
            var activiy = AddActivities()[0];
            var timeEntryRequest = new TimeEntryViewModel
            {
                Task = "Created layout",
                ActivityId = activiy.Id,
                BeginDateTime = new DateTime(2000, 11, 20, 2, 0, 0),
                EndDateTime = new DateTime(2000, 11, 20, 4, 0, 0)
            };
            var stringValue = JsonConvert.SerializeObject(timeEntryRequest);
            var content = new StringContent(stringValue, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/timeentry/create", content);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var timeEntry = JsonConvert.DeserializeObject<TimeEntryDetail>(responseString);

            Assert.IsNotNull(timeEntry);

            AddedTimeEntryIds.Add(timeEntry.Id);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Get_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries()[0];
            var response = await _client.GetAsync("/api/timeentry/get/" + dbTimeEntry.Id);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TimeEntryDetail>(responseString);

            Assert.IsNotNull(result);
            Assert.AreEqual(dbTimeEntry.Id, result.Id);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Search_TimeEntries()
        {
            AddTimeEntries(5);
            var date = new DateTime(2000, 11, 20);
            var query = $"beginDateTime={date}&endDateTime={date.AddHours(10)}&creatorId={UserId}";
            var response = await _client.GetAsync("/api/timeentry/search?" + query);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TimeEntry[]>(responseString);

            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.Length);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Update_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries()[0];
            var timeEntryRequest = new TimeEntryViewModel
            {
                Task = "Test update",
                Comment = "test comment",
                ActivityId = dbTimeEntry.ActivityId,
                BeginDateTime = dbTimeEntry.BeginDateTime,
                EndDateTime = dbTimeEntry.EndDateTime
            };
            var stringValue = JsonConvert.SerializeObject(timeEntryRequest);
            var content = new StringContent(stringValue, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync($"/api/timeentry/update/{dbTimeEntry.Id}", content);

            response.EnsureSuccessStatusCode();

            var updatedTimeEntry = GetTimeEntryFromDB(dbTimeEntry.Id);

            Assert.AreEqual(timeEntryRequest.Task, updatedTimeEntry.Task);
            Assert.AreEqual(timeEntryRequest.Comment, updatedTimeEntry.Comment);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Submit_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries()[0];
            var requestData = new[] { new { id = dbTimeEntry.Id, comment = "test manager comment for submit" } };
            var requestDataJon = JsonConvert.SerializeObject(requestData);
            var content = new StringContent(requestDataJon, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/timeentry/submit/", content);

            response.EnsureSuccessStatusCode();

            var updatedTimeEntry = GetTimeEntryFromDB(dbTimeEntry.Id);

            Assert.AreEqual(TimeEntryStatus.Submitted, updatedTimeEntry.Status);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Unsubmit_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries(1, TimeEntryStatus.Submitted)[0];
            var requestData = new[] { new { id = dbTimeEntry.Id, comment = "test manager comment for unsubmit" } };
            var requestDataJon = JsonConvert.SerializeObject(requestData);
            var content = new StringContent(requestDataJon, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/timeentry/unsubmit", content);

            response.EnsureSuccessStatusCode();

            var updatedTimeEntry = GetTimeEntryFromDB(dbTimeEntry.Id);

            Assert.AreEqual(TimeEntryStatus.Saved, updatedTimeEntry.Status);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Approve_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries(1, TimeEntryStatus.Submitted)[0];
            var requestData = new[] { new { id = dbTimeEntry.Id, comment = "test manager comment for approval" } };
            var requestDataJon = JsonConvert.SerializeObject(requestData);
            var content = new StringContent(requestDataJon, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/timeentry/approve", content);

            response.EnsureSuccessStatusCode();

            var updatedTimeEntry = GetTimeEntryFromDB(dbTimeEntry.Id);

            Assert.AreEqual(TimeEntryStatus.Approved, updatedTimeEntry.Status);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Deny_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries(1, TimeEntryStatus.Submitted)[0];
            var requestData = new[] { new { id = dbTimeEntry.Id, comment = "test manager comment for denial" } };
            var requestDataJon = JsonConvert.SerializeObject(requestData);
            var content = new StringContent(requestDataJon, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/timeentry/deny", content);

            response.EnsureSuccessStatusCode();

            var updatedTimeEntry = GetTimeEntryFromDB(dbTimeEntry.Id);

            Assert.AreEqual(TimeEntryStatus.Denied, updatedTimeEntry.Status);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Delete_TimeEntry()
        {
            var dbTimeEntry = AddTimeEntries()[0];
            var response = await _client.DeleteAsync($"/api/timeentry/delete/{dbTimeEntry.Id}");

            response.EnsureSuccessStatusCode();

            var timeEntry = GetTimeEntryFromDB(dbTimeEntry.Id);

            Assert.IsNull(timeEntry);

            AddedTimeEntryIds.Remove(dbTimeEntry.Id);
        }
    }
}
