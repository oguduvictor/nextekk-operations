﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.WebApi;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.ConfigurationHelper;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.DbEntityHelper;

namespace NexTekk.Operations.Tests.Integration.WebApi
{
    [TestClass]
    public class ActivityControllerTests
    {
        private const string API_URL = "/api/activity/";
        private static TestServer _server;
        private static HttpClient _client;

        public ActivityControllerTests()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseContentRoot(ProjectBaseDirectory)
                .UseStartup<Startup>());

            _client = _server.CreateClient();
            _client.SetBearerToken(GetAccessToken());
        }

        [ClassCleanup]
        public static void AfterAll()
        {
            _server.Dispose();
            _client.Dispose();
        }

        [TestCleanup]
        public void AfterEach()
        {
            DeleteAddedActivities();
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Get_Activity()
        {
            var dbActivity = AddActivities()[0];
            var response = await _client.GetAsync(API_URL + dbActivity.Id);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Activity>(responseString);

            Assert.IsNotNull(result);
            Assert.AreEqual(dbActivity.Id, result.Id);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Get_Activities()
        {        
            var dbActivities = AddActivities(5);
            var response = await _client.GetAsync(API_URL);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Activity[]>(responseString);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length >= 5);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Create_Activity()
        {
            var activity = new ActivityModel
            {
                Billable = false,
                Title = "Test activity"
            };
            var json = JsonConvert.SerializeObject(activity);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(API_URL, content);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ActivityEntity>(responseString);

            Assert.IsNotNull(result);
            Assert.AreNotEqual(Guid.Empty, result.Id);

            AddedActivityIds.Add(result.Id);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Update_Activity()
        {
            var dbActivity = AddActivities()[0];
            var activity = new ActivityModel
            {
                Billable = true,
                Title = "Updated Test activity"
            };
            var json = JsonConvert.SerializeObject(activity);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync(API_URL + dbActivity.Id, content);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var result = GetActivityFromDB(dbActivity.Id);

            Assert.IsNotNull(result);
            Assert.AreEqual(activity.Billable, result.Billable);
            Assert.AreEqual(activity.Title, result.Title);
        }

        [TestMethod, TestCategory(IntegrationTest)]
        public async Task Can_Delete_Activity()
        {
            var dbActivity = AddActivities()[0];
            var response = await _client.DeleteAsync(API_URL + dbActivity.Id);

            response.EnsureSuccessStatusCode();

            var deletedActivity = GetActivityFromDB(dbActivity.Id);

            Assert.AreEqual(true, deletedActivity.Deleted);
        }
    }
}
