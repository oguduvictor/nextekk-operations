﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Framework;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NexTekk.Operations.Tests.Integration.Services
{
    [TestClass]
    public class EmailServiceTests
    {
        // Ignored to avoid sending unnecessary emails
        [Ignore, TestMethod]
        public async Task EmailService_Should_Send_Email()
        {
            var restClient = new RestClient(new HttpClient());
            var apiUrl = "http://nextekk.com";
            var emailService = new EmailService(restClient, apiUrl);
            var email = new Email
            {
                Subject = "Hi there",
                Body = "This is a test",
                Sender = "noreply@nextekk.com",
                Recipients = new List<string> { "info@nextekk.com" }
            };

            var isSuccess = await emailService.SendEmail(email);

            Assert.IsTrue(isSuccess);
        }
    }
}
