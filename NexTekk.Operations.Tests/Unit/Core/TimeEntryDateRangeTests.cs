﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NexTekk.Operations.Core.Models;
using System;
using static NexTekk.Operations.Tests.Helpers.Constants;

namespace NexTekk.Operations.Tests.Unit.Core
{
    [TestClass]
    public class TimeEntryDateRangeTests
    {
        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Does_Not_Overlap_With_Time_After_It()
        {
            var range1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 9, 20, 0),
                EndDateTime = new DateTime(2017, 2, 2, 10, 30, 0)
            };

            var range2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 10, 30, 0),
                EndDateTime = new DateTime(2017, 2, 2, 13, 0, 0)
            };

            var overlap = range1.Overlaps(range2);
            Assert.IsFalse(overlap);
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Does_Not_Overlap_With_Time_Before_I1()
        {
            var range1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 10, 30, 0),
                EndDateTime = new DateTime(2017, 2, 2, 13, 0, 0)
            };

            var range2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 9, 20, 0),
                EndDateTime = new DateTime(2017, 2, 2, 10, 30, 0)
            };

            var overlap = range1.Overlaps(range2);
            Assert.IsFalse(overlap);
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Overlaps_With_Time_Over_It()
        {
            var range1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 9, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 11, 0, 0)
            };

            var range2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 9, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 12, 0, 0)
            };

            var overlap = range1.Overlaps(range2);
            Assert.IsTrue(overlap);
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Overlaps_With_Time_Inside_It()
        {
            var range1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 14, 0, 0)
            };

            var range2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 11, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 12, 0, 0)
            };

            var overlap = range1.Overlaps(range2);
            Assert.IsTrue(overlap);
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Overlaps_With_Time_Before_In_Into_It()
        {
            var range1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 12, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 14, 0, 0)
            };

            var range2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 13, 1, 0)
            };

            var overlap = range1.Overlaps(range2);
            Assert.IsTrue(overlap);
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Overlaps_With_Time_From_Within_It_To_Past_It()
        {
            var range1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 12, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 14, 0, 0)
            };

            var range2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 13, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 15, 1, 0)
            };

            var overlap = range1.Overlaps(range2);
            Assert.IsTrue(overlap);
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryDateRange_Is_Not_Valid_For_Default_Dates()
        {
            var dateRange = new TimeEntryDateRange();

            Assert.IsFalse(dateRange.IsValid());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryRange_Is_Not_Valid_For_Range_Shorter_Than_15_Mins()
        {
            var dateRange = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 12, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 12, 14, 59)
            };

            Assert.IsFalse(dateRange.IsValid());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryRange_Is_Not_Valid_For_BeginDate_After_EndDate()
        {
            var dateRange = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 13, 0, 0),
                EndDateTime = new DateTime(2017, 2, 2, 12, 0, 0)
            };

            Assert.IsFalse(dateRange.IsValid());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryRange_Is_InValid_For_2_Different_Days()
        {
            var dateRange = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 2, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 3, 12, 0, 0)
            };

            Assert.IsFalse(dateRange.IsValid());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void TimeEntryRange_Is_Valid_For_BeginDate_Greater_By_15_Mins_Or_More()
        {
            var dateRange1 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 1, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 10, 15, 0)
            };

            var dateRange2 = new TimeEntryDateRange()
            {
                BeginDateTime = new DateTime(2017, 2, 1, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 10, 30, 0)
            };

            Assert.IsTrue(dateRange1.IsValid());
            Assert.IsTrue(dateRange1.IsValid());
        }
    }
}
