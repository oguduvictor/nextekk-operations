﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using NexTekk.Operations.Core.Helpers;
using static NexTekk.Operations.Tests.Helpers.Constants;

namespace NexTekk.Operations.Tests.Unit.Core
{
    [TestClass]
    public class ExtensionMethodsTests
    {
        [TestMethod, TestCategory(UnitTest)]
        public void IsNull_Returns_True_For_Null_Value()
        {
            Assert.IsTrue(default(string).IsNull());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void IsNull_Returns_False_For_NonNull_Value()
        {
            Assert.IsFalse("value".IsNull());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void IsNullOrEmpty_Returns_True_For_Empty_Value()
        {
            Assert.IsTrue(default(string).IsNullOrEmpty());
            Assert.IsTrue(string.Empty.IsNullOrEmpty());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void IsNullOrEmpty_Returns_False_For_NonEmpty_Value()
        {
            Assert.IsFalse("value".IsNullOrEmpty());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void ToJson_Serializes_Object_To_Valid_Json_String()
        {
            var obj = new { value1 = 1, value2 = 2 };
            var expectedResult = "{\"value1\":1,\"value2\":2}";

            Assert.AreEqual(expectedResult, obj.ToJson());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void FromJson_Deerializes_Object_To_Valid_Object()
        {
            var jsonString = "{\"value1\":1,\"value2\":2}";
            var obj = jsonString.FromJson<JObject>();

            Assert.AreEqual(1, obj["value1"].Value<int>());
            Assert.AreEqual(2, obj["value2"].Value<int>());
        }

        [TestMethod, TestCategory(UnitTest)]
        public void IsAnyOf_Returns_True_If_Item_Belongs()
        {
            var value = 3;

            Assert.IsTrue(value.IsAnyOf(4, 5, 3));
        }

        [TestMethod, TestCategory(UnitTest)]
        public void IsAnyOf_Returns_False_If_Item_Does_Not_Belong()
        {
            var value = 3;

            Assert.IsFalse(value.IsAnyOf());
            Assert.IsFalse(value.IsAnyOf(4, 5, 0));
        }
    }
}
