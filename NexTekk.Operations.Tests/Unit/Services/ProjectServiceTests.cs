﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Services
{
    [TestClass]
    public class ProjectServiceTests
    {
        private static Mock<IClientRepository> _clientRepository;
        private static Mock<IContactRepository> _contactRepository;
        private static Mock<IProjectRepository> _projectRepository;
        private static Mock<IUserContext> _userContext;
        private static Mock<IUserRepository> _userRepository;
        
        private IProjectService _service;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContest)
        {
            _clientRepository = new Mock<IClientRepository>();
            _contactRepository = new Mock<IContactRepository>();
            _projectRepository = new Mock<IProjectRepository>();
            _userContext = new Mock<IUserContext>();
            _userRepository = new Mock<IUserRepository>();

            _projectRepository.Setup(x => x.GetProjects(It.IsAny<ProjectSearchCriteria>())).ReturnsAsync(ProjectDB);
            _projectRepository
                .Setup(x => x.GetProject(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(ProjectDB.First(x => x.Id == input));
                });
            _projectRepository
                .Setup(x => x.GetTeamMembers(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(TeamMemberDB.Where<TeamMemberEntity>(x => x.ProjectId == input));
                });

            _clientRepository.Setup(x => x.GetAll(false)).ReturnsAsync(ClientDB);
            _clientRepository
                .Setup(x => x.Get(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(ClientDB.FirstOrDefault(x => x.Id == input));
                });
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _service = new ProjectService(
                _clientRepository.Object,
                _projectRepository.Object,
                _userContext.Object,
                _userRepository.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Project_For_SearchCriteria()
        {
            var search = new ProjectSearchCriteria { CreatorId = Guid.NewGuid() };
            var result = await _service.GetProjects(search);

            Assert.AreNotEqual(0, result.Count());
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Details_For_A_Project_Entry()
        {
            var projectId = ProjectDB.First().Id;
            var project = await _service.GetProject(projectId);

            Assert.IsNotNull(project);
        }

        [TestMethod, ExpectedException(typeof(OperationException)), TestCategory(UnitTest)]
        public async Task Should_Throw_Exception_When_Trying_To_Get_Project_That_Does_Not_Exist()
        {
            var projectId = Guid.NewGuid();
            await _service.GetProject(projectId);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Save_Project_When_Right_Parameters_Are_Supplied()
        {
            var client = ClientDB.First();
            var contact = ContactDB.First();
            var project = new Project
            {
                Title = "New Project",
                Status = ProjectStatus.NotStarted,
                Client = new Client(client),
                Billable = true
            };

            var result = await _service.SaveProject(project);
            
            Assert.AreEqual(result.Title, "New Project");
            Assert.AreEqual(result.Status, ProjectStatus.NotStarted);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Update_Existing_Project()
        {
            var client = ClientDB.First();
            var contact = ContactDB.First();
            var projectId = ProjectDB.First().Id;
            var project = new Project
            {
                Id = projectId,
                Title = "Updated Project",
                Status = ProjectStatus.NotStarted,
                Client = new Client(client),
                Billable = true
            };

            var result = await _service.SaveProject(project);
            
            Assert.AreEqual(result.Title, "Updated Project");
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Save_TeamMembers()
        {
            var teamMember = new TeamMemberEntity
            {
                ProjectId = ProjectDB.First().Id,
                UserId = Guid.NewGuid(),
                Role = TeamMemberRole.ProjectManager
            };

            await _service.SaveTeamMembers(new List<TeamMemberEntity> { teamMember });

            _projectRepository.Verify(x => x.UpdateTeamMembers(new List<TeamMemberEntity> { teamMember }), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Do_Soft_Delete_For_Deleting_Project()
        {
            var project = ProjectDB.First();

            await _service.DeleteProject(project.Id);

            _projectRepository.Verify(x => x.Delete(new Guid[] { project.Id }), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_TeamMembers_For_Specified_Project()
        {
            var projectId = ProjectDB.First().Id;

            var result = await _service.GetTeamMembers(projectId);

            Assert.AreEqual(2, result.Count());
        }
    }
}
