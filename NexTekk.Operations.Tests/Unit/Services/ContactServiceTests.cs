﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Services
{
    [TestClass]
    public class ContactServiceTests
    {
        private static Mock<IContactRepository> _contactRepository;
        private static Mock<IUserContext> _userContext;

        private IContactService _contactService;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _contactRepository = new Mock<IContactRepository>();
            _userContext = new Mock<IUserContext>();

            _contactRepository.Setup(x => x.GetAll(false)).ReturnsAsync(ContactDB);
            _contactRepository
                .Setup(x => x.Get(It.IsAny<int>()))
                .Returns((int input) =>
                {
                    return Task.FromResult(ContactDB.FirstOrDefault(x => x.Id == input));
                });
            _contactRepository.Setup(x => x.GetContactSummaries(false)).ReturnsAsync(ContactDB);
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _contactService = new ContactService(_contactRepository.Object, _userContext.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Contacts()
        {
            var contacts = await _contactService.GetContacts();

            Assert.IsInstanceOfType(contacts, typeof(IEnumerable<Contact>));
            Assert.AreEqual(3, contacts.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_ContactSummaries()
        {
            var contacts = await _contactService.GetContactSummaries();

            Assert.IsInstanceOfType(contacts, typeof(IEnumerable<ContactSummary>));
            Assert.AreEqual(3, contacts.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Particular_Contact()
        {
            var id = ContactDB.First().Id;
            var contact = await _contactService.GetContact(id);

            Assert.IsNotNull(contact);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Save_A_New_Contact()
        {
            var model = new ContactEntity
            {
                FirstName = "Mark",
                LastName = "Essien"
            };

            var result = await _contactService.SaveContact(model);

            Assert.AreEqual(result.FirstName, "Mark");
            Assert.AreEqual(result.LastName, "Essien");
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_An_Existing_Contact()
        {
            var model = ContactDB.First();
            model.FirstName = "Jones";

            await _contactService.SaveContact(model);

            _contactRepository.Verify(x => x.Update(It.Is<ContactEntity>(p => p.FirstName == "Jones")), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Do_Soft_Delete_For_Contact()
        {
            var id = ContactDB.First().Id;

            await _contactService.DeleteContact(id);

            _contactRepository.Verify(x => x.Delete(id), Times.Once);
        }
    }
}
