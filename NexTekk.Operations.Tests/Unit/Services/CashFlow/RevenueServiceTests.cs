﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Services.CashFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NexTekk.Operations.WebApi.App_Start;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Services.CashFlow
{
    [TestClass]
    public class RevenueServiceTests
    {
        private static Mock<IRevenueRepository> _revenueRepository;
        private static Mock<IExpenseRepository> _expenseRepository;
        private static Mock<IUserContext> _userContext;
        private static Mock<IRevenueEmailSender> _emailSender;
        private IRevenueService _revenueService;

        [ClassInitialize]
        public static void BeforeAll(TestContext context)
        {
            AutoMapperProfileConfig.Initialize();

            _revenueRepository = new Mock<IRevenueRepository>();
            _expenseRepository = new Mock<IExpenseRepository>();
            _userContext = new Mock<IUserContext>();
            _emailSender = new Mock<IRevenueEmailSender>();

            _revenueRepository.Setup(x => x.Get(It.IsAny<Guid>()))
                .ReturnsAsync((Guid input) => RevenueDB.FirstOrDefault(y => y.Id == input));
            
            _revenueRepository.Setup(x => x.Create(It.IsAny<Revenue>()))
                .ReturnsAsync((Revenue input) => RevenueDB.FirstOrDefault(y => y.ClientId == input.ClientId));

            AutoMapperProfileConfig.Reset();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            AutoMapperProfileConfig.Initialize();

            _revenueService = new RevenueService(
                _revenueRepository.Object,
                _userContext.Object,
                _expenseRepository.Object,
                _emailSender.Object
                );
        }

        [TestCleanup]
        public void AfterAll()
        {
            AutoMapperProfileConfig.Reset();
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_All_Revenues_Only_Async()
        {
            _revenueRepository.Setup(x => x.GetAll(It.IsAny<RevenueSearchCriteria>()))
                   .ReturnsAsync(RevenueDB);

            var searchCriteria = new RevenueSearchCriteria();

            var revenues = await _revenueService.GetRevenuesAsync(searchCriteria);

            Assert.IsInstanceOfType(revenues, typeof(IEnumerable<Revenue>));
            Assert.AreEqual(revenues.Count(), 3);
            Assert.AreEqual(revenues.Count(x => x.Amount > 0), 3);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_All_Revenues_And_Expenses_Async()
        {
            _expenseRepository.Setup(x => x.Search(It.IsAny<ExpenseSearchCriteria>()))
                .ReturnsAsync(ExpenseDB);

            _revenueRepository.Setup(x => x.GetAll(It.IsAny<RevenueSearchCriteria>()))
                .ReturnsAsync(RevenueDB);

            var searchCriteria = new RevenueSearchCriteria { IncludeExpenses = true };

            var revenues = await _revenueService.GetRevenuesAsync(searchCriteria);
            
            Assert.AreEqual(revenues.Count(), 6);
            Assert.AreEqual(revenues.Count(x => x.ClientId == default(Guid)), 3);
            Assert.AreEqual(revenues.Count(x => x.Amount < 0), 3);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Create_New_Revenue_Async()
        {
            var revenue = new Revenue { ClientId = ClientDB[0].Id };

            var result = await _revenueService.SaveRevenueAsync(revenue);

            Assert.AreEqual(result.ClientId, revenue.ClientId);
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Update_Revenue_Async()
        {
            var revenue = RevenueDB[0];

            revenue.Description = "Edited Description";

            var result = await _revenueService.SaveRevenueAsync(revenue);

            Assert.AreEqual(revenue.Description, result.Description);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async  Task Should_Throw_Exception_When_Modified_Date_Is_More_Than_30_Days_During_Revenue_Update_Async()
        {
            var revenue = RevenueDB[2];

            await Assert.ThrowsExceptionAsync<OperationException>(async () => await _revenueService.SaveRevenueAsync(revenue));
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Delete_Revenue_Async()
        {
            var id = RevenueDB[0].Id;
            
            await _revenueService.DeleteRevenueAsync(id);

            _revenueRepository.Verify(x => x.Update(It.Is<Revenue>(p => p.Id == id && p.Deleted)), Times.Once);
        }
        

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Throw_Exception_When_Created_Date_Is_More_Than_30_Days_During_Revenue_Delete_Async()
        {
            var revenue = RevenueDB[2];

            await Assert.ThrowsExceptionAsync<OperationException>(async () => await _revenueService.SaveRevenueAsync(revenue));
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Throw_Exception_When_Revenue_Does_Not_Exist_For_Specified_Id_During_Delete_Operation_Async()
        {
            var id = Guid.Empty;

            await Assert.ThrowsExceptionAsync<OperationException>(async () => await _revenueService.DeleteRevenueAsync(id));
        }
    }
}
