﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Services.CashFlow;
using NexTekk.Operations.WebApi.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Services.CashFlow
{
    [TestClass]
    public class AssetServiceTests
    {
        private static Mock<IAssetRepository> _assetRepository;
        private static Mock<IUserContext> _userContext;
        private static Mock<IAssetEmailSender> _assetEmailSender;

        private static Asset assetObject;

        private IAssetService _assetService;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            AutoMapperProfileConfig.Initialize();

            _assetRepository = new Mock<IAssetRepository>();
            _assetEmailSender = new Mock<IAssetEmailSender>();

            _assetRepository.Setup(x => x.GetAssetSummariesAsync(It.IsAny<AssetSearchCriteria>()))
                .ReturnsAsync((AssetSearchCriteria criteria) => Mapper.Map<IList<AssetSummary>>(AssetDB.Where(x => criteria.Item.Contains(x.Item) || criteria.Condition == x.Condition)));

            _assetRepository.Setup(x => x.Get(It.IsAny<Guid>()))
                .ReturnsAsync((Guid input) =>
                {
                    assetObject = AssetDB.FirstOrDefault(x => x.Id == input);
                    return assetObject;
                });

            _assetRepository.Setup(x => x.Create(It.IsAny<Asset>()))
                .ReturnsAsync((Asset input) => assetObject = input);
            
            _userContext = new Mock<IUserContext>();
            _userContext.Setup(x => x.GetUserId()).Returns(AssetDB.First().CreatedById);

            AutoMapperProfileConfig.Reset();
        }

        [TestCleanup]
        public void AfterEach()
        {
            AutoMapperProfileConfig.Reset();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            AutoMapperProfileConfig.Initialize();

            _assetService = new AssetService(
                                _assetRepository.Object,
                                _userContext.Object,
                                _assetEmailSender.Object
                                );
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Assets_Based_On_SearchCriteria()
        {
            _userContext.Setup(x => x.IsUserManager()).Returns(true);

            var criteria = new AssetSearchCriteria
            {
                Item = "Laptop"
            };

            var assets = await _assetService.GetAssetSummariesAsync(criteria);

            Assert.AreEqual(1, assets.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Manager_Can_Delete_Assets()
        {
            _userContext.Setup(x => x.IsUserManager()).Returns(true);

            var id = AssetDB.Last().Id;

            await _assetService.DeleteAsset(id);

            _assetRepository.Verify(x => x.Update(It.Is<Asset>(p => p.Id == AssetDB.First().Id)), Times.Once);
        }

        [ExpectedException(typeof(OperationException)), TestMethod, TestCategory(UnitTest)]
        public async Task Throws_Error_When_NonManager_Tries_To_Delete()
        {
            _userContext.Setup(x => x.IsUserManager()).Returns(false);

            var id = AssetDB.First().Id;

            await _assetService.DeleteAsset(id);
        }
        
        [ExpectedException(typeof(OperationException)), TestMethod, TestCategory(UnitTest)]
        public async Task Should_Throw_Error_When_NonAssigned_Or_NonManager_Tries_Edit()
        {
            _userContext.Setup(x => x.GetUserId()).Returns(Guid.Parse("2783B692-765B-4F94-A43B-BE614DD8EE20"));
            _userContext.Setup(x => x.IsUserManager()).Returns(false);

            var assetModel = AssetDB.First();
            
            assetModel.Cost = 762000;

            var assetEntity = await _assetService.SaveAssetAsync(assetModel);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Manager_Can_Edit_Asset()
        {
            _userContext.Setup(x => x.IsUserManager()).Returns(true);

            AssetDB.First().Cost = 68000;

            var assetEntity = await _assetService.SaveAssetAsync(AssetDB.First());

            Assert.AreEqual(assetEntity.Cost, AssetDB.First().Cost);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Assignee_Can_Change_Asset_Condition()
        {
            var asset = AssetDB[2];
            _userContext.Setup(x => x.IsUserManager()).Returns(false);
            _userContext.Setup(x => x.GetUserId()).Returns(asset.AssigneeId ?? default(Guid));
            
            asset.Condition = Condition.Bad;

            var assetEntity = await _assetService.SaveAssetAsync(asset);

            Assert.AreEqual(Condition.Bad, assetEntity.Condition);
        }
    }
}
