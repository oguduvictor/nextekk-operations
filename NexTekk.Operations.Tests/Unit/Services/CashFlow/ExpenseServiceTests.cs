﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.Services.CashFlow;
using NexTekk.Operations.WebApi.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Services.CashFlow
{
    [TestClass]
    public class ExpenseServiceTests
    {
        private static Mock<IExpenseRepository> _expenseRepository;
        private static Mock<IUserContext> _userContext;
        private static Mock<IExpenseEmailSender> _expenseEmailSender;

        private IExpenseService _expenseService;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            AutoMapperProfileConfig.Initialize();

            _expenseRepository = new Mock<IExpenseRepository>();

            _expenseRepository.Setup(x => x.Search(It.IsAny<ExpenseSearchCriteria>())).ReturnsAsync(ExpenseDB);
            _expenseRepository.Setup(x => x.Get(It.IsAny<Guid>()))
                .Returns((Guid input) => 
                {
                    return Task.FromResult(ExpenseDB.FirstOrDefault(x => x.Id == input));
                });

            _expenseRepository.Setup(x => x.Update(It.IsAny<IEnumerable<Expense>>()))
                               .ReturnsAsync((IEnumerable<Expense> entities) => ExpenseDB.Where(x => entities.Any(y => y.Id == x.Id)));

            _expenseRepository.Setup(x => x.Create(It.IsAny<Expense>()))
                              .ReturnsAsync((Expense entity) => ExpenseDB.FirstOrDefault(x => x.Description == entity.Description));

            _userContext = new Mock<IUserContext>();
            _userContext.Setup(x => x.GetUserId()).Returns(ExpenseDB.Last().Id);
            _userContext.Setup(x => x.IsUserManager()).Returns(true);

            AutoMapperProfileConfig.Reset(); ;
        }

        [TestInitialize]
        public void BeforeEach()
        {
            AutoMapperProfileConfig.Initialize();

            _expenseService = new ExpenseService(
                               _expenseRepository.Object,
                               _userContext.Object,
                               _expenseEmailSender.Object
                               );
        }

        [TestCleanup]
        public void AfterEach()
        {
            _userContext.Setup(x => x.GetUserId()).Returns(ExpenseDB.Last().Id);
            _userContext.Setup(x => x.IsUserManager()).Returns(true);

            AutoMapperProfileConfig.Reset();
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Expenses()
        {
            var criteria = new ExpenseSearchCriteria();

            var expenses = await _expenseService.GetAll(criteria);

            Assert.IsInstanceOfType(expenses, typeof(IEnumerable<Expense>));
            Assert.AreEqual(3, expenses.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_Expense()
        {
            var expense = ExpenseDB[1];

            await _expenseService.DeleteExpense(expense);

            _expenseRepository.Verify(x => x.Update(It.Is<Expense>(y => y.Id == expense.Id &&  y.Deleted)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Expense()
        {
            var expense = ExpenseDB[2];
            expense.Reason = "Spectranet Dues";
            expense.Currency = Currency.Naira;

            await _expenseService.SaveExpense(expense);

            _expenseRepository.Verify(x => x.Update(It.Is<Expense>(p => p.Reason == expense.Reason)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Create_New_Expense_Async()
        {
            var expense = new Expense
            {
                Currency = Currency.Dollar,
                Description = "Expense on purchase of fuel",
                IncurredById = ExpenseDB.Last().Id
            };

            var result = await _expenseService.SaveExpense(expense);

            Assert.AreEqual(result.Description, expense.Description);
        }

        [ExpectedException(typeof(OperationException)), TestMethod, TestCategory(UnitTest)]
        public async Task Adding_Expense_Should_Throw_Exception_When_LoggedIn_User_Is_Not_Manager_And_Is_Not_Incurrer_Of_Expense()
        {
            _userContext
               .Setup(x => x.IsUserManager())
               .Returns(false);

            var expense = ExpenseDB[1];

            await _expenseService.SaveExpense(expense);
        }
    }
}
