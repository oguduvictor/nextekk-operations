﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using NexTekk.Operations.Core.Models.Enums;

namespace NexTekk.Operations.Tests.Unit.Services
{
    [TestClass]
    public class TimeEntryServiceTests
    {
        private static Mock<ITimeEntryRepository> _timeEntryRepository;
        private static Mock<IUserContext> _userContext;
        private static Mock<IActivityRepository> _activityRepository;
        private static Mock<IUserRepository> _userRepository;
        private static Mock<IProjectRepository> _projectRepository;
        private static Mock<IReportService> _reportService;
        private static Mock<ITimeTrackingEmailSender> _emailSender;
        private static Mock<ILogger> _logger;

        private static Guid _userId = Guid.NewGuid();
        private static IEnumerable<ActivityEntity> _activityDB;
        private static IEnumerable<TimeEntryEntity> _timeEntryDB;

        private ITimeEntryService _service;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _emailSender = new Mock<ITimeTrackingEmailSender>();
            _reportService = new Mock<IReportService>();
            _logger = new Mock<ILogger>();
            _projectRepository = new Mock<IProjectRepository>();

            _activityDB = new List<ActivityEntity>
            {
                new ActivityEntity { Id = Guid.NewGuid(), Title = "Activity 1", Billable = false },
                new ActivityEntity { Id = Guid.NewGuid(), Title = "Project 1", Billable = true }
            };

            var activity = _activityDB.First();

            _timeEntryDB = new List<TimeEntryEntity>
            {
                new TimeEntryEntity
                {
                    Id = Guid.NewGuid(), Task = "Task1", Activity = activity, ActivityId = activity.Id, Status = TimeEntryStatus.Saved, CreatorId = _userId
                },
                new TimeEntryEntity
                {
                    Id = Guid.NewGuid(), Task = "Task2", Activity = activity, ActivityId = activity.Id, Status = TimeEntryStatus.Saved, CreatorId = _userId
                }
            };

            _userContext = new Mock<IUserContext>();
            _userContext.Setup(x => x.GetUserId()).Returns(_userId);

            _activityRepository = new Mock<IActivityRepository>();
            _activityRepository.Setup(x => x.GetAll(false)).ReturnsAsync(_activityDB);
            _activityRepository
                .Setup(x => x.Get(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(_activityDB.FirstOrDefault(x => x.Id == input));
                });

            _timeEntryRepository = new Mock<ITimeEntryRepository>();
            _timeEntryRepository.Setup(x => x.GetAll(It.IsAny<TimeEntrySearchCriteria>())).ReturnsAsync(_timeEntryDB);
            _timeEntryRepository
                .Setup(x => x.Get(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(_timeEntryDB.FirstOrDefault(x => x.Id == input));
                });
            _timeEntryRepository.Setup(x => x.GetAll(It.IsAny<IEnumerable<Guid>>())).ReturnsAsync(_timeEntryDB);
            
            _userRepository = new Mock<IUserRepository>();
            _userRepository
                .Setup(x => x.GetAll(null, It.IsAny<bool>()))
                .Returns((IEnumerable<Guid> input) =>
                {
                    return Task.FromResult(input.Select(x => new UserEntity(x, "John", "Smith")));
                });
            _userRepository
                .Setup(x => x.Get(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(new UserEntity(input, "John", "Smith"));
                });

            _reportService
                .Setup(x => x.GetTabularReport(It.IsAny<IEnumerable<TimeEntry>>(), It.IsAny<FileFormat>()))
                .Returns(new byte[6]);
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _service = new TimeEntryService(
                _userContext.Object, 
                _timeEntryRepository.Object,
                _activityRepository.Object, 
                _userRepository.Object,
                _projectRepository.Object,
                _reportService.Object, 
                _emailSender.Object,
                _logger.Object);
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Entries_For_SearchCriteria()
        {
            var search = new TimeEntrySearchCriteria { CreatorId = _userId };
            var result = await _service.GetTimeEntries(search);

            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Details_For_A_Time_Entry()
        {
            var timeEntryId = _timeEntryDB.FirstOrDefault().Id;
            var timeEntry = await _service.GetTimeEntry(timeEntryId);

            Assert.IsNotNull(timeEntry);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Activities()
        {
            var activities = await _service.GetAllActivities();

            Assert.AreEqual(2, activities.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Save_Time_Entry()
        {
            var activity = _activityDB.FirstOrDefault();
            var timeEntryRequest = new TimeEntry
            {
                Task = "Added Test",
                BeginDateTime = DateTime.UtcNow.AddHours(-1),
                EndDateTime = DateTime.UtcNow,
                Activity = new ActivitySummary(_activityDB.First())
            };

            var result = await _service.SaveTimeEntry(timeEntryRequest);

            Assert.IsInstanceOfType(result, typeof(TimeEntryDetail));
            Assert.AreEqual(result.Task, "Added Test");
            Assert.AreEqual(result.Activity.Id, activity.Id);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Time_Entry_Status()
        {
            var timeEntry = _timeEntryDB.First();
            
            var commentPairs = new List<IdCommentPair>
            {
                new IdCommentPair { Id = timeEntry.Id, Comment = "Time starts now" }
            };

            await _service.UpdateTimeEntryStatus(commentPairs, TimeEntryStatus.Submitted);

            _timeEntryRepository.Verify(x => x.Update(_timeEntryDB), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_TimeEntry()
        {
            var timeEntry = _timeEntryDB.First();
            var timeEntryIds = new List<Guid> { timeEntry.Id };

            await _service.DeleteTimeEntry(timeEntryIds);

            _timeEntryRepository.Verify(x => x.Delete(timeEntryIds), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        [ExpectedException(typeof(TimeEntryOverlapException))]
        public async Task Check_For_Overlapping_Times_When_Saving_Time_Entry_Throws_Exception()
        {
            var entry1 = _timeEntryDB.ElementAt(0);
            var entry2 = _timeEntryDB.ElementAt(1);

            entry1.BeginDateTime = new DateTime(2017, 2, 1, 9, 0, 0);
            entry1.EndDateTime = new DateTime(2017, 2, 1, 10, 0, 0); 

            entry2.BeginDateTime = new DateTime(2017, 2, 1, 11, 0, 0);
            entry2.EndDateTime = new DateTime(2017, 2, 1, 13, 0, 0);

            var activity = _activityDB.FirstOrDefault();

            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 8, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 11, 0, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Check_For_Overlapping_Times_When_Saving_TimeEntry_Throws_Doesnt_Exception()
        {
            var entry1 = _timeEntryDB.ElementAt(0);
            var entry2 = _timeEntryDB.ElementAt(1);

            entry1.BeginDateTime = new DateTime(2017, 2, 1, 9, 0, 0);
            entry1.EndDateTime = new DateTime(2017, 2, 1, 10, 0, 0);

            entry2.BeginDateTime = new DateTime(2017, 2, 1, 11, 0, 0);
            entry2.EndDateTime = new DateTime(2017, 2, 1, 13, 0, 0);

            var activity = _activityDB.FirstOrDefault();

            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 8, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 9, 0, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry);
        }

        [TestMethod, TestCategory(UnitTest)]
        [ExpectedException(typeof(TimeEntryOverlapException))]
        public async Task Check_For_Overlapping_Time_When_Updating_TimeEntry_Throws_Exception()
        {
            var entry1 = _timeEntryDB.ElementAt(0);
            var entry2 = _timeEntryDB.ElementAt(1);

            var activity = _activityDB.FirstOrDefault();

            entry1.BeginDateTime = new DateTime(2017, 2, 1, 9, 0, 0);
            entry1.EndDateTime = new DateTime(2017, 2, 1, 10, 0, 0);

            entry2.BeginDateTime = new DateTime(2017, 2, 1, 11, 25, 0);
            entry2.EndDateTime = new DateTime(2017, 2, 1, 13, 0, 0);

            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 11, 34, 0),
                EndDateTime = new DateTime(2017, 2, 1, 14, 50, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry, entry1.Id);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Check_For_Overlapping_Time_When_Updating_TimmeEntry_Doesnt_Throw_Exception()
        {
            var entry1 = _timeEntryDB.ElementAt(0);
            var entry2 = _timeEntryDB.ElementAt(1);

            var activity = _activityDB.FirstOrDefault();

            entry1.BeginDateTime = new DateTime(2017, 2, 1, 9, 0, 0);
            entry1.EndDateTime = new DateTime(2017, 2, 1, 10, 0, 0);

            entry2.BeginDateTime = new DateTime(2017, 2, 1, 11, 25, 0);
            entry2.EndDateTime = new DateTime(2017, 2, 1, 13, 0, 0);

            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 10, 34, 0),
                EndDateTime = new DateTime(2017, 2, 1, 11, 20, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry, entry1.Id);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Send_Reminder_Emails()
        {
            var userWithoutSubmission = new List<UserEntity>();
            var dateRange = new DateTimeRange
            {
                BeginDateTime = new DateTime(),
                EndDateTime = new DateTime()
            };
            
            await _service.SendReminderEmail(dateRange);

            _emailSender.Verify(x => x.SendLateSubmissionReminder(userWithoutSubmission, dateRange), Times.Once); 
        }

        [TestMethod, TestCategory(UnitTest)]
        [ExpectedException(typeof(InvalidDateRangeException))]
        public async Task Throws_Exception_If_Time_Range_Is_Less_Than_15_Minutes()
        {
            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 10, 14, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Doesnt_Throw_Exception_If_Time_Range_Is_Greater_Than_15_Minutes()
        {
            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 10, 20, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Doesnt_Throws_Exception_If_Time_Range_Is_Equal_To_15_Minutes()
        {
            var newTimeEntry = new TimeEntry
            {
                Task = "new task",
                BeginDateTime = new DateTime(2017, 2, 1, 10, 0, 0),
                EndDateTime = new DateTime(2017, 2, 1, 10, 15, 0),
                Activity = new ActivitySummary(_activityDB.First())
            };

            await _service.SaveTimeEntry(newTimeEntry);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Tabular_Report()
        {
            var searchCriteria = new TimeEntrySearchCriteria();
            var fileFormat = FileFormat.CSV;
            var timeEntries = await _service.GetTimeEntries(searchCriteria);

            var result  = await _service.GetTabularReport(searchCriteria, fileFormat);

            Assert.IsNotNull(result);

        }
    }
}