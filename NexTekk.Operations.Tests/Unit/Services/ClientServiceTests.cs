﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Services
{
    [TestClass]
    public class ClientServiceTests
    {
        private static Mock<IClientRepository> _clientRepository;
        private static Mock<IUserContext> _userContext;

        private IClientService _clientService;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _clientRepository = new Mock<IClientRepository>();
            _userContext = new Mock<IUserContext>();

            _clientRepository.Setup(x => x.GetAll(false)).ReturnsAsync(ClientDB);
            _clientRepository
                .Setup(x => x.Get(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(ClientDB.FirstOrDefault(x => x.Id == input));
                });
            _clientRepository.Setup(x => x.GetClientSummaries(false)).ReturnsAsync(ClientDB);
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _clientService = new ClientService(
                _clientRepository.Object,
                _userContext.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Clients()
        {
            var clients = await _clientService.GetClients();

            Assert.IsInstanceOfType(clients, typeof(IEnumerable<Client>));
            Assert.AreEqual(2, clients.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_ClientSummaries()
        {
            var clients = await _clientService.GetClientSummaries();

            Assert.IsInstanceOfType(clients, typeof(IEnumerable<ClientSummary>));
            Assert.AreEqual(2, clients.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Particular_Client()
        {
            var id = ClientDB.First().Id;
            var client = await _clientService.GetClient(id);

            Assert.IsNotNull(client);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Add_Contacts_To_Existing_Client()
        {
            var client = new Client(ClientDB.Last());
            client.Contacts = new List<ContactSummary>
            {
                new ContactSummary(ContactDB.Last()),
            };

            await _clientService.SaveClient(client);

            _clientRepository.Verify(x => x.SaveContacts(It.Is<IEnumerable<ClientsContactsEntity>>(p => p.First().ContactId == ContactDB.Last().Id)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Save_Contacts()
        {
            var model = new List<ClientsContactsEntity>
            {
                new ClientsContactsEntity { ClientId = ClientDB.First().Id, ContactId = ContactDB.First().Id },
                new ClientsContactsEntity { ClientId = ClientDB.First().Id, ContactId = ContactDB.Last().Id }
            };

            await _clientService.SaveContacts(model);

            _clientRepository.Verify(x => x.SaveContacts(
                It.Is<IEnumerable<ClientsContactsEntity>>(
                    p => p.First().ContactId == ContactDB.First().Id)),
                    Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Save_A_New_Client_Without_Contacts()
        {
            var model = new Client
            {
                Name = "Microsoft"
            };

            var result = await _clientService.SaveClient(model);

            Assert.IsInstanceOfType(result, typeof(Client));
            Assert.AreEqual(result.Name, "Microsoft");
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Create_New_Client_With_Contacts()
        {
            var model = new Client
            {
                Name = "Apple",
                Contacts = new List<ContactSummary>
                {
                    new ContactSummary(ContactDB.ElementAt(1))
                }
            };

            await _clientService.SaveClient(model);

            _clientRepository.Verify(x => x.Create(It.Is<ClientEntity>(p => p.Name == "Apple")), Times.Once);
            _clientRepository.Verify(x => x.SaveContacts(
                It.Is<IEnumerable<ClientsContactsEntity>>(
                    p => p.First().ContactId == ContactDB.ElementAt(1).Id)),
                    Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_An_Existing_Client_Without_Adding_Contacts()
        {
            var model = new Client(ClientDB.First());
            model.Name = "NexTekk";

            var result = await _clientService.SaveClient(model);

            _clientRepository.Verify(x => x.Update(It.Is<ClientEntity>(p => p.Name == "NexTekk")), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Do_Soft_Delete_For_Client()
        {
            var client = ClientDB.First();

            await _clientService.DeleteClient(client.Id);

            _clientRepository.Verify(x => x.HasProjects(client.Id), Times.Once);
            _clientRepository.Verify(x => x.Update(It.Is<ClientEntity>(p => p.Deleted == true)), Times.Once);
        }
    }
}
