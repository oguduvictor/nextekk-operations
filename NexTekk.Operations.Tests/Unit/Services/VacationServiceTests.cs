﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Data.Sql.Repositories;
using NexTekk.Operations.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.MockData;
using static NexTekk.Operations.Tests.Helpers.Constants;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Tests.Unit.Services
{
    [TestClass]
    public class VacationServiceTests
    {
        private static  Mock<IVacationRepository> _vacationRepository;
        private static Mock<IUserContext> _userContext;
        private static Mock<ITimeEntryService> _timeEntryService;

        private IVacationService _vacationService;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _userContext = new Mock<IUserContext>();

            _userContext
                .Setup(x => x.IsUserManager())
                .Returns(() => true);

            _userContext.Setup(x => x.GetUserId())
                .Returns(() =>
                {
                    return VacationDB.FirstOrDefault().UserId;
                });


            _timeEntryService = new Mock<ITimeEntryService>();

            _timeEntryService
                .Setup(x => x.GetTimeEntries(It.IsAny< TimeEntrySearchCriteria>()))
                .Returns((TimeEntrySearchCriteria searchCriteria) => 
                {
                    var result = new List<TimeEntry> { new TimeEntry() };
                    return Task.FromResult(new List<TimeEntry> { new TimeEntry() }.AsEnumerable());
                });

            _vacationRepository = new Mock<IVacationRepository>();

            _vacationRepository
                .Setup(x => x.Get(It.IsAny<Guid>(), It.IsAny<int>()))
                .Returns((Guid userId, int year) =>
                {
                    return Task.FromResult(VacationDB.FirstOrDefault(x => x.UserId == userId));
                });
            _vacationRepository
                .Setup(x => x.GetAll(It.IsAny<int>()))
                .Returns((int input) => 
                {
                    return Task.FromResult(VacationDB.Where(x => x.Year == input));
                });

            _vacationRepository
                .Setup(x => x.GetSetting(It.IsAny<Guid>()))
                .Returns((Guid input) => 
                {
                    return Task.FromResult(VacationSettingDB.FirstOrDefault(x => x.UserId == input));
                });
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _vacationService = new VacationService(_userContext.Object, _timeEntryService.Object, _vacationRepository.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Saved_Vacation()
        {
            var user = VacationDB.First();
            var savedVacation = await _vacationService.GetVacation(user.UserId, 2016);

            Assert.AreEqual(user.UserId, savedVacation.Id);

            _vacationRepository.Verify(x => x.Get(user.UserId, 2016), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Vacations_For_A_Given_Year()
        {
            var allVacations = await _vacationService.GetVacations(2016);

            Assert.AreEqual(1, allVacations.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Gets_All_Vacations_For_The_Current_Year()
        {
            var allVacations = await _vacationService.GetVacations(0);
            Assert.AreEqual(1, allVacations.Count());

            var currentYear = DateTime.Now.Year;

            _vacationRepository.Verify(x => x.GetAll(currentYear), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Vacation_Time_Entries()
        {
            var id = Guid.Parse("2A843B5E-D1A1-4D59-8C89-9081F6F1DB39");
            var timeEntries = await _vacationService.GetVacationTimeEntries(id, 2016);

            _timeEntryService.Verify(x => x.GetTimeEntries(It.Is<TimeEntrySearchCriteria>(y => y.CreatorId == id)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Hours_Per_Period()
        {
            var id = Guid.Parse("2A843B5E-D1A1-4D59-8C89-9081F6F1DB39");
            var result = await _vacationService.UpdateHoursPerPeriod(id, 6.0m);

            Assert.AreEqual(6.0m, result);

            _vacationRepository.Verify(x => x.GetSetting(id), Times.Once);

            _vacationRepository.Verify(x => x.Update(It.Is<VacationSetting>(y => y.HoursPerPeriod == 6.0m)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Update_Hours_Per_Period_Should_Create_New_Vacation_If_Setting_Does_Not_Exist()
        {
            var id = Guid.NewGuid();

            var result = await _vacationService.UpdateHoursPerPeriod(id, 6.7m);

            _vacationRepository
                .Verify(x => x.Create(It.Is<VacationSetting>(y => y.UserId == id && y.HoursPerPeriod == 6.7m)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Year_Begin_Balance()
        {
            var id = Guid.Parse("2A843B5E-D1A1-4D59-8C89-9081F6F1DB39");

            var result = await _vacationService.UpdateYearBeginBalance(id, 4.32m);

            _vacationRepository.Verify(x => x.GetSetting(id));

            _vacationRepository.Verify(x => x.Update(It.Is<VacationSetting>(y => y.YearBeginBalance == 4.32m)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Update_Year_Begin_Balance_Creates_New_Vacation_If_Setting_Does_Not_Exist()
        {
            var id = Guid.NewGuid();

            var result = await _vacationService.UpdateYearBeginBalance(id, 1.27m);

            _vacationRepository
                .Verify(x => x.Create(It.Is<VacationSetting>(y => y.UserId == id && y.YearBeginBalance == 1.27m)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Save_New_Vacation()
        {
            var id = Guid.NewGuid();

            var result = await _vacationService.SaveVacation(id, 8.0m, 6.5m);

            _vacationRepository
                .Verify(x => x.Create(It.Is<VacationSetting>(y => y.UserId == id && y.HoursPerPeriod == 8.0m && y.YearBeginBalance == 6.5m)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Saved_Vacation()
        {
            var id = Guid.Parse("2A843B5E-D1A1-4D59-8C89-9081F6F1DB39");

            var result = await _vacationService.SaveVacation(id, 8.30m, 6.15m);

            _vacationRepository
                .Verify(x => x.Update(It.Is<VacationSetting>(y => y.UserId == id && y.HoursPerPeriod == 8.30m && y.YearBeginBalance == 6.15m)), Times.Once);
        }
    }
}
