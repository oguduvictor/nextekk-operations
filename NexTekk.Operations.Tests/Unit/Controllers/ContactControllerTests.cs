﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class ContactControllerTests
    {
        private static Mock<IContactService> _contactService;
        private static ContactController _contactController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _contactService = new Mock<IContactService>();
            _contactService
                .Setup(x => x.GetContacts())
                .ReturnsAsync(ContactDB.Select(x => new Contact(x)));
            _contactService
                .Setup(x => x.GetContact(It.IsAny<int>()))
                .Returns((int input) =>
                {
                    return Task.FromResult(ContactDB.Select(x => new ContactDetail(x)).FirstOrDefault(x => x.Id == input));
                });
            _contactService
                .Setup(x => x.GetContactSummaries())
                .ReturnsAsync(ContactDB.Select(x => new ContactSummary(x)));
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _contactController = new ContactController(_contactService.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_List_Of_Contacts()
        {
            var result = await _contactController.GetAll();

            Assert.IsInstanceOfType(result, typeof(IEnumerable<Contact>));
            Assert.AreEqual(3, result.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_List_Of_ContactSummaries()
        {
            var result = await _contactController.GetContactSummaries();

            Assert.IsInstanceOfType(result, typeof(IEnumerable<ContactSummary>));
            Assert.AreEqual(3, result.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Specific_Contact()
        {
            var id = ContactDB.First().Id;
            var result = await _contactController.Get(id);

            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Create_Contact()
        {
            var model = new ContactViewModel { FirstName = "Tom", LastName = "Cat" };

            await _contactController.Create(model);

            _contactService.Verify(x => x.SaveContact(It.Is<ContactEntity>(p => p.FirstName == "Tom")), Times.Once);
            _contactService.Verify(x => x.SaveContact(It.Is<ContactEntity>(p => p.LastName == "Cat")), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_Contact()
        {
            var id = ContactDB.First().Id;

            await _contactController.Delete(id);

            _contactService.Verify(x => x.DeleteContact(id), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Contact()
        {
            var model = new ContactViewModel { FirstName = "Jerry", LastName = "Tom" };
            var id = ContactDB.First().Id;

            await _contactController.Update(id, model);

            _contactService.Verify(x => x.SaveContact(It.Is<ContactEntity>(p => p.FirstName == "Jerry")), Times.Once);
            _contactService.Verify(x => x.SaveContact(It.Is<ContactEntity>(p => p.LastName == "Tom")), Times.Once);
        }
    }
}
