﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.WebApi.App_Start;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class RevenueControllerTests
    {
        private static Mock<IRevenueService> _revenueService;
        private static Mock<IUserLookupService> _userLookupService;
        private static Mock<IClientLookupService> _clientLookupService;
        private RevenuesController _revenueController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            AutoMapperProfileConfig.Initialize();

            _revenueService = new Mock<IRevenueService>();
            _userLookupService = new Mock<IUserLookupService>();
            _clientLookupService = new Mock<IClientLookupService>();

            _revenueService.Setup(x => x.GetRevenuesAsync(It.IsAny<RevenueSearchCriteria>()))
                .ReturnsAsync(Mapper.Map<IList<Revenue>>(RevenueDB));
            _revenueService.Setup(x => x.GetRevenueAsync(It.IsAny<Guid>()))
                .ReturnsAsync((Guid input) => Mapper.Map<Revenue>(RevenueDB.FirstOrDefault(y => y.Id == input)));

            AutoMapperProfileConfig.Reset();
        }

        [TestCleanup]
        public void AfterEach()
        {
            AutoMapperProfileConfig.Reset();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            AutoMapperProfileConfig.Initialize();

            _revenueController = new RevenuesController(
                _revenueService.Object,
                _userLookupService.Object,
                _clientLookupService.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_List_Of_Revenue_Based_On_Search_Criteria_Async()
        {
            var searchCriteria = new RevenueSearchCriteriaModel();

            var result = await _revenueController.GetAll(searchCriteria);
            
            Assert.AreEqual(result.Count(), 3);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Be_Able_To_Create_Revenue_Async()
        {
            var expectedResult = default(Revenue);

            _revenueService.Setup(x => x.SaveRevenueAsync(It.IsAny<Revenue>()))
                .ReturnsAsync((Revenue input) =>
                {
                    expectedResult = input;
                    return Mapper.Map<Revenue>(RevenueDB.FirstOrDefault(y => y.ClientId == input.ClientId));
                });

            var viewModel = new SaveRevenueModel
            {
                Amount = 20190,
                Description = "New Revenue",
                Currency = Currency.Dollar,
                PaymentDate = DateTime.Today,
                ClientId = ClientDB[0].Id
            };

            await _revenueController.Create(viewModel);
            
            Assert.AreEqual(expectedResult.Amount, viewModel.Amount);
            Assert.AreEqual(expectedResult.Currency, viewModel.Currency);
            Assert.AreEqual(expectedResult.Description, viewModel.Description);
            Assert.AreEqual(expectedResult.PaymentDate, viewModel.PaymentDate);
            Assert.AreEqual(expectedResult.ClientId, viewModel.ClientId);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Be_Able_To_Update_Revenue_Async()
        {
            var expectedResult = default(Revenue);

            _revenueService.Setup(x => x.SaveRevenueAsync(It.IsAny<Revenue>()))
                .ReturnsAsync((Revenue input) =>
                {
                    expectedResult = input;
                    return Mapper.Map<Revenue>(RevenueDB.FirstOrDefault(y => y.ClientId == input.ClientId));
                });

            var viewModel = new SaveRevenueModel
            {
                Amount = 3000,
                Description = "Latest Changes",
                Currency = Currency.Dollar,
                PaymentDate = DateTime.Today,
                ClientId = ClientDB[1].Id
            };

            var id = RevenueDB[0].Id;

            await _revenueController.Update(id, viewModel);

            Assert.AreEqual(expectedResult.Id, id);
            Assert.AreEqual(expectedResult.Amount, viewModel.Amount);
            Assert.AreEqual(expectedResult.Currency, viewModel.Currency);
            Assert.AreEqual(expectedResult.Description, viewModel.Description);
            Assert.AreEqual(expectedResult.PaymentDate, viewModel.PaymentDate);
            Assert.AreEqual(expectedResult.ClientId, viewModel.ClientId);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Be_Able_To_Delete_Revenue_Async()
        {
            var id = RevenueDB[0].Id;

            await _revenueController.Delete(id);

            _revenueService.Verify(x => x.DeleteRevenueAsync(id), Times.Once);
        }
    }
}
