﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.WebApi.App_Start;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class AssetsControllerTests
    {
        private static Mock<IAssetService> _assetService;
        private static Mock<IUserLookupService> _userLookupService;

        private AssetsController _assetController;
        private static Asset assetResult;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            AutoMapperProfileConfig.Initialize();

            _assetService = new Mock<IAssetService>();
            _userLookupService = new Mock<IUserLookupService>();

            _assetService.Setup(x => x.GetAssetSummariesAsync(It.IsAny<AssetSearchCriteria>()))
                .ReturnsAsync(AssetDB);

            _assetService.Setup(x => x.Get(It.IsAny<Guid>()))
                .ReturnsAsync((Guid id) => AssetDB.FirstOrDefault(x => x.Id == AssetDB.First().Id));

            _assetService.Setup(x => x.SaveAssetAsync(It.IsAny<Asset>()))
                .ReturnsAsync((Asset asset) => assetResult = asset);

            AutoMapperProfileConfig.Reset();
        }

        [TestCleanup]
        public void AfterEach()
        {
            AutoMapperProfileConfig.Reset();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            AutoMapperProfileConfig.Initialize();

            _assetController = new AssetsController(_assetService.Object, _userLookupService.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_List_Of_Asset_Based_On_Search_Criteria_Async()
        {
            var asset = await _assetController.Search(new AssetSearchCriteriaModel { Condition = Condition.New });

            Assert.AreEqual(asset.Count(), 3);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_Asset_Details_Async()
        {
            var assetDetail = await _assetController.Get(AssetDB.First().Id);
            var actualResult = AssetDB.First();

            Assert.AreEqual(assetDetail.Id, actualResult.Id);
            Assert.AreEqual(assetDetail.Item, actualResult.Item);
            Assert.AreEqual(assetDetail.Model, actualResult.Model);
            Assert.AreEqual(assetDetail.PurchasedDate, actualResult.PurchasedDate);
            Assert.AreEqual(assetDetail.Category, actualResult.Category);
            Assert.AreEqual(assetDetail.Cost, actualResult.Cost);
            Assert.AreEqual(assetDetail.Currency, actualResult.Currency);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Create_Asset_Async()
        {
            var assetModel = new SaveAssetModel { Item = "Monitor", Manufacturer = "Dell" };
            var asset = await _assetController.Create(assetModel);

            _assetService.Verify(x => x.SaveAssetAsync(It.Is<Asset>(p => p.Item == assetModel.Item)), Times.Once);

            Assert.AreEqual(assetResult.Item, assetModel.Item);
            Assert.AreEqual(assetResult.Manufacturer, assetModel.Manufacturer);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Update_Asset_Async()
        {
            var assetModel = new SaveAssetModel { Item = "Laptop", Model = "Inspiron", Manufacturer = "HP" };
            var id = AssetDB.First().Id;

            await _assetController.Update(id, assetModel);

            _assetService.Verify(x => x.SaveAssetAsync(It.Is<Asset>(p => p.Item == assetModel.Item)), Times.Once);

            Assert.AreEqual(assetResult.Id, id);
            Assert.AreEqual(assetResult.Item, assetModel.Item);
            Assert.AreEqual(assetResult.Model, assetModel.Model);
            Assert.AreEqual(assetResult.Manufacturer, assetModel.Manufacturer);
        }
    }
}
