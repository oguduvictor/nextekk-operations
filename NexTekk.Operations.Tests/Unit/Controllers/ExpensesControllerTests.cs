﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.WebApi.App_Start;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class ExpenseControllerTests
    {
        private static Mock<IExpenseService> _expenseService;
        private static Mock<IUserLookupService> _userLookupService;
        private ExpensesController _expenseController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            AutoMapperProfileConfig.Initialize();

            _expenseService = new Mock<IExpenseService>();
            _userLookupService = new Mock<IUserLookupService>();

            _expenseService.Setup(x => x.GetAll(It.IsAny<ExpenseSearchCriteria>()))
                .ReturnsAsync(Mapper.Map<IEnumerable<Expense>>(ExpenseDB));
            _expenseService.Setup(x => x.SaveExpense(It.IsAny<Expense>()))
                .ReturnsAsync((Expense expense) => Mapper.Map<Expense>(ExpenseDB.FirstOrDefault(y => y.Id == expense.Id)));
            _expenseService.Setup(x => x.Get(It.IsAny<Guid>()))
                .ReturnsAsync((Guid expense) => Mapper.Map<Expense>(ExpenseDB.FirstOrDefault(y => y.Id == expense)));

            AutoMapperProfileConfig.Reset();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            AutoMapperProfileConfig.Initialize();

            _expenseController = new ExpensesController(
                _expenseService.Object,
                _userLookupService.Object);
        }

        [TestCleanup]
        public void AfterEach()
        {
            AutoMapperProfileConfig.Reset();
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Get_List_Of_Expense_Based_On_Search_Criteria_Async()
        {
            var searchCriteria = new ExpenseSearchCriteriaModel();

            var result = await _expenseController.GetAll(searchCriteria);
            
            Assert.AreEqual(result.Count(), 3);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Be_Able_To_Create_Expense_Async()
        {
            var viewModel = new SaveExpenseModel
            {
                Amount = 20190,
                Description = "New Expense",
                Currency = Currency.Dollar,
                Date = DateTime.Today
            };

            await _expenseController.Create(viewModel);

            _expenseService.Verify(x => x.SaveExpense(It.Is<Expense>(p => p.Amount == viewModel.Amount)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Be_Able_To_Update_Expense_Async()
        {
            var viewModel = new SaveExpenseModel
            {
                Amount = 3000,
                Description = "Latest Changes",
                Currency = Currency.Dollar,
                Date = DateTime.Today
            };

            var id = ExpenseDB[1].Id;

            await _expenseController.Update(id, viewModel);

            _expenseService.Verify(x => x.SaveExpense(It.Is<Expense>(p => p.Id == id && p.Amount == viewModel.Amount)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Be_Able_To_Delete_Expense_Async()
        {
            var id = ExpenseDB[0].Id;

            await _expenseController.Delete(id);

            _expenseService.Verify(x => x.DeleteExpense(It.Is<Expense>(y => y.Id == id)), Times.Once);
        }
    }
}
