﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class ClientControllerTests
    {
        private static Mock<IClientService> _clientService;
        private static ClientController _clientController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _clientService = new Mock<IClientService>();
            _clientService
                .Setup(x => x.GetClients())
                .ReturnsAsync(ClientDB.Select(x => new ClientDetail(x)));
            _clientService
                .Setup(x => x.GetClient(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(ClientDB.Select(x => new ClientDetail(x)).FirstOrDefault(x => x.Id == input));
                });
            _clientService
                .Setup(x => x.GetClientSummaries())
                .ReturnsAsync(ClientDB.Select(x => new ClientSummary(x)));
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _clientController = new ClientController(_clientService.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_List_Of_Clients()
        {
            var result = await _clientController.GetAll();

            Assert.IsInstanceOfType(result, typeof(IEnumerable<Client>));
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_List_Of_All_ClientSummaries()
        {
            var result = await _clientController.GetClientSummaries();
            Assert.IsInstanceOfType(result, typeof(IEnumerable<ClientSummary>));
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Specific_Client()
        {
            var id = ClientDB.First().Id;
            var result = await _clientController.Get(id);

            Assert.IsNotNull(result);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Create_Client()
        {
            var model = new ClientViewModel { Name = "Dell" };

            await _clientController.Create(model);

            _clientService.Verify(x => x.SaveClient(It.Is<Client>(p => p.Name == "Dell")), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Client()
        {
            var model = new ClientViewModel { Name = "Microsoft", ContactIds = new List<int>{ 1, 4 } };
            var id = ClientDB.First().Id;

            await _clientController.Update(id, model);

            _clientService.Verify(x => x.SaveClient(It.Is<Client>(p => p.Name == "Microsoft")), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_Client()
        {
            var id = ClientDB.First().Id;

            await _clientController.Delete(id);

            _clientService.Verify(x => x.DeleteClient(id), Times.Once);
        }
    }
}
