﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Services;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class ActivityControllerTests
    {
        private static Mock<IActivityService> _activityService;

        private static Guid _userId = Guid.NewGuid();
        private static IEnumerable<ActivityEntity> _activityDB;
        private static ActivityController _activityController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _activityDB = new List<ActivityEntity>
            {
                new ActivityEntity { Id = Guid.NewGuid(), Title = "Activity 1", Billable = false },
                new ActivityEntity { Id = Guid.NewGuid(), Title = "Project 1", Billable = true }
            };
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _activityService = new Mock<IActivityService>();
            _activityService.Setup(x => x.GetActivities()).ReturnsAsync(_activityDB.Select(x => new Activity(x)));
            _activityService
                .Setup(x => x.GetActivityById(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    var entity = _activityDB.FirstOrDefault(x => x.Id == input);
                    var activity = entity == null ? null : new ActivityDetail(entity);

                    return Task.FromResult(activity);
                });

            _activityController = new ActivityController(_activityService.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_List_Of_All_Activities()
        {
            var response  = await _activityController.Get();

            Assert.AreEqual(2, response.Count());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Activity_By_Id()
        {
            var id = _activityDB.First().Id;
            var response = await _activityController.Get(id);

            Assert.IsNotNull(response);
            Assert.AreEqual("Activity 1", response.Title);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Create_A_New_Activity()
        {
            var activity = new ActivityModel();
            var response = await _activityController.Post(activity);

            _activityService.Verify(x => x.SaveActivity(It.IsAny<ActivityEntity>()), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_An_Activity()
        {
            var activity = new ActivityModel();
            var response = await _activityController.Post(activity);

            _activityService.Verify(x => x.SaveActivity(It.IsAny<ActivityEntity>()), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_An_Activity()
        { 
            var id = _activityDB.First().Id;

            await _activityController.Delete(id);

            _activityService.Verify(x => x.DeleteActivity(id), Times.Once);   
        }
    }
}