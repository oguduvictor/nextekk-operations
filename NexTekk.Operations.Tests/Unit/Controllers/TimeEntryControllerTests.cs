﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class TimeEntryControllerTests
    {
        private static Mock<ITimeEntryService> _timeEntryService;
        private static Mock<IUserContext> _userContext;
        private static Mock<ILogger> _logger;
        private static IEnumerable<ActivitySummary> _activityDB;
        private static IEnumerable<TimeEntry> _timeEntryDB;
        private static IEnumerable<TimeEntryDetail> _timeEntryDetailDB;
        private TimeEntryController _timeEntryController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _activityDB = new List<ActivitySummary>
            {
                new ActivitySummary { Id = Guid.NewGuid(), Title = "Activity 1", Billable = false },
                new ActivitySummary { Id = Guid.NewGuid(), Title = "Project 1", Billable = true }
            };

            var activity = _activityDB.First();

            _timeEntryDB = new List<TimeEntry>
            {
                new TimeEntry { Id = Guid.NewGuid(), Task = "Task1", Activity = activity },
                new TimeEntry { Id = Guid.NewGuid(), Task = "Task2", Activity = activity }
            };

            _timeEntryDetailDB = new List<TimeEntryDetail>
            {
                new TimeEntryDetail { Id = Guid.NewGuid(), Task = "Task1", Activity = activity },
                new TimeEntryDetail { Id = Guid.NewGuid(), Task = "Task2", Activity = activity }
            };

            _timeEntryService = new Mock<ITimeEntryService>();
            _timeEntryService.Setup(x => x.GetTimeEntries(It.IsAny<TimeEntrySearchCriteria>())).ReturnsAsync(_timeEntryDB);
            _timeEntryService
                .Setup(x => x.GetTimeEntry(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(_timeEntryDetailDB.FirstOrDefault(x => x.Id == input));
                });
            _timeEntryService
                .Setup(x => x.SaveTimeEntry(It.IsAny<TimeEntry>(), null))
                .Returns(Task.FromResult(_timeEntryDetailDB.FirstOrDefault()));
            _timeEntryService.Setup(x => x.GetAllActivities(null)).ReturnsAsync(_activityDB);
            _timeEntryService.Setup(x => x.GetAllActivities(It.IsAny<Guid>())).ReturnsAsync(_activityDB);
            

            _userContext = new Mock<IUserContext>();
            _userContext.Setup(x => x.GetUserId()).Returns(Guid.NewGuid());

            _logger = new Mock<ILogger>();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _timeEntryController = new TimeEntryController(_timeEntryService.Object, _userContext.Object, _logger.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Search_TimeEntries_With_SearchCriteria()
        {
            var timeEntrySearchCriteria = new TimeEntrySearchCriteria();
            var response = await _timeEntryController.Search(timeEntrySearchCriteria);
            
            _timeEntryService.Verify(x => x.GetTimeEntries(It.IsAny<TimeEntrySearchCriteria>()), Times.Once);

            Assert.IsNotNull(response);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Search_TimeEntry_With_Id()
        {
            var id = _timeEntryDetailDB.First().Id;
            var response = await _timeEntryController.Get(id);

            Assert.IsNotNull(response);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_All_Activities()
        {
            var response = await _timeEntryController.AllActivities();

            Assert.AreEqual(2, response.Count());
            _timeEntryService.Verify(x => x.GetAllActivities(null), Times.Once);         
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Create_New_TimeEntry()
        {
            var timeEntryViewModel = new TimeEntryViewModel
            {
                Task = "Task1",
                ActivityId = _activityDB.First().Id
            };

            var response = await _timeEntryController.Create(timeEntryViewModel);

            Assert.AreEqual(response.Task, "Task1");
            Assert.AreEqual(response.Activity, _activityDB.First());
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Set_The_Status_Of_Time_Entry_To_Submitted()
        {
            var id = _timeEntryDB.First().Id;

            var idCommentPairs = new IdCommentPair[]
            {
                new IdCommentPair { Id = id, Comment = "this is the first comment" }
            };

            await _timeEntryController.Submit(idCommentPairs);

            _timeEntryService.Verify(x => x.UpdateTimeEntryStatus(idCommentPairs, TimeEntryStatus.Submitted), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Set_The_Status_Of_Time_Entry_To_Saved()
        {
            var id = _timeEntryDB.First().Id;

            var idCommentPairs = new IdCommentPair[]
            {
                new IdCommentPair { Id = id, Comment = "this is the first comment" }
            };

            await _timeEntryController.Unsubmit(idCommentPairs);

            _timeEntryService.Verify(x => x.UpdateTimeEntryStatus(idCommentPairs, TimeEntryStatus.Saved), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Set_The_Status_Of_Time_Entry_To_Denied()
        {
            var id = _timeEntryDB.First().Id;

            var idCommentPairs = new IdCommentPair[]
            {
                new IdCommentPair { Id = id, Comment = "this is the first comment" }
            };

            await _timeEntryController.Deny(idCommentPairs);

            _timeEntryService.Verify(x => x.UpdateTimeEntryStatus(idCommentPairs, TimeEntryStatus.Denied), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Set_The_Status_Of_Time_Entry_To_Approved()
        {
            var id = _timeEntryDB.First().Id;

            var idCommentPairs = new IdCommentPair[]
            {
                new IdCommentPair { Id = id, Comment = "this is the first comment" }
            };

            await _timeEntryController.Approve(idCommentPairs);

            _timeEntryService.Verify(x => x.UpdateTimeEntryStatus(idCommentPairs, TimeEntryStatus.Approved), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_TimeEntry_From_Database()
        {
            var id = _timeEntryDB.First().Id;

            await _timeEntryController.Delete(id);
            
            _timeEntryService.Verify(x => x.DeleteTimeEntry(new Guid[] { id }), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Send_Reminder_Email()
        {
            var dateRange = new DateTimeRange
            {
                BeginDateTime = new DateTime(),
                EndDateTime = new DateTime()
            };
            
            await _timeEntryController.SendReminderEmail(dateRange);

            _timeEntryService.Verify(x => x.SendReminderEmail(dateRange), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_User_Activities()
        {
            var result = await _timeEntryController.UserActivities();

            Assert.AreEqual(result.Count(), 2);
        }
    }
}
