﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using NexTekk.Operations.WebApi.Controllers;
using NexTekk.Operations.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static NexTekk.Operations.Tests.Helpers.Constants;
using static NexTekk.Operations.Tests.Helpers.MockData;

namespace NexTekk.Operations.Tests.Unit.Controllers
{
    [TestClass]
    public class ProjectControllerTests
    {
        private static Mock<IProjectService> _projectService;
        private static ProjectController _projectController;

        [ClassInitialize]
        public static void BeforeAll(TestContext testContext)
        {
            _projectService = new Mock<IProjectService>();
            _projectService
                .Setup(x => x.GetProjects(It.IsAny<ProjectSearchCriteria>()))
                .ReturnsAsync(ProjectDB.Select(x => new Project(x)));
            _projectService
                .Setup(x => x.GetProject(It.IsAny<Guid>()))
                .Returns((Guid input) =>
                {
                    return Task.FromResult(ProjectDB.Select(x => new ProjectDetail(x)).FirstOrDefault(x => x.Id == input));
                });
            _projectService
                .Setup(x => x.SaveProject(It.IsAny<Project>()))
                .Returns(() =>
                {
                    return Task.FromResult(ProjectDB.Select(x => new Project(x)).ElementAt(1));
                });

        }

        [TestInitialize]
        public void BeforeEach()
        {
            _projectController = new ProjectController(_projectService.Object);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_List_Of_Projects_With_Search_Criteria()
        {
            var search = new ProjectSearchCriteria();
            var result = await _projectController.Search(search);

            Assert.AreEqual(2, result.Count());
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Get_Specific_Project()
        {
            var id = ProjectDB.First().Id;
            var result = await _projectController.Get(id);

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Title, ProjectDB.First().Title);
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Create_Project()
        {
            var model = new ProjectViewModel();
            model.Title = "Expense Management";
            model.Status = ProjectStatus.NotStarted;

            var result = await _projectController.Create(model);

            Assert.AreEqual("Expense Management", result.Title);
            Assert.AreEqual(ProjectStatus.NotStarted, result.Status);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Update_Existing_Project()
        {
            var id = ProjectDB.First().Id;
            var model = new ProjectViewModel
                        {
                            Title = "NexTekk Operations", Status = ProjectStatus.InProgress
                        };
            
            var result = await _projectController.Update(id, model);

            _projectService.Verify(x => x.SaveProject(It.Is<Project>(p => p.Title == "NexTekk Operations")), Times.Once);
            _projectService.Verify(x => x.SaveProject(It.Is<Project>(p => p.Status == ProjectStatus.InProgress)), Times.Once);
        }

        [TestMethod, TestCategory(UnitTest)]
        public async Task Can_Delete_Project()
        {
            var id = ProjectDB.First().Id;

            await _projectController.Delete(id);

            _projectService.Verify(x => x.DeleteProject(id), Times.Once);
        }
        
        [TestMethod, TestCategory(UnitTest)]
        public async Task Should_Save_TeamMembers()
        {
            var projectId = ProjectDB.First().Id;
            var teamMember = new TeamMemberViewModel
            {
                ProjectId = projectId,
                UserId = Guid.NewGuid(),
                Role = TeamMemberRole.ProjectManager
            };

            await _projectController.SaveTeamMember(projectId, new List<TeamMemberViewModel> { teamMember });

            _projectService.Verify(x => x.SaveTeamMembers(It.Is<IEnumerable<TeamMemberEntity>>(
                p => p.First().Role == TeamMemberRole.ProjectManager)
                ), Times.Once);

            _projectService.Verify(x => x.SaveTeamMembers(It.Is<IEnumerable<TeamMemberEntity>>(
                p => p.First().ProjectId == projectId)
                ), Times.Once);
        }
    }
}
