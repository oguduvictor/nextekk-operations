﻿CREATE TABLE [dbo].[TeamMemberRoles]
(
	[Id] INT NOT NULL , 
    [Name] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_TeamMemberRoles] PRIMARY KEY ([Id])
)