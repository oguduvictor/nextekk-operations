﻿CREATE TABLE [dbo].[Revenues]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
    [PaymentDate] DATETIME2 NOT NULL,
    [Currency] INT NOT NULL,
    [Amount] MONEY NOT NULL,
    [Description] NVARCHAR(128) NOT NULL,
	[ClientId] UNIQUEIDENTIFIER NULL,
    [Created] DATETIME2 NOT NULL,
    [CreatedById] UNIQUEIDENTIFIER NOT NULL,
    [Modified] DATETIME2 NOT NULL,
    [ModifiedById] UNIQUEIDENTIFIER NOT NULL,
    [Deleted] BIT NOT NULL DEFAULT 0,
	
    CONSTRAINT [PK_Revenues] PRIMARY KEY ([Id]),
    
	CONSTRAINT [FK_Revenues_Clients] FOREIGN KEY ([ClientId]) REFERENCES [Clients]([Id])
		ON DELETE SET NULL
)

GO

CREATE INDEX [IX_Revenues_PaymentDate] ON [dbo].[Revenues] ([PaymentDate])

GO

CREATE INDEX [IX_Revenues_Currency] ON [dbo].[Revenues] ([Currency])

GO

CREATE INDEX [IX_Revenues_Amount] ON [dbo].[Revenues] ([Amount])

GO

CREATE INDEX [IX_Revenues_ClientId] ON [dbo].[Revenues] ([ClientId])

GO

CREATE INDEX [IX_Revenues_Description] ON [dbo].[Revenues] ([Description])
