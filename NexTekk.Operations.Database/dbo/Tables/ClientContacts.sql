﻿CREATE TABLE [dbo].[ClientContacts]
(
	[ClientId] UNIQUEIDENTIFIER NOT NULL , 
    [ContactId] INT NOT NULL, 
    [Created] DATETIME NOT NULL DEFAULT SYSUTCDATETIME(), 
    CONSTRAINT [PK_Clients_Contacts] PRIMARY KEY ([ContactId], [ClientId]), 
    CONSTRAINT [FK_Clients_Contacts_Clients] FOREIGN KEY ([ClientId]) REFERENCES [Clients]([Id])
		ON DELETE CASCADE,
    CONSTRAINT [FK_Clients_Contacts_Contacts] FOREIGN KEY ([ContactId]) REFERENCES [Contacts]([Id])
		ON DELETE CASCADE
)
