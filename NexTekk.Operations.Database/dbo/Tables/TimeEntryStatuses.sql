﻿CREATE TABLE [dbo].[TimeEntryStatuses]
(
	[Id] INT NOT NULL,
	[Name] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_TimeEntryStatuses] PRIMARY KEY CLUSTERED ([Id] ASC)
)
