﻿CREATE TABLE [dbo].[ProjectStatuses]
(
	[Id] INT NOT NULL,
	[Name] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_ProjectStatuses] PRIMARY KEY CLUSTERED ([Id] ASC)
)
