﻿CREATE TABLE [dbo].[Expenses]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [IncurredById] UNIQUEIDENTIFIER NOT NULL, 
    [Currency] INT NOT NULL, 
    [Amount] MONEY NOT NULL, 
    [Description] NVARCHAR(128) NOT NULL, 
    [Reason] NVARCHAR(256) NULL, 
    [Reimbursed] DATETIME2 NULL, 
    [Created] DATETIME2 NULL, 
    [CreatedById] UNIQUEIDENTIFIER NULL, 
    [Modified] DATETIME2 NULL, 
    [ModifiedById] UNIQUEIDENTIFIER NULL, 
    [Date] DATETIME2 NOT NULL, 
    [Deleted] BIT NOT NULL DEFAULT 0,
	
    CONSTRAINT [PK_Expenses] PRIMARY KEY ([Id])
)

GO

CREATE INDEX [IX_Expenses_Date] ON [dbo].[Expenses] ([Date])

GO

CREATE INDEX [IX_Expenses_IncurredById] ON [dbo].[Expenses] ([IncurredById])

GO

CREATE INDEX [IX_Expenses_Currency] ON [dbo].[Expenses] ([Currency])

GO

CREATE INDEX [IX_Expenses_Reimbursed] ON [dbo].[Expenses] ([Reimbursed])

GO

CREATE INDEX [IX_Expenses_Amount] ON [dbo].[Expenses] ([Amount])
