﻿CREATE TABLE [dbo].[TeamMembers]
(
    [ProjectId] UNIQUEIDENTIFIER NOT NULL, 
    [UserId] UNIQUEIDENTIFIER NOT NULL, 
    [RoleId] INT NOT NULL DEFAULT 0, 
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    CONSTRAINT [PK_TeamMembers] PRIMARY KEY ([ProjectId], [UserId]), 
    CONSTRAINT [FK_TeamMembers_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [Projects]([Id])
		ON DELETE CASCADE, 
    CONSTRAINT [FK_TeamMembers_TeamMemberRoles] FOREIGN KEY ([RoleId]) REFERENCES [TeamMemberRoles]([Id])
		ON DELETE SET DEFAULT
)

GO

CREATE INDEX [IX_TeamMembers_RoleId] ON [dbo].[TeamMembers] ([RoleId])
