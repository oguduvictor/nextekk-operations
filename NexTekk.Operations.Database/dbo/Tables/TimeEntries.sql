﻿CREATE TABLE [dbo].[TimeEntries]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [Task] NVARCHAR(256) NOT NULL, 
    [BeginDateTime] DATETIME2 NOT NULL, 
    [EndDateTime] DATETIME2 NOT NULL, 
    [Comment] NVARCHAR(4000) NULL, 
    [ManagerComment] NVARCHAR(4000) NULL, 
    [ProjectId] UNIQUEIDENTIFIER NULL, 
    [ActivityId] UNIQUEIDENTIFIER NULL, 
    [Status] INT NOT NULL DEFAULT 0, 
    [Billable] BIT NOT NULL DEFAULT 0, 
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [CreatorId] UNIQUEIDENTIFIER NOT NULL, 
	[Edited] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(),
    [EditorId] UNIQUEIDENTIFIER NOT NULL, 
    [Approved] DATETIME2 NULL, 
    [ApproverId] UNIQUEIDENTIFIER NULL, 
    [Denied] DATETIME2 NULL, 
    [DenierId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_TimeEntries] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_TimeEntries_TimeEntryStatuses] FOREIGN KEY ([Status]) REFERENCES [TimeEntryStatuses]([Id]) 
		ON DELETE SET DEFAULT, 
    CONSTRAINT [FK_TimeEntries_Activities] FOREIGN KEY ([ActivityId]) REFERENCES [Activities]([Id])
		ON DELETE SET NULL, 
    CONSTRAINT [FK_TimeEntries_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [Projects]([Id])
		ON DELETE SET NULL
)

GO

CREATE INDEX [IX_TimeEntries_BeginDateTime_EndDateTime] ON [dbo].[TimeEntries] ([BeginDateTime],[EndDateTime])

GO

CREATE INDEX [IX_TimeEntries_ProjectId] ON [dbo].[TimeEntries] ([ProjectId])

GO

CREATE INDEX [IX_TimeEntries_ActivityId] ON [dbo].[TimeEntries] ([ActivityId])

GO

CREATE INDEX [IX_TimeEntries_Status] ON [dbo].[TimeEntries] ([Status])

GO

CREATE INDEX [IX_TimeEntries_CreatorId] ON [dbo].[TimeEntries] ([CreatorId])
