﻿CREATE TABLE [dbo].[Assets]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[Item] NVARCHAR(50) NOT NULL,
    [PurchasedDate] DATETIME2 NOT NULL, 
    [SerialNo] NVARCHAR(50) NULL,
    [PurchasedById] UNIQUEIDENTIFIER NOT NULL,
    [AssigneeId] UNIQUEIDENTIFIER NULL, 
    [AssignedDate] DATETIME2 NULL,  
    [Condition] INT NOT NULL, 
    [Currency] INT NOT NULL, 
    [Category] INT NOT NULL,
    [Description]  NVARCHAR(160) NULL,
    [Manufacturer] NVARCHAR(50) NOT NULL,
    [Model] NVARCHAR(50) NULL,
    [ServiceTag] NVARCHAR(50) NULL,
	[ServiceCode] NVARCHAR(50) NULL, 
    [Cost] MONEY NOT NULL,  
    [LastAssigneeId] UNIQUEIDENTIFIER NULL,
    [MarkedBadOn]  DATETIME2 NULL,  
    [MarkedBadById] UNIQUEIDENTIFIER NULL,
    [EnteredById]  UNIQUEIDENTIFIER NULL,
    [Created] DATETIME2 NOT NULL,
    [CreatedById] UNIQUEIDENTIFIER NOT NULL,
    [Modified] DATETIME2 NOT NULL,
	[ModifiedById] UNIQUEIDENTIFIER NOT NULL,
    [LastAssignedDate] DATETIME2 NULL,
    [Deleted] BIT NOT NULL DEFAULT 0,
	
    CONSTRAINT [PK_Assets] PRIMARY KEY ([Id]),
)
GO
CREATE INDEX [IX_Assets_Item] ON [dbo].[Assets]([Item])

GO
CREATE INDEX [IX_Assets_SerialNo] ON [dbo].[Assets]([SerialNo])

GO
CREATE INDEX [IX_Assets_PurchasedDate] ON [dbo].[Assets] ([PurchasedDate])

GO

CREATE INDEX [IX_Assets_PurchasedById] ON [dbo].[Assets] ([PurchasedById])

GO
CREATE INDEX [IX_Assets_Assignee] ON [dbo].[Assets] ([AssigneeId])

GO
CREATE INDEX [IX_Assets_AssignedDate] ON [dbo].[Assets] ([AssignedDate])

GO

CREATE INDEX [IX_Assets_Condition] ON [dbo].[Assets] ([Condition])

