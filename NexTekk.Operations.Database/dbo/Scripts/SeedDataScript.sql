﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

USE [$(DatabaseName)]
GO

ALTER TABLE dbo.TimeEntries 
NOCHECK CONSTRAINT FK_TimeEntries_TimeEntryStatuses;

ALTER TABLE dbo.TeamMembers 
NOCHECK CONSTRAINT FK_TeamMembers_TeamMemberRoles;

ALTER TABLE dbo.Projects
NOCHECK CONSTRAINT FK_Projects_ProjectStatuses;

DELETE TimeEntryStatuses;
DELETE TeamMemberRoles;
DELETE ProjectStatuses;

GO

INSERT INTO TimeEntryStatuses (Id, Name) 
VALUES
(0, 'None'),
(20, 'Saved'),
(30, 'Submitted'),
(40, 'Denied'),
(90, 'Approved');

PRINT 'TimeEntryStatuses inserted'

GO

INSERT INTO ProjectStatuses (Id, Name) 
VALUES
(0, 'None'),
(300, 'NotStarted'),
(400, 'InProgress'),
(600, 'Suspended'),
(900, 'Completed');

GO

INSERT INTO TeamMemberRoles (Id, Name)
VALUES
(0, 'None'),
(1100, 'Developer'),
(1120, 'Architect'),
(1130, 'QA Analyst'),
(1140, 'Business Analyst'),
(1150, 'Project Manager');

PRINT 'TeamMemberRoles inserted'

GO

ALTER TABLE dbo.TimeEntries 
CHECK CONSTRAINT FK_TimeEntries_TimeEntryStatuses;

ALTER TABLE dbo.Projects
CHECK CONSTRAINT FK_Projects_ProjectStatuses;

ALTER TABLE dbo.TeamMembers 
CHECK CONSTRAINT FK_TeamMembers_TeamMemberRoles;

GO

-- CREATE VACATION_SETTINGS ENTRIES
DECLARE
	@vacationCreatorId UNIQUEIDENTIFIER = '4D109539-E0D7-4D96-BCC5-D6DADB1CA0AA',
	@hoursPerPayPeriod DECIMAL(5,2) = 3.33;

Merge INTO VacationSettings AS target
USING(SELECT Id FROM [vw_Users]) as source (NewUserId)
	ON (target.UserId = source.NewUserId)
WHEN NOT MATCHED BY TARGET THEN
	INSERT (UserId, HoursPerPeriod, YearBeginBalance, CreatorId, EditorId) 
		VALUES (NewUserId, @hoursPerPayPeriod, 0, @vacationCreatorId, @vacationCreatorId);

PRINT 'VacationSettings created'

GO

-- CREATE EXTERNAL TABLE FOR USERS
DECLARE 
	@userViewScript NVARCHAR(4000),
	@userTable NVARCHAR(256) = N'[$(ExternalDb)].[dbo].[$(ExternalDbObject)]',
	@rolesTable NVARCHAR(256) = N'[$(ExternalDb)].[dbo].[$(ExternalDbRoles)]',
	@userRolesTable NVARCHAR(256) = N'[$(ExternalDb)].[dbo].[$(ExternalDbUserRoles)]';

IF ('$(CreateExternalTable)' = 'True')
BEGIN
	SET @userTable = N'dbo.Users';

	IF EXISTS(SELECT 1 FROM sys.external_tables WHERE name = 'Users')
	BEGIN
		EXEC sp_executesql N'
		DROP EXTERNAL TABLE Users;

		DROP EXTERNAL DATA SOURCE UserReferenceData;

		DROP DATABASE SCOPED CREDENTIAL $(ExternalDbUser);  

		DROP MASTER KEY'; 
	END

	EXEC sp_executesql N'
 
	CREATE DATABASE SCOPED CREDENTIAL $(ExternalDbUser) WITH IDENTITY = ''$(ExternalDbUser)'';

	CREATE EXTERNAL DATA SOURCE UserReferenceData
	WITH
	(
		TYPE=RDBMS,
		LOCATION=''$(ExternalDbServer)'',
		DATABASE_NAME=''$(ExternalDb)'',
		CREDENTIAL= $(ExternalDbUser)
	);

	create EXTERNAL TABLE [dbo].[Users] (
		UserId nvarchar(450) not null, 
		Email nvarchar(256) not null, 
		PhoneNumber nvarchar(25) null, 
		FirstName nvarchar(50) not null, 
		LastName  nvarchar(50) not null
	)
	WITH
	(
		DATA_SOURCE = UserReferenceData,
		SCHEMA_NAME = ''dbo'',
		OBJECT_NAME = ''$(ExternalDbObject)''
	)'

	PRINT 'External data source created'
END

--CREATE USERS VIEW
IF EXISTS(SELECT 1 FROM sys.views WHERE name = 'vw_Users')
	DROP VIEW vw_Users

SET @userViewScript = N'
CREATE VIEW [dbo].[vw_Users]
AS 
SELECT 
	CAST(UserId as UNIQUEIDENTIFIER) as "Id", 
	Email, 
	PhoneNumber, 
	FirstName, 
	LastName 
FROM ' + @userTable;

EXEC sp_executesql @userViewScript;

PRINT 'vw_Users created'

--CREATE ROLES VIEW
IF EXISTS(SELECT 1 FROM sys.views WHERE name = 'vw_Roles')
	DROP VIEW vw_Roles

SET @userViewScript = N'
CREATE VIEW [dbo].[vw_Roles]
AS 
SELECT 
	Id			    as "Id", 
	[Name]			as "Name"
FROM ' + @rolesTable;

EXEC sp_executesql @userViewScript;

PRINT 'vw_Roles created'

--CREATE USERROLES VIEW
IF EXISTS(SELECT 1 FROM sys.views WHERE name = 'vw_UserRoles')
	DROP VIEW vw_UserRoles

SET @userViewScript = N'
CREATE VIEW [dbo].[vw_UserRoles]
AS 
SELECT 
	CAST(UserId as UNIQUEIDENTIFIER) as "UserId",
	RoleId as "RoleId"
FROM ' + @userRolesTable;

EXEC sp_executesql @userViewScript;

PRINT 'vw_UserRoles created'
