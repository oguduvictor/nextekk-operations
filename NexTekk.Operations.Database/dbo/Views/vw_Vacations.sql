﻿CREATE VIEW [dbo].[vw_Vacations]
AS 
SELECT
	v.UserId,
	v.HoursPerPeriod,
	v.YearBeginBalance,
	v.Created,
	v.Edited,
	v.CreatorId,
	v.EditorId,
	DATEPART(YEAR, ISNULL(t.BeginDateTime, GETDATE())) AS "Year",
	CAST(SUM(DATEDIFF(HOUR, t.BeginDateTime, t.EndDateTime)) AS DECIMAL) AS "TotalHoursTaken"
FROM 
	VacationSettings v 
	LEFT JOIN TimeEntries t ON v.UserId = t.CreatorId AND t.[Status] = 90 AND t.ActivityId IS NOT NULL
	LEFT JOIN Activities a ON t.ActivityId = a.Id AND a.IsVacation = 1 AND a.Deleted <> 1
GROUP BY
	v.UserId,
	v.HoursPerPeriod,
	v.YearBeginBalance,
	v.Created,
	v.Edited,
	v.CreatorId,
	v.EditorId,
	DATEPART(YEAR, ISNULL(t.BeginDateTime, GETDATE()))