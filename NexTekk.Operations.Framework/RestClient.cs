﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NexTekk.Operations.Framework
{
    public class RestClient : IRestClient
    {
        private readonly string _jsonMediaType = "application/json";
        private readonly HttpClient _httpClient;

        public RestClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<T> Get<T>(string absoluteUrl, Dictionary<string, string> query)
        {
            var parameters = ToParams(absoluteUrl, query);
            var response = await _httpClient.GetStringAsync(parameters);

            return response.FromJson<T>();
        }

        public async Task<T> Post<S, T>(string absoluteUrl, S data)
        {
            var jsonData = data.Equals(default(S)) ? string.Empty : data.ToJson();
            var content = new StringContent(jsonData, Encoding.UTF8, _jsonMediaType);
            var response = await _httpClient.PostAsync(absoluteUrl, content);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(response.ReasonPhrase);
            }

            var result = await response.Content.ReadAsStringAsync();

            return result.FromJson<T>();
        }

        private string ToParams(string url, Dictionary<string, string> query)
        {
            if (query.IsNullOrEmpty())
            {
                return url;
            }

            var sb = new StringBuilder(url);

            sb.Append("?");

            foreach (var item in query)
            {
                sb.Append(item.Key);
                sb.Append("=");
                sb.Append(Uri.EscapeUriString(item.Value));
                sb.Append("&");
            }

            return sb.ToString().TrimEnd('&');
        }
    }
}
