﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Providers;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace NexTekk.Operations.Framework
{
    public class Logger : Core.Interfaces.ILogger
    {
        private const string FILE_TARGET = "file";
        private const string CONSOLE_TARGET = "console";
        private const string DATABASE_TARGET = "database";
        private const string DEFAULT_LEVEL = "Warn";
        private const int QUEUE_LIMIT = 5000;

        private static NLog.Logger _logger;
        private readonly AsyncTargetWrapper _targetWrapper;
        private readonly LogLevel _defaultLogLevel;

        public Logger()
        {
            var levelString = AppSettingsProvider.Current.DefaultLogLevel;

            _targetWrapper = new AsyncTargetWrapper();
            _targetWrapper.QueueLimit = QUEUE_LIMIT;
            _targetWrapper.OverflowAction = AsyncTargetWrapperOverflowAction.Discard;
            _defaultLogLevel = LogLevel.FromString(levelString ?? DEFAULT_LEVEL);

            InitTargetsConfig();
        }

        public Guid Debug(string message)
        {
            return LogEvent(LogLevel.Debug, message, null);
        }

        public Guid Error(Exception exception)
        {
            return LogEvent(LogLevel.Error, exception.Message, exception);
        }

        public Guid Error(string message)
        {
            return LogEvent(LogLevel.Error, message, null);
        }

        public Guid Fatal(string message)
        {
            return LogEvent(LogLevel.Fatal, message, null);
        }

        public Guid Info(string message)
        {
            return LogEvent(LogLevel.Info, message, null);
        }

        public Guid Trace(string message)
        {
            return LogEvent(LogLevel.Trace, message, null);
        }

        public Guid Warn(string message)
        {
            return LogEvent(LogLevel.Warn, message, null);
        }

        private void InitTargetsConfig()
        {
            var config = new LoggingConfiguration();
            var defaultTarget = AppSettingsProvider.Current.LoggingDbConnectionString.IsNullOrEmpty() ? FILE_TARGET : DATABASE_TARGET;
            var targets = AppSettingsProvider.Current.LoggingTargets;

            if (targets.IsNullOrEmpty())
            {
                targets = new string[] { defaultTarget };
            }

            if (targets.Contains(CONSOLE_TARGET))
            {
                AddConsoleTarget(config);
            }

            if (targets.Contains(FILE_TARGET))
            {
                AddFileTarget(config);
            }

            if (targets.Contains(DATABASE_TARGET))
            {
                AddDatabaseTarget(config);
            }

            LogManager.Configuration = config;

            _logger = LogManager.GetCurrentClassLogger();

            SimpleConfigurator.ConfigureForTargetLogging(_targetWrapper, LogLevel.Debug);
        }

        private void AddConsoleTarget(LoggingConfiguration config)
        {
            var consoleTarget = new ColoredConsoleTarget(CONSOLE_TARGET);
            var rule = new LoggingRule("*", _defaultLogLevel, consoleTarget);

            consoleTarget.Layout = GetLayout(@"${date:format=HH\:mm\:ss} ${logger} ${message}");
            config.AddTarget(consoleTarget);
            config.LoggingRules.Add(rule);
        }

        private void AddFileTarget(LoggingConfiguration config)
        {
            var fileTarget = new FileTarget(FILE_TARGET);
            var rule = new LoggingRule("*", _defaultLogLevel, fileTarget);
            
            fileTarget.FileName = $"{Directory.GetCurrentDirectory()}/logs/${{shortdate}}.log";
            fileTarget.Layout = GetLayout();
            fileTarget.KeepFileOpen = false;
            fileTarget.Encoding = Encoding.UTF8;

            if (_targetWrapper.WrappedTarget == null)
            {
                _targetWrapper.WrappedTarget = fileTarget;
            }
            else
            {
                config.AddTarget(fileTarget);
            }

            config.LoggingRules.Add(rule);
        }

        private void AddDatabaseTarget(LoggingConfiguration config)
        {
            if (AppSettingsProvider.Current.LoggingDbConnectionString.IsNullOrEmpty())
            {
                return;
            }

            var dbTarget = new DatabaseTarget(DATABASE_TARGET);
            var dbRule = new LoggingRule("*", _defaultLogLevel, dbTarget);

            dbTarget.ConnectionString = AppSettingsProvider.Current.LoggingDbConnectionString;
            dbTarget.CommandText = @"exec sp_WriteLog @id, @thread, @level, @logger, @message, @exceptionType, @stacktrace, @baseException, @appName";

            dbTarget.Parameters.Add(new DatabaseParameterInfo("@id", new SimpleLayout("${event-properties:item=LogId}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@thread", new SimpleLayout("${threadid}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@level", new SimpleLayout("${level}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@logger", new SimpleLayout("${logger}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@message", new SimpleLayout("${message}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@exceptionType", new SimpleLayout("${event-properties:item=ExceptionType}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@stacktrace", new SimpleLayout("${stacktrace:format=Raw:skipFrames=2:topFrames=5}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@baseException", new SimpleLayout("${event-properties:item=BaseException}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@appName", GetAppName()));

            if (_targetWrapper.WrappedTarget == null)
            {
                _targetWrapper.WrappedTarget = dbTarget;
            }
            else
            {
                config.AddTarget(dbTarget);
            }

            config.LoggingRules.Add(dbRule);
        }

        private Guid LogEvent(LogLevel logLevel, string message, Exception exception)
        {
            var id = Guid.NewGuid();
            var evt = new LogEventInfo(logLevel, _logger.Name, null, message, null, exception);

            evt.Properties.Add("LogId", id.ToString());
            evt.Properties.Add("ExceptionType", exception?.GetType().FullName);

            if (exception != null)
            {
                evt.Properties.Add("BaseException", GetBaseException(exception));
            }

            _logger.Log(evt);

            return id;
        }

        private string GetLayout(string defaultLayout = null)
        {
            var layout = AppSettingsProvider.Current.GetValue("LoggingLayout");

            if (!layout.IsNullOrEmpty())
            {
                return layout;
            }

            if (defaultLayout != null)
            {
                return defaultLayout;
            }

            return "${longdate} ${uppercase:${level}} ${message}";
        }

        private string GetAppName()
        {
            var appName = AppSettingsProvider.Current.GetValue("ApplicationName");

            if (!appName.IsNullOrEmpty())
            {
                return appName;
            }

            var envVariables = Environment.GetEnvironmentVariables();
            var sitenameKey = envVariables.Keys.Cast<string>().FirstOrDefault(x => x.EndsWith("_SITENAME"));

            if (sitenameKey != null)
            {
                return envVariables[sitenameKey].ToString();
            }

            return string.Empty;
        }

        private string GetBaseException(Exception exemption)
        {
            var ex = exemption.GetBaseException();

            return $"{ex.Message}; {ex.StackTrace}";
        }
    }
}
