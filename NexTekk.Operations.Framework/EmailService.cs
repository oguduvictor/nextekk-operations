﻿using Newtonsoft.Json.Linq;
using NexTekk.Operations.Core.Interfaces;
using NexTekk.Operations.Core.Models;
using System.Threading.Tasks;

namespace NexTekk.Operations.Framework
{
    public class EmailService : IEmailService
    {
        private readonly IRestClient _restClient;
        private readonly string _messagingApiUrl;

        public EmailService(IRestClient restClient, string messagingApiUrl)
        {
            _restClient = restClient;
            _messagingApiUrl = messagingApiUrl;
        }

        public async Task<bool> SendEmail(Email email)
        {
            var response = await _restClient.Post<Email, JObject>(_messagingApiUrl, email);

            return response["successful"].Value<bool>();
        }
    }
}
