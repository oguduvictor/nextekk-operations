export const environment = {
  production: true,
  apiUrl: '/',
  nextekkSite: 'http://nextekk.com',
  loginEndPoint: 'http://nextekkidentity.azurewebsites.net/Account/Login'
};
