export const environment = {
  production: true,
  apiUrl: '/',
  nextekkSite: 'http://nextekk.com',
  loginEndPoint: 'http://stg-nextekkidentity.azurewebsites.net/Account/Login'
};
