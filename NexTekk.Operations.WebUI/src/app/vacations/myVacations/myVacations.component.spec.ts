import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationService, SpinnerService } from '../../shared';
import { MyVacationsComponent } from './myVacations.component';
import { VacationDataService } from '../common/services/vacationData.service';
import { Observable } from 'rxjs/Observable';
import { Vacation } from '../common/models/vacation.model';
import { TimeEntrySummary, TimeEntryModalOptions } from '../../timeTracking/common';
import { AppModule } from '../../app.module';

describe('MyVacationsComponent', () => {
    let _fixture: ComponentFixture<MyVacationsComponent>;
    let _vacationComponent: MyVacationsComponent;
    let _timeEntryDTO = <TimeEntrySummary>{
        id: 'id',
        task: 'task',
        comment: 'comment',
        status: 'status',
        beginDateTime: new Date('2017-05-18 09:00:00'),
        endDateTime: new Date('2017-05-18 12:00:00'),
        activityId: 'a_id',
        activity: <any>{ title: 'title', id: 'a_id', billable: false },
        creator: <any>{ id: 'creator_id', firstName: 'john', lastName: 'lula' }
    };
    let _service = {
        getEmployeeVacation: jasmine.createSpy('getEmployeeVacation').and.returnValue(Observable.of(new Vacation('12', 3, 12))),
        getEmployeeVacationTimeEntries: function (id) {
            return Observable.of([_timeEntryDTO]);
        },
        saveVacation: function () {
            return Observable.of(new Vacation('12', 3, 12));
        }
    };

    beforeEach(() => {
        let toaster = {
            showError: function () { },
            showSuccess: function () { }
        };

        TestBed.configureTestingModule({
            imports: [AppModule, RouterTestingModule.withRoutes([])],
            declarations: [],
            providers: [{ provide: VacationDataService, useValue: _service },
            { provide: NotificationService, useValue: toaster }, SpinnerService]
        });

        _fixture = TestBed.createComponent(MyVacationsComponent);
        _vacationComponent = <MyVacationsComponent>_fixture.debugElement.componentInstance;
    });

    it('can be instantiated successfully', () => {
        expect(_vacationComponent).toBeTruthy();
    });

    it('can get hours difference of task', () => {
        let result = _vacationComponent.getHours(_timeEntryDTO);

        expect(result).toBe(3);
    });

    it('getHours returns 0 if task has no beginDateTime', () => {
        let entry = new TimeEntrySummary();
        entry.beginDateTime = null;

        let hours = _vacationComponent.getHours(entry);

        expect(hours).toEqual(0);
    });

    it('getHours returns 0 if task has no endDateTime', () => {
        let entry = new TimeEntrySummary();
        entry.endDateTime = null;

        let hours = _vacationComponent.getHours(entry);

        expect(hours).toEqual(0);
    });

    it('can check if user is logged in', () => {
        _vacationComponent.employeeId = 'test_id';
        _vacationComponent.loggedInUser = <any>{ id: 'test_id' };
        expect(_vacationComponent.isLoggedInUser).toBe(true);
    });

    it('can call onVacationSaved', () => {
        spyOn(_vacationComponent, 'onVacationSaved');
        _vacationComponent.onVacationSaved(new Vacation());
        expect(_vacationComponent.onVacationSaved).toHaveBeenCalled();
    });

    it('can check if logged in user is a manager', () => {
        _vacationComponent.loggedInUser = <any>{ id: 'test_id', isManager: jasmine.createSpy('isManager').and.returnValue(true) };

        expect(_vacationComponent.isManager).toBe(true);
    });

    it('can display timeEntry task', () => {
        let entry = new TimeEntrySummary();
        let timeEntryModal = (<any>_vacationComponent).timeEntryModal;

        spyOn(timeEntryModal, 'show');
        _vacationComponent.displayTask(entry);

        expect(timeEntryModal.show).toHaveBeenCalledWith(entry, TimeEntryModalOptions.view);
    });

    it('can edit vacation', () => {
        let modal = (<any>_vacationComponent)._vacationModal;

        spyOn(modal, 'show');
        _vacationComponent.edit();

        expect(modal.show).toHaveBeenCalled();
    });

    it('should get user vacations on initialization using _route.params[id]', () => {
        (<any>_vacationComponent)._route.snapshot.data['user'] = { id: null };
        (<any>_vacationComponent)._route.snapshot.params = { id: '1' };

        _vacationComponent.ngOnInit();

        expect(_service.getEmployeeVacation).toHaveBeenCalled();
    });

    it('should get user vacations on initialization using loggedInUser', () => {
        (<any>_vacationComponent)._route.snapshot.data['user'] = { id: 'test_id' };
        (<any>_vacationComponent)._route.snapshot.params = { id: null };

        let loadUserVacations = jasmine.createSpy('loadUserVacations');
        (<any>_vacationComponent).loadUserVacations = loadUserVacations;

        _vacationComponent.ngOnInit();

        expect(loadUserVacations).toHaveBeenCalledWith('test_id');
    });
});
