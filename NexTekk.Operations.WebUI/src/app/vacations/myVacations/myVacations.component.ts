import { ViewChild, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User, SpinnerService, NotificationService, ModalComponent } from '../../shared';
import { VacationDataService } from '../common/services/vacationData.service';
import { VacationModalComponent } from '../common/widgets/vacationModal/vacationModal.component';
import { TimeEntrySummary, TimeEntryModalComponent, TimeEntryModalOptions } from '../../timeTracking/common';
import { Vacation } from '../common/models/vacation.model';
import * as _ from 'lodash';

@Component({
    selector: 'app-my-vacation',
    templateUrl: './myVacations.component.html',
    styleUrls: ['./myVacations.component.css']
})
export class MyVacationsComponent implements OnInit {
    vacation: Vacation;
    timeEntries: TimeEntrySummary[];
    employeeId: string;
    loggedInUser: User;

    hoursPerPeriod: number;
    yearBeginBalance: number;

    get isLoggedInUser(): boolean {
        return this.employeeId === this.loggedInUser.id;
    }

    get isManager(): boolean {
        return this.loggedInUser.isManager();
    }

    @ViewChild(VacationModalComponent)
    private _vacationModal: VacationModalComponent;

    @ViewChild(TimeEntryModalComponent)
    private timeEntryModal: TimeEntryModalComponent;

    constructor(
        private _dataService: VacationDataService,
        private _spinnerService: SpinnerService,
        private _route: ActivatedRoute,
        private _notificationService: NotificationService
    ) {
        this.vacation = new Vacation();
        this.timeEntries = [];
    }

    displayTask(task: TimeEntrySummary) {
        this.timeEntryModal.show(task, TimeEntryModalOptions.view);
    }

    edit(): void {
        this._vacationModal.show(_.clone(this.vacation), this.employeeId, false);
    }

    onVacationSaved(vacation: Vacation) {
        _.merge(this.vacation, vacation);
    }

    getHours(task: TimeEntrySummary): number {
        if (!task.beginDateTime || !task.endDateTime) {
            return 0;
        }

        return task.endDateTime.getHours() - task.beginDateTime.getHours();
    }

    ngOnInit(): void {
        this.loggedInUser = this._route.snapshot.data['user'];
        this.employeeId = this._route.snapshot.params['id'] || this.loggedInUser.id;
        this.loadUserVacations(this.employeeId);
    }

    private loadUserVacations(id: string): void {
        this._spinnerService.show();
        this._dataService.getEmployeeVacation(id).subscribe(data => {
            this.vacation = data;
            this._spinnerService.hide();
        }, err => this._spinnerService.hide());

        this._spinnerService.show();
        this._dataService.getEmployeeVacationTimeEntries(id).subscribe(data => {
            this.timeEntries = data;
            this._spinnerService.hide();
        }, err => this._spinnerService.hide());
    }
}
