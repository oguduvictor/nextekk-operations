import { User, DateTimeHelper } from '../../../shared';

export class Vacation {
    constructor (
        public id?: string,
        public hoursPerPeriod: number = null,
        public yearBeginBalance: number = null,
        public totalHoursAvailable?: number,
        public totalHoursTaken?: number,
        public totalHoursAccrued?: number,
        public employee?: User,
        public edited?: string
    ) {
        this.employee = new User();
     }

    fromDto(dto: any) {
        this.id = dto.id;
        this.hoursPerPeriod = dto.hoursPerPeriod;
        this.yearBeginBalance = dto.yearBeginBalance;
        this.totalHoursAvailable = dto.totalHoursAvailable;
        this.totalHoursTaken = dto.totalHoursTaken;
        this.totalHoursAccrued = dto.totalHoursAccrued;
        this.edited = DateTimeHelper.format(dto.edited, 'yyyy-MM-dd');
        if (dto.employee != null) {
            this.employee.fromDto(dto.employee);
        }
    }
}
