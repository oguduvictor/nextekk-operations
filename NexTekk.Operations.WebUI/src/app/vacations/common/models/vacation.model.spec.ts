import { Vacation } from './vacation.model';

describe('VacationModel', () => {
    let vacation: Vacation;

    xit('can be instantiated', () => {
        vacation = new Vacation();

        expect(vacation).toBeTruthy();
    });

    it('can convert dto to a vacation model', () => {
        vacation = new Vacation();
        let dto = {
            id: '1',
            hoursPerPeriod: 3,
            yearBeginBalance: 30,
            totalHoursAvailable: 10,
            totalHoursTaken: 20,
            totalHoursAccrued: 0
        };

        vacation.fromDto(dto);

        expect(vacation.id).toBe(dto.id);
        expect(vacation.hoursPerPeriod).toBe(dto.hoursPerPeriod);
        expect(vacation.totalHoursAvailable).toBe(dto.totalHoursAvailable);
        expect(vacation.totalHoursTaken).toBe(dto.totalHoursTaken);
        expect(vacation.totalHoursAccrued).toBe(dto.totalHoursAccrued);
    });
});
