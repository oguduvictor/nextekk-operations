import { getTestBed, TestBed } from '@angular/core/testing';
import { BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AuthService, HttpService } from '../../../shared';
import { VacationDataService } from './vacationData.service';
import { User } from '../../../shared';
import { TimeEntrySummary } from '../../../timeTracking/common';

describe('VacationDataService', () => {
    let _backend: MockBackend;
    let _service: VacationDataService;
    let _url: string;
    let _authService = {
        getAccessToken: () => 'token_0001'
    };
    let _vacationDto = <any> {
        id: '1',
        employee: new User('1', 'tim', 'rogers'),
        hoursPerPeriod: 8,
        yearBeginBalance: 50,
        totalHoursAvailable: 20,
        totalHoursTaken: 30
    };
    let _timeEntryDTO = {
        id : 'test_id',
        task : 'test_task',
        comment : 'test_comment',
        status : 'test_status',
        beginDateTime : '2017-05-18 09:00:00',
        endDateTime : '2017-05-28 17:00:00',
        activity: <any>{ title: 'activity_title', id: 'activity_id', billable: 'activity_billable'},
        creator: <any>{id: 'creator_id', firstName: 'john', lastName: 'lula'}
     };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                VacationDataService,
                { provide: AuthService, useValue: _authService },
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions,
                        AuthService
                    ],
                    provide: HttpService,
                    useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions, authService: AuthService) => {
                        let notificationService = <any>{showError: function(){} };
                        return new HttpService(<any>backend, defaultOptions, authService, notificationService);
                    }
                }
            ]
        });

        _url = 'http://localhost:33895/api/Vacation/';

        let testbed = getTestBed();
        _backend = testbed.get(MockBackend);
        _service = TestBed.get(VacationDataService);
    });

    function setupConnections(backend: MockBackend, options: any, url?) {
        backend.connections.subscribe((connection: MockConnection) => {
            if (connection.request.url === url) {
                let responseOptions = new ResponseOptions(options);
                let response = new Response(responseOptions);
                connection.mockRespond(response);
            } else {
                throw new Error('Invalid Url');
            }
        });
    }

    it('can be instantiated successfully', () => {
        expect(_service).toBeTruthy();
    });

    it('should return list of all vacations', () => {
        _url += 'getAll';

        setupConnections(_backend, {
            body: [_vacationDto],
            status: 200
        }, _url);

        _service.getAllVacations().subscribe(data => {

            expect(data.length).toBe(1);
            expect(data[0].id).toBe('1');
            expect(data[0].totalHoursAvailable).toBe(20);
        });
    });

    it('should return employee vacation', () => {
        _url += 'get/1';

        setupConnections(_backend, {
            body: _vacationDto,
            status: 200
        }, _url);

        _service.getEmployeeVacation('1').subscribe(data => {
            expect(data.id).toBe('1');
            expect(data.totalHoursAvailable).toBe(20);
            expect(data.yearBeginBalance).toBe(50);
        });
    });

    it('should return employee vacation timeEntries', () => {
        _url += 'getTimeEntries/1';

        setupConnections(_backend, {
            body: [ _timeEntryDTO ],
            status: 200
        }, _url);

        _service.getEmployeeVacationTimeEntries('1').subscribe(data => {

            expect(data instanceof Array).toBeTruthy();
            expect(data.length).toBe(1);
            expect(data[0] instanceof TimeEntrySummary).toBeTruthy();
        });
    });

    it('should return sucess when EmployeeHours is updated', () => {
        _url += 'updateHours/1';

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.updateEmployeeHours('1', 12).subscribe(data => {
            expect(data).toBeTruthy();
        });
    });

    it('should return sucess when Employee Year Begin Balance is updated', () => {
        _url += 'updateYearBeginBalance/1';

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.updateYearBeginBalance('1', 12).subscribe(data => {
            expect(data).toBeTruthy();
        });
    });

    it('should return success on successful creation of vacation setting', () => {
        _url += 'saveSettings/1?hours=3';

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.saveVacation('1', 3, 30).subscribe(data => {
            expect(data).toBeTruthy();
        });
    });

});
