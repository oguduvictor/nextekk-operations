import { Injectable } from '@angular/core';
import { Vacation } from '../models/vacation.model';
import { TimeEntrySummary } from '../../../timeTracking/common';
import { HttpService } from '../../../shared';
import { Observable } from 'rxjs/Observable';
import { RequestOptions, Headers } from '@angular/http';

@Injectable()
export class VacationDataService {
    private _vacationApiUrl = 'api/Vacation/';
    private _options = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});

    constructor (private _http: HttpService) {}

    getAllVacations(): Observable<Vacation[]> {
        return this._http.getMultiple(this._vacationApiUrl + 'getAll', Vacation);
    }

    getEmployeeVacation(employeeId: string): Observable<Vacation> {
        return this._http.getData(this._vacationApiUrl + `get/${employeeId}`, Vacation);
    }

    getEmployeeVacationTimeEntries(employeeId: string): Observable<TimeEntrySummary[]> {
        return this._http.getMultiple(this._vacationApiUrl + `getTimeEntries/${employeeId}`, TimeEntrySummary);
    }

    updateEmployeeHours(employeeId: string, hours: number): Observable<number> {
        return this._http.putData(this._vacationApiUrl + 'updateHours/' + employeeId, hours, null, this._options);
    }

    updateYearBeginBalance(employeeId: string, beginBalance: number): Observable<number> {
        return this._http
            .putData(this._vacationApiUrl + `updateYearBeginBalance/${employeeId}`, beginBalance, null, this._options);
    }

    saveVacation(employeeId: string, hours: number, yearBeginBalance: number): Observable<Vacation> {
        return this._http
            .putData(this._vacationApiUrl + `saveSettings/${employeeId}?hours=${hours}`, yearBeginBalance, null, this._options );
    }
}
