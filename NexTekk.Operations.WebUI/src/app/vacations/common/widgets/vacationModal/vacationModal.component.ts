import { ViewChild, Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User, UserService, SpinnerService, NotificationService, ModalComponent } from '../../../../shared';
import { VacationDataService } from '../../../common/services/vacationData.service';
import { Vacation } from '../../../common/models/vacation.model';

@Component({
    selector: 'app-vacation-modal',
    templateUrl: './vacationModal.component.html'
})
export class VacationModalComponent implements OnInit {
    employeeId: string;
    vacation: Vacation;
    canSelectUser = false;
    allUsers: User[] = [];

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }

        return this.form.valid;
    }

    get hoursPerPeriodIsValid() {
        return (this.vacation.hoursPerPeriod > 0 && this.vacation.hoursPerPeriod <= 10);
    }

    get yearBeginBalanceIsValid() {
        return this.vacation.yearBeginBalance <= 200;
    }

    @Output()
    onSaved = new EventEmitter<Vacation>();

    constructor(
        private _dataService: VacationDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService,
        private _userService: UserService
    ) { }

    show(vacation: Vacation, employeeId: string, enableSelect: boolean) {
        this.canSelectUser = enableSelect;
        this.modal.title = (this.canSelectUser) ? 'Add Vacation Settings' : 'Update Employee Vacation Details';

        this.modal.closable = true;
        this.vacation = vacation;
        this.employeeId = employeeId;
        this.modal.show();
    }

    saveVacation() {
        this._spinnerService.show();
        this._dataService.saveVacation(this.employeeId, this.vacation.hoursPerPeriod, this.vacation.yearBeginBalance).subscribe(data => {
            this._spinnerService.hide();
            this.onSaved.emit(data);
            this._notificationService
                .showSuccess(
                'Vacation was updated successfully',
                'Employee Vacation'
                );
            this.modal.close();
        }, () => {
            this._spinnerService.hide();
            this._notificationService.showError('Vacation Update was unsuccessful');
            this.modal.close();
        });
    }

    close() {
        this.modal.close();
    }

    ngOnInit() {
        this._spinnerService.show();
        this._userService.getAllUsers().subscribe(users => {
            this._spinnerService.hide();
            this.allUsers = users;
        },
            () => {
                this._spinnerService.hide();
            }
        );
    }
}
