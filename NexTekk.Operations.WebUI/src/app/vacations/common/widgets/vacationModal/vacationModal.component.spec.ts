import { TestBed, ComponentFixture } from '@angular/core/testing';
import { VacationModalComponent } from './vacationModal.component';
import { VacationDataService } from '../../../common/services/vacationData.service';
import { Vacation } from '../../../common/models/vacation.model';
import { FormsModule } from '@angular/forms';
import { ModalComponent, User, UserService, SpinnerService, NotificationService } from '../../../../shared';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import { VacationModule } from '../../../vacations.module';

describe('VacationModalComponent', () => {
    let _vacationModal: VacationModalComponent;
    let _fixture: ComponentFixture<VacationModalComponent>;
    let _mockDataService: VacationDataService;
    let _mockUserService: UserService;
    let options: ToastOptions = <ToastOptions>{
        positionClass: 'toast-bottom-right',
        newestOnTop: true,
        toastLife: 3000,
        showCloseButton: true
    };

    beforeEach(() => {
        _mockDataService = <any>{
            saveVacation: jasmine.createSpy('saveVacation').and.returnValue(Observable.of({hoursPerPeriod: 2, yearBeginBalance: 12}))
        };

        _mockUserService = <any>{
            getAllUsers: jasmine.createSpy('getAllUsers').and.returnValue(Observable.of([new User('1', 'Ayo', 'Dan')]))
        };

        TestBed.configureTestingModule({
            imports: [FormsModule, VacationModule],
            providers: [
                SpinnerService, NotificationService, ToastsManager,
                { provide: VacationDataService, userValue: _mockDataService },
                { provide: UserService, useValue: _mockUserService },
                { provide: ToastOptions, useValue: options }
            ]
        });

        _fixture = TestBed.createComponent(VacationModalComponent);
        _vacationModal = _fixture.debugElement.componentInstance;
        (<any>_vacationModal)._dataService = _mockDataService;
        let modal = (<any>_vacationModal).modal;
        (<any>modal).modal.config = { backdrop: true, ignoreBackdropClick: true, keyboard: true };
    });

    it('can be instantiated successfully', () => {
        expect(_vacationModal).toBeTruthy();
    });

    it('can display existing vacation', () => {
        expect(_vacationModal.vacation).toBeUndefined();

        let vacation = new Vacation();
        vacation.id = '1';

        _vacationModal.show(vacation, 'employeeId', true);

        expect(_vacationModal.vacation.id).toEqual('1');
    });

    it('can add user vacation setting', () => {
        expect(_vacationModal.canSelectUser).toBe(false);
        let vacation = new Vacation();
        vacation.id = '1';

        _vacationModal.show(vacation, 'employeeId', true);

        expect(_vacationModal.canSelectUser).toBe(true);
    });

    it('should disable user selection when user is editing vacation settings', () => {
        expect(_vacationModal.canSelectUser).toBe(false);
        let vacation = new Vacation();
        vacation.id = '1';

        _vacationModal.show(vacation, 'employeeId', false);

        expect(_vacationModal.canSelectUser).toBe(false);
    });

    it('can save vacation', () => {
        _vacationModal.employeeId = '1';
        _vacationModal.vacation = <Vacation>{hoursPerPeriod: 2, yearBeginBalance: 12};

        _vacationModal.saveVacation();

        expect(_mockDataService.saveVacation).toHaveBeenCalledWith('1', 2, 12);
    });

    it('should hide modal once close is clicked', () => {
        let modal = (<any>_vacationModal).modal;
        spyOn(modal, 'close');

        _vacationModal.close();
        expect(modal.close).toHaveBeenCalled();
    });

    it('should get all existing users on initializing', () => {
        expect(_vacationModal.allUsers.length).toEqual(0);

        _vacationModal.ngOnInit();

        expect(_mockUserService.getAllUsers).toHaveBeenCalled();
        expect(_vacationModal.allUsers.length).toEqual(1);
    });

    it('returns true if vacation hours per period is les than 10', () => {
        _vacationModal.vacation = <Vacation> {hoursPerPeriod: 2.4};

        expect(_vacationModal.hoursPerPeriodIsValid).toBe(true);
    });

    it('returns false if vacation hours per period is less than 0', () => {
        _vacationModal.vacation = <Vacation> {hoursPerPeriod: -2};

        expect(_vacationModal.hoursPerPeriodIsValid).toBe(false);
    });

    it('returns false if vacation hours per period is equal to 0', () => {
        _vacationModal.vacation = <Vacation> {hoursPerPeriod: 0};

        expect(_vacationModal.hoursPerPeriodIsValid).toBe(false);
    });

    it('returns false if vacation hours per period is greater than 10', () => {
        _vacationModal.vacation = <Vacation> {hoursPerPeriod: 11};

        expect(_vacationModal.hoursPerPeriodIsValid).toBe(false);
    });

    it('returns true if year begin balance is less than 200', () => {
        _vacationModal.vacation = <Vacation> {yearBeginBalance: 12};

        expect(_vacationModal.yearBeginBalanceIsValid).toBe(true);
    });

    it('returns false if year begin balance is greater than 200', () => {
        _vacationModal.vacation = <Vacation> {yearBeginBalance: 230};

        expect(_vacationModal.yearBeginBalanceIsValid).toBe(false);
    });
});
