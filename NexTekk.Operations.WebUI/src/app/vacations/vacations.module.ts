﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule, ManagerGuardService, UserService } from '../shared';
import { RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { MyVacationsComponent } from './myVacations/myVacations.component';
import { VacationDataService } from './common/services/vacationData.service';
import { TimeTrackingModule } from '../timeTracking/timeTracking.module';
import { UsersVacationsComponent } from './usersVacations/usersVacations.component';
import { VacationModalComponent } from './common/widgets/vacationModal/vacationModal.component';

@NgModule({
    declarations: [
        MyVacationsComponent,
        UsersVacationsComponent,
        VacationModalComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        TimeTrackingModule,
        RouterModule.forChild([
            { path: 'vacations', component: MyVacationsComponent, resolve: {
                user: UserService
            }},
            { path: 'vacations/:id', component: MyVacationsComponent, canActivate: [ManagerGuardService], resolve: {
                user: UserService
            }},
            { path: 'users-vacations', component: UsersVacationsComponent, canActivate: [ManagerGuardService]}
        ])
    ],
    providers: [VacationDataService]
})
export class VacationModule {
}
