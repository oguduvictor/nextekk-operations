import { Component, OnInit, ViewChild } from '@angular/core';
import { VacationDataService } from '../common/services/vacationData.service';
import { SpinnerService } from '../../shared';
import { Vacation } from '../common/models/vacation.model';
import { VacationModalComponent } from '../common/widgets/vacationModal/vacationModal.component';
import * as _ from 'lodash';

@Component({
    selector: 'app-my-users-vacation',
    templateUrl: './usersVacations.component.html',
    styleUrls: ['./usersVacations.component.css']
})
export class UsersVacationsComponent implements OnInit  {
    employeeId: string;
    vacations: Vacation[];

    @ViewChild(VacationModalComponent)
    private _vacationModal: VacationModalComponent;

    constructor(
        private _dataService: VacationDataService,
        private _spinnerService: SpinnerService
    ) {}

    onVacationSaved(vacationSetting) {
        this.loadAllVacations();
    }

    addVacation(): void {
        this._vacationModal.show(_.clone(new Vacation()), this.employeeId, true);
    }

    private loadAllVacations(): void {
        this._spinnerService.show();
        this._dataService.getAllVacations().subscribe(data => {
            this.vacations = data;
            this._spinnerService.hide();
        }, err => {
            this._spinnerService.hide();
        });
    }

    ngOnInit() {
        this.loadAllVacations();
    }
}
