import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UsersVacationsComponent } from './usersVacations.component';
import { VacationDataService } from '../common/services/vacationData.service';
import { Observable } from 'rxjs/Observable';
import { Vacation } from '../common/models/vacation.model';
import { SpinnerService, SharedModule } from '../../shared';
import { VacationModalComponent } from '../common/widgets/vacationModal/vacationModal.component';
import { FormsModule, NgForm } from '@angular/forms';
import { NotificationService } from '../../shared';
import { UserService } from '../../shared/services/user.service';
import { HttpService } from '../../shared/services/http.service';

describe('UserVacationComponent', () => {
    let _fixture: ComponentFixture<UsersVacationsComponent>;
    let _vacationComponent: UsersVacationsComponent;
    let _service = {
        getAllVacations: function () {
            return Observable.of([new Vacation()]);
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, RouterTestingModule.withRoutes([]), FormsModule],
            declarations: [UsersVacationsComponent, VacationModalComponent],
            providers: [
                { provide: VacationDataService, useValue: _service },
                {provide: UserService, useValue: {}},
                {provide: HttpService, useValue: {}},
                SpinnerService, NotificationService
            ]
        });

        _fixture = TestBed.createComponent(UsersVacationsComponent);
        _vacationComponent = <UsersVacationsComponent>_fixture.debugElement.componentInstance;
    });

    it('can be instantiated successfully', () => {
        expect(_vacationComponent).toBeTruthy();
    });

    it('can get all vacations when initialized', () => {
        _vacationComponent.ngOnInit();

        expect(_vacationComponent.vacations[0] instanceof Vacation).toBeTruthy();
        expect(_vacationComponent.vacations.length).toBe(1);
    });

    it('can add vacation', () => {
        let vacationModal = (<any>_vacationComponent)._vacationModal;
        spyOn(vacationModal, 'show');

        _vacationComponent.employeeId = '1';

        _vacationComponent.addVacation();

        expect(vacationModal.show).toHaveBeenCalled();
    });

    it('loads all entries on vacation saved', () => {
        let vacationComponent = (<any>_vacationComponent);
        spyOn(vacationComponent, 'loadAllVacations');

        _vacationComponent.onVacationSaved({});

        expect(vacationComponent.loadAllVacations).toHaveBeenCalled();
    });
});
