﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule, ManagerGuardService, UserService } from '../shared';
import { TimeEntryDataService, ActivityDataService, TimeEntryModalComponent } from './common';
import { MyTimeEntriesComponent } from './myTimeEntries/myTimeEntries.component';
import { PendingComponent } from './pendingApproval/pending.component';
import { ApprovedComponent } from './approved/approved.component';
import { ActivitiesComponent } from './activities/activities.component';

@NgModule({
    declarations: [
        MyTimeEntriesComponent,
        TimeEntryModalComponent,
        ActivitiesComponent,
        PendingComponent,
        ApprovedComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        RouterModule.forChild([
            { path: 'time-entries', component: MyTimeEntriesComponent, resolve: {
                user: UserService
            }},
            { path: 'pending-approval', component: PendingComponent, canActivate: [ManagerGuardService] },
            { path: 'activities', component: ActivitiesComponent, canActivate: [ManagerGuardService] },
            { path: 'approved', component : ApprovedComponent, canActivate: [ManagerGuardService] }
        ])
    ],
    exports: [TimeEntryModalComponent],
    providers: [TimeEntryDataService, ActivityDataService]
})
export class TimeTrackingModule {
}
