import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ApprovedComponent } from './approved.component';
import { WeeklyTimeEntry, TimeEntrySearch, TimeEntryModalComponent, TimeEntryDataService, TimeEntryStatus } from '../common';
import {
    User, SpinnerService, ModalComponent, DialogService,
    FilterByPipe, NotificationService, UserService
} from '../../shared';
import { Observable } from 'rxjs/Observable';
import { TimeEntrySummary } from '../common/models/timeEntrySummary.model';
import { TimeTrackingModule } from '../timeTracking.module';

describe('ApprovedComponent', () => {
    const _excelFormat = 'excel';
    const _csvFormat = 'csv';
    const _mockSaveFile = jasmine.createSpy('saveFile');

    let _fixture: ComponentFixture<ApprovedComponent>;
    let _approvedComponent: ApprovedComponent;
    let mockWeeklyEntries: WeeklyTimeEntry;
    let _mockDataService;
    let _timeEntryModal: TimeEntryModalComponent;
    let _userService: UserService = <any>{
        getCurrentUser: jasmine.createSpy('getCurrentUser').and.returnValue(Observable.of(new User()))
    };

    beforeEach(() => {
        mockWeeklyEntries = <any>{
            timeEntries: [new TimeEntrySummary()],
            currentWeekStartDate: new Date(),
            currentWeek: jasmine.createSpy('currentWeek'),
            nextWeek: jasmine.createSpy('nextWeek'),
            previousWeek: jasmine.createSpy('previousWeek')
        };

        _mockDataService = {
            getTimeEntries() {
                return Observable.of([]);
            },
            downloadReport: jasmine.createSpy('downloadReport').and.returnValue(Observable.of('success'))
        };

        TestBed.configureTestingModule({
            imports: [TimeTrackingModule],
            providers: [SpinnerService, DialogService,
                { provide: TimeEntryDataService, useValue: _mockDataService },
                { provide: UserService, useValue: _userService },
                { provide: NotificationService, useValue: {} }
            ]
        });

        _fixture = TestBed.createComponent(ApprovedComponent);
        _approvedComponent = <ApprovedComponent>_fixture.debugElement.componentInstance;
        _approvedComponent.ngOnInit();

        _approvedComponent.weeklyTimes = mockWeeklyEntries;
        (<any>_approvedComponent).saveFile = _mockSaveFile;

        _timeEntryModal = <TimeEntryModalComponent>_fixture.debugElement.componentInstance;
    });

    it('can be initialized', () => {
        expect(_approvedComponent).toBeTruthy();
    });

    it('can get correct timeEntrySearch', () => {
        let search = _approvedComponent.getSearch();

        expect(search.status).toBe(TimeEntryStatus.approved);
        expect(search.beginDateTime).toBe(mockWeeklyEntries.currentWeekStartDate);
    });

    it('should create search parameters to download file', () => {
        let search = new TimeEntrySearch();
        search.beginDateTime = new Date('2017-01-18' + ' ' + '09:00:00');
        search.userId = 'user1';
        search.endDateTime = new Date('2017-02-18' + ' ' + '10:00:00');
        search.projectManagerId = 'manager1';

        spyOn(_approvedComponent, 'getSearch').and.returnValue(search);

        _approvedComponent.download(_excelFormat);

        expect(_approvedComponent.getSearch).toHaveBeenCalledTimes(1);
    });

    it('should call api to download report', () => {
        _approvedComponent.download(_csvFormat);

        expect(_mockDataService.downloadReport).toHaveBeenCalled();
    });

    it('should save downloaded file as csv', () => {
        _approvedComponent.download(_csvFormat);

        expect(_mockSaveFile).toHaveBeenCalledWith(_mockDataService.downloadReport().value, 'csv');
    });

    it('should save downloaded file as excel', () => {
        _approvedComponent.download(_excelFormat);

        expect(_mockSaveFile).toHaveBeenCalledWith(_mockDataService.downloadReport().value, 'excel');
    });

    it('should not download data if there are no time entries to download', () => {
        (<any>_approvedComponent).weeklyTimes.timeEntries = [];
        _approvedComponent.download(_excelFormat);

        expect(_mockDataService.downloadReport).not.toHaveBeenCalled();
    });

    it('can go to previous week', () => {
        _approvedComponent.goPrevious();

        expect(mockWeeklyEntries.previousWeek).toHaveBeenCalled();
    });

    it('can go to next week', () => {
        _approvedComponent.goNext();

        expect(mockWeeklyEntries.nextWeek).toHaveBeenCalled();
    });

    it('can go to currentWeek week', () => {
        _approvedComponent.goCurrent();

        expect(mockWeeklyEntries.currentWeek).toHaveBeenCalled();
    });

    it('can dsplay time entry modal', () => {
        let modal = {show: jasmine.createSpy('show')};
        (<any>_approvedComponent).timeEntryModal = modal;

        _approvedComponent.display(new TimeEntrySummary());

        expect(modal.show).toHaveBeenCalled();
    });
});
