import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import {
    WeeklyTimeEntry, TimeEntrySearch, TimeEntryDataService, TimeEntryModalComponent,
    TimeEntrySummary, TimeEntryModalOptions, TimeEntryStatus
} from '../common';
import { DateTimeHelper, SpinnerService, NotificationService, UserService } from '../../shared';
import * as FileSaver from 'file-saver';
declare var $: any;

@Component({
    selector: 'app-approved-component',
    templateUrl: './approved.component.html'
})
export class ApprovedComponent implements OnInit, AfterViewInit {
    private _userId: string;
    weeklyTimes: WeeklyTimeEntry;
    readonly EXCEL_FORMAT = 'excel';
    readonly CSV_FORMAT = 'csv';

    @ViewChild(TimeEntryModalComponent)
    public timeEntryModal: TimeEntryModalComponent;

    @ViewChild('datePicker')
    datePicker: ElementRef;

    @ViewChild('selectedDate')
    selectedDate: ElementRef;

    constructor(
        private _dataService: TimeEntryDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService,
        private _userService: UserService
    ) {
    }

    display(timeEntry: TimeEntrySummary): void {
        this.timeEntryModal.show(timeEntry, TimeEntryModalOptions.view);
    }

    ngOnInit(): void {
        this._userService.getCurrentUser().subscribe(user => {
            let search = new TimeEntrySearch();

            this._userId = user.id;

            search.status = TimeEntryStatus.approved;
            this.weeklyTimes = new WeeklyTimeEntry(
                this._dataService, this._spinnerService,
                this._notificationService, search
            );
        });
    }

    ngAfterViewInit(): void {
        $(this.datePicker.nativeElement).datepicker({autoclose: true});

        $(this.selectedDate.nativeElement)
        .on('change', (e) => {
            let selectedDate = new Date($(this.selectedDate.nativeElement).val());
            this.weeklyTimes.goToWeek(selectedDate);
        });
    }

    download(format: string) {
        if (this.weeklyTimes.timeEntries.length <= 0) {
            return;
        }

        this._spinnerService.show();

        this._dataService.downloadReport(format, this.getSearch().toParams())
            .subscribe(
            data => {
                this._spinnerService.hide();
                this.saveFile(data, format);
            },
            () => {
                this._spinnerService.hide();
                this._notificationService.showError('Error downloading File');
            }
            );
    }

    getSearch(): TimeEntrySearch {
        let search = new TimeEntrySearch();

        search.beginDateTime = this.weeklyTimes.currentWeekStartDate;
        search.endDateTime = DateTimeHelper.getWeekLastDate(this.weeklyTimes.currentWeekStartDate);
        search.status = TimeEntryStatus.approved;

        return search;
    }

    goPrevious(): void {
        this.weeklyTimes.previousWeek();
    }

    goCurrent(): void {
        this.weeklyTimes.currentWeek();
    }

    goNext(): void {
        this.weeklyTimes.nextWeek();
    }

    private saveFile(data, format): void {
        let blob = (format === this.CSV_FORMAT) ? new Blob([data]) :
            new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

        let date = new Date();

        format = (format === this.CSV_FORMAT) ? format : 'xlsx';

        FileSaver.saveAs(blob, `time_tracking_report-${date.toDateString()}.${format}`);
    }
}
