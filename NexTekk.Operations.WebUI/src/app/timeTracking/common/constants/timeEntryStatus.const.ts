export class TimeEntryStatus {
    static saved = 'saved';
    static submitted = 'submitted';
    static approved = 'approved';
    static denied = 'denied';
}
