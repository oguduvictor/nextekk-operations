import { Activity } from '../models/activity.model';
import { ActivityDataService } from './activityData.service';
import { Observable } from 'rxjs/Observable';

describe('TimeEntryDataService', () => {
    let _mockHttp;
    let dataService: ActivityDataService;
    let testActivity: Activity;

    beforeEach(() => {
        _mockHttp = <any>{
            putData: jasmine.createSpy('putData').and.returnValue(Observable.of(new Activity())),
            postData: jasmine.createSpy('postData').and.returnValue(Observable.of(new Activity())),
            deleteData: jasmine.createSpy('deleteData').and.returnValue(Observable.of(new Activity())),
            getMultiple: jasmine.createSpy('postData').and.returnValue(Observable.of([new Activity()])),
            getData: jasmine.createSpy('getData').and.returnValue(Observable.of(new Activity()))
        };

        dataService = new ActivityDataService(_mockHttp);

        testActivity = new Activity(
            'test_id', 'test_activity',
            true, 'for unit tests only'
        );
    });

    it('can be instantiated successfully', () => {
        expect(dataService).toBeTruthy();
    });

    it('can save an new activity', () => {
        testActivity.id = null;
        dataService.saveActivity(testActivity);

        expect(_mockHttp.postData).toHaveBeenCalled();
    });

    it('can update an existing activity', () => {
        dataService.saveActivity(testActivity);

        expect(_mockHttp.putData).toHaveBeenCalled();
    });

    it('can delete an activity', () => {
        dataService.deleteActivity('test_id');

        expect(_mockHttp.deleteData).toHaveBeenCalled();
    });

    it('can retrieve an existing activity', () => {
        dataService.getActivity('test_id');

        expect(_mockHttp.getData).toHaveBeenCalled();
    });

    it('can get activities', () => {
        dataService.getActivities();

        expect(_mockHttp.getMultiple).toHaveBeenCalled();
    });
});
