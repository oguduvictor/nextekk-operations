import { Injectable } from '@angular/core';
import { Activity } from '../models/activity.model';
import { HttpService } from '../../../shared';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ActivityDataService {
    private _activityApiUrl = 'api/activity/';

    constructor (private _http: HttpService) {}

    saveActivity(activityEntry: Activity): Observable<Activity> {
        return !!activityEntry.id
            ? this._http.putData(this._activityApiUrl + activityEntry.id, activityEntry.toDto(), Activity)
            : this._http.postData(this._activityApiUrl, activityEntry.toDto(), Activity);
    }

    deleteActivity(activityId: string): Observable<string> {
        return this._http.deleteData(this._activityApiUrl + activityId);
    }

    getActivities(): Observable<Activity[]> {
        return this._http.getMultiple<Activity>(this._activityApiUrl, Activity);
    }

    getActivity(activityId: string): Observable<Activity> {
        return this._http.getData(this._activityApiUrl + activityId, Activity);
    }
}
