import { Injectable } from '@angular/core';
import { TimeEntrySummary } from '../models/timeEntrySummary.model';
import { TimeEntrySearch } from '../models/timeEntrySearch.model';
import { Activity } from '../models/activity.model';
import { IdCommentPair } from '../models/idCommentPair.model';
import { TimeEntryStatus } from '../constants/timeEntryStatus.const';
import { HttpService } from '../../../shared';
import { Observable } from 'rxjs/Observable';
import { URLSearchParams, Response } from '@angular/http';
import { RequestOptions, ResponseContentType } from '@angular/http';

@Injectable()
export class TimeEntryDataService {
    private _timeEntryApiUrl = 'api/timeEntry/';
    constructor (private _http: HttpService) {}

    getTimeEntries(search: TimeEntrySearch): Observable<TimeEntrySummary[]> {
        return this._http.getMultiple(this._timeEntryApiUrl + 'search', TimeEntrySummary, { search: search.toParams() });
    }

    saveTimeEntry(timeEntry: TimeEntrySummary): Observable<TimeEntrySummary> {
        return !!timeEntry.id
            ? this._http.putData(this._timeEntryApiUrl + 'update/' + timeEntry.id, timeEntry.toDto(), TimeEntrySummary)
            : this._http.postData(this._timeEntryApiUrl + 'create', timeEntry.toDto(), TimeEntrySummary);
    }

    approveTimeEntries(requestData: IdCommentPair[]): Observable<string> {

        return this.sendChangeStatusRequest(requestData, 'approve');
    }

    denyTimeEntry(timeEntryId: string, managerComment: string): Observable<string> {
        let requestData = [new IdCommentPair(timeEntryId, managerComment)];

        return this.sendChangeStatusRequest(requestData, 'deny');
    }

    deleteTimeEntry(id: string): Observable<string> {
        return this._http.deleteData(this._timeEntryApiUrl + 'delete/' + id);
    }

    submitTimeEntries(timeEntryIds: string[]): Observable<string> {
        let requestData = this.getIdCommentPairs(timeEntryIds);

        return this.sendChangeStatusRequest(requestData, 'submit');
    }

    unsubmitTimeEntries(timeEntryIds: string[]): Observable<string> {
        let requestData = this.getIdCommentPairs(timeEntryIds);

        return this.sendChangeStatusRequest(requestData, 'unsubmit');
    }

    getActivities(): Observable<Activity[]> {
        return this._http.getMultiple(this._timeEntryApiUrl + 'UserActivities', Activity);
    }

    private sendChangeStatusRequest(idCommentPairs: IdCommentPair[], action: string): Observable<string> {
        return this._http.putData(this._timeEntryApiUrl + action, idCommentPairs)
            .map(() => 'success');
    }

    changeStatusToSaved(entryIds: string[]): Observable<string> {
        let idCommentPair = this.getIdCommentPairs(entryIds);

        return this._http.putData(this._timeEntryApiUrl + 'changestatus?status=' + TimeEntryStatus.saved, idCommentPair)
            .map(() => 'success');
    }

    // TODO: turn this back to private or move it into a helper class
    getIdCommentPairs(ids: string[]): IdCommentPair[] {
        return ids.map((value: string) => {
            return new IdCommentPair(value, null);
        });
    }

    downloadReport(format: string, search: URLSearchParams) {
        let options = new RequestOptions();
        options.responseType = ResponseContentType.ArrayBuffer;

        return this._http.getData<Response>(`${this._timeEntryApiUrl}DownloadReport?format=${format}&${search}`, null, options);
    }
}
