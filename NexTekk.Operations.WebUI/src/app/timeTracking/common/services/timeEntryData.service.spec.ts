import { getTestBed, TestBed } from '@angular/core/testing';
import { BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { TimeEntryDataService } from './timeEntryData.service';
import { TimeEntrySummary } from '../models/timeEntrySummary.model';
import { TimeEntrySearch } from '../models/timeEntrySearch.model';
import { Activity} from '../models/activity.model';
import { TimeEntryStatus} from '../../common';
import { IdCommentPair } from '../models/idCommentPair.model';
import { AuthService, HttpService } from '../../../shared';

describe('TimeEntryDataService', () => {
    let _backend: MockBackend;
    let _service: TimeEntryDataService;
    let _url: string;
    let _authService = {
        getAccessToken: () => 'token_0001'
    };
    let _timeEntryDTO = {
        id : 'test_id',
        task : 'test_task',
        comment : 'test_comment',
        status : 'test_status',
        beginDateTime : '2017-05-18 09:00:00',
        endDateTime : '2017-05-28 17:00:00',
        activity: <any>{ title: 'activity_title', id: 'activity_id', billable: 'activity_billable'},
        creator: <any>{id: 'creator_id', firstName: 'john', lastName: 'lula'}
     };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                TimeEntryDataService,
                { provide: AuthService, useValue: _authService },
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions,
                        AuthService
                    ],
                    provide: HttpService,
                    useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions, authService: AuthService) => {
                        let noficationService = <any>{showError: function(){} };
                        return new HttpService(<any>backend, defaultOptions, authService, noficationService);
                    }
                }
            ]
        });

        let testbed = getTestBed();
        _backend = testbed.get(MockBackend);
        _service = TestBed.get(TimeEntryDataService);
    });

    function setupConnections(backend: MockBackend, options: any, url?) {
        backend.connections.subscribe((connection: MockConnection) => {
            if (connection.request.url === url) {
                let responseOptions = new ResponseOptions(options);
                let response = new Response(responseOptions);
                connection.mockRespond(response);
            } else {
                throw new Error('Invalid Url');
            }
        });
    }

    it('getTimeEntries() should get list of TimeEntries', () => {
        let entrySearch = new TimeEntrySearch();

        entrySearch.beginDateTime = new Date();
        entrySearch.endDateTime = new Date();
        _url = `http://localhost:33895/api/timeEntry/search?${entrySearch.toParams()}`;

        setupConnections(_backend, {
          body: [ _timeEntryDTO ],
          status: 200
        }, _url);

        _service.getTimeEntries(entrySearch).subscribe((data: TimeEntrySummary[]) => {

          expect(data instanceof Array).toBeTruthy();
          expect(data[0] instanceof TimeEntrySummary).toBeTruthy();
        });
    });

    it('getActivities() should get a list of Observable activities', () => {
        _url = 'http://localhost:33895/api/timeEntry/UserActivities';

        setupConnections(_backend, {
          body: [ Activity ],
          status: 200
        }, _url);

        _service.getActivities().subscribe((data: Activity[]) => {

          expect(data instanceof Array).toBeTruthy();
          expect(data[0] instanceof Activity).toBeTruthy();
        });
    });

    it('saveTimeEntry() should send and create timeEntry', () => {
        _url = 'http://localhost:33895/api/timeEntry/create';
        let entry = new TimeEntrySummary();

        setupConnections(_backend, {
            body: _timeEntryDTO,
            status: 200
        }, _url);

        _service.saveTimeEntry(entry).subscribe((data: TimeEntrySummary) => {

            expect(data instanceof TimeEntrySummary).toBeTruthy();
        });
    });

    it('deleteTimeEntry() should return success', () => {
        let _id = 'testId';
        _url = `http://localhost:33895/api/timeEntry/delete/${_id}`;

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.deleteTimeEntry(_id).subscribe((data: string) => {

            expect(data).toBe('success');
        });
    });

    it('approveTimeEntries() should call sendChangeStatusRequest', () => {
        _url = 'http://localhost:33895/api/timeEntry/approve';
        // let timeEntryIds: string[] = ['id1'];
        let timeEntryIds = [new IdCommentPair('id', 'any comment')];

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.approveTimeEntries(timeEntryIds).subscribe((data: string) => {

            expect(data).toBe('success');
        });
    });

    it('submitTimeEntries() should call sendChangeStatusRequest', () => {
        _url = 'http://localhost:33895/api/timeEntry/submit';
        let timeEntryIds: string[] = ['id1'];

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.submitTimeEntries(timeEntryIds).subscribe((data: string) => {

            expect(data).toBe('success');
        });
    });

    it('can deny submitted time entries', () => {

        _url = 'http://localhost:33895/api/timeEntry/deny';
        let timeEntryId = 'test_id';

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.denyTimeEntry(timeEntryId, 'managers_Comment').subscribe((data: string) => {

            expect(data).toBe('success');
        });
    });

    it ('it can change status', () => {
        _url = 'http://localhost:33895/api/timeEntry/changestatus?status=' + TimeEntryStatus.saved;

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        let timeEntryId = 'test_Id';
        _service.changeStatusToSaved([timeEntryId])
        .subscribe((data: string) => {
            expect(data).toEqual('success');
        });
    });
});
