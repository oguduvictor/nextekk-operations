﻿export enum TimeEntryModalOptions {
    view,
    edit,
    create,
    managerEdit
}
