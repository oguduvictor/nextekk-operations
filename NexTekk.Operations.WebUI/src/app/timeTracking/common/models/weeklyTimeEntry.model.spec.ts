
import { WeeklyTimeEntry } from './weeklyTimeEntry.model';
import { TimeEntryDataService } from '../services/timeEntryData.service';
import { TimeEntrySummary } from './timeEntrySummary.model';
import { TimeEntrySearch } from './timeEntrySearch.model';
import { DateTimeHelper, SpinnerService, NotificationService, User } from '../../../shared';
import { TimeEntryStatus } from '../constants/timeEntryStatus.const';
import { Observable } from 'rxjs/Observable';
import { Activity } from './activity.model';

describe('WeeklyTimeEntry', () => {
    let _mockDataService2 = {
        getActivities: function (): Observable<any[]> {
            return Observable.of([]);
        },

        getTimeEntries: function (search: TimeEntrySearch): Observable<TimeEntrySummary[]> {
            return Observable.of([]);
        },
        submitTimeEntries: function (timeEntriesids: string[]): Observable<string> {
            return Observable.of('string');
        },
        unsubmitTimeEntries: function (timeEntryIds: string[]): Observable<string> {
            return Observable.of('');
        },

        getIdCommentPairs: function (timeEntryIds: string[]) {
            return [];
        }
    };

    let _mock: TimeEntryDataService;
    let _mockNotification: NotificationService;
    let _weekTimeEntry: WeeklyTimeEntry;
    let _timeEntries: TimeEntrySummary[];
    let _mockSpinnerService;

    beforeEach(() => {
        _mock = <TimeEntryDataService>_mockDataService2;
        _mock.approveTimeEntries = function (): Observable<string> {
            return Observable.of('approved');
        };

        _mockNotification = <NotificationService>jasmine.createSpyObj('NotificationService', [
            'showError', 'showSuccess', 'showInfo', 'showWarning'
        ]);

        _mockSpinnerService = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);

        _weekTimeEntry = new WeeklyTimeEntry(_mock, _mockSpinnerService, _mockNotification);

        _timeEntries = getTimeEntries();
    });

    it('Can Be initialized', () => {
        let weeklyTimeEntry = new WeeklyTimeEntry(_mock, _mockSpinnerService, _mockNotification);

        expect(weeklyTimeEntry).toBeTruthy();
    });

    it('Can get thisweek', () => {
        let thisWeek = _weekTimeEntry.isThisWeek;

        expect(thisWeek).toBe(true);
    });

    it('Can get totalHours', () => {
        let totalHours = _weekTimeEntry.totalHours;

        expect(totalHours).toBe(0);
    });

    it('canSubmit a timeEntry', () => {
        _weekTimeEntry.timeEntries = [];
        let canSubmit = _weekTimeEntry.canSubmit;

        expect(canSubmit).toBe(false);
    });

    it('canUnSubmit a timeEntry', () => {
        _weekTimeEntry.timeEntries = [];
        let canUnSubmit = _weekTimeEntry.canUnsubmit;

        expect(canUnSubmit).toBe(false);
    });

    it('can get nextweek start date', () => {
        let thisWeekStartDate = DateTimeHelper.getWeekFirstDate();
        let nextwekk = thisWeekStartDate.valueOf() + (DateTimeHelper.microsecondsPerDay * 7);

        spyOn(_weekTimeEntry, 'reloadTimeEntries');

        _weekTimeEntry.nextWeek();

        expect(_weekTimeEntry.reloadTimeEntries).toHaveBeenCalled();
        expect(_weekTimeEntry.currentWeekStartDate.valueOf()).toEqual(nextwekk);
    });

    xit('can get previousweek start date', () => {
        let thisWeekStartDate = DateTimeHelper.getWeekFirstDate();
        let previousWeek = thisWeekStartDate.valueOf() - (DateTimeHelper.microsecondsPerDay * 7);

        spyOn(_weekTimeEntry, 'reloadTimeEntries');

        _weekTimeEntry.previousWeek();

        expect(_weekTimeEntry.reloadTimeEntries).toHaveBeenCalled();
        expect(_weekTimeEntry.currentWeekStartDate.valueOf()).toEqual(previousWeek);
    });

    it('can get currentweek start date', () => {
        let thisweek = DateTimeHelper.getWeekFirstDate();
        spyOn(_weekTimeEntry, 'reloadTimeEntries');

        _weekTimeEntry.currentWeek();

        expect(_weekTimeEntry.reloadTimeEntries).toHaveBeenCalled();
        expect(_weekTimeEntry.currentWeekStartDate).toEqual(thisweek);
    });

    it('can get submit weeklytimeentry', () => {
        spyOn(_weekTimeEntry, 'reloadTimeEntries');

        _weekTimeEntry.submit();

        expect(_weekTimeEntry.reloadTimeEntries).toHaveBeenCalled();
        expect(_weekTimeEntry.isLoaded).toBe(false);
    });

    it('can get unsubmit weeklytimeentry', () => {
        spyOn(_weekTimeEntry, 'reloadTimeEntries');

        _weekTimeEntry.unsubmit();

        expect(_weekTimeEntry.reloadTimeEntries).toHaveBeenCalled();
        expect(_weekTimeEntry.isLoaded).toBe(false);
    });

    it('approveTimeEntries() should approve selected time entries', () => {
        let timeEntry = new TimeEntrySummary();

        _weekTimeEntry = new WeeklyTimeEntry(_mock, _mockSpinnerService, _mockNotification);

        spyOn(_weekTimeEntry, 'reloadTimeEntries');

        _weekTimeEntry.approveTimeEntries([timeEntry.id]);

        expect(_weekTimeEntry.reloadTimeEntries).toHaveBeenCalled();
        expect(_weekTimeEntry.isLoaded).toBe(false);
    });

    it('can reload timeEntries', () => {
        _weekTimeEntry.reloadTimeEntries();

        expect(_weekTimeEntry.timeEntriesTree.length).toEqual(7);
    });

    it('should return unique activities for an array of time entries', () => {
        _mock.getTimeEntries = function (): Observable<TimeEntrySummary[]> {
            return Observable.of(_timeEntries);
        };

        _weekTimeEntry = new WeeklyTimeEntry(_mock, _mockSpinnerService, _mockNotification);
        _weekTimeEntry.reloadTimeEntries();

        expect(_weekTimeEntry.uniqueActivities.length).toEqual(2);

    });

    it('does not repeat employee when populating uniqueEmployees', () => {
        _mock.getTimeEntries = function (): Observable<TimeEntrySummary[]> {
            return Observable.of(_timeEntries);
        };

        _weekTimeEntry = new WeeklyTimeEntry(_mock, _mockSpinnerService, _mockNotification);

        expect(_weekTimeEntry.timeEntries.length).toBe(3);

        expect(_weekTimeEntry.uniqueEmployees.length).toEqual(2);
    });

    it('returns the right hours when timeEntries are filtered by activityId', () => {
        _mock.getTimeEntries = function (): Observable<TimeEntrySummary[]> {
            return Observable.of(_timeEntries);
        };

        _weekTimeEntry = new WeeklyTimeEntry(_mock, _mockSpinnerService, _mockNotification);
        _weekTimeEntry.selectedActivityId = 'activity1';
        let result = _weekTimeEntry.totalHours;

        expect(result).toBe(2);
    });

    it('displays spinner service when submit is called', () => {
        _weekTimeEntry.submit();

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('displays spinner service when unSubmit is called', () => {
        _weekTimeEntry.unsubmit();

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('displays spinner service when approveTimeEntries is called', () => {
        _weekTimeEntry.approveTimeEntries(['id1', 'id2']);

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('hides spinner service when submit is successful', () => {
        _weekTimeEntry.submit();

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('hides spinner service when unsubmit is successful', () => {
        _weekTimeEntry.unsubmit();

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('hides spinner service when approveTimeEntries is successful', () => {
        _weekTimeEntry.approveTimeEntries(['id1']);

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('shows notification when time entries are submitted successfully', () => {
        _weekTimeEntry.timeEntries = _timeEntries;

        _weekTimeEntry.submit();
        expect(_mockNotification.showSuccess).toHaveBeenCalled();
    });

    it('shows notification when time entries are unsubmitted successfully', () => {
        _weekTimeEntry.timeEntries = _timeEntries;

        _weekTimeEntry.unsubmit();
        expect(_mockNotification.showSuccess).toHaveBeenCalled();
    });

    it('should reset selectedActivityId and selectedEmployeeId to undefined when nextWeek() is called', () => {
        _weekTimeEntry.selectedActivityId = 'activity1';
        _weekTimeEntry.selectedEmployeeId = 'user1';

        expect(_weekTimeEntry.selectedActivityId).toEqual('activity1');
        expect(_weekTimeEntry.selectedEmployeeId).toEqual('user1');

        _weekTimeEntry.nextWeek();

        expect(_weekTimeEntry.selectedActivityId).toBe(undefined);
        expect(_weekTimeEntry.selectedEmployeeId).toBe(undefined);
    });

    it('should reset should reset selectedActivityId and selectedEmployeeId to undefined when currentWeek() is called', () => {
        _weekTimeEntry.selectedActivityId = 'activity1';
        _weekTimeEntry.selectedEmployeeId = 'user1';

        expect(_weekTimeEntry.selectedActivityId).toEqual('activity1');
        expect(_weekTimeEntry.selectedEmployeeId).toEqual('user1');

        _weekTimeEntry.currentWeek();

        expect(_weekTimeEntry.selectedActivityId).toBe(undefined);
        expect(_weekTimeEntry.selectedEmployeeId).toBe(undefined);
    });

    it('should reset should reset selectedActivityId and selectedEmployeeId to undefined when previousWeek() is called', () => {
        _weekTimeEntry.selectedActivityId = 'activity1';
        _weekTimeEntry.selectedEmployeeId = 'user1';

        expect(_weekTimeEntry.selectedActivityId).toEqual('activity1');
        expect(_weekTimeEntry.selectedEmployeeId).toEqual('user1');

        _weekTimeEntry.previousWeek();

        expect(_weekTimeEntry.selectedActivityId).toBe(undefined);
        expect(_weekTimeEntry.selectedEmployeeId).toBe(undefined);
    });

    it('should round off the value of total hours to 2 decimal place', () => {
        _timeEntries[0].beginDateTime = new Date('2017-2-14 08:28:00');
        _timeEntries[0].endDateTime = new Date('2017-2-14 09:00:00');

        _timeEntries[1].beginDateTime = new Date('2017-2-14 09:20:00');
        _timeEntries[1].endDateTime = new Date('2017-2-14 10:00:00');

        _timeEntries[2].beginDateTime = new Date('2017-2-14 10:47:00');
        _timeEntries[2].endDateTime = new Date('2017-2-14 13:00:00');

        _weekTimeEntry.timeEntries = _timeEntries;

        expect(_weekTimeEntry.totalHours).toEqual(3.42);
    });
});

function getTimeEntries(): TimeEntrySummary[] {
    let sampleTimeEntries: TimeEntrySummary[];

    let t1 = new TimeEntrySummary();
    t1.id = 'id1';
    t1.activityId = 'activity1';
    t1.beginDateTime = new Date('2017-2-14 8:00:00');
    t1.endDateTime = new Date('2017-2-14 10:00:00');
    t1.activity = <Activity>{id: 'activity1'};
    t1.status = TimeEntryStatus.saved;
    t1.creator = <any>{id: 'user1', firstName: 'moses', lastName: false};

    let t2 = new TimeEntrySummary();
    t2.id = 'id2';
    t2.activityId = 'activity2';
    t2.beginDateTime = new Date('2017-2-14 11:00:00');
    t2.endDateTime = new Date('2017-2-14 1:00:00');
    t2.activity = <Activity>{id: 'activity2'};
    t2.status = TimeEntryStatus.saved;
    t2.creator = <any>{id: 'user1', firstName: 'moses', lastName: false};

    let t3 = new TimeEntrySummary();
    t3.id = 'id3';
    t3.activityId = 'activity2';
    t3.beginDateTime = new Date('2017-2-14 3:00:00');
    t3.endDateTime = new Date('2017-2-14 6:00:00');
    t3.activity = <Activity>{id: 'activity2'};
    t3.status = TimeEntryStatus.denied;
    t3.creator = <any>{id: 'user2'};

    sampleTimeEntries = [t1, t2, t3];

    return sampleTimeEntries;
}
