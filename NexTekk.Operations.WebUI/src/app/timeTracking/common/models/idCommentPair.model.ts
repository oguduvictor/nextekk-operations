export class IdCommentPair {
    constructor(
        public id: string = null,
        public comment: string = null) { }
}
