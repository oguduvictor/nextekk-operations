﻿import { TimeEntrySummary } from './timeEntrySummary.model';
import { DateTimeHelper } from '../../../shared';
import { TimeEntryDataService } from '../services/timeEntryData.service';
import { TimeEntrySearch } from './timeEntrySearch.model';
import { Activity } from './activity.model';
import { User, FilterByPipe, SpinnerService, NotificationService } from '../../../shared';
import { TimeEntryStatus } from '../constants/timeEntryStatus.const';
import * as _ from 'lodash';

export class WeeklyTimeEntry {
    private _timeEntries: TimeEntrySummary[] = [];
    private _selectedActivityId: string;
    private _selectedEmployeeId: string;
    private _shortDateFormat = 'yyyy-MM-dd';
    private _selectedStatus: TimeEntryStatus;

    timeEntriesTree: [string, Date, TimeEntrySummary[]][];
    timeEntries: TimeEntrySummary[] = [];
    timeEntriesCount: number;
    uniqueActivities: Activity[] = [];
    uniqueEmployees: User[] = [];
    currentWeekStartDate: Date;
    thisWeekStartDate: Date;
    isLoaded = false;
    errorMessage: any;
    allStatus = [TimeEntryStatus.approved, TimeEntryStatus.denied, TimeEntryStatus.saved,
    TimeEntryStatus.submitted];

    constructor(
        private _dataService: TimeEntryDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService,
        private _timeEntrySearch?: TimeEntrySearch) {

        this.thisWeekStartDate = DateTimeHelper.getWeekFirstDate();
        this.currentWeekStartDate = this.thisWeekStartDate;

        if (!this._timeEntrySearch) {
            this._timeEntrySearch = new TimeEntrySearch();
        }

        this.currentWeek();
    }

    get selectedActivityId(): string {
        return this._selectedActivityId;
    }

    get selectedEmployeeId(): string {
        return this._selectedEmployeeId;
    }

    get selectedStatus(): TimeEntryStatus {
        return this._selectedStatus;
    }

    set selectedStatus(selectedStatus: TimeEntryStatus) {
        this._selectedStatus = selectedStatus;
        this.refreshTimeEntryTree();
    }

    set selectedActivityId(activityId: string) {
        this._selectedActivityId = activityId;
        this.refreshTimeEntryTree();
    }

    set selectedEmployeeId(employeeId: string) {
        this._selectedEmployeeId = employeeId;
        this.refreshTimeEntryTree();
    }

    get isThisWeek(): boolean {
        return DateTimeHelper.shortDate(this.currentWeekStartDate) === DateTimeHelper.shortDate(this.thisWeekStartDate);
    }

    get totalHours(): number {
        let hrs =  this.timeEntries.reduce<number>((prevValue, currentValue) => {
            return prevValue + currentValue.hours;
        }, 0);

        return Number(hrs.toFixed(2));
    }

    get canSubmit(): boolean {
        return !!this._timeEntries.find(x => x.status === TimeEntryStatus.saved || x.status === TimeEntryStatus.denied);
    }

    get canUnsubmit(): boolean {
        return !!this._timeEntries.find(x => x.status === TimeEntryStatus.submitted);
    }

    get canAddNew(): boolean {
        return this.canSubmit || this._timeEntries.length === 0;
    }

    nextWeek(): void {
        this.currentWeekStartDate =  this.getAdjustedStartTime(
            new Date(this.currentWeekStartDate.valueOf() + (DateTimeHelper.microsecondsPerDay * 7)));
        this.reloadTimeEntries();
    }

    previousWeek(): void {
        this.currentWeekStartDate = this.getAdjustedStartTime(
            new Date(this.currentWeekStartDate.valueOf() - (DateTimeHelper.microsecondsPerDay * 7)));
        this.reloadTimeEntries();
    }

    currentWeek(): void {
        this.currentWeekStartDate = this.thisWeekStartDate;
        this.reloadTimeEntries();
    }

    goToWeek(date: Date): void {
        let weekFirstDay = DateTimeHelper.getWeekFirstDate(date);
        this.currentWeekStartDate = this.getAdjustedStartTime(weekFirstDay);
        this.reloadTimeEntries();
    }

    submit(): void {

        let ids = this.getIds(TimeEntryStatus.saved);
        let deniedIds = this.getIds(TimeEntryStatus.denied);
        ids = ids.concat(deniedIds);

        this.isLoaded = false;
        this._spinnerService.show();

        this._dataService
            .submitTimeEntries(ids)
            .subscribe(
            () => {
                this._spinnerService.hide();
                this.reloadTimeEntries();
                this._notificationService.showSuccess('submitted sucessfully!');
            },
            err => {
                this._spinnerService.hide();
                this.errorMessage = err;
                this._notificationService.showError('submit was not successful!');
            }
            );
    }

    unsubmit(): void {
        let ids = this.getIds(TimeEntryStatus.submitted);

        this.isLoaded = false;

        this._spinnerService.show();

        this._dataService
            .unsubmitTimeEntries(ids)
            .subscribe(
            () => {
                this.reloadTimeEntries();
                this._notificationService.showSuccess('unsubmitted successfully!');
            },
            err => {
                this._spinnerService.hide();
                this.errorMessage = err;
                this._notificationService.showError('unsubmit was not succesful');
            }
            );
    }

    addTimeEntry(timeEntry: TimeEntrySummary): void {
        let key = DateTimeHelper.format(timeEntry.beginDateTime, this._shortDateFormat);
        let item = this.timeEntriesTree.find(x => x[0] === key);

        if (!item) {
            return;
        }

        let existingTimeEntry = item[2].find(x => x.id === timeEntry.id);

        if (existingTimeEntry) {
            _.merge(existingTimeEntry, timeEntry);
            return;
        }

        item[2].push(timeEntry);
    }

    approveTimeEntries(entriesIds: string[]): void {
        this.isLoaded = false;

        this._spinnerService.show();

        this._dataService
            .approveTimeEntries(this._dataService.getIdCommentPairs(entriesIds))
            .subscribe(
            () => {
                this._spinnerService.hide();
                this.reloadTimeEntries();
            },
            err => {
                this._spinnerService.hide();
                this.errorMessage = err;
            }
            );
    }

    reloadTimeEntries(): void {
        this.resetTimeEntries(this.currentWeekStartDate);
        this.searchTimeEntries();
    }

    private getUniqueActivities() {
        this.uniqueActivities = [];

        this.timeEntries.forEach((value: TimeEntrySummary) => {
            if (!this.uniqueActivities.find(x => x.id === value.activityId)) {
                this.uniqueActivities.push(value.activity);
            }
        });
    }

    private getUniqueEmployees() {
        this.uniqueEmployees = [];

        this._timeEntries.forEach((value: TimeEntrySummary) => {
            if (!this.uniqueEmployees.find(x => x.id === value.creator.id)) {
                this.uniqueEmployees.push(value.creator);
            }
        });
    }

    private resetFilterDropdowns(): void {
        this.selectedActivityId = undefined;
        this.selectedEmployeeId = undefined;
    }

    private resetTimeEntries(startDate: Date): void {
        let dateValue = startDate.valueOf();

        this.timeEntriesTree = [];
        this._timeEntries = [];

        for (let i = 0; i < 7; i++) {
            let date = new Date(dateValue + (i * DateTimeHelper.microsecondsPerDay));
            let dateStr = DateTimeHelper.format(date, this._shortDateFormat);
            this.timeEntriesTree.push([dateStr, date, []]);
        }
    }

    private refreshTimeEntryTree() {
        let filter = new FilterByPipe();

        this.timeEntries = filter.transform(
            this._timeEntries,
            { creatorId: this.selectedEmployeeId, activityId: this.selectedActivityId, status: this.selectedStatus }
        );

        for (let item of this.timeEntriesTree) {
            item[2] = [];
        }

        for (let timeEntry of this.timeEntries) {
            this.addTimeEntry(timeEntry);
        }
    }

    private searchTimeEntries(): void {
        let search = this.getTimeEntrySearch();

        this.isLoaded = false;

        this._spinnerService.show();

        this._dataService
            .getTimeEntries(search)
            .subscribe(data => {
                this._spinnerService.hide();
                data.forEach(x => this.addTimeEntry(x));
                this._timeEntries = this.timeEntries = data;
                this.timeEntriesCount = this._timeEntries.length;
                this.getUniqueActivities();
                this.getUniqueEmployees();
                this.resetFilterDropdowns();
                this.isLoaded = true;
            },
            error => {
                this._spinnerService.hide();
                this.errorMessage = <any>error;
            }
            );
    }

    private getIds(status: string = null): string[] {
        let ids = this._timeEntries.reduce<string[]>((prevValue, currentValue) => {
            if (status === null || currentValue.status === status) {
                prevValue.push(currentValue.id);
            }
            return prevValue;
        }, []);

        return ids;
    }

    private getTimeEntrySearch() {
        this._timeEntrySearch.beginDateTime = this.currentWeekStartDate;
        this._timeEntrySearch.endDateTime = DateTimeHelper.getWeekLastDate(this.currentWeekStartDate);

        return this._timeEntrySearch;
    }

    // adjusts datetime for daylight saving
    private getAdjustedStartTime(date: Date) {
        const microsecondsPerHour = 1000 * 60 * 60;
        const hour = date.getHours();
        let dateValue = date.valueOf();

        if (hour === 23) { // an hour behind, pushing it into the previous day
            dateValue += microsecondsPerHour;
        } else if (hour === 1) { // an hour ahead
            dateValue -= microsecondsPerHour;
        } else {
            return date;
        }

        return new Date(dateValue);
    }
}
