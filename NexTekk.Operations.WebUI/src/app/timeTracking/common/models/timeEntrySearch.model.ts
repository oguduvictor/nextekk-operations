import { URLSearchParams  } from '@angular/http';

export class TimeEntrySearch {
    beginDateTime: Date;
    endDateTime: Date;
    userId: string;
    projectManagerId: string;
    status: string;
    activityId: string;

    public toParams() {
        let params = new URLSearchParams();

        params.set('beginDateTime', this.beginDateTime.toUTCString());
        params.set('endDateTime', this.endDateTime.toUTCString());
        params.set('creatorId', this.userId);
        params.set('projectManagerId', this.projectManagerId);
        params.set('status', this.status);
        params.set('activityId', this.activityId);

        return params;
    }
}
