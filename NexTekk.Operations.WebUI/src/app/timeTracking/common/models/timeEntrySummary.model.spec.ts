import { TimeEntrySummary } from './timeEntrySummary.model';
import { DateTimeHelper } from '../../../shared';

describe('TimeEntrySummary', () => {
    let testDTO = {
        id: 'test_id',
        task: 'test_task',
        comment: 'test_comment',
        status: 'test_status',
        beginDateTime: '2017-05-18 09:00:00',
        endDateTime: '2017-05-18 17:00:00',
        activity: <any>{ title: 'activity_title', id: 'activity_id', billable: 'activity_billable' },
        creator: <any>{ id: 'creator_id', firstName: 'john', lastName: 'lula' }
    };

    it('get number of hours', () => {
        let summary: TimeEntrySummary = new TimeEntrySummary();
        summary.fromDto(testDTO);

        expect(summary.hours).toEqual(8);
    });

    it('takes minutes into account when computing number of hours', () => {
        let summary = new TimeEntrySummary();
        summary.beginDateTime = new Date('2017-05-18 09:30:00');
        summary.endDateTime = new Date('2017-05-18 11:00:00');

        expect(summary.hours).toEqual(1.5);
    });

    it('returns minutes in units of hrs', () => {
        let summary = new TimeEntrySummary();
        summary.beginDateTime = new Date('2017-05-18 09:10:00');
        summary.endDateTime = new Date('2017-05-18 9:40:00');

        expect(summary.hours).toEqual(0.5);
    });

    it('can successfully create dto', () => {
        let summary: TimeEntrySummary = new TimeEntrySummary();
        summary.fromDto(testDTO);

        let newDTO = summary.toDto();

        expect(newDTO.id).toEqual(summary.id);
        expect(newDTO.task).toEqual(summary.task);
        expect(newDTO.comment).toEqual(summary.comment);
        expect(newDTO.activityId).toEqual(summary.activity.id);
        expect(newDTO.endDateTime).toBe(summary.endDateTime.toUTCString());
    });

    it('can pull values from dto correctly', () => {
        let summary: TimeEntrySummary = new TimeEntrySummary();
        summary.fromDto(testDTO);
        expect(summary.id).toEqual(testDTO.id);
        expect(summary.task).toEqual(testDTO.task);
        expect(summary.comment).toEqual(testDTO.comment);
        expect(summary.status).toEqual(testDTO.status);
        expect(summary.date === DateTimeHelper.format(testDTO.beginDateTime, 'yyyy-MM-dd')).toBe(true);
        expect(summary.activity.id === testDTO.activity.id).toBe(true);
        expect(summary.activity.title === testDTO.activity.title).toBe(true);
        expect(summary.activity.billable === testDTO.activity.billable).toBe(true);
    });

    it('returns the correct value for begin date & time', () => {
        let summary = new TimeEntrySummary();
        summary.fromDto(testDTO);

        let dtoBeginTime = DateTimeHelper.format(testDTO.beginDateTime, 'HH:mm:ss');
        let dtoDate = DateTimeHelper.format(testDTO.beginDateTime, 'yyyy-MM-dd');
        expect(summary.beginDateTime).toEqual(new Date(dtoDate + ' ' + dtoBeginTime));

    });

    it('should be instantiated correctly', () => {
        let summary = new TimeEntrySummary();
        let today = DateTimeHelper.format(new Date(), 'yyyy-MM-dd');

        expect(summary.activity).not.toBe(null);
        expect(summary.date).toEqual(today);
        expect(summary.beginTime).toEqual('09:00:00');
        expect(summary.endTime).toEqual('17:00:00');
    });

    it('should be able to get date', () => {
        let timeEntry = new TimeEntrySummary();
        timeEntry.beginDateTime = new Date('2017-05-18 09:00:00');

        expect(timeEntry.date).toBe('2017-05-18');
    });

    it('should be able to set date', () => {
        let timeEntry = new TimeEntrySummary();

        timeEntry.beginDateTime = new Date('2017-05-18 09:00');
        timeEntry.date = '2016-06-11';

        expect(timeEntry.date).toBe('2016-06-11');
        expect(timeEntry.beginDateTime.getFullYear()).toBe(2016);
        expect(timeEntry.beginDateTime.getMonth()).toBe(5);
        expect(timeEntry.beginDateTime.getDate()).toBe(11);
    });

    it('should be able to get beginTime', () => {
        let timeEntry = new TimeEntrySummary();
        timeEntry.beginDateTime = new Date('2017-05-18 09:00:00');

        expect(timeEntry.beginTime).toBe('09:00:00');
    });

    it('should be able to set beginTime', () => {
        let timeEntry = new TimeEntrySummary();

        timeEntry.beginDateTime = new Date('2017-05-18 09:00');
        timeEntry.beginTime = '11:15:00';

        expect(timeEntry.beginTime).toBe('11:15:00');
        expect(timeEntry.beginDateTime.getHours()).toBe(11);
        expect(timeEntry.beginDateTime.getMinutes()).toBe(15);
    });

    it('should be able to get endTime', () => {
        let timeEntry = new TimeEntrySummary();
        timeEntry.endDateTime = new Date('2017-05-18 09:00:00');

        expect(timeEntry.endTime).toBe('09:00:00');
    });

    it('should be able to set endTime', () => {
        let timeEntry = new TimeEntrySummary();

        timeEntry.endDateTime = new Date('2017-05-18 09:00');
        expect(timeEntry.endTime).toBe('09:00:00');

        timeEntry.endTime = '11:15:00';

        expect(timeEntry.endTime).toBe('11:15:00');
        expect(timeEntry.endDateTime.getHours()).toBe(11);
        expect(timeEntry.endDateTime.getMinutes()).toBe(15);
    });

    it('returns false if endDateTime - beginDateTime < 15', () => {
        let timeEntry = new TimeEntrySummary();

        timeEntry.beginDateTime = new Date('2017-2-19 9:00:00');
        timeEntry.endDateTime = new Date('2017-2-19 9:12:00');

        expect(timeEntry.timeRangeIsValid).toBe(false);
    });

    it('returns true if endDateTime - beginDateTime = 15', () => {
        let timeEntry = new TimeEntrySummary();

        timeEntry.beginDateTime = new Date('2017-2-19 9:00:00');
        timeEntry.endDateTime = new Date('2017-2-19 9:15:00');

        expect(timeEntry.timeRangeIsValid).toBe(true);
    });

    it('returns true if endDateTime - beginDateTime > 15', () => {
        let timeEntry = new TimeEntrySummary();

        timeEntry.beginDateTime = new Date('2017-2-19 9:00:00');
        timeEntry.endDateTime = new Date('2017-2-19 9:20:00');

        expect(timeEntry.timeRangeIsValid).toBe(true);
    });
});
