import {TimeEntrySearch} from './timeEntrySearch.model';

describe('TimeEntrySearch', () => {
    it('sets search params correctly', () => {
        let timeEntrySearch = new TimeEntrySearch();
        let now = new Date( '2017-01-18' + ' ' + '09:00:00' );
        let userId = 'my_test_user_id';
        let projectManagerId = '12345';

        timeEntrySearch.beginDateTime = now;
        timeEntrySearch.endDateTime = now;
        timeEntrySearch.userId = userId;
        timeEntrySearch.projectManagerId = projectManagerId;

        let params = timeEntrySearch.toParams();

        expect(params.has('beginDateTime')).toEqual(true);
        expect(params.get('beginDateTime')).toEqual(now.toUTCString());

        expect(params.has('endDateTime')).toBe(true);
        expect(params.get('endDateTime')).toEqual(now.toUTCString());

        expect(params.has('creatorId')).toBe(true);
        expect(params.get('creatorId')).toEqual(userId);

        expect(params.has('projectManagerId')).toBe(true);
        expect(params.get('projectManagerId')).toEqual(projectManagerId);
    });
});
