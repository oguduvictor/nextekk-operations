import { User } from '../../../shared';

export class Activity {
    constructor(
        public id: string = null,
        public title: string = null,
        public billable = false,
        public description: string = null,
        public createdDate = new Date(),
        public editedDate = new Date(),
        public creator: User = null,
        public editor: User = null,
        public isVacation: boolean = false
    ) { }

    fromDto(dto: any) {
        this.id = dto.id;
        this.title = dto.title;
        this.billable = dto.billable;
        this.description = dto.description;
        this.createdDate = new Date(dto.created);
        this.editedDate = new Date(dto.edited);
        this.isVacation = dto.isVacation;

        if (dto.creator) {
            this.creator = new User();
            this.creator.fromDto(dto.creator);
        }

        if (dto.editor) {
            this.editor = new User();
            this.editor.fromDto(dto.editor);
        }
    }

    toDto(): any {
        return {
            id: this.id,
            title: this.title,
            billable: this.billable,
            description: this.description,
            created: this.createdDate.toUTCString(),
            editedDate: this.editedDate.toUTCString(),
            isVacation: this.isVacation
        };
    }
}
