import { Activity } from './activity.model';
import { User, DateTimeHelper } from '../../../shared';
import * as _ from 'lodash';

export class TimeEntrySummary {
    id: string = null;
    task: string = null;
    comment: string;
    activity: Activity = null;
    activityId: string;
    status: string = null;
    beginDateTime: Date;
    endDateTime: Date;
    managerComment: string;
    creator: User;
    creatorId: string;

    constructor() {
        this.activity = new Activity();
        this.beginDateTime = new Date();
        this.endDateTime = new Date();

        this.beginDateTime.setHours(9, 0, 0, 0);
        this.endDateTime.setHours(17, 0, 0, 0);
    }

    get timeRangeIsValid(): boolean {
        let timeDiff = this.endDateTime.getTime() - this.beginDateTime.getTime();

        return timeDiff >= 900000;
    }

    get hours(): number {
        if (!this.beginDateTime || !this.endDateTime) {
            return 0;
        }

        let diff = this.endDateTime.getTime() - this.beginDateTime.getTime();
        let hrs = diff / (1000 * 60 * 60);

        return Number(hrs.toFixed(2));
    }

    get date(): string {
        return DateTimeHelper.format(this.beginDateTime, 'yyyy-MM-dd');
    }

    set date(value: string) {
        DateTimeHelper.setDate(this.beginDateTime, value);
        DateTimeHelper.setDate(this.endDateTime, value);
    }

    get beginTime(): string {
        return DateTimeHelper.getTime(this.beginDateTime);
    }

    set beginTime(time: string) {
        if (this.isTimeValid(time)) {
            DateTimeHelper.setTime(this.beginDateTime, time);
        }
    }

    get endTime(): string {
        return DateTimeHelper.getTime(this.endDateTime);
    }

    set endTime(time: string) {
        if ( this.isTimeValid(time)) {
            DateTimeHelper.setTime(this.endDateTime, time);
        }
    }

    private isTimeValid(time: string) {
        let timeParts = (time || '').split(':');
        if ( timeParts.length < 2 ) {
            return false;
        }

        if  (!_.isNumber(Number(timeParts[0])) || !_.isNumber(Number(timeParts[1]))) {
            return false;
        }

        return true;

    }

    fromDto(dto: any) {
        this.id = dto.id;
        this.task = dto.task;
        this.comment = dto.comment;
        this.managerComment = dto.managerComment;
        this.status = dto.status;
        this.beginDateTime = new Date(dto.beginDateTime);
        this.endDateTime = new Date(dto.endDateTime);

        if (dto.activity) {
            this.activity = new Activity(dto.activity.id, dto.activity.title, dto.activity.billable);
            this.activityId = dto.activity.id;
        }

        if (dto.creator) {
            this.creator = new User();
            this.creator.fromDto(dto.creator);
            this.creatorId = dto.creator.id;
        }
    }

    toDto(): any {
        let dto = <any>{};

        dto.id = this.id;
        dto.task = this.task;
        dto.beginDateTime = this.beginDateTime.toUTCString();
        dto.endDateTime = this.endDateTime.toUTCString();
        dto.comment = this.comment;
        dto.managerComment = this.managerComment;
        dto.activityId = this.activity.id;

        return dto;
    }
}
