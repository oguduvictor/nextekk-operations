import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { TimeEntryModalComponent } from './timeEntryModal.component';
import { TimeEntryDataService } from '../../services/timeEntryData.service';
import { TimeEntryModalOptions } from '../../models/timeEntryModalOptions.enum';
import { ModalComponent, SpinnerService } from '../../../../shared';
import { DialogService } from '../../../../shared';
import { TimeEntrySummary } from '../../models/timeEntrySummary.model';
import { TimeEntryStatus } from '../../constants/timeEntryStatus.const';
import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TimeTrackingModule } from '../../../timeTracking.module';
import { IdCommentPair } from '../../models/idCommentPair.model';

describe('TimeEntryModalComponent', () => {
    let _modal: ModalComponent;
    let _entry: TimeEntrySummary;
    let _timeEntryModal: TimeEntryModalComponent = null;
    let _fixture: ComponentFixture<TimeEntryModalComponent>;
    let _mockSpinnerService: SpinnerService;
    let _mockDataService = <any>{
        getActivities: function () {
            return Observable.of([]);
        },
        saveTimeEntry: function () {
            return Observable.of(TimeEntrySummary);
        },
        changeStatusToSaved: jasmine.createSpy('changeStatusToSaved').and.returnValue(Observable.of({})),

        submitTimeEntries: jasmine.createSpy('submitTimeEntries').and.returnValue(Observable.of({})),

        unsubmitTimeEntries: jasmine.createSpy('unsubmitTimeEntries').and.returnValue(Observable.of({})),

        deleteTimeEntry: jasmine.createSpy('deleteTimeEntry').and.returnValue(Observable.of('success')),

        approveTimeEntries: jasmine.createSpy('approveTimeEntries').and.returnValue(Observable.of('success')),

        denyTimeEntry: jasmine.createSpy('approveTimeEntries').and.returnValue(Observable.of('success'))
     };

    beforeEach(() => {
        _mockSpinnerService = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);

        TestBed.configureTestingModule({
            imports: [TimeTrackingModule],
            providers: [
                DialogService,
                { provide: TimeEntryDataService, useValue: _mockDataService },
                { provide: SpinnerService, useValue: _mockSpinnerService }
            ]
        });
        _fixture = TestBed.createComponent(TimeEntryModalComponent);
        _timeEntryModal = <TimeEntryModalComponent>_fixture.debugElement.componentInstance;
        _entry = new TimeEntrySummary();
        _modal = (<any>_timeEntryModal).modal;
        (<any>_modal).modal.config = { backdrop: true, ignoreBackdropClick: true, keyboard: true };

        (<any>_timeEntryModal)._dialogService = {
            showConfirm: () => {
                return new Promise((resolve, confirm) => resolve(true));
            }
        };
    });

    it('canDeny getter returns true if timeEntry.managerComment isnot empty', () => {
        _timeEntryModal.timeEntry = <TimeEntrySummary>{ managerComment: '' };
        _timeEntryModal.timeEntry.managerComment = 'test_comment';

        expect(_timeEntryModal.canDeny).toBe(true);
    });

    it('canDeny getter returns false if timeEntry.managerComment is empty', () => {
        _timeEntryModal.timeEntry = <TimeEntrySummary>{ managerComment: '' };
        expect(_timeEntryModal.canDeny).toBe(false);
    });

    it('should be able to call deny()', () => {

        _timeEntryModal.timeEntry = <TimeEntrySummary>{ task: 'any_task' };
        spyOn(_timeEntryModal, 'deny');

        _timeEntryModal.deny();
        expect(_timeEntryModal.deny).toHaveBeenCalled();

    });

    it('should create an instance', () => {
        expect(_timeEntryModal).toBeTruthy();
    });

    it('should instantiate with initial conditions', () => {
        expect(_timeEntryModal.title).toBe('Time Entry');
        expect(_timeEntryModal.activities).toBe(null);
        expect(_timeEntryModal.timeEntry).toBeUndefined();
        expect(_timeEntryModal.canDelete).toBe(false);
        expect(_timeEntryModal.onSaved instanceof EventEmitter).toBe(true);
        expect(_timeEntryModal.onDeleted instanceof EventEmitter).toBe(true);
        expect(_timeEntryModal.isManager).toBe(false);
        expect(_timeEntryModal.canSubmitTimeEntry).toBe(false);
        expect(_timeEntryModal.errorMessage).toBe(null);
    });

    it('should display when show method is called', () => {
        spyOn(_modal, 'show');

        _timeEntryModal.show(_entry, TimeEntryModalOptions.view);

        expect(_timeEntryModal.timeEntry).toBe(_entry);
        expect(_modal.show).toHaveBeenCalled();
    });

    it('closes modal when close option is clicked', () => {
        spyOn(_modal, 'close');

        _timeEntryModal.close();

        _fixture.detectChanges();

        expect(_modal.close).toHaveBeenCalled();
    });

    it('properties values are set currectly when TimeEntryModalOptions.Deny is passed to show', () => {
        _entry = new TimeEntrySummary();

        _fixture.detectChanges();
        _timeEntryModal.show(_entry, TimeEntryModalOptions.managerEdit);

        expect(_timeEntryModal.title).toEqual('Approve Or Deny Time Entry');

        expect(_timeEntryModal.isManager).toEqual(true);
        expect(_timeEntryModal.canSave).toEqual(false);
        expect(_timeEntryModal.isReadOnly).toBe(true);
    });

    it('should change property values when time options is changed to create', () => {
        _entry = new TimeEntrySummary();

        _fixture.detectChanges();
        _timeEntryModal.show(_entry, TimeEntryModalOptions.create);

        _fixture.detectChanges();

        expect(_timeEntryModal.title).toBe('New Time Entry');
        expect(_timeEntryModal.canDelete).toBe(false);
        expect(_timeEntryModal.canSave).toBe(true);
        expect(_timeEntryModal.isReadOnly).toBe(false);
    });

    it('should emit save event Changes when save method is called', () => {
        let timeEntry = new TimeEntrySummary();

        _timeEntryModal.timeEntry = timeEntry;
        spyOn(_timeEntryModal.onSaved, 'emit');
        spyOn(_timeEntryModal, 'close');

        _timeEntryModal.save();

        _fixture.detectChanges();

        expect(_timeEntryModal.onSaved.emit).toHaveBeenCalled();
        expect(_timeEntryModal.close).toHaveBeenCalled();
    });

    it('isValid property should return true if form is invalid', () => {
        _timeEntryModal.form = null;

        let result = _timeEntryModal.isValid;

        expect(result).toBe(true);
    });

    it('canSubmitTimeEntry is true when timeEntry.status equals Denied', () => {
        let entry = new TimeEntrySummary();
        entry.status = TimeEntryStatus.denied;

        _fixture.detectChanges();
        _timeEntryModal.show(entry, TimeEntryModalOptions.managerEdit);

        expect(_timeEntryModal.canSubmitTimeEntry).toBe(true);
    });

    it('submitTimeEntry should be callable', () => {
        spyOn(_timeEntryModal, 'submitTimeEntry');
        _timeEntryModal.submitTimeEntry();

        expect(_timeEntryModal.submitTimeEntry).toHaveBeenCalled();
    });

    it('should show spinner service when save called', () => {
        let timeEntry = new TimeEntrySummary();

        _timeEntryModal.timeEntry = timeEntry;

        _timeEntryModal.save();

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('should hide spinner service when save is succefully', () => {
        let timeEntry = new TimeEntrySummary();

        _timeEntryModal.timeEntry = timeEntry;

        _timeEntryModal.save();

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('should change status of time entry to saved if it was denied', () => {
        let timeEntry = new TimeEntrySummary();
        timeEntry.status = TimeEntryStatus.denied;

        _timeEntryModal.timeEntry = timeEntry;
        _timeEntryModal.save();

        expect(_mockDataService.changeStatusToSaved).toHaveBeenCalled();
    });

    it('can submit timeEntry', () => {
        _timeEntryModal.timeEntry = new TimeEntrySummary();
        _timeEntryModal.submitTimeEntry();

        expect(_mockDataService.submitTimeEntries).toHaveBeenCalled();
    });

    it('can unsubmit submited time entry', () => {
        _timeEntryModal.unSubmitEntry('test_id');

        expect(_mockDataService.unsubmitTimeEntries).toHaveBeenCalledWith(['test_id']);
    });

    it('sets canSave to true if edit option is passed ', () => {
        _entry = new TimeEntrySummary();
        _timeEntryModal.timeEntry = new TimeEntrySummary();
        _timeEntryModal.show(_entry, TimeEntryModalOptions.edit);

        _fixture.detectChanges();

        expect(_timeEntryModal.canSave).toEqual(true);
    });

    it('can delete time entry', () => {
        _entry.id = '1';
        _timeEntryModal.timeEntry = _entry;

        _timeEntryModal.delete();

        setTimeout(() => {expect(_mockDataService.deleteTimeEntry).toHaveBeenCalledWith(_entry.id); }, 0);
    });

    it('can approve time entries', () => {
        _entry.id = '1';
        _entry.managerComment = 'good job';
        _timeEntryModal.timeEntry = _entry;

        _timeEntryModal.approve();

        setTimeout(() => {
            expect(_mockDataService.approveTimeEntries).toHaveBeenCalled();
         });
    });

    it('can deny time entries', () => {
        _entry.id = '1';
        _entry.managerComment = 'good job';
        _timeEntryModal.timeEntry = _entry;

        _timeEntryModal.deny();

        setTimeout(() => {
            expect(_mockDataService.denyTimeEntry).toHaveBeenCalled();
        });
    });
});
