﻿import { Component, Output, ViewChild, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TimeEntrySummary } from '../../models/timeEntrySummary.model';
import { TimeEntryDataService } from '../../services/timeEntryData.service';
import { Activity } from '../../models/activity.model';
import { TimeEntryStatus } from '../../constants/timeEntryStatus.const';
import { TimeEntryModalOptions } from '../../models/timeEntryModalOptions.enum';
import { ModalComponent, SpinnerService, DialogService } from '../../../../shared';
import { IdCommentPair } from '../../models/idCommentPair.model';

@Component({
    selector: 'app-time-entry-modal',
    templateUrl: './timeEntryModal.component.html'
})
export class TimeEntryModalComponent {
    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @Output()
    onSaved = new EventEmitter<TimeEntrySummary>();

    @Output()
    onDeleted = new EventEmitter<TimeEntrySummary>();

    timeEntry: TimeEntrySummary;
    activities: Observable<Activity[]> = null;
    title = 'Time Entry';
    canDelete = false;
    canSave = false;
    canUnsubmitTimeEntry = false;
    isReadOnly = false;
    isManager = false;
    canSubmitTimeEntry = false;
    errorMessage: string = null;

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }

        return this.form.valid && this.timeEntry.timeRangeIsValid;
    }

    get showError(): boolean {
        return !!this.errorMessage;
    }

    get canDeny(): boolean {
        return !!this.timeEntry.managerComment && this.isValid;
    }

    constructor(
        private _dataService: TimeEntryDataService,
        private _spinnerService: SpinnerService,
        private _dialogService: DialogService
    ) { }

    show(entry: TimeEntrySummary, modalOption: TimeEntryModalOptions): void {
        if (!this.activities) {
            this.activities = this._dataService.getActivities();
        }

        switch (modalOption) {
            case TimeEntryModalOptions.create:
                this.title = 'New Time Entry';
                this.canDelete = false;
                this.canSave = true;
                this.isReadOnly = false;
                break;

            case TimeEntryModalOptions.edit:
                this.title = 'Edit Time Entry';
                this.canSave = true;
                this.canDelete = true;
                this.isReadOnly = false;
                break;

            case TimeEntryModalOptions.view:
                this.title = 'Time Entry';
                this.isReadOnly = true;
                break;

            case TimeEntryModalOptions.managerEdit:
                this.title = 'Approve Or Deny Time Entry';
                this.isManager = true;
                this.canSave = false;
                this.isReadOnly = true;
                break;

            default:
                throw new Error('Unrecognized modal option');
        }

        this.errorMessage = null;

        this.timeEntry = entry;
        this.canSubmitTimeEntry = this.timeEntry.status === TimeEntryStatus.denied || this.timeEntry.status === TimeEntryStatus.saved;
        this.canUnsubmitTimeEntry = (!this.isManager && (this.timeEntry.status === TimeEntryStatus.submitted));
        this.modal.show();
    }

    close(): void {
        this.modal.close();
    }

    save(): void {
        this._spinnerService.show();
        this._dataService
            .saveTimeEntry(this.timeEntry)
            .subscribe(x => {
                if (this.timeEntry.status === TimeEntryStatus.denied) {
                    this._dataService.changeStatusToSaved([this.timeEntry.id])
                        .subscribe(() => {
                            this._spinnerService.hide();
                            this.onSaved.emit(this.timeEntry);
                            this.close();
                        });
                } else {
                    this._spinnerService.hide();
                    this.onSaved.emit(x);
                    this.close();
                }
            },
            err => {
                this._spinnerService.hide();
                this.errorMessage = err;
            });
    }

    delete(): void {
        let msg = '"' + this.timeEntry.task + '" will be permanently deleted. Click OK to continue.';

        this._dialogService.showConfirm('Delete Task Entry?', msg).then(isOk => {
            if (!isOk) {
                return;
            }

            this._spinnerService.show();
            this._dataService
                .deleteTimeEntry(this.timeEntry.id)
                .subscribe(() => {
                    this._spinnerService.hide();
                    this.onDeleted.emit(this.timeEntry);
                    this.modal.close();
                });
        });
    }

    approve(): void {
        let msg = `Are you sure you want to approve "${this.timeEntry.task}"? `;

        this._dialogService.showConfirm('Approve Task Entry?', msg).then(isOk => {
            if (!isOk) {
                return;
            }

            this._spinnerService.show();
            this._dataService
                .approveTimeEntries([new IdCommentPair(this.timeEntry.id, this.timeEntry.managerComment)])
                .subscribe(() => {
                    this._spinnerService.hide();
                    this.onSaved.emit(this.timeEntry);
                    this.modal.close();
                });
        });
    }

    deny(): void {
        let msg = `Are you sure you want to deny "${this.timeEntry.task}"?`;

        this._dialogService.showConfirm('Deny time Entry', msg).then(isOk => {
            if (!isOk) {
                return;
            }

            this._spinnerService.show();
            this._dataService
                .denyTimeEntry(this.timeEntry.id, this.timeEntry.managerComment)
                .subscribe(() => {
                    this._spinnerService.hide();
                    this.onSaved.emit(this.timeEntry);
                    this.close();
                });
        });
    }

    submitTimeEntry(): void {
        this._spinnerService.show();
        this._dataService
            .saveTimeEntry(this.timeEntry)
            .subscribe(() => {
                this._dataService.submitTimeEntries([this.timeEntry.id])
                    .subscribe(() => {
                        this._spinnerService.hide();
                        this.onSaved.emit(this.timeEntry);
                        this.close();
                    });
            });
    }

    unSubmitEntry(id) {
        this._spinnerService.show();

        this._dataService
            .unsubmitTimeEntries([id])
            .subscribe(
            () => {
                this._spinnerService.hide();
                this.onSaved.emit(this.timeEntry);
                this.close();
            },
            err => {
                this._spinnerService.hide();
                this.errorMessage = err;
            });
    }
}
