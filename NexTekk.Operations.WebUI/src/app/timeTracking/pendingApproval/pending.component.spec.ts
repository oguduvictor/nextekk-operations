import { TestBed, ComponentFixture} from '@angular/core/testing';
import { FormsModule} from '@angular/forms';
import { TimeEntryModalComponent, WeeklyTimeEntry, Activity, TimeEntrySummary, TimeEntryDataService } from '../common';
import { DialogService, ModalComponent, FilterByPipe, SpinnerService,
    NotificationService, UserService, User } from '../../shared';
import { PendingComponent} from './pending.component';
import { Observable} from 'rxjs/Observable';
import { TimeTrackingModule } from '../timeTracking.module';

describe('PendingComponent', () => {
    let mockWeeklyEntries: WeeklyTimeEntry;
    let _mockDataService;
    let pendingComp: PendingComponent;
    let _fixture: ComponentFixture<PendingComponent>;
    let _userService: UserService = <any>{
        getCurrentUser: jasmine.createSpy('getCurrentUser').and.returnValue(Observable.of(<User>{id: 'test_id'}))
    };

    beforeEach(() => {
        mockWeeklyEntries = <WeeklyTimeEntry>jasmine.createSpyObj('weeklyTimeEntries', ['reloadTimeEntries',
            'nextWeek', 'previousWeek', 'unsubmit', 'submit', 'addTimeEntry', 'currentWeek', 'getTotalHours', 'approveTimeEntries']);

        _mockDataService = {
            getActivities: jasmine.createSpy('getActivities').and.returnValue(Observable.of([new Activity()])),
            getTimeEntries: jasmine.createSpy('getTimeEntries').and.returnValue(Observable.of([new TimeEntrySummary()]))
        };

        TestBed.configureTestingModule({
            imports: [TimeTrackingModule],
            providers: [
                DialogService, SpinnerService,
                { provide: TimeEntryDataService, useValue: _mockDataService },
                { provide: UserService, useValue: _userService},
                { provide: NotificationService, useValue: {}}
            ]
        });

        _fixture = TestBed.createComponent(PendingComponent);
        pendingComp = <PendingComponent>_fixture.debugElement.componentInstance;
        pendingComp.weeklyTimes = mockWeeklyEntries;
    });

    it('openApprovalModal can be invoked successfully', () => {
        spyOn(pendingComp, 'openApprovalModal');
        pendingComp.openApprovalModal();
        expect(pendingComp.openApprovalModal).toHaveBeenCalled();
    });

    it('can invoke display successfully', () => {
        spyOn(pendingComp, 'display');

        let entry = new TimeEntrySummary();
        pendingComp.display(entry);

        expect(pendingComp.display).toHaveBeenCalled();
    });

    it('canApprove can return false ', () => {
        pendingComp.entryIds  = {'id1': true, 'id2': false, 'id3': true};
        expect(pendingComp.canApprove).toEqual(false);
    });

    it('canApprove can return true', () => {
        pendingComp.entryIds = {'id1': false, 'id2': false, 'id3': false};
        expect(pendingComp.canApprove).toEqual(true);
    });

    it('canApprove returns true if entryIds is empty', () => {
        pendingComp.entryIds = {};
        expect(pendingComp.canApprove).toEqual(true);
    });

    it('can be instatiated successfully', () => {
        expect(pendingComp).toBeTruthy();
    });

    it('get allChecked returns true if all timeEntries are checked', () => {
        pendingComp.weeklyTimes.timeEntries = [
            <TimeEntrySummary>{id: 'id1'},
            <TimeEntrySummary>{id: 'id2'},
            <TimeEntrySummary>{id: 'id3'}
        ];

        pendingComp.entryIds = {'id1': true, 'id2': true, 'id3': true};

        expect(pendingComp.allChecked).toBe(true);
    });

    it('get allChecked returns false if not all timeEntries are checked', () => {
        pendingComp.weeklyTimes.timeEntries = [
            <TimeEntrySummary>{id: 'id1'},
            <TimeEntrySummary>{id: 'id2'},
            <TimeEntrySummary>{id: 'id3'}
        ];

        pendingComp.entryIds = {'id1': true, 'id2': false, 'id3': true};

        expect(pendingComp.allChecked).toBe(false);
    });

    it('get allChecked returns false if weeklytimes.timeEntries is empty', () => {
        pendingComp.weeklyTimes.timeEntries = [];
        expect(pendingComp.allChecked).toBe(false);
    });

    it('set allChecked can set all timeEntries to true', () => {
        pendingComp.entryIds = {'id1': false, 'id2': false, 'id3': true};

        pendingComp.weeklyTimes.timeEntries = [
            <TimeEntrySummary>{id: 'id1'},
            <TimeEntrySummary>{id: 'id2'},
            <TimeEntrySummary>{id: 'id3'}
        ];

        pendingComp.allChecked = true;
        let entryIds = pendingComp.entryIds;

        expect(entryIds['id1']).toBe(true);
        expect(entryIds['id2']).toBe(true);
        expect(entryIds['id3']).toBe(true);
    });

    it('set allChecked can set all timeEntries to false', () => {
        pendingComp.entryIds = {'id1': true, 'id2': false, 'id3': true};

        pendingComp.weeklyTimes.timeEntries = [
            <TimeEntrySummary>{id: 'id1'},
            <TimeEntrySummary>{id: 'id2'},
            <TimeEntrySummary>{id: 'id3'}
        ];

        pendingComp.allChecked = false;
        let entryIds = pendingComp.entryIds;

        expect(entryIds['id1']).toBe(false);
        expect(entryIds['id2']).toBe(false);
        expect(entryIds['id3']).toBe(false);
    });

    it('goPrevious calls weeklyTimes.previousWeek', () => {
        pendingComp.goPrevious();
        expect(pendingComp.weeklyTimes.previousWeek).toHaveBeenCalled();

    });

    it('goCurrent calls weeklyTimes.currentWeek', () => {
        pendingComp.goCurrent();
        expect(pendingComp.weeklyTimes.currentWeek).toHaveBeenCalled();

    });

    it('goNext calls weeklyTimes.nextWeek', () => {
        pendingComp.goNext();
        expect(pendingComp.weeklyTimes.nextWeek).toHaveBeenCalled();

    });

    it('can reload weekly time entries', () => {
        pendingComp.onTimeEntryApprovedOrDenied({});

        expect(pendingComp.weeklyTimes.reloadTimeEntries).toHaveBeenCalled();
    });

    it('can open Approval modal', () => {
        pendingComp.entryIds = {'1': true};
        let weekly = <any>{approveTimeEntries: jasmine.createSpy('approveTimeEntries')};

        (<any>pendingComp)._weeklyTimes = weekly;
        (<any>pendingComp)._dialogService = {
            showConfirm: () => {
                return new Promise((resolve, reject) => {resolve(true); });
            }
        };

        pendingComp.openApprovalModal();

        setTimeout(() => {
            expect(weekly.approveTimeEntries).toHaveBeenCalled();
        }, 0);
    });

    it('can toggle checkbox', () => {
        let entry = new TimeEntrySummary();
        entry.id = '1';

        (<any>pendingComp).weeklyTimes.timeEntries = [entry];
        pendingComp.entryIds = {'i': false};

        expect(pendingComp.allChecked).toBe(false);

        pendingComp.toggleCheckBox();

        expect(pendingComp.allChecked).toBe(true);
    });

    it('can go to previous week', () => {
        pendingComp.goPrevious();

        expect(pendingComp.weeklyTimes.previousWeek).toHaveBeenCalled();
    });

    it('can go to currentWeek', () => {
        pendingComp.goCurrent();

        expect(pendingComp.weeklyTimes.currentWeek).toHaveBeenCalled();
    });

    it('can go next week', () => {
        pendingComp.goNext();

        expect(pendingComp.weeklyTimes.nextWeek).toHaveBeenCalled();
    });

    it('can display time entry', () => {
        let entry = new TimeEntrySummary();

        let timeEntryModal = (<any>pendingComp).timeEntryModal;
        spyOn(timeEntryModal, 'show');

        let modal = (<any>((<any>timeEntryModal).modal).modal);
        modal.config = { backdrop: true, ignoreBackdropClick: true, keyboard: true };

        pendingComp.display(entry);

        expect(timeEntryModal.show).toHaveBeenCalled();
    });

    it('gets current user on initializing', () => {
        pendingComp.weeklyTimes = undefined;
        pendingComp.ngOnInit();

        expect(pendingComp.weeklyTimes).toBeDefined();
    });

    it('gets current user on initialization', () => {
        pendingComp.ngOnInit();

        expect(_userService.getCurrentUser).toHaveBeenCalled();
    });
});
