import { Component, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { TimeEntryDataService, TimeEntrySearch, TimeEntrySummary, TimeEntryModalComponent,
    TimeEntryModalOptions, WeeklyTimeEntry, TimeEntryStatus } from '../common';
import { DialogService, SpinnerService, NotificationService, UserService } from '../../shared';
import * as _ from 'lodash';
declare var $: any;

@Component({
    selector: 'app-pending-approval',
    templateUrl: './pending.component.html'
})
export class PendingComponent implements OnInit, AfterViewInit {
    weeklyTimes: WeeklyTimeEntry;
    entryIds = {};

    @ViewChild(TimeEntryModalComponent)
    private timeEntryModal: TimeEntryModalComponent;

    @ViewChild('datePicker')
    datePicker: ElementRef;

    @ViewChild('selectedDate')
    selectedDate: ElementRef;

    get allChecked(): boolean {
        if (_.isEmpty(this.weeklyTimes.timeEntries)) {
            return false;
        }

        let checked = _.filter(this.entryIds, x => {
            return x === true;
        });

        return checked.length === this.weeklyTimes.timeEntries.length;
    };

    get canApprove(): boolean {
        let entriesId = this.getSelectedIds();
        return entriesId.length === 0;
    };

    set allChecked(isChecked: boolean) {
        for (let timeEntry of this.weeklyTimes.timeEntries) {
            this.entryIds[timeEntry.id] = isChecked;
        }
    };

    constructor(
        private _dataService: TimeEntryDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService,
        private _userService: UserService,
        private _dialogService: DialogService
    ) { }

    goPrevious(): void {
        this.weeklyTimes.previousWeek();
    }

    goCurrent(): void {
        this.weeklyTimes.currentWeek();
    }

    goNext(): void {
        this.weeklyTimes.nextWeek();
    }

    onTimeEntryApprovedOrDenied($event) {
        this.weeklyTimes.reloadTimeEntries();
    }

    openApprovalModal(): void {
        let msg = 'Are you sure you want to approve the selected Time Entries?';
        let entriesId = this.getSelectedIds();

        this._dialogService.showConfirm('Approve Task Entries?', msg).then(isOk => {
            if (!isOk) {
                return ;
            }
            this.weeklyTimes.approveTimeEntries(entriesId);
            this.entryIds = {};
        });
    }

    toggleCheckBox() {
        this.allChecked = !this.allChecked;
    }

    display(entry: TimeEntrySummary) {
        let timeEntryCopy = _.merge(new TimeEntrySummary(), entry);
        let modalOption = TimeEntryModalOptions.managerEdit;

        this.timeEntryModal.show(timeEntryCopy, modalOption);
    }

    ngOnInit(): void {
        this._userService.getCurrentUser().subscribe(user => {
            let search = new TimeEntrySearch();

            search.userId = null;
            search.projectManagerId = user.id;
            search.status = TimeEntryStatus.submitted;

            this.weeklyTimes = new WeeklyTimeEntry(this._dataService, this._spinnerService,
                this._notificationService, search);
        });
    }

    ngAfterViewInit() {
        $(this.datePicker.nativeElement).datepicker({autoclose: true});

        $(this.selectedDate.nativeElement)
        .on('change', (e) => {
            let selectedDate = new Date($(this.selectedDate.nativeElement).val());
            this.weeklyTimes.goToWeek(selectedDate);
        });
    }

    private getSelectedIds(): string[] {
        return _.reduce(this.entryIds, function(result, value, key) {
            if (value) {
                result.push(key);
            }
            return result;
            }, []);
    }
}
