import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule} from '@angular/forms';
import { SpinnerService, ModalComponent, DialogService, User } from '../../shared';
import { ActivitiesComponent } from './activities.component';
import { Activity, ActivityDataService } from '../common';
import { Observable } from 'rxjs/Observable';
import { TimeTrackingModule } from '../timeTracking.module';

describe('ActivitiesComponent', () => {
    let _fixture: ComponentFixture<ActivitiesComponent>;
    let _activitiesComponent: ActivitiesComponent;
    let _modal: ModalComponent;
    let _spinnerServiceMock: SpinnerService;
    let _user = new User('789', 'John', 'Doe');
    let _activity = new Activity('12345', 'activity1', false, null, new Date(), new Date(), _user, _user);
    let _mockDataService = {
        getActivities: () => Observable.of([_activity]),
        saveActivity: (activity: Activity) => Observable.of(activity),
        getActivity: () => Observable.of(_activity)
    };

    beforeEach(() => {
        _spinnerServiceMock = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);

        TestBed.configureTestingModule({
            imports: [TimeTrackingModule],
            providers: [
                DialogService,
                {provide: ActivityDataService, useValue: _mockDataService},
                {provide: SpinnerService, useValue: _spinnerServiceMock}
            ]
        });
        _fixture = TestBed.createComponent(ActivitiesComponent);
        _activitiesComponent = <ActivitiesComponent>_fixture.debugElement.componentInstance;
        _modal = <ModalComponent>_fixture.debugElement.componentInstance;
    });

    it('should create an instance', () => {
        expect(_activitiesComponent).toBeTruthy();
        });

    it('should get all activities', () => {
        _activitiesComponent.ngOnInit();
        let activities = _activitiesComponent.activities;
        expect(activities[0] instanceof Activity).toBeTruthy();
    });

    it ('should display modal with edit option', () => {
        let activityId = '12345';
        spyOn(_activitiesComponent, 'show');
        _activitiesComponent.display(activityId);

        expect(_activitiesComponent.show).toHaveBeenCalledWith(jasmine.any(Activity));
    });

    it ('should display modal with create option', () => {
        let activity = new Activity();

        _fixture.detectChanges();
        _activitiesComponent.show(activity);

        _fixture.detectChanges();

        expect(_activitiesComponent.title).toBe('Create New Activity');
        expect(_activitiesComponent.canDelete).toBe(false);

    });

    it ('should display modal with edit option', () => {
        _fixture.detectChanges();
        _activitiesComponent.show(_activity);

        _fixture.detectChanges();

        expect(_activitiesComponent.title).toBe('Edit Activity');
        expect(_activitiesComponent.canDelete).toBe(true);
    });

    it ('should open new activity modal', () => {
        spyOn(_activitiesComponent, 'show');

        _activitiesComponent.openNewActivityModal();
        _fixture.detectChanges();

        expect(_activitiesComponent.show).toHaveBeenCalledWith(jasmine.any(Activity));

     });

    it ('should close the modal', () => {
        spyOn(_modal, 'close');

        _activitiesComponent.close();

        expect(_modal.close).toHaveBeenCalled();
    });

    it ('should save activities', () => {

        spyOn(_mockDataService, 'saveActivity');
        spyOn(_activitiesComponent, 'onActivitiesSaved');

        _activitiesComponent.save();

        expect(_activitiesComponent.onActivitiesSaved).toHaveBeenCalledWith(_activitiesComponent.activity);
    });

    it ('should remove activity from list of activites after delete', () => {
        let activity  = new Activity();
        _activitiesComponent.activities = [activity];

        _activitiesComponent.onActivitiesDeleted(activity);

        expect(_activitiesComponent.activities.length).toBe(0);
    });

    it ('should update list of activites after save', () => {
        let activity  = new Activity();
        activity.id = '1';
        _activitiesComponent.activities = [activity];
        activity.billable = true;

        _activitiesComponent.onActivitiesSaved(activity);

        expect((_activitiesComponent.activities)[0].billable).toBe(true);
    });

    it ('should add new activity to list activites after save', () => {
        let activity  = new Activity();
        activity.id = '1';
        _activitiesComponent.activities = [];
        activity.billable = true;

        _activitiesComponent.onActivitiesSaved(activity);

        expect((_activitiesComponent.activities)[0].id).toBe('1');
    });

    it('shows spinner service when activities is called', () => {
        _activitiesComponent.ngOnInit();

        expect(_spinnerServiceMock.show).toHaveBeenCalled();
    });

    it('hides spinner service when activities is successfull', () => {
        _activitiesComponent.ngOnInit();

        expect(_spinnerServiceMock.hide).toHaveBeenCalled();
    });

    it('shows spinner service when save is called', () => {
        let activity = _activitiesComponent.activity = new Activity();
        _activitiesComponent.activities = [activity];
        _activitiesComponent.save();

        expect(_spinnerServiceMock.show).toHaveBeenCalled();
    });

    it('hides spinner when save is successfull', () => {
        let activity = _activitiesComponent.activity = new Activity();
        _activitiesComponent.activities = [activity];
        _activitiesComponent.save();

        expect(_spinnerServiceMock.hide).toHaveBeenCalled();
    });

});
