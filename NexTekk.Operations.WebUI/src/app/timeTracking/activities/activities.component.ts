import { Component, ViewChild, OnInit } from '@angular/core';
import { ModalComponent, SpinnerService, DialogService } from '../../shared';
import { ActivityDataService, Activity } from '../common';
import * as _ from 'lodash';

@Component({
    selector: 'app-activites-component',
    templateUrl: './activities.component.html'
})
export class ActivitiesComponent implements OnInit {
    activities: Activity[];
    title = 'Activity';
    activity: Activity;
    canDelete = false;
    isNew = true;

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }

        return this.form.valid;
    }

    constructor(
        private _dataService: ActivityDataService,
        private _spinnerService: SpinnerService,
        private _dialogService: DialogService
    ) { }

    display(activityId: string): void {
        this._dataService.getActivity(activityId).subscribe(activity => {
            this._spinnerService.hide();
            this.show(activity);
        });
    }

    show(entryActivity: Activity) {
        this.isNew = !entryActivity.id;
        this.title = this.isNew ? 'Create New Activity' : 'Edit Activity';
        this.canDelete = !this.isNew;
        this.activity = entryActivity;

        this.modal.show();
    }

    openNewActivityModal(): void {
        this.show(new Activity());
    }

    close(): void {
        this.modal.close();
    }

    save(): void {
        this._spinnerService.show();

        this._dataService
            .saveActivity(this.activity)
            .subscribe(x => {
                this._spinnerService.hide();
                this.onActivitiesSaved(x);
                this.close();
            }, err => this._spinnerService.hide());
    }

    delete(): void {
        let msg = '"' + this.activity.title + '" will be permanently deleted. Click OK to continue.';

        this._dialogService.showConfirm('Delete Activity?', msg).then(isOk => {
            if (!isOk) {
                return;
            }
            this._spinnerService.show();

            this._dataService.deleteActivity(this.activity.id)
                .subscribe(x => {
                    this._spinnerService.hide();
                    this.onActivitiesDeleted(this.activity);
                }, err => this._spinnerService.hide());
            this.close();
        });
    }

    onActivitiesDeleted(activity: Activity) {
        _.remove(this.activities, x => {
            return(x.id === activity.id);
        });

    }

    onActivitiesSaved(activity: Activity) {
        let existingActivity = this.activities.find(x => x.id === activity.id);

        if (existingActivity) {
            _.merge(existingActivity, activity);
            return;
        }

        this.activities.push(activity);
    }

    ngOnInit() {
        this._spinnerService.show();

        this._dataService.getActivities().subscribe(data => {
            this._spinnerService.hide();
            this.activities = data;
        });
    }
}
