import { Component, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TimeEntryDataService, TimeEntrySearch, TimeEntrySummary, TimeEntryModalComponent,
    TimeEntryModalOptions, WeeklyTimeEntry, TimeEntryStatus } from '../common';
import { NotificationService, User, SpinnerService, DialogService } from '../../shared';
import * as _ from 'lodash';
declare var $: any;

@Component({
    selector: 'app-my-time-entries',
    templateUrl: './myTimeEntries.component.html'
})
export class MyTimeEntriesComponent implements OnInit, AfterViewInit {
    weeklyTimes: WeeklyTimeEntry;

    @ViewChild(TimeEntryModalComponent)
    private timeEntryModal: TimeEntryModalComponent;

    @ViewChild('datePicker')
    datePicker: ElementRef;

    @ViewChild('selectedDate')
    selectedDate: ElementRef;

    constructor(
        private _dataService: TimeEntryDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService,
        private _route: ActivatedRoute,
        private _dialogService: DialogService
    ) { }

    display(entry: TimeEntrySummary): void {
        let timeEntryCopy = _.cloneDeep(entry);
        let modalOption = (entry.status === TimeEntryStatus.saved || entry.status === TimeEntryStatus.denied) ?
            TimeEntryModalOptions.edit :
            TimeEntryModalOptions.view;

        this.timeEntryModal.show(timeEntryCopy, modalOption);
    }

    openNewTimeEntryModal(): void {
        this.timeEntryModal.show(new TimeEntrySummary(), TimeEntryModalOptions.create);
    }

    submitTimeEntries(): void {
         if (this.weeklyTimes.totalHours < 40) {
            let msg = 'The total number of hours for your time entries is less than 40, are you sure you want to submit?';
            this._dialogService.showConfirm('Submit Time Entry', msg).then(isOk => {
                if (!isOk) {
                    return;
                }
                this.weeklyTimes.submit();
                return;
            });
        }else {
         this.weeklyTimes.submit();
        }
    }

    unsubmitTimeEntries(): void {
        this.weeklyTimes.unsubmit();
    }

    goPrevious(): void {
        this.weeklyTimes.previousWeek();
    }

    goCurrent(): void {
        this.weeklyTimes.currentWeek();
    }

    goNext(): void {
        this.weeklyTimes.nextWeek();
    }

    onTimeEntrySaved(entry: TimeEntrySummary): void {
        this.weeklyTimes.addTimeEntry(entry);
        this.weeklyTimes.reloadTimeEntries();
    }

    onTimeEntryDeleted(entry: TimeEntrySummary): void {
        this.weeklyTimes.reloadTimeEntries();
    }

    ngOnInit(): void {
        let user = <User>this._route.snapshot.data['user'];
        let search = new TimeEntrySearch();

        search.userId = user.id;
        this.weeklyTimes = new WeeklyTimeEntry(this._dataService,
                        this._spinnerService, this._notificationService, search);
    }

    ngAfterViewInit(): void {
        $(this.datePicker.nativeElement).datepicker({autoclose: true});

        $(this.selectedDate.nativeElement)
        .on('change', (e) => {
            let selectedDate = new Date($(this.selectedDate.nativeElement).val());
            this.weeklyTimes.goToWeek(selectedDate);
        });
    }
}
