import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { MyTimeEntriesComponent } from './myTimeEntries.component';
import { ActivatedRoute } from '@angular/router';
import {
    TimeEntryDataService, WeeklyTimeEntry, TimeEntryStatus,
    TimeEntryModalOptions, TimeEntrySummary, TimeEntryModalComponent
} from '../common';

import { Observable } from 'rxjs/Observable';
import { DialogService, ModalComponent, NotificationService, SpinnerService, CamelSeparatePipe } from '../../shared';
import { TimeTrackingModule } from '../timeTracking.module';

describe('MyTimeEntriesComponent', () => {
    let _fixture: ComponentFixture<MyTimeEntriesComponent>;
    let _entriesComp: MyTimeEntriesComponent;
    let _mockDataService = {
        getTimeEntries: () => {
            return Observable.of([new TimeEntrySummary()]);
        }
    };
    let mockWeeklyEntries: WeeklyTimeEntry;
    const _mockRoute: ActivatedRoute = <any>{
        snapshot: {
            data: {
                user: {
                    id: 'test_id'
                }
            }
        }
    };

    beforeEach(() => {
        mockWeeklyEntries = <WeeklyTimeEntry>jasmine.createSpyObj('weeklyTimeEntries', ['reloadTimeEntries',
            'nextWeek', 'previousWeek', 'unsubmit', 'submit', 'addTimeEntry', 'currentWeek']);

        TestBed.configureTestingModule({
            imports: [TimeTrackingModule, RouterTestingModule.withRoutes([])],
            providers: [
                SpinnerService, DialogService,
                { provide: TimeEntryDataService, useValue: _mockDataService },
                { provide: NotificationService, useValue: {} }
            ]
        });

        _fixture = TestBed.createComponent(MyTimeEntriesComponent);
        _entriesComp = <MyTimeEntriesComponent>_fixture.debugElement.componentInstance;
        _entriesComp.weeklyTimes = mockWeeklyEntries;

    });

    it('can be instatiated successfully', () => {
        expect(_entriesComp).toBeTruthy();
    });

    it('can call reloadTimeEntries OF weeklyTimes', () => {

        let entry = new TimeEntrySummary();
        _entriesComp.onTimeEntryDeleted(entry);

        expect(_entriesComp.weeklyTimes.reloadTimeEntries).toHaveBeenCalled();
    });

    it('can call addTimeEntry of weeklyTimeEntries', () => {

        let entry = new TimeEntrySummary();
        _entriesComp.onTimeEntrySaved(entry);

        expect(_entriesComp.weeklyTimes.addTimeEntry).toHaveBeenCalled();
    });

    it('can call nextWeek of weeklyTimeEntries', () => {

        _entriesComp.goNext();
        expect(_entriesComp.weeklyTimes.nextWeek).toHaveBeenCalled();
    });

    it('can call currentWeek of weeklyTimeEntries', () => {

        _entriesComp.goCurrent();
        expect(_entriesComp.weeklyTimes.currentWeek).toHaveBeenCalled();
    });

    it('can call previousWeek of weeklyTimeEntries', () => {

        _entriesComp.goPrevious();
        expect(_entriesComp.weeklyTimes.previousWeek).toHaveBeenCalled();
    });

    it('can call unsubmit of weeklyTimeEntries', () => {

        _entriesComp.unsubmitTimeEntries();
        expect(_entriesComp.weeklyTimes.unsubmit).toHaveBeenCalled();
    });

    it('can call submit of weeklyTimeEntries', () => {

        _entriesComp.submitTimeEntries();
        expect(_entriesComp.weeklyTimes.submit).toHaveBeenCalled();
    });

    it('can display time entry modal', () => {
        spyOn(_entriesComp, 'display');

        let entry = new TimeEntrySummary();
        _entriesComp.display(entry);

        expect(_entriesComp.display).toHaveBeenCalledWith(entry);
    });

    it('can add new timeEntry', () => {
        spyOn(_entriesComp, 'openNewTimeEntryModal');

        _entriesComp.openNewTimeEntryModal();
        expect(_entriesComp.openNewTimeEntryModal).toHaveBeenCalled();
    });

    it('can display time entry modal with modalOption set to "view"', () => {
        let entry = new TimeEntrySummary();

        let modal = (<any>_entriesComp).timeEntryModal;

        spyOn(modal, 'show');

        _entriesComp.display(entry);

        expect(modal.show).toHaveBeenCalledWith(entry, TimeEntryModalOptions.view);
    });

    it('displays time entry modal with modal option set to "edit"', () => {
        let entry = new TimeEntrySummary();
        entry.status = TimeEntryStatus.saved;

        let modal = (<any>_entriesComp).timeEntryModal;

        spyOn(modal, 'show');

        _entriesComp.display(entry);

        expect(modal.show).toHaveBeenCalledWith(entry, TimeEntryModalOptions.edit);
    });

    it('can display new time entry modal', () => {
        let timeEntryModal = (<any>_entriesComp).timeEntryModal;

        spyOn(timeEntryModal, 'show');
        _entriesComp.openNewTimeEntryModal();

        expect(timeEntryModal.show).toHaveBeenCalled();
    });

    it('should instatiate weeklyTimes on initialization', () => {
        (<any>_entriesComp)._route = _mockRoute;

        _entriesComp.weeklyTimes = undefined;

        _entriesComp.ngOnInit();

        expect(_entriesComp.weeklyTimes).toBeDefined();
    });

    it('can submit time entries when total hours is less than 40', () => {
        let weekly = <any>{
            totalHours: 30,
            submit: jasmine.createSpy('submit')
        };

        let dialog = <any>{
            showConfirm: () => {
                return new Promise((resolve, confirm) => {resolve(true); });
            }
        };

        (<any>_entriesComp)._weeklyTimes = weekly;
        (<any>_entriesComp)._dialogService = dialog;

        _entriesComp.submitTimeEntries();

        setTimeout(() => {
            expect(weekly.submit).not.toHaveBeenCalled();
        });
    }, 0);
});
