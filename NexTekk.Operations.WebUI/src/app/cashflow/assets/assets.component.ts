import { Component, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { Asset } from '../common/models/asset.model';
import { AssetDataService } from '../common/services/assetDataService';
import * as _ from 'lodash';
import { ColumnDefinitions, FilterDefinitions, CellDefinition, IdValuePair, User, UserService, SpinnerService, NotificationService, DialogService, GridComponent, FileSaver } from '../../shared';
import { Condition } from '../common/models/condition';
import { Category } from '../common/models/category';
import { AssetSearchCriteria } from '../common/models/assetSearchCriteria.model';
import { Currency } from '../common/models/currency.model';
import { ChartConfig } from '../common/helpers/chart.config';
import { AssetAsideComponent } from '../common/widgets/assetAside/assetAside.component';
import { AssetAsideOptions } from '../common/models/assetAsideOptions.enum';

@Component({
    selector: 'app-assets',
    templateUrl: './assets.component.html'
})
export class AssetsComponent {
    disableDelete: boolean;
    disableAdd: boolean;
    disableViewButton: boolean;
    disableRequest: boolean;
    assets: Asset[];
    loggedInUser: User;
    rateData: number;
    isCollapsed = false;
    conditions: IdValuePair[];
    categories: IdValuePair[];
    currencies: IdValuePair[];
    chartConfig = ChartConfig.assets;
    users: User[];

    get cantDownload(): boolean {
        return !this.assets.length;
    }
    get gridHeader(): ColumnDefinitions[] {
        return this._gridHeader;
    }

    private _gridHeader: ColumnDefinitions[];

    @ViewChild(GridComponent)
    private gridComponent: GridComponent;

    @ViewChild(AssetAsideComponent)
    private assetAsideComponent: AssetAsideComponent;

    constructor(
        private _dataService: AssetDataService,
        private _userService: UserService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService,
        private _dialogService: DialogService) {
        this.assets = [];
        this.users = [];
        this._gridHeader = [];
        this.disableRequest = true;
        this.disableAdd = false;
        this.disableViewButton = true;
        this.rateData = 350;
    }

    add() {
        this.assetAsideComponent.open(new Asset(), AssetAsideOptions.create);
    }

    view() {
        const selectedAsset = this.gridComponent.getSelectedRows<Asset>()[0];

        if(!selectedAsset) return;

        this._dataService.get(selectedAsset.id).subscribe(data => {
            this.assetAsideComponent.open(data, AssetAsideOptions.view);
        },
        err => this._notificationService.showError(err));
    }

    onFilter() {
        this.assets = [];
    }

    onSearchCriteriaChanged($event): void {
        const searchCriteria = new AssetSearchCriteria();

        for (let prop in $event) {
            searchCriteria[prop] = $event[prop];
        }

        this.getAssets(searchCriteria);
    }

    onRowSelected(rowNode: any[]) {
        const asset: Asset = rowNode[0];

        this.disableRequest = !(asset && !asset.assignee.id); 
        this.disableDelete = !(this.loggedInUser.isManager() && asset);
        this.disableViewButton = !asset;
    }

    onAssetSaved(asset: Asset) {
        const existingAsset = this.assets.find(x => x.id == asset.id);

        asset.assignee = this.users.find(user => user.id == asset.assignee.id) || new User();
        asset.purchasedBy = this.users.find(user => user.id == asset.purchasedBy.id) || new User();

        this.gridComponent.deSelectAll();

        if(!existingAsset) {
            this.assets = [asset, ...this.assets];
        }
        else {
            _.merge(existingAsset, asset);
            this.assets = [...this.assets];                
        }
    }

    request() {
        const selectedAsset = this.gridComponent.getSelectedRows<Asset>()[0];

        if (!selectedAsset) return ;

        this._dialogService.showConfirm('Request Asset', `Are you sure you want to request ${selectedAsset.item}?`).then(isOk => {
            if (!isOk) {
                return ;
            } else if (selectedAsset.assignee.id) {
                throw new Error(`${selectedAsset.item} is already assigned and can not be requested`);
            }

            this._dataService.request(selectedAsset.id).subscribe(() => {
                this._notificationService.showSuccess(`${selectedAsset.item} has been requested successfully`);
                this.gridComponent.deSelectAll();
            },
            err => this._notificationService.showError(err));
        });
    }

    deleteSelectedRows() {
        const selectedAsset = this.gridComponent.getSelectedRows<Asset>()[0];

        if (!selectedAsset) return ;

        this._dialogService.showConfirm('Delete Asset', `Are you sure you want to delete ${selectedAsset.item}?`).then(isOk => {
            if (!isOk) {
                return ;
            }

            this._spinnerService.show();

            this._dataService.delete(selectedAsset.id).subscribe(data => {
                _.remove(this.assets, y => y.id == selectedAsset.id);
                this.gridComponent.deleteSelectedRows();
                this._spinnerService.hide();
                this._notificationService.showSuccess('Asset has been succesfully deleted');
            },
            err => {
                this._spinnerService.hide();
                this._notificationService.showError(err);
            });
        });
    }

    downloadReport() {
        const searchCriteria = new AssetSearchCriteria();
        const criteria = this.gridComponent.getSearchCriteria();        
        
        for (let prop in criteria) {
            searchCriteria[prop] = criteria[prop];
        }

        this._spinnerService.show();
        this._dataService.download(searchCriteria).subscribe(data => {
            FileSaver.saveFile(data, 'assets');
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError('Failed to download report');
        });
    }

    ngOnInit() {
        this._userService.getCurrentUser().subscribe(user => this.loggedInUser = user);

        this._userService.getAllUsers().subscribe(users => {
            this.users = users;
            this.setGridHeader();
        },
        err => this._notificationService.showError(err));
    }

    setCollapse(): void {
        this.isCollapsed = !this.isCollapsed;
    }

    private getAssets(searchCriteria: AssetSearchCriteria): void {
        this._spinnerService.show();
        this._dataService.getAll(searchCriteria).subscribe(data => {
            this.assets = _.unionBy(this.assets, data, 'id');
            this.disableAdd = false;
            this.disableDelete = true;
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError(err);
        })
    }

    private setGridHeader(): void {
        this._gridHeader = [
            new ColumnDefinitions('Item', 'item', 103, 'item', new FilterDefinitions(false, 'text', 'contains')),
            new ColumnDefinitions('Serial #', 'serialNo', 120, 'serialNo', new FilterDefinitions(false, 'text', 'contains')),
            new ColumnDefinitions('Purchase Date', 'purchasedDate', 120, 'purchasedDate', new FilterDefinitions(false, 'date', 'inRange'), new CellDefinition(false, null, null, 'input')),
            new ColumnDefinitions('Purchased By', 'purchasedBy.fullName', 120, 'purchasedBy', new FilterDefinitions(false, 'dropdown', this.usersToIdValuePairs())),
            new ColumnDefinitions('Assignee', 'assignee.fullName', 120, 'assignee', new FilterDefinitions(false, 'dropdown', this.usersToIdValuePairs())),
            new ColumnDefinitions('Assigned Date', 'assignedDate', 120, 'assignedDate', new FilterDefinitions(false, 'date', 'inRange'), new CellDefinition(false, null, null, 'input')),
            new ColumnDefinitions('Condition', 'condition', 120, 'condition', new FilterDefinitions(false, 'dropdown', Condition.toIdValuePair()))
        ];
    }

    private usersToIdValuePairs = () => this.users.map(user => new IdValuePair(user.id, user.fullName));
}
