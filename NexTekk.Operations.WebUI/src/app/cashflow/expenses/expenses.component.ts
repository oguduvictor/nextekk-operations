import { Component, OnInit, ViewChild } from '@angular/core';
import { Expense } from '../common/models/expense.model';
import { ExpenseSearchCriteria } from '../common/models/expenseSearchCriteria.model';
import { ExpenseDataService } from '../common/services/expenseDataService';
import { Currency } from '../common/models/currency.model';
import { User, UserService, SpinnerService, NotificationService, GridComponent, IdValuePair, ColumnDefinitions, FilterDefinitions, CellDefinition, FileSaver } from '../../shared';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { ChartConfig } from '../common/helpers/chart.config';

@Component({
    selector: 'app-expenses',
    templateUrl: './expenses.component.html',
    styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {
    expenses: Expense[];
    disableDelete = true;
    rateData: number;
    isCollapsed = false;
    disableAdd: boolean;
    loggedInUser: Observable<User>;
    chartConfig = ChartConfig.expenses;

    get cantDownload(): boolean {
        return !this.expenses.length;
    }
    
    get gridHeader(): ColumnDefinitions[] {
        return this._gridHeader;
    }

    @ViewChild(GridComponent)
    private gridComponent: GridComponent;

    private _loggedInUser: User;
    private _users: User[];
    private _gridHeader: ColumnDefinitions[];

    constructor(
        private _expenseDataService: ExpenseDataService,
        private _userService: UserService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService
    ) {
        this._gridHeader = [];
        this.expenses = [];
        this.rateData = 350;
        this.disableAdd = false;
        this._loggedInUser = new User();
    }

    ngOnInit(): void {
        this._spinnerService.show();
        this.loggedInUser = this._userService.getCurrentUser();
        this.loggedInUser.subscribe(user => this._loggedInUser = user);

        this._userService.getAllUsers().subscribe(users => {
            this._users = users;
            this.setGridHeader();
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError("Error Occurred");
        });
    }

    addRow(): void {
        const newExpense = new Expense();
        newExpense['currency'] = this.GetDefaultCurrency();
        newExpense['incurredBy'] = this._loggedInUser;
        newExpense['incurrerFullName'] = this._loggedInUser.fullName;
        newExpense['amount'] = 0;

        this.gridComponent.addRow<Expense>(newExpense);

        this.disableAdd = true;
    }

    onCellValueChanged($event) {
        const cellChanged = $event.oldValue !== $event.newValue;
        const expense: Expense = $event.data;

        if (cellChanged && expense.isValid(this._loggedInUser.isManager())) {
            expense.incurredBy = this._users.find(user => user.fullName.includes(expense.incurrerFullName));;
            
            this.save(expense);
        }
    }

    onRowSelected(rowNode: Expense[]) {
        if (!_.isEmpty(rowNode)) {
            this.disableDelete = !rowNode[0].canDelete;
        } else {
            this.disableDelete = true;
        }
    }

    deleteSelectedRows(): void {
        const selectedRows = this.gridComponent.getSelectedRows<Expense>();
        
        if (selectedRows.length < 1) {
            return ;
        }

        const expense = selectedRows[0];

        if (!expense.id) {
            this.gridComponent.deleteSelectedRows();
        } else {
            this._spinnerService.show();
            this._expenseDataService.deleteExpense(expense.id)
            .subscribe(x => {
                _.remove(this.expenses, y => y.id == expense.id);
                this.gridComponent.deleteSelectedRows();
                this._spinnerService.hide();
                this._notificationService.showSuccess('Deleted Expense Succesfully');
            },
            err => {
                this._spinnerService.hide();
                this._notificationService.showError('Delete expense failed');
            });
        }
    }

    onFilter() {
        this.expenses = [];
    }

    onSearchCriteriaChanged($event): void {
        const searchCriteria = new ExpenseSearchCriteria();

        for (let prop in $event) {
            searchCriteria[prop] = $event[prop];
        }

        this.getExpenses(searchCriteria);
    }

    setCollapse(): void {
        this.isCollapsed = !this.isCollapsed;
    }

    downloadReport(): void {
        const searchCriteria = new ExpenseSearchCriteria();
        const criteria = this.gridComponent.getSearchCriteria();        
        
        for (let prop in criteria) {
            searchCriteria[prop] = criteria[prop];
        }

        this._spinnerService.show();
        this._expenseDataService.downloadReport(searchCriteria).subscribe(data => {
            FileSaver.saveFile(data, 'revenue');
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError('Failed to download report');
        });
    }

    private setGridHeader(): void {
        this._gridHeader = [
            new ColumnDefinitions('Date', 'date', 100, 'date', new FilterDefinitions(false, 'date', 'inRange'), new CellDefinition(true, 'date', null, 'input')),
            new ColumnDefinitions('Person', 'incurrerFullName', 123, 'incurredById', new FilterDefinitions(false, 'dropdown', this.usersToIdValuePairs()), new CellDefinition(this._loggedInUser.isManager(), 'dropdown',  this.usersToIdValuePairs())),
            new ColumnDefinitions('Currency', 'currency', 90, 'currency', new FilterDefinitions(false, 'dropdown', Currency.toIdValuePair()), new CellDefinition(CellDefinition.enableCellEdit, 'dropdown', Currency.toIdValuePair())),
            new ColumnDefinitions('Amount', 'amount', 90, 'amount', new FilterDefinitions(true, 'number', 'inRange'), new CellDefinition(CellDefinition.enableCellEdit, 'input', null, 'input')),
            new ColumnDefinitions('Description', 'description', 155, 'description', new FilterDefinitions(false, 'text', 'contains'), new CellDefinition(true, 'input')),
            new ColumnDefinitions('Reason', 'reason', 150, 'reason', new FilterDefinitions(true), new CellDefinition(true, 'input')),
            new ColumnDefinitions('Reimbursed', 'reimbursed', 115, 'reimbursed', new FilterDefinitions(false, 'dropdown', IdValuePair.GenerateYesNoOption()), new CellDefinition(CellDefinition.enableCellEdit, 'date', null, 'input'))
        ];
    }

    private getExpenses(searchCriteria: ExpenseSearchCriteria): void {
        this._spinnerService.show();
        this._expenseDataService.getExpenses(searchCriteria).subscribe(expenses => {
            this.expenses = _.unionBy(this.expenses, expenses, 'id');
            this.disableAdd = false;
            this.disableDelete = true;
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError('Error occurred');
        });
    }

    private save(expense: Expense) {
        this._spinnerService.show();
            this._expenseDataService.saveExpense(expense)
                .subscribe(savedExpense => {
                    const existingExpense = this.expenses.find(x => x.id == (savedExpense.id || expense.id));

                    if (existingExpense) {
                        this.expenses = [...this.expenses];
                    }
                    else {
                        savedExpense.incurredBy = expense.incurredBy;
                        savedExpense.incurrerFullName = expense.incurrerFullName;
                        
                        this.expenses = [savedExpense, ...this.expenses].sort((a, b) => a.date.getMilliseconds() - b.date.getMilliseconds());
                    }

                    this.disableAdd = false;
                    this._spinnerService.hide();
                },
                err => {
                    this._spinnerService.hide();
                    this._notificationService.showError(err);
                });
    }

    private usersToIdValuePairs = () => this._users.map(user => new IdValuePair(user.id, user.fullName));

    private GetDefaultCurrency = (): Currency => (new Date()).toString().indexOf('GMT+0100') ? Currency.naira : Currency.dollar;
}
