import { ExpensesComponent } from './expenses.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ExpenseDataService } from '../common/services/expenseDataService';
import { Expense } from '../common/models/expense.model';
import { Currency } from '../common/models/currency.model'
import { User, UserService, SpinnerService, NotificationService, SharedModule, GridComponent } from '../../shared';
import { Observable } from 'rxjs/Observable';
import { CashFlowModule } from '../cashflow.module';
import { ExpenseSearchCriteria } from '../common/models/expenseSearchCriteria.model';

describe('ExpenseComponent', () => {
    const _userService = <any> {
        getCurrentUser: () => {
            return Observable.of(_user1);
        },
        getAllUsers: () => {
            return Observable.of([_user1, _user2]);
        }
    };
    
    let _mockDataService;
    let _expense1;
    let _expense2;
    let _expense3;

    const _spinnerService: SpinnerService = jasmine.createSpyObj('SpinnerService', ['show', 'hide']);
    const _notificationService: NotificationService = jasmine.createSpyObj('NotificationService', [
        'showError', 'showInfo', 'showWarning', 'showSuccess'
    ]);
    const _user1 = new User('_user1_id', 'Joe', 'Blow', 'joe@blow.com', [ 'Manager', 'Employee']);
    const _user2 = new User('_user2_id', 'Malcolm', 'Merlin', 'malcom@merlin.com', [ 'Employee' ]);
    const _date1 = new Date('11-2-2017 09:00:00');
    const _date2 = new Date('10-5-2017 11:45:00');
    const _date3 = new Date('12-10-2017 10:00:00');

    let _fixture: ComponentFixture<ExpensesComponent>;
    let _expenseComponent: ExpensesComponent;

    beforeEach(() => {
        _expense1 = new Expense(_user1, Currency.naira, '_expense1_id', _date1, 45000, 'for lunch', 'Wanted Something light', null);
        _expense2 = new Expense(_user1, Currency.dollar, '_expense2_id', _date2, 6700, 'for jeans', 'Wanted new ones', _date3);
        _expense3 = new Expense(_user2, Currency.naira, '_expense_3', _date2, 7800, 'donated to charity', 'i felt bad' );

        _mockDataService = {
            getExpenses() {
                return Observable.of([_expense1, _expense2, _expense3]);
            },
            saveExpense(expense: Expense) {
                if (expense.id) {
                    return _mockDataService.updateExpense(expense);
                }
    
                return _mockDataService.addExpense(expense);
            },
            updateExpense(expense: Expense) {
                return Observable.of();
            },
            addExpense(expense: Expense) {
                expense.id = '2133-134-2123-82633826';
                expense.created = new Date();
    
                return Observable.of(expense);
            },
            downloadReport(searchCriteria: ExpenseSearchCriteria){
                return Observable.of({});
            }
        };

        TestBed.configureTestingModule({
            imports: [ CashFlowModule ],
            providers: [
                { provide: ExpenseDataService, useValue: _mockDataService },
                { provide: UserService, useValue: _userService },
                { provide: SpinnerService, useValue: _spinnerService },
                { provide: NotificationService, useValue: _notificationService }
            ]
        });

        _fixture = TestBed.createComponent(ExpensesComponent);
        _expenseComponent = <ExpensesComponent>_fixture.debugElement.componentInstance;
    });

    it('can be instantiated successfully', () => {
        expect(_expenseComponent).toBeTruthy();

        expect(_expenseComponent.disableDelete).toBe(true);
        expect(_expenseComponent.rateData).toEqual(350);
        expect(_expenseComponent.isCollapsed).toBeFalsy();
    });

    it('should set grid definitions on initialization', () => {
        _expenseComponent.ngOnInit();

        expect(_expenseComponent.gridHeader.length).toBe(7);
    });

    it('should populate expenses list when search criteria change event is emitted', () => {
        _expenseComponent.onSearchCriteriaChanged({});

        expect(_expenseComponent.expenses.length).toBe(3);
        expect(_expenseComponent.expenses[0]).toBe(_expense1);
        expect(_expenseComponent.expenses[1]).toBe(_expense2);
        expect(_expenseComponent.expenses[2]).toBe(_expense3);
    });

    it('should increase list of expenses when cell value changes and new expense is emitted from cell value changes', () => {
        _expenseComponent.ngOnInit();
        _expenseComponent.onSearchCriteriaChanged({});
        
        const newExpense = Object.create(_expense1);
        newExpense.id = null;

        const event = {
            oldValue: 12,
            newValue: 50,
            data: newExpense
        };

        _expenseComponent.onCellValueChanged(event);

        expect(_expenseComponent.expenses.length).toBe(4);
    });

    it('should update list of expenses when cell value changes and expense is emitted from cell value changes', () => {
        _expenseComponent.ngOnInit();
        _expenseComponent.onSearchCriteriaChanged({});
        
        _expense1.amount = 50;

        const event = {
            oldValue: 12,
            newValue: 50,
            data: _expense1
        };

        _expenseComponent.onCellValueChanged(event);

        const updatedExpense = _expenseComponent.expenses.find(x => x.id == _expense1.id);

        expect(updatedExpense.amount).toBe(50);
    });

    it('should download expense report when download button is clicked', () => {
        _expenseComponent.ngOnInit();
        _expenseComponent.onSearchCriteriaChanged({});

        spyOn(_expenseComponent, 'downloadReport');

        const compiled = _fixture.debugElement.nativeElement;

        compiled.querySelector('button.btn-default#downloadBtn').click();

        expect(_expenseComponent.downloadReport).toHaveBeenCalledTimes(1);
    });
});
