import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpensesComponent } from './expenses/expenses.component';
import { RouterModule } from '@angular/router';
import { ExpenseDataService } from './common/services/expenseDataService';
import { FormsModule } from '@angular/forms';
import { BarChartComponent } from './common/widgets/barChart/barChart.component';
import { PieChartComponent } from './common/widgets/pieChart/pieChart.component';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { ManagerGuardService, EmployeeManagerGuardService, UserService, SpinnerService } from '../shared'
import { RevenueComponent } from './revenue/revenue.component';
import { RevenueDataService } from './common/services/revenueDataService';
import { BoxSummaryComponent } from './common/widgets/box-summary/box-summary.component';
import { SharedModule } from '../shared/shared.module';
import { AssetsComponent } from './assets/assets.component';
import { AssetDataService } from './common/services/assetDataService';
import { AssetAsideComponent } from './common/widgets/assetAside/assetAside.component';

@NgModule({
    declarations: [
        ExpensesComponent,
        RevenueComponent,
        AssetsComponent,
        BarChartComponent,
        PieChartComponent,
        BoxSummaryComponent,
        AssetAsideComponent
    ],
    imports: [
        SharedModule,
        FormsModule,
        CommonModule,
        RouterModule.forChild([
            { path: 'expenses', component: ExpensesComponent, canActivate: [EmployeeManagerGuardService] },
            { path: 'revenue', component: RevenueComponent, canActivate: [ManagerGuardService] },
            { path: 'assets', component: AssetsComponent, canActivate: [EmployeeManagerGuardService] }
        ]),
        BsDatepickerModule.forRoot()
    ],
    providers: [ExpenseDataService, RevenueDataService, AssetDataService]
})
export class CashFlowModule {
}
