import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { User, UserService, SpinnerService, NotificationService,ColumnDefinitions, FilterDefinitions, GridComponent, CellDefinition, IdValuePair, FileSaver } from '../../shared';
import { Currency } from '../common/models/currency.model';
import { RevenueSearchCriteria } from '../common/models/revenueSearchCriteria.model';
import { RevenueDataService } from '../common/services/revenueDataService';
import { Revenue } from '../common/models/revenue.model';
import * as _ from 'lodash';
import { Client } from '../../projectManagement/common/models/client.model';
import { Observable } from 'rxjs/Observable';
import { ExpenseSearchCriteria } from '../common/models/expenseSearchCriteria.model';
import { ChartConfig } from '../common/helpers/chart.config';
import 'rxjs/add/observable/concat';

@Component({
    selector: 'app-revenue',
    templateUrl: './revenue.component.html',
    styleUrls: ['./revenue.component.css']
})
export class RevenueComponent implements OnInit {
    revenues: Revenue[];
    disableDelete: boolean;
    disableAdd: boolean;
    columnDefs: any[];
    rateData: number;
    isCollapsed: boolean;
    includeExpense: boolean;
    loggedInUser: Observable<User>;
    chartConfig = ChartConfig.revenues;
    
    get cantDownload(): boolean {
        return !this.revenues.length;
    }

    get gridHeader(): ColumnDefinitions[] {
        return this._gridHeader;
    }

    private _users: User[];

    @ViewChild(GridComponent)
    private gridComponent: GridComponent;

    private _clients: Client[];
    private _loggedInUser: User;
    private _gridHeader: ColumnDefinitions[];

    constructor(
        private _revenueDataService: RevenueDataService,
        private _userService: UserService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService
    ) {
        this.disableDelete = true;
        this.disableAdd = false;
        this.rateData = 350;
        this.revenues = [];
        this.isCollapsed = false;
        this._users = [];
        this._clients = [];
        this.includeExpense = false;
        this._gridHeader = [];
    }

    downloadReport() {
        const searchCriteria = new RevenueSearchCriteria();
        const criteria = this.gridComponent.getSearchCriteria();        
        
        for (let prop in criteria) {
            searchCriteria[prop] = criteria[prop];
        }

        searchCriteria.includeExpenses = this.includeExpense;

        this._spinnerService.show();
        this._revenueDataService.downloadReport(searchCriteria).subscribe(data => {
            FileSaver.saveFile(data, 'revenue');
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError(err);
        });
    }

    addRow() {
        const revenue = new Revenue();
        revenue.createdBy = this._loggedInUser;
        revenue.currency = this.GetDefaultCurrency();
        revenue.client = _.head(this._clients);
        revenue.created = new Date();
        revenue.amount = 0;

        this.gridComponent.addRow<Revenue>(revenue);

        this.disableAdd = true;
    }

    deleteSelectedRows() {
        const selectedRows = this.gridComponent.getSelectedRows<Revenue>();
        if (selectedRows.length < 1) {
            return ;
        }

        const revenue: Revenue = selectedRows[0];

        if (!revenue.id) {
            this.gridComponent.deleteSelectedRows();
        } else {
            this._spinnerService.show();
            this._revenueDataService.deleteRevenue(revenue.id).subscribe(x => {
                _.remove(this.revenues, y => y.id == revenue.id);
                this.gridComponent.deleteSelectedRows();
                this._spinnerService.hide();
                this._notificationService.showSuccess('Deleted Revenue Succesfully');
            },
            err => {
                this._spinnerService.hide();
                this._notificationService.showError(err);
            });
        }
    }

    onCellValueChanged($event: any) {
        const cellChanged = $event.oldValue !== $event.newValue;
        const revenue: Revenue = $event.data;

        if (cellChanged && revenue.isValid) {
            revenue.client = this._clients.find(client => client.name == revenue.client.name);
            
            this.save(revenue);
        }
    }

    onRowSelected(rowNode: Revenue[]) {
        if (!_.isEmpty(rowNode)) {
            this.disableDelete = !rowNode[0].canDelete;
        } else {
            this.disableDelete = true;
        }
    }

    onFilter() {
        this.revenues = [];
    }

    onSearchCriteriaChanged($event) {
        const searchCriteria = new RevenueSearchCriteria();

        for (let prop in $event) {
            searchCriteria[prop] = $event[prop];
        }

        searchCriteria.includeExpenses = this.includeExpense;

        this.getRevenues(searchCriteria);
    }

    setCollapse(): void {
        this.isCollapsed = !this.isCollapsed;
    }
    
    getExpenses() {
        this.includeExpense = !this.includeExpense;
        const searchCriteria = this.gridComponent.getSearchCriteria();
        this.revenues = [];
        this.gridComponent.refreshData();
    }

    ngOnInit(): void {
        this.loggedInUser = this._userService.getCurrentUser();

        this.loggedInUser.subscribe(user => this._loggedInUser = user);

        Observable.concat(this._revenueDataService.getClientSummaries(), this._userService.getAllUsers()).subscribe(data => {
            if (data[0] instanceof Client) {
                this._clients = <Client[]>data;
            }
            else if (data[0] instanceof User) {
                this._users = <User[]>data;
            }
        },
        err => this._notificationService.showError('Error Occured'),
        () => this.setGridHeader());
    }

    private setGridHeader(): void {
        this._gridHeader = [
            new ColumnDefinitions('Payment Date', 'paymentDate', 160, 'paymentDate', new FilterDefinitions(false, 'date', 'inRange'), new CellDefinition(CellDefinition.enableCellEdit, 'date', null, 'input')),
            new ColumnDefinitions('Entered By', 'enteredBy', 150, 'enteredBy', new FilterDefinitions(false, 'dropdown', this.usersToIdValuePairs()), new CellDefinition(false, 'dropdown',  this.usersToIdValuePairs())),
            new ColumnDefinitions('Currency', 'currency', 90, 'currency', new FilterDefinitions(false, 'dropdown', Currency.toIdValuePair()), new CellDefinition(CellDefinition.enableCellEdit, 'dropdown', Currency.toIdValuePair())),
            new ColumnDefinitions('Amount', 'amount', 90, 'amount', new FilterDefinitions(false, 'number', 'inRange'), new CellDefinition(CellDefinition.enableCellEdit, 'input', null, 'input')),
            new ColumnDefinitions('Client', 'client.name', 152, 'clientIds', new FilterDefinitions(false, 'dropdown', this.clientsToIdValuePairs()), new CellDefinition(CellDefinition.enableCellEdit, 'dropdown', this.clientsToIdValuePairs())),
            new ColumnDefinitions('Description', 'description', 180, 'description', new FilterDefinitions(false, 'text', 'contains'), new CellDefinition(CellDefinition.enableCellEdit, 'input'))
        ];
    }

    private getRevenues(searchCriteria: RevenueSearchCriteria) {
        this._spinnerService.show();
        this._revenueDataService.getRevenue(searchCriteria).subscribe(revenues => {
            this.revenues = _.unionBy(revenues, this.revenues, 'id');
            this.disableAdd = false;
            this.disableDelete = true;

            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide(); 
            this._notificationService.showError(err);
        });
    }

    private save(revenue: Revenue) {
        this._spinnerService.show();
        this._revenueDataService.saveRevenue(revenue).subscribe(savedRevenue => {
            const existingRevenue = this.revenues.find(x => x.id === (savedRevenue.id || revenue.id));
            
            if (existingRevenue) {
                _.merge(existingRevenue, revenue);
                this.revenues = [...this.revenues];
            }
            else {
                this.revenues = [savedRevenue, ...this.revenues].sort((a, b) => a.paymentDate.getMilliseconds() - b.paymentDate.getMilliseconds());
            }

            this.disableAdd = false;
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError(err);
        });
    }

    private GetDefaultCurrency = (): Currency => (new Date()).toString().indexOf('GMT+0100') ? Currency.naira : Currency.dollar;
    
    private usersToIdValuePairs = () => this._users.map(user => new IdValuePair(user.id, user.fullName));
    
    private clientsToIdValuePairs = () => this._clients.map(client => new IdValuePair(client.id, client.name));
}
