import { Observable } from "rxjs/Observable";
import { User, SpinnerService, NotificationService, UserService } from "../../shared";
import { CashFlowModule } from "../cashflow.module";
import { TestBed, ComponentFixture } from "@angular/core/testing";
import { RevenueDataService } from "../common/services/revenueDataService";
import { RevenueSearchCriteria } from "../common/models/revenueSearchCriteria.model";
import { Revenue } from "../common/models/revenue.model";
import { Currency } from "../common/models/currency.model";
import { Client } from "../../projectManagement/common/models/client.model";
import { RevenueComponent } from "./revenue.component";

describe('RevenueComponent', () => {
    const _user1 = new User('_user1_id', 'Joe', 'Blow', 'joe@blow.com', [ 'Manager', 'Employee']);
    const _user2 = new User('_user2_id', 'Malcolm', 'Merlin', 'malcom@merlin.com', [ 'Employee' ]);
    const _date1 = new Date(2017, 10, 12, 9, 4, 3);
    const _date2 = new Date(2017, 11, 13, 11, 1, 9);
    const _date3 = new Date(2017, 12, 10, 10, 3, 4);
    const _client1 = new Client('1', 'Microsoft LTD');
    const _client2 = new Client('2', 'Alphabet LLC');
    const _client3 = new Client('3', 'Consolidated Group');
    const _userService = <any> {
        getCurrentUser: () => {
            return Observable.of(_user1);
        },
        getAllUsers: () => {
            return Observable.of([_user1, _user2]);
        }
    };
    const _spinnerService: SpinnerService = jasmine.createSpyObj('SpinnerService', ['show', 'hide']);
    const _notificationService: NotificationService = jasmine.createSpyObj('NotificationService', [
        'showError', 'showInfo', 'showWarning', 'showSuccess'
    ]);
    let _revenueDataService;
    let _revenue1;
    let _revenue2;
    let _revenue3;
    let _fixture: ComponentFixture<RevenueComponent>;
    let _revenueComponent: RevenueComponent;

    beforeEach(() => {
        _revenue1 = new Revenue('1', _date1, Currency.naira, 3000, _client1, 'Infrastructure', _date3, _user1, _date3);
        _revenue2 = new Revenue('2', _date2, Currency.dollar, 12, _client2, 'Design', _date3, _user2, _date3);
        _revenue3 = new Revenue('3', _date3, Currency.dollar, 90, _client3, 'Calculating', _date3, _user1, _date3);
        
        _revenueDataService = {
            getRevenue(searchCriteria: RevenueSearchCriteria) {
                return Observable.of([_revenue1, _revenue2, _revenue3]);
            },
            saveRevenue(revenue: Revenue) {
                if (revenue.id) {
                    return _revenueDataService.updateRevenue(revenue);
                }
    
                return _revenueDataService.addRevenue(revenue);
            },
            updateRevenue(revenue: Revenue) {
                return Observable.of('');
            },
            addRevenue(revenue: Revenue) {
                revenue.id = '2133-134-2123-82633826';
                
                return Observable.of(revenue);
            },
            downloadReport(searchCriteria: RevenueSearchCriteria){
                return Observable.of({});
            },
            deleteRevenue(RevenueId: string) {
                return Observable.of(true);
            },
            getClientSummaries() {
                return Observable.of([_client1, _client2, _client3]);
            }
        };

        TestBed.configureTestingModule({
            imports: [ CashFlowModule ],
            providers: [
                { provide: RevenueDataService, useValue: _revenueDataService },
                { provide: UserService, useValue: _userService },
                { provide: SpinnerService, useValue: _spinnerService },
                { provide: NotificationService, useValue: _notificationService }
            ]
        });

        _fixture = TestBed.createComponent(RevenueComponent);
        _revenueComponent = <RevenueComponent>_fixture.debugElement.componentInstance;
    });
    
    it('can be instantiated', () => {
        expect(_revenueComponent).toBeTruthy();
    });

    it('should set grid definitions on initialization', () => {
        expect(_revenueComponent.gridHeader.length).toBe(0);

        _revenueComponent.ngOnInit();

        expect(_revenueComponent.gridHeader.length).toBe(6);
    });

    it('should populate revenue list when search criteria change event is emitted', () => {
        _revenueComponent.onSearchCriteriaChanged({});

        expect(_revenueComponent.revenues.length).toBe(3);
        expect(_revenueComponent.revenues[0]).toBe(_revenue1);
        expect(_revenueComponent.revenues[1]).toBe(_revenue2);
        expect(_revenueComponent.revenues[2]).toBe(_revenue3);
    });

    it('should increase list of revenues when cell value changes and new revenue is emitted from cell value changes', () => {
        _revenueComponent.ngOnInit();
        _revenueComponent.onSearchCriteriaChanged({});
        
        const newRevenue = Object.create(_revenue1);
        newRevenue.id = null;

        const event = {
            oldValue: 12,
            newValue: 50,
            data: newRevenue
        };

        _revenueComponent.onCellValueChanged(event);

        expect(_revenueComponent.revenues.length).toBe(4);
    });

    it('should update list of revenues when cell value changes and revenue is emitted from cell value changes', () => {
        _revenueComponent.ngOnInit();
        _revenueComponent.onSearchCriteriaChanged({});
        
        _revenue1.amount = 50;

        const event = {
            oldValue: 12,
            newValue: 50,
            data: _revenue1
        };

        _revenueComponent.onCellValueChanged(event);

        const updatedrevenue = _revenueComponent.revenues.find(x => x.id == _revenue1.id);

        expect(updatedrevenue.amount).toBe(50);
    });

    it('should download revenue report when download button is clicked', () => {
        _revenueComponent.ngOnInit();
        _revenueComponent.onSearchCriteriaChanged({});

        spyOn(_revenueComponent, 'downloadReport');

        const compiled = _fixture.debugElement.nativeElement;

        compiled.querySelector('button.btn-default#downloadBtn').click();

        expect(_revenueComponent.downloadReport).toHaveBeenCalledTimes(1);
    });
});
