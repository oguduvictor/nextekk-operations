import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared';
import { Asset } from '../models/asset.model';
import { Observable } from 'rxjs/Observable';
import { ResponseContentType } from '@angular/http';
import { AssetSearchCriteria } from '../models/assetSearchCriteria.model';

@Injectable()
export class AssetDataService {
    private _apiUrl = 'api/assets/';
    
    constructor(private _http: HttpService) { }

    public getAll = (search: AssetSearchCriteria): Observable<Asset[]> => 
        this._http.getMultiple<Asset>(`${this._apiUrl}search`, Asset, { params: search.toParams() } );

    public get = (id: string): Observable<Asset> => 
        this._http.getData<Asset>(`${this._apiUrl}${id}`, Asset);

    public save = (asset: Asset): Observable<Asset> => {
        return !!asset.id ? this.update(asset) : this.create(asset);
    }

    public download = (searchCriteria: AssetSearchCriteria): Observable<Response> => 
        this._http.getData<Response>(`${this._apiUrl}download`, null, { responseType: ResponseContentType.ArrayBuffer });

    public request = (id: string) => 
        this._http.getData(`${this._apiUrl}request`, null, { params: { id } });

    public delete = (id: string): Observable<string> => 
        this._http.deleteData<string>(`${this._apiUrl}${id}`);

    private create = (asset: Asset): Observable<Asset> => 
        this._http.postData<Asset>(this._apiUrl, asset.toDto(), Asset);

    private update = (asset: Asset): Observable<Asset> => 
        this._http.putData<Asset>(`${this._apiUrl}${asset.id}`, asset.toDto(), Asset);
}