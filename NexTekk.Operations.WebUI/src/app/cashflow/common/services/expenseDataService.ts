import { Expense } from '../models/expense.model';
import { HttpService } from '../../../shared';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../../../shared';
import { ExpenseSearchCriteria } from '../models/expenseSearchCriteria.model';
import { RequestOptions, ResponseContentType } from '@angular/http';

@Injectable()
export class ExpenseDataService {
    private _expenseApiUrl = 'api/Expenses/';

    constructor(private _http: HttpService) {}

    public getExpenses(searchCriteria: ExpenseSearchCriteria): Observable<Expense[]> {
        let options = new RequestOptions();

        options.params = searchCriteria.toParams();
        return this._http.getMultiple<Expense>(this._expenseApiUrl, Expense, options);
    }

    public addExpense(expense: Expense): Observable<Expense> {
        return this._http.postData<Expense>(this._expenseApiUrl, expense, Expense);
    }

    public updateExpense(expense: Expense): Observable<Expense> {

        return this._http.putData<Expense>(`${this._expenseApiUrl}${expense.id}`, expense, Expense);
    }

    public saveExpense(expense: Expense): Observable<Expense> {
        if (expense.created) {
            return this.updateExpense(expense.toDto());
        }

        return this.addExpense(expense.toDto());
    }

    public deleteExpense(expenseId: string): Observable<string> {
        return this._http.deleteData(`${this._expenseApiUrl}${expenseId}`);
    }

    downloadReport(searchCriteria: ExpenseSearchCriteria): Observable<Response> {
        let options = new RequestOptions();
        options.responseType = ResponseContentType.ArrayBuffer;

        options.params = searchCriteria.toParams();

        return this._http.getData<Response>(`${this._expenseApiUrl}download`, null, options);
    }
}
