import { Revenue } from '../models/revenue.model';
import { HttpService } from '../../../shared';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../../../shared';
import { RevenueSearchCriteria } from '../models/revenueSearchCriteria.model';
import { RequestOptions, ResponseContentType } from '@angular/http';
import { Currency } from '../models/currency.model';
import { Client } from '../../../projectManagement/common/models/client.model';

@Injectable()
export class RevenueDataService {
    private _revenueApiUrl = 'api/Revenues/';
    private _clientApiUrl = 'api/Client/';

    constructor(private _http: HttpService) { }

    public getRevenue(searchCriteria: RevenueSearchCriteria): Observable<Revenue[]> {
        const options = new RequestOptions();
        const today = new Date();

        options.params = searchCriteria.toParams();

        return this._http.getMultiple<Revenue>(this._revenueApiUrl, Revenue, options);
    }

    public addRevenue(revenue: Revenue): Observable<Revenue> {
        return this._http.postData<Revenue>(this._revenueApiUrl, revenue, Revenue);
    }

    public updateRevenue(revenue: Revenue): Observable<Revenue> {
        return this._http.putData<Revenue>(`${this._revenueApiUrl}${revenue.id}`, revenue, Revenue);
    }

    public saveRevenue(revenue: Revenue): Observable<Revenue> {
        if (revenue.id) {
            return this.updateRevenue(revenue.toDto());
        }

        return this.addRevenue(revenue.toDto());
    }

    public deleteRevenue(RevenueId: string): Observable<string> {
        return this._http.deleteData(`${this._revenueApiUrl}${RevenueId}`);
    }

    public getClientSummaries(): Observable<Client[]> {
        return this._http.getMultiple(`${this._clientApiUrl}GetClientSummaries`, Client);
    }

    public downloadReport(searchCriteria: RevenueSearchCriteria): Observable<Response> {
        let options = new RequestOptions();
        options.responseType = ResponseContentType.ArrayBuffer;

        options.params = searchCriteria.toParams();

        return this._http.getData<Response>(`${this._revenueApiUrl}download`, null, options);
    }
}
