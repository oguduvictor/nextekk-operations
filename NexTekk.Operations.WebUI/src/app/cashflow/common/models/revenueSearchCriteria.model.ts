import { URLSearchParams } from '@angular/http';

export class RevenueSearchCriteria {
    constructor(
        public revenueIds?: string[],
        public expenseIds?: string[],
        public clientIds?: string[],
        public enteredBy?: string,
        public currency?: string,
        public paymentDate?: object,
        public description?: string,
        public pageNumber?: number,
        public minAmount?: number,
        public maxAmount?: number,
        public includeExpenses?: boolean
    ) { }

    public toParams(): URLSearchParams {
        let params = new URLSearchParams();

        if (this.revenueIds) {
            this.revenueIds.forEach(id => params.append('revenueIds', id));
        }

        if (this.clientIds) {
            params.set('clientIds', this.clientIds.toString());
        }

        if (this.paymentDate) {
            params.set('paymentDate.beginDateTime', this.paymentDate['beginDateTime'] && this.paymentDate['beginDateTime']);
            params.set('paymentDate.endDateTime', this.paymentDate['endDateTime'] && this.paymentDate['endDateTime']);
        }
        
        if (this.pageNumber) {
            params.set('pageNumber', this.pageNumber.toString());
        }
        
        if (this.includeExpenses) {
            params.set('includeExpenses', this.includeExpenses.toString());
        }

        if (this.expenseIds) {
            this.expenseIds.forEach(id => params.append('expenseIds', id));
        }

        params.set('enteredBy', this.enteredBy);
        params.set('currency', this.currency);
        params.set('description', this.description);
        
        if (this.minAmount) {
            params.set('minAmount', this.minAmount.toString());
        }

        if (this.maxAmount) {
            params.set('maxAmount', this.maxAmount.toString());
        }

        return params;
    }
}
