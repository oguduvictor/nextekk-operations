import { Currency } from './currency.model';
import { User, DateTimeHelper } from '../../../shared';

export class Expense {
    incurrerFullName: string;

    constructor(
        public incurredBy?: User,
        public currency?: Currency,
        public id?: string,
        public date?: Date,
        public amount?: number,
        public description?: string,
        public reason?: string,
        public reimbursed?: Date,
        public created?: Date,
        public modified?: Date,
        public selected?: boolean
    ) {
        this.incurredBy = incurredBy || new User();
    }

    public isValid(isManager: boolean): boolean {
        if (!isManager) {
            return !!this.currency && !!this.date && !!this.description && !!this.amount;
        }

        return !!this.currency && !!this.date && !!this.description && !!this.amount && !!this.incurredBy;
    }

    public get canDelete(): boolean {
        if (this.reimbursed && this.modified) {
            let today = new Date();
            let modified = new Date(this.modified);

            let diff = today.getTime() - modified.getTime();
            let hrs = diff / (1000 * 60 * 60);

            return Number(hrs.toFixed(2)) <= 24;
        }
        return true;
    }

    fromDto(dto: any) {
        this.id = dto.id;
        this.date = new Date(dto.date);
        this.currency = dto.currency === 'naira' ? Currency.naira : Currency.dollar;
        this.amount = dto.amount;
        this.description = dto.description;
        this.reason = dto.reason;
        this.reimbursed = dto.reimbursed && new Date(dto.reimbursed);
        this.created = new Date(dto.date);
        this.modified = new Date(dto.modified);

        if (dto.incurredBy) {
            this.incurredBy = new User(dto.incurredBy.id, dto.incurredBy.firstName, dto.incurredBy.lastName);
            this.incurrerFullName = this.incurredBy && this.incurredBy.fullName;
        }
    }

    toDto(): any {
        return {
            id: this.id,
            date: this.date,
            incurredById: this.incurredBy.id,
            currency: this.currency === Currency.naira ? 'naira' : 'dollar',
            amount: this.amount,
            description: this.description,
            reason: this.reason,
            reimbursed: this.reimbursed
        };
    }
}
