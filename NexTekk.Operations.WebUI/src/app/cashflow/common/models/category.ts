import { IdValuePair } from "../../../shared";

export class Category {
    static unknown = 'Unknown';
    static computer = 'Computer';
    static phone = 'Phone';
    static computerAccessory = 'Computer Accessory';
    static furniture = 'Furniture';
    static officeSupply = 'Office Supply';
    static souvenir = 'Souvenir';
    static other = 'Other';

    public static toIdValuePair = (): IdValuePair[] => {
        const list = [];
        for (let prop in Category) {
            if (!(Category[prop] instanceof Function)) {
                list.push(new IdValuePair(Category[prop], prop));
            }
        }

        return list;
    }
}