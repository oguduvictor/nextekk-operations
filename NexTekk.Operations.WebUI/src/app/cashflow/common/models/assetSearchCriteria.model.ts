import { URLSearchParams  } from '@angular/http';

export class AssetSearchCriteria {
    public item?: string;
    public serialNo?: string;
    public purchasedDate?: object;
    public purchasedBy?: string;
    public assignee?: string;
    public assignedDate?: object;
    public condition?: string;
    public assetIds?: string[];
    public pageNumber?: number;

    public toParams(): URLSearchParams {
        let params = new URLSearchParams();

        params.set('item', this.item);
        params.set('serialNo', this.serialNo);
        
        if (this.purchasedDate) {
            params.set('purchasedDate.beginDateTime', this.purchasedDate['beginDateTime'] && this.purchasedDate['beginDateTime']);
            params.set('purchasedDate.endDateTime', this.purchasedDate['endDateTime'] && this.purchasedDate['endDateTime']);
        }
        
        if (this.assignedDate) {
            params.set('assignedDate.beginDateTime', this.assignedDate['beginDateTime'] && this.assignedDate['beginDateTime']);
            params.set('assignedDate.endDateTime', this.assignedDate['endDateTime'] && this.assignedDate['endDateTime']);
        }

        params.append('purchasedByIds', this.purchasedBy);
        params.append('assigneeIds', this.assignee);
        params.set('condition', this.condition);
        params.set('pageNumber', this.pageNumber.toString());

        return params;
    }
}