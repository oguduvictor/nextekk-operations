import { Currency } from './currency.model';
import { IdValuePair } from '../../../shared';

describe('Currency', () => {
    it('can convert currencies to IdValuePair', () => {
        let idValuePairs: IdValuePair[] = Currency.toIdValuePair();

        expect(idValuePairs.length).toEqual(2);
        expect(idValuePairs[0] instanceof IdValuePair).toBe(true);
        expect(idValuePairs[1] instanceof IdValuePair).toBe(true);
    })
});