import { IdValuePair } from "../../../shared";

export class Currency {
    static naira = '₦';
    static dollar = '$';

    public static toIdValuePair() {
        return [
            new IdValuePair('naira', this.naira),
            new IdValuePair('dollar', this.dollar)
        ]
    }

    public static getCurrencySymbol(currency: string) {
        switch (currency) {
            case 'dollar':
                return '$';
            case 'naira':
                return '₦';
            default:
                return '';
        }
    }
}
