import { User, DateTimeHelper } from "../../../shared";
import { Currency } from "./currency.model";
import { Condition } from "./condition";
import { Category } from "./category";

export class Asset {
    constructor(public id?: string,
        public item?: string,
        public serialNo?: string,
        public purchasedDate?: string,
        public purchasedBy?: User,
        public assignee?: User,
        public assignedDate?: string,
        public condition?: Condition,
        public description?: string,
        public manufacturer?: string,
        public model?: string,
        public serviceTag?: string,
        public serviceCode?: string,
        public category?: Category,
        public cost?: number,
        public currency?: Currency,
        public lastAssignee?: User,
        public lastAssignedDate?: string,
        public markedBadBy?: User,
        public markedBadOn?: string,
        public enteredBy?: User) {
            this.purchasedBy = new User();
            this.assignee = new User();
            this.lastAssignee = new User();
            this.markedBadBy = new User();
            this.enteredBy = new User();
        }

        fromDto(dto: any): void {
            this.id = dto.id;
            this.item = dto.item;
            this.serialNo = dto.serialNo;
            this.purchasedBy = dto.purchasedBy || new User();
            this.purchasedDate = dto.purchasedDate && DateTimeHelper.format(dto.purchasedDate, 'yyyy-MM-dd');;
            this.assignee = dto.assignee || new User();
            this.assignedDate = dto.assignedDate && DateTimeHelper.format(dto.assignedDate, 'yyyy-MM-dd');
            this.condition = dto.condition;
            this.description = dto.description;
            this.manufacturer = dto.manufacturer;
            this.model = dto.model;
            this.serviceTag = dto.serviceTag;
            this.serviceCode = dto.serviceCode;
            this.category = dto.category;
            this.cost = dto.cost;
            this.currency = dto.currency;
            this.lastAssignee = dto.lastAssignee || new User();
            this.lastAssignedDate = dto.lastAssignedDate && DateTimeHelper.format(dto.lastAssignedDate, 'yyyy-MM-dd');
            this.markedBadBy = dto.markedBadBy || new User();
            this.markedBadOn = dto.markedBadOn && DateTimeHelper.format(dto.markedBadOn, 'yyyy-MM-dd');
            this.enteredBy = dto.enteredBy || new User();
        }

        toDto(): object {
            return {
                id: this.id,
                item: this.item,
                serialNo: this.serialNo,
                purchasedById: this.purchasedBy.id,
                purchasedDate: this.purchasedDate,
                assigneeId: this.assignee.id,
                assignedDate: this.assignedDate,
                condition: this.condition,
                description: this.description,
                manufacturer: this.manufacturer,
                model: this.model,
                serviceTag: this.serviceTag,
                serviceCode: this.serviceCode,
                category: this.category,
                cost: this.cost,
                currency: this.currency
            };
        }
}
