import { Expense } from './expense.model';
import { User } from '../../../shared';
import { Currency } from './currency.model';

describe('Expense', () => {
    let _expense: Expense;
    let _incurrer: User;

    const _testDate1 = new Date(2017,10, 20, 12, 10, 30);
    const _testDate2 = new Date(2017, 12, 16, 12, 12, 30);

    const _expenseId1 = 'f7d77fec-25f3-419d-bc74-833135afd439';
    const _expenseId = 'ec04dbdf-40b2-43fc-b7ae-273361831367';
    const _userId = '4d109539-e0d7-4d96-bcc5-d6dadb1ca0aa';

    beforeEach(() => {
        _incurrer = new User(_userId, 'Helen', 'Blake');

        _expense = new Expense(
            _incurrer,
            Currency.naira,
            _expenseId,
            _testDate1,
            4000,
            'for_fuel',
            'fuel_got_finished',
            _testDate2,
            _testDate2
        );
    });

    it('can be instantiated', () => {
        expect(_expense).toBeTruthy();
        expect(_expense.date).toEqual(_testDate1);
        expect(_expense.reimbursed).toEqual(_testDate2);
        expect(_expense.currency).toEqual(Currency.naira);
        expect(_expense.amount).toEqual(4000);
        expect(_expense.description).toEqual('for_fuel');
        expect(_expense.reason).toEqual('fuel_got_finished');
        expect(_expense.id).toEqual(_expenseId);
    });

    it('should set the corresponding properties when fromDto is called', () => {
        const dtoUser = { firstName: 'Dayo', lastName: 'D1', id: 'user_id' };

        const date1 = new Date('01-01-2018 07:00:00');
        const date2 = new Date('01-02-2018 09:00:00')

        const expenseDto = {
            incurredBy: dtoUser,
            currency: Currency.dollar, id: _expenseId1, date: date1, amount: 3000,
            description: 'new sofa', reason: 'for our conference room',
            reimbursed: date2, created: date2
        };

        _expense.fromDto(expenseDto);

        expect(_expense.date).toEqual(expenseDto.date);
        expect(_expense.reimbursed).toEqual(expenseDto.reimbursed);
        expect(_expense.currency).toEqual(expenseDto.currency);
        expect(_expense.amount).toEqual(expenseDto.amount);
        expect(_expense.description).toEqual(expenseDto.description);
        expect(_expense.reason).toEqual(expenseDto.reason);
        expect(_expense.id).toEqual(expenseDto.id);

        expect(_expense.incurredBy.firstName).toEqual(dtoUser.firstName);
        expect(_expense.incurredBy.lastName).toEqual(dtoUser.lastName);
    });

    it('returns an object with value of all current properties when toDto is called', () => {
        const expenseDto = _expense.toDto();

        expect(expenseDto.date).toEqual(_expense.date);
        expect(expenseDto.incurredById).toEqual(_incurrer.id);
        expect(expenseDto.reimbursed).toEqual(_expense.reimbursed);
        expect(expenseDto.amount).toEqual(_expense.amount);
        expect(expenseDto.description).toEqual(_expense.description);
        expect(expenseDto.reason).toEqual(_expense.reason);
        expect(expenseDto.id).toEqual(_expense.id);
    });

    it('is valid when all required values are set', () => {
        expect(_expense.isValid(false)).toBeTruthy();
    });

    it('returns isValid as false if loggedInUser is not a manager; currency, date, description, amount and incurredBy are not undefined', () => {
        const isManager = false;
        const expense = new Expense();
        const result = expense.isValid(isManager);

        expense.date = undefined;

        expect(result).toEqual(false);
    });

    it('returns isValid as true if isManager is false and properties are set', () => {
        const isManager = false;
        const result = _expense.isValid(isManager);

        expect(result).toEqual(true);
    });

    it('returns isValid as true if isManager is true and properties are set', () => {
        const isManager = true;
        const result = _expense.isValid(isManager);

        expect(result).toEqual(true);
    });

    it('should return true when canDelete is called and the reimbursed is not set', () => {
        _expense.reimbursed = undefined;
        
        expect(_expense.canDelete).toEqual(true);
    });

    it('should return true when canDelete is called and the reimbursed is set and is not within 24Hr and reimbursed is not set', () => {
        _expense.reimbursed = new Date('01-02-2017 09:00:00');
        _expense.modified = new Date();
        
        expect(_expense.canDelete).toEqual(true);
    });
    
    it('should return false when canDelete is called and the reimbursed is set and is not within 24Hr and reimbursed set', () => {
        _expense.reimbursed = new Date('01-02-2017 09:00:00');
        _expense.modified = new Date('01-02-2016 09:00:00');
        
        expect(_expense.canDelete).toEqual(false);
    });
    
    it('should return true when canDelete is called and the reimbursed is set and within 24Hrs', () => {
        _expense.reimbursed = new Date();
        
        expect(_expense.canDelete).toEqual(true);
    });
});
