import { Currency } from '../models/currency.model';
import * as d3 from 'd3';
import * as _ from 'lodash'
import { Asset } from './asset.model';
import { Revenue } from './revenue.model';
import { Expense } from './expense.model';
import { Category } from './category';

export class ChartData {
    public static extractExpensesPieChartData(expenses: Expense[], rate: number): any[][] {
      const twelveMonthsData = this.lastTwelveMonthsData(expenses, 'date');
      const reimbursed = this.sum(twelveMonthsData.filter(x => x.reimbursed), rate);
      const total = this.sum(twelveMonthsData, rate);
      const nonReimbursed = total - reimbursed;
      
      return [['nonReimbursed', nonReimbursed], ['reimbursed', reimbursed]];
    }

    public static extractRevenuesPieChartData(revenues: Revenue[], rate: number): any[][] {
      const twelveMonthsDataRevenueExpense = this.lastTwelveMonthsData(revenues, 'paymentDate');
      const mainRevenues = twelveMonthsDataRevenueExpense.filter(x => x.amount > 0);
      const expenses = twelveMonthsDataRevenueExpense.filter(x => x.amount < 0);
      const expenseSum = -1 * this.sum(expenses, rate);
      const revenueSum = this.sum(mainRevenues, rate);

      return [['Expenses', expenseSum], ['Revenues', revenueSum]];
    }

    public static extractAssetsPieChartData(assets: Asset[]): any[][] {
      const assignedItems = assets.filter(asset => asset.assignee.id && asset.assignedDate);
      const unAssignedItems = _.differenceBy(assets, assignedItems, x => x.id);

      return [['Assigned Items', assignedItems.length], ['Unassigned Items', unAssignedItems.length]];
    }

    public static extractExpensesBarChartData(expenses: Expense[], rate: number): any[][] {
      const months = { 0: 'JA', 1: 'F', 2: 'MR', 3: 'AP', 4: 'MY',5: 'JN', 6: 'Jl', 7: 'AU', 8: 'S', 9: 'O', 10: 'N', 11: 'D'};  
      const startMonth = new Date();
      const data: any[][] = [];
      let monthIndex = 0;

      while (monthIndex < 12) {
          const currentmonth = new Date();
          let currentMonthIndex = startMonth.getMonth() - monthIndex;

          currentmonth.setMonth(currentMonthIndex);

          if (currentMonthIndex >= 0 ) {
              currentmonth.setFullYear(startMonth.getFullYear());
          } else {
              currentmonth.setFullYear(startMonth.getFullYear() - 1);
          }

          let currentMonthExpenses = expenses.filter(x => x.date.getMonth() === currentmonth.getMonth()
                                  && x.date.getFullYear() === currentmonth.getFullYear());

          if (currentMonthIndex < 0) {
              currentMonthIndex += 12;
          }

          data.push([months[currentMonthIndex], this.sum(currentMonthExpenses, rate)]);
          monthIndex++;
      }

      return _.reverse(data);
    }

    public static extractRevenuesBarChartData(revenues: Revenue[], rate: number): any[][] {
      const data: any[][] = [];
      
      if (revenues.length > 0) {
          const mainRevenues = revenues.filter( x => x.amount > 0);
          const clients = _.uniq(mainRevenues.map(x => x.client.name));
          
          clients.forEach(client => {
            const totalrevenue  = mainRevenues.filter(x => x.client.name === client);
          
            data.push([client, this.sum(totalrevenue, rate)]);
          });
      }

      return data.sort(this.comparator);
    }

    public static extractAssetsBarChartData(assets: Asset[]): any[][] {
      const data = [];
      
      Category.toIdValuePair().forEach(category => {
        const assignedCategoriesCount = assets.filter(asset => asset.assignee.id && asset.assignedDate && asset.category == category.value).length;
        const unAssignedCategoriesCount = assets.filter(asset => !asset.assignee.id && !asset.assignedDate && asset.category == category.value).length;

        data.push({ group: 'Assigned', count: assignedCategoriesCount, category: category.value.slice(0,2) });
        data.push({ group: 'UnAssigned', count: unAssignedCategoriesCount, category: category.value.slice(0,2) })
      });

      return data;
    }

    private static comparator(a: any[], b: any[]): number {
      if (a[0] < b[0]) return -1;
      if (a[0] > b[0]) return 1;
      return 0;
    }
    
    private static lastTwelveMonthsData(data: any[], dateFilterProperty: string) {
      const maxMonth = new Date(), minMonth = new Date();
      const monthDifference = maxMonth.getMonth() - 11;
      
      minMonth.setMonth(monthDifference);

      if (monthDifference < 0) {
        minMonth.setFullYear(maxMonth.getFullYear() - 1);
      }

      return data.filter(x => (x[dateFilterProperty].getMonth() <= maxMonth.getMonth() &&
                                                      x[dateFilterProperty].getFullYear() === maxMonth.getFullYear()) ||
                                                     (x[dateFilterProperty].getMonth() >= minMonth.getMonth() &&
                                                      x[dateFilterProperty].getFullYear() === minMonth.getFullYear()));
    }

    private static sum(data: any[], rate: number): number {
      return data.reduce<number>((prevValue, currentValue) => {
        return prevValue + (currentValue.currency === Currency.dollar ? currentValue.amount * rate : currentValue.amount);
        }, 0);
    }
}
