import { Currency } from './currency.model';
import { User, DateTimeHelper } from '../../../shared';
import { Client } from '../../../projectManagement/common/models/client.model';

export class Revenue {
    get enteredBy() {
        return this.createdBy.fullName || '';
    };

    constructor(
        public id?: string,
        public paymentDate?: Date,
        public currency?: Currency,
        public amount?: number,
        public client?: Client,
        public description?: string,
        public modified?: Date,
        public createdBy?: User,
        public created?: Date,
        public selected?: boolean
    ) { }

    public get isValid(): boolean {
        return !!this.paymentDate && !!this.currency && !!this.client && !!this.description && !!this.amount && (this.amount > 0);
    }

    public get canDelete(): boolean {
        if (!this.canEdit) {
            return false;
        } else if (this.modified) {
            const today = new Date();

            return (today.getTime() - this.modified.getTime()) <= (DateTimeHelper.microsecondsPerDay * 30);
        }

        return true;
    }

    public get canEdit(): boolean {
        if (this.client && this.amount >= 0) {
            const today = new Date();

            return (today.getTime() - this.created.getTime()) <= (DateTimeHelper.microsecondsPerDay * 30);
        }
        return false;
    }

    fromDto(dto: any) {
        this.id = dto.id;
        this.currency = dto.currency === 'naira' ? Currency.naira : Currency.dollar;
        this.amount = dto.amount;
        this.description = dto.description;
        this.modified = new Date(dto.modified);
        this.paymentDate = new Date(dto.paymentDate);
        this.created = new Date(dto.created);

        if (dto.createdBy) {
            this.createdBy = dto.createdBy;
        }

        if (dto.client) {
            this.client = new Client(dto.client.id, dto.client.name);
        }
    }

    toDto(): any {
        return {
            id: this.id,
            currency: this.currency === Currency.naira ? 'naira' : 'dollar',
            amount: this.amount,
            description: this.description,
            clientId: this.client.id,
            paymentDate: this.paymentDate
        };
    }
}
