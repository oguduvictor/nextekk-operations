import { IdValuePair } from "../../../shared";

export class Condition {
    static unknown = 'unknown';
    static 'new' = 'new';
    static good = 'good';
    static fair = 'fair';
    static bad = 'bad';

    public static toIdValuePair = (): IdValuePair[] => {
        const list = [];
        for (let prop in Condition) {
            if (!(Condition[prop] instanceof Function)) list.push(new IdValuePair(Condition[prop], prop));
        }

        return list;
    }
}