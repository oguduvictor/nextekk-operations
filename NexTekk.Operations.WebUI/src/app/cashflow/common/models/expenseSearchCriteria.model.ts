import { URLSearchParams  } from '@angular/http';

export class ExpenseSearchCriteria {
    constructor(
        public expensesIds?: string[],
        public userId?: string,
        public incurredById?: string,
        public currency?: string,
        public date?: Date,
        public beginDateTime?: string,
        public endDateTime?: string,
        public reimbursed?: boolean,
        public descriptionContains?: string,
        public pageNumber?: number
    ) {}

    public toParams(): URLSearchParams {
        let params = new URLSearchParams();

        if (this.expensesIds) {
            this.expensesIds.forEach(id => {
                params.append('expensesIds', id);
            });
        }

        if (this.date) {
            params.set('date.beginDateTime', this.date['beginDateTime'] && this.date['beginDateTime']);
            params.set('date.endDateTime', this.date['endDateTime'] && this.date['endDateTime']);
        }

        if (this.pageNumber) {
            params.set('pageNumber', this.pageNumber.toString());
        }

        params.set('currentUserId', this.userId);
        params.set('incurredById', this.incurredById);
        params.set('currency', this.currency);
        params.set('description', this.descriptionContains);

        if (this.reimbursed !== undefined && this.reimbursed !== null) {
            params.set('reimbursed', this.reimbursed.toString());
        }

        return params;
    }

    get hasAnyParameters(): boolean {
        return !!(this.expensesIds || this.userId || this.incurredById ||
                  this.currency || this.date || this.reimbursed || this.descriptionContains
                 );
    }

    get hasDate(): boolean {
        return !!(this.beginDateTime || this.endDateTime)
    }
}
