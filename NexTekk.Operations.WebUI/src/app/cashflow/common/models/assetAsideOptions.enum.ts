export enum AssetAsideOptions {
    create,
    view,
    userEdit,
    managerEdit
}
