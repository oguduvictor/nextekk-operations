import { Component, ViewChild, EventEmitter, Output, Input } from "@angular/core";
import { Asset } from "../../models/asset.model";
import { IdValuePair, User, SpinnerService, NotificationService, AsideComponent } from "../../../../shared";
import { Condition } from "../../models/condition";
import { Category } from "../../models/category";
import { Currency } from "../../models/currency.model";
import { AssetDataService } from "../../services/assetDataService";
import { NgForm } from "@angular/forms";
import { AssetAsideOptions } from "../../models/assetAsideOptions.enum";

@Component({
    selector: 'app-asset-aside',
    templateUrl: './assetAside.component.html',
    styleUrls: ['./assetAside.component.css']
})

export class AssetAsideComponent {
    asset: Asset;
    conditions: IdValuePair[];
    categories: IdValuePair[];
    currencies: IdValuePair[];
    viewOnly: boolean;
    readonly: boolean;
    canEditCondition: boolean;
    title: string;

    get userCanEdit(): boolean {
        return this.loggedInUser.isManager() || this.loggedInUser.id == this.asset.assignee.id;
    }
    
    @Output()
    OnAssetSaved: EventEmitter<Asset>;

    @Input()
    users: User[];

    @Input()
    loggedInUser: User;

    @ViewChild(AsideComponent)
    private asideComponent: AsideComponent;

    @ViewChild('frm')
    form: NgForm;

    get isValid(): boolean {
        if (!this.form) {
            return true;
        }

        return this.form.valid;
    }

    constructor(
        private _dataService: AssetDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService
    ){
        this.conditions = Condition.toIdValuePair();
        this.categories = Category.toIdValuePair();
        this.currencies = Currency.toIdValuePair();
        this.OnAssetSaved = new EventEmitter<Asset>();
    }

    open(asset: Asset, options: AssetAsideOptions) {
        this.asset = asset;
        switch(options){
            case AssetAsideOptions.create:
                this.readonly = false;
                this.viewOnly = false;
                this.canEditCondition = true;
                this.title = 'New Asset';
                break;
            case AssetAsideOptions.userEdit:
                this.readonly = true;
                this.viewOnly = false;
                this.canEditCondition = true;
                this.title = 'Edit Asset';                
                break;
            case AssetAsideOptions.managerEdit:
                this.readonly = false;
                this.viewOnly = false;
                this.canEditCondition = true;
                this.title = 'Edit Asset';                
                break;
            case AssetAsideOptions.view:
                this.readonly = false;
                this.viewOnly = true;
                this.canEditCondition = false;
                this.title = 'Asset Detail';                
                break;
            default:
                throw new Error('Aside option not recognized');
        }

        this.asideComponent.show();
    }

    save() {
        if (this.form.pristine) {
            this.asideComponent.close();
            return ;
        }

        this._spinnerService.show();

        this._dataService.save(this.asset).subscribe(data => {
            this.OnAssetSaved.emit(data || this.asset);
            this._spinnerService.hide();
            this._notificationService.showSuccess("Asset was saved", "Success");
            this.asideComponent.close();
        },
        err => {
            this._spinnerService.hide();
            this._notificationService.showError(err);
        });
    }

    edit() {
        this.viewOnly = false;
        if (this.loggedInUser.isManager()) {
            this.open(this.asset, AssetAsideOptions.managerEdit);
        }
        else if (this.loggedInUser.id == this.asset.assignee.id) {
            this.open(this.asset, AssetAsideOptions.userEdit)
        }
        else {
            throw new Error('Current loggedin Employee is not authorized to edit this asset');
        }
    }

    cancel() {
        this.asideComponent.close();
    }

    private usersToIdValuePairs = () => this.users.map(user => new IdValuePair(user.id, user.fullName));    
}