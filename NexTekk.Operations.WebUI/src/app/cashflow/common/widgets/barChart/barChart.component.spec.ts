import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BarChartComponent } from './barChart.component';
import { Expense } from '../../models/expense.model';
import * as d3 from 'd3';

describe('BarChartComponent', () => {
  let barChartComponent: BarChartComponent;
  let fixture: ComponentFixture<BarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartComponent);
    barChartComponent = <BarChartComponent>fixture.debugElement.componentInstance;
    fixture.detectChanges();
    let expense = new Expense();
    expense.date = new Date('2017-11-22T11:08:55.761Z');
    expense.amount = 7000;
    (<any>barChartComponent).expenses = [expense];
  });

  it('should create', () => {
    expect(barChartComponent).toBeTruthy();
  });

  // it ('should extract data for chart from expenses list', () => {
  //   // barChartComponent.extractChartData();
  //   let data = (<any>barChartComponent).data;

  //   expect(data.length).toBe(12);
  //   expect(data[10][1]).toEqual(7000);
  // })

  // it ('should draw chartArea after initializtion', () => {
  //   // barChartComponent.ngOnInit();
  //   expect((<any>barChartComponent).chartArea).not.toBeUndefined()
  // })

  // it('should render bars based on available data', () => {
  //   // barChartComponent.ngOnInit();
  //   let chart = (<any>barChartComponent).chartArea;
  //   let bars = chart.selectAll('rect');
  // })

});
