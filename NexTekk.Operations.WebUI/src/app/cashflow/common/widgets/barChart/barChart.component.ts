import { Component, ViewEncapsulation, ElementRef, ViewChild, Input, OnChanges } from '@angular/core';
import { ChartData } from '../../models/chartData.model';
import * as d3 from 'd3';
import * as _ from 'lodash';
import { ChartConfig } from '../../helpers/chart.config';

@Component({
    selector: 'app-bar-chart',
    templateUrl: './barChart.component.html',
    styleUrls: ['./barChart.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class BarChartComponent implements OnChanges {
    @ViewChild('barchart')
    private chartContainer: ElementRef;
    
    @Input()
    private data: any[];

    @Input()
    private rateData: number;

    @Input()
    private title: string;

    @Input()
    private chartConfig: string;

    private barChartData: any[][];
    private xAxis: any;
    private yAxis: any;

    ngOnChanges(): void {
        this.extractChartData();
        
        if (this.chartConfig == ChartConfig.assets) {
            this.renderChart();
            return;
        }

        if (!this.xAxis && this.barChartData.length > 0){
            this.createChart();
        }
        else if(this.barChartData.length > 0) {
            this.updateResetChart();
        }
    }

    private createChart() {
        const element = this.chartContainer.nativeElement;
        const margin: any = { top: 20, bottom: 20, left: 40, right: 0 };
        const width = this.barChartData.length == 1 ? 50 : element.offsetWidth - margin.left - margin.right;
        const height = element.offsetHeight - margin.top - margin.bottom;

        let svg = d3.select(element)
            .append('svg')
            .attr('width', element.offsetWidth)
            .attr('height', element.offsetHeight);
        
        svg.append('g')
            .attr('transform', `translate(${(width / 4)}, ${15})`)
            .append('text')
            .text(this.title)
            .style('color', 'black');

        const chartArea = svg.append('g')
                            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        const xDomain = this.barChartData.map(x => x[0]);
        const yDomain = [0, d3.max(this.barChartData, x => x[1])];
        const colorlist  = ['#4dd0e1', '#ffa726', '#ffee58', '#69f0ae', '#9e9d24', '#40c4ff', '#ff5252'];

        const xScale = d3.scaleBand().domain(xDomain).rangeRound([0, width]);
        const yScale = d3.scaleLinear().domain(yDomain).range([height, 0]);
        const colors: any = d3.scaleOrdinal().range(colorlist);

        this.xAxis = svg.append('g')
                        .attr('class', 'axis axis-x')
                        .attr('transform', `translate(${margin.left}, ${margin.top + height})`)
                        .call(d3.axisBottom(xScale)
                        .tickFormat((d: any, i) => {
                            if (d[0].length > 1) {
                               return d[0][0];
                            }
                            return d.slice(0, 4) + '.';
                        }));

        this.yAxis =  svg.append('g')
                         .attr('class', 'axis axis-y')
                         .attr('transform', `translate(${margin.left}, ${margin.top})`)
                         .call(d3.axisLeft(yScale)
                         .ticks(5, '.1s'));

        chartArea
            .selectAll('rect')
            .data(this.barChartData)
            .enter()
            .append('rect')
            .attr('x', d => xScale(d[0]))
            .attr('width', d => xScale.bandwidth())
            .attr('y', d => yScale(0))
            .attr('height', 0)
            .transition()
            .delay((d, i) => i * 100)
            .attr('y', d => yScale(d[1]))
            .attr('height', d => height - yScale(d[1] ))
            .style('fill', colors);
    }

    private updateResetChart() {
        d3.select('div .bar-chart').selectAll('*').remove();
        
        this.createChart();
    }

    private extractChartData() {
        switch(this.chartConfig) {
            case ChartConfig.expenses:
                this.barChartData = ChartData.extractExpensesBarChartData(this.data, this.rateData);
                break;
            case ChartConfig.revenues:
                this.barChartData = ChartData.extractRevenuesBarChartData(this.data, this.rateData);
                break;
            case ChartConfig.assets:
                this.barChartData = ChartData.extractAssetsBarChartData(this.data);
                break;
        }
    }

    private renderChart() {
        const data: any[] = this.barChartData;
        const element = this.chartContainer.nativeElement;
        let margin: any = { top: 20, bottom: 20, left: 30, right: 20 };
        let width = this.barChartData.length == 1 ? 50 : element.offsetWidth - margin.left - margin.right;
        let height = element.offsetHeight - margin.top - margin.bottom;
        
        d3.select('div .bar-chart').selectAll('*').remove();
        
        let svg = d3.select(element)
            .append('svg')
            .attr('width', element.offsetWidth)
            .attr('height', element.offsetHeight);;
        
        let color = d3.scaleOrdinal(d3.schemeCategory10);

        let x = d3.scaleBand().rangeRound([0, width]).padding(0.1);
        let y = d3.scaleLinear().rangeRound([height, 0]);
        
        svg.append('text')
        .attr('transform', `translate(${(width / 9)}, ${10})`)
        .text(this.title)
        .style('color', 'black');

        let g = svg.append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
            
        let ymaxdomain = d3.max(data, d => d.count);
        
        x.domain(data.map(d => d.category));
        y.domain([0, ymaxdomain]);
        
        let x1 = d3.scaleBand()
            .rangeRound([0, x.bandwidth()])
            .padding(0.05)
            .domain(data.map(d => d.group));
        
        color.domain(data.map(d => d.group));
        
        let groups = g.selectAll(null)
            .data(data)
            .enter()
            .append('g')
            .attr('transform', d => 'translate(' + x(d.category) + ',0)')
        
        let bars = groups.selectAll(null)
            .data(d => [d])
            .enter()
            .append('rect')
            .attr('x', (d, i) => x1(d.group))
            .attr('y', (d) =>  y(d.count))
            .attr('width', x1.bandwidth())
            .attr('height', d => height - y(d.count))
            .attr('fill', d => color(d.group))
        
        g.append('g')
            .attr('class', 'axis')
            .attr('transform', 'translate(0,' + height + ')')
            .call(d3.axisBottom(x));
        
        g.append('g')
            .attr('class', 'axis')
            .call(d3.axisLeft(y).ticks(null, 's'))
            .append('text')
            .attr('x', 2)
            .attr('y', y(y.ticks().pop()) + 0.5)
            .attr('dy', '0.32em')
            .attr('fill', '#000')
            .attr('font-weight', 'bold')
            .attr('text-anchor', 'start');
    }
}
