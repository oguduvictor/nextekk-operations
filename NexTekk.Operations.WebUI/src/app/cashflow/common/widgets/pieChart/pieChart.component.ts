import { Component, OnInit, ViewEncapsulation, Input, ElementRef, ViewChild, OnChanges } from '@angular/core';
import { ChartData } from '../../models/chartData.model';
import * as d3 from 'd3';
import { ChartConfig } from '../../helpers/chart.config';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pieChart.component.html',
  styleUrls: ['./pieChart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PieChartComponent implements OnInit, OnChanges {
  @Input()
  private data: any[];

  @Input()
  private rateData: number;

  @Input()
  private title: string;
  
  @Input()
  private chartConfig: string;

  @ViewChild('pieChart')
  private chartContainer: ElementRef;

  private pieChartdata: any[] = [];
  private margin = { top: 20, left: 20, bottom : 20, right : 20 }
  private arc: any;
  private arcLabel: any;
  private arcs: any;

  constructor() { }

  ngOnInit() {
    this.fetchData();
    this.createChart();
  }

  ngOnChanges() {
    if (this.arc) {
      this.fetchData();
      this.updateChart();
    }
  }

  private createChart() {
      let element =  this.chartContainer.nativeElement;
      const width = element.offsetWidth - this.margin.left - this.margin.right;
      const height = element.offsetHeight - this.margin.top - this.margin.bottom;
      const color = d3.scaleOrdinal().range(['#1de9b6', '#ffcc80']);

      const svg = d3.select(element)
                  .append('svg')
                  .attr('width', element.offsetWidth)
                  .attr('height', element.offsetHeight);

      const chartArea = svg.append('g')
                         .attr('transform', `translate(${element.offsetWidth / 2}, ${element.offsetHeight / 2 + 10})`);

      // Chart title is set here
      svg.append('g')
          .attr('transform', `translate(${(width / 7)}, ${15})`)
          .append('text')
          .text(this.title)
          .style('color', 'black');

      const radius = Math.min(width, height) / 2 + 5;

      const pie = d3.pie()
                  .value((d) => {
                    return d[1];
                  })(this.pieChartdata);

      this.arc = d3.arc()
                  .innerRadius(0)
                  .outerRadius(radius);

      this.arcLabel = d3.arc()
                       .innerRadius(radius - 40)
                       .outerRadius(radius - 40);

      this.arcs = chartArea.selectAll('arc')
                          .data(pie)
                          .enter()
                          .append('g')
                          .attr('class', 'arc');

      this.arcs.append('path')
          .attr('fill', (d, i) => {
              return color(i)
          })
          .attr('d', this.arc);

      this.arcs.append('text')
          .attr('transform', (d) => `translate(${this.arcLabel.centroid(d)})`)
          .text((d) => {
            if (d.data[1] < 1) {
                return '';
            } else {
              return d.data[0]
            }
          })
          .style('fill', 'black')

  }

  private updateChart() {
      let element =  this.chartContainer.nativeElement;
      let pie = d3.pie()
      .value((d) => d[1])(this.pieChartdata);

      this.arcs = d3.select(element)
      .selectAll('path')
      .data(pie)

      this.arcs.attr('d', this.arc);

      d3.selectAll('text').data(pie)
                        .attr('transform', (d) => 'translate(' + this.arcLabel.centroid(d) + ')')
                        .text((d) => {
                          if (d.data[1] < 1) {
                              return '';
                          } else {
                            return d.data[0]
                          }
                        });
  }

  private fetchData() {
    switch(this.chartConfig) {
      case ChartConfig.expenses:
          this.pieChartdata = ChartData.extractExpensesPieChartData(this.data, this.rateData);
          break;
      case ChartConfig.revenues:
          this.pieChartdata = ChartData.extractRevenuesPieChartData(this.data, this.rateData);
          break;
      case ChartConfig.assets:
          this.pieChartdata = ChartData.extractAssetsPieChartData(this.data);
          break;
      }
  }
}
