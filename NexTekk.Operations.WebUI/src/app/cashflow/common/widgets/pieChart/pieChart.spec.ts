import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PieChartComponent } from './pieChart.component';
import { Expense } from '../../models/expense.model';

describe('PieChartComponent', () => {
  let pieChartComponent: PieChartComponent;
  let fixture: ComponentFixture<PieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PieChartComponent);
    pieChartComponent = <PieChartComponent>fixture.debugElement.componentInstance;
    fixture.detectChanges();
    let expense = new Expense();
    expense.date = new Date('2017-11-22T11:08:55.761Z');
    expense.amount = 7000;
    (<any>pieChartComponent).expenses = [expense];
  });

  it('should create', () => {
    expect(pieChartComponent).toBeTruthy();
  });

  // it('should extract data for chart from expense list', () => {
  //   let expense = new Expense()
  //   expense.reimbursed = new Date('2017-11-22T11:08:55.761Z');
  //   expense.date = new Date('2017-11-22T11:08:55.761Z');
  //   expense.amount = 5000;
  //   (<any>pieChartComponent).expenses.push(expense);

  //   let data = (<any>pieChartComponent).data;

  //   expect(data.length).toBe(2);
  //   expect(data[0][0]).toBe('Non-Reimbursed');
  //   expect(data[0][1]).toBe(7000);
  //   expect(data[1][0]).toBe('Reimbursed');
  //   expect(data[1][1]).toBe(5000);
  // });

});
