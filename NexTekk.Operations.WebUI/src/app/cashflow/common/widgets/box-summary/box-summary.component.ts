import { Component, Input, OnChanges } from '@angular/core';
import {Expense} from '../../models/expense.model';
import { Currency } from '../../models/currency.model';
import { Revenue } from '../../models/revenue.model';
import { Asset } from '../../models/asset.model';
import { Condition } from '../../models/condition';

@Component({
  selector: 'app-summary-boxes',
  templateUrl: './box-summary.component.html',
  styleUrls: ['./box-summary.component.css']
})
export class BoxSummaryComponent implements OnChanges {
  totalsumExpense: number;
  totalsumRevenue: number;
  totalExpense: number;
  totalReimbursed: number;
  totalNotReimbursed: number;
  totalAssetsSum: number;
  totalGoodAssets: number;
  totalUnassignedAssets: number;
  isDollar: boolean;
  
  @Input()
  expenses: Expense[];

  @Input()
  rateData: number;

  @Input()
  isRevenue: boolean;

  @Input()
  isExpense: boolean;

  @Input()
  isAsset: boolean;

  @Input()
  revenues: Revenue[];

  @Input()
  assets: Asset[];

  public get currentCurrency(): string {
    return this.isDollar ? Currency.dollar : Currency.naira;
  }

  constructor() {
    this.isDollar = true;
  }

  ngOnChanges(): void { // To-Do Refactor this
    if (!!this.revenues) {
      this.calculateRevenue(this.currentCurrency);
    }
    if (!!this.expenses) {
      this.calculateExpense(this.currentCurrency);
    }

    if (!!this.assets) {
      this.calculateAsset(this.currentCurrency);
    }
  }

  toggleCurrency(): void {
    this.isDollar = !this.isDollar;
    if (this.isRevenue) {
      this.calculateRevenue(this.currentCurrency);
    }

    if (this.isExpense) {
      this.calculateExpense(this.currentCurrency);
    }

    if (this.isAsset) {
      this.calculateAsset(this.currentCurrency);
    }
  }

  private calculateRevenue(currency: Currency): void {
    this.totalsumRevenue = 0;
    this.totalsumExpense = 0;

    const actualRevenue = this.revenues.filter(x => x.amount > 0);
    const actualExpense = this.revenues.filter(x => x.amount < 0);

    this.totalsumExpense = -1 * (this.sum(actualExpense, currency));
    this.totalsumRevenue = this.sum(actualRevenue, currency);
  }

  private calculateExpense(currency: Currency): void {
    this.totalExpense = this.sum(this.expenses, currency);
    let reimburedExpenses = this.expenses.filter(x => x.reimbursed);
    this.totalReimbursed = this.sum(reimburedExpenses, currency);
    this.totalNotReimbursed = this.totalExpense - this.totalReimbursed;
  }

  private calculateAsset(currency: Currency) {
    this.totalAssetsSum = this.assets.reduce<number>((prevValue, currValue) => prevValue + (Currency.getCurrencySymbol(`${currValue.currency}`) == currency ? currValue.cost : this.convert(currValue)), 0);
    this.totalGoodAssets = this.assets.filter(x => x.condition == Condition.good).length;
    this.totalUnassignedAssets = this.assets.filter(x => !x.assignee.id && !x.assignedDate).length;
  }

  private convert(data: any): number {
    if (this.rateData <= 0) {
      return 0;
    }

    if (data.currency === Currency.naira || data.currency === 'naira') {
      return (data.amount || data.cost) / this.rateData;
    }

    return (data.amount || data.cost) * this.rateData;
  }

  private sum(data: any[], currency: Currency): number {
    return data.reduce<number>
    ((prevValue, currentValue) => {
      return prevValue + ((currentValue.currency === currency) ?
                          (currentValue.amount || currentValue.cost) :
                          this.convert(currentValue));
    }, 0);
  }
}