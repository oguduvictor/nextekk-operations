import {Component} from '@angular/core'
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Expense} from '../../models/expense.model';
import { Currency } from '../../models/currency.model';
import { User } from '../../../../shared';

import { BoxSummaryComponent } from './box-summary.component';

describe('ExpenseSummaryComponent', () => {
  let component: BoxSummaryComponent;
  let fixture: ComponentFixture<BoxSummaryComponent>;

  let _rateData = 10;
  let _expenses: Expense[];
  let _incurrer: User;
  let _currencyDollar: Currency;
  let _currencyNaira: Currency;

  let _testDate1 = new Date('2017-10-20T12:10:30.024Z');
  let _testDate2 = new Date('2017-12-16T12:12:30.024Z');

  let _expenseId1 = 'f7d77fec-25f3-419d-bc74-833135afd439';
  let _expenseId = 'ec04dbdf-40b2-43fc-b7ae-273361831367';
  let _userId = '4d109539-e0d7-4d96-bcc5-d6dadb1ca0aa';

  beforeEach(() => {
    _currencyDollar = Currency.dollar;
    _currencyNaira = Currency.naira;

    _incurrer = new User(_userId, 'Bayo', 'Nextekk');

    _expenses = [
      new Expense( _incurrer, _currencyDollar,
      _expenseId, _testDate1, 5000, 'for_water', 'drinking_for_employee',
       _testDate2, _testDate2),
      new Expense( _incurrer, _currencyNaira, _expenseId,
        _testDate1, 4000, 'for_fuel', 'fuel_got_finished', _testDate2, _testDate2)
     ]

    TestBed.configureTestingModule({
      declarations: [ BoxSummaryComponent ]
    });

    fixture = TestBed.createComponent(BoxSummaryComponent);
    component = <BoxSummaryComponent>fixture.debugElement.componentInstance;

    component.totalExpense = 0;
    (<any>component).expenses = _expenses;
    (<any>component).rateData = _rateData;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
/*
  it('should be able to calculate expense', () => {
    spyOn(component, 'sumExpenses');
    // component.calculateExpense(_currencyDollar);
    expect(component.sumExpenses).toHaveBeenCalled();
  });
  it('should convert to zero if rateData is zero or less when expense is in dollars and convert is called', () => {
    (<any>component).rateData = -2;
    expect(component.convertExpense(_expenses[0])).toBe(0);
  });
  it('should convert greater than zero if rateData is greater than zero when expense is in dollars and convert is called', () => {
    (<any>component).rateData = 24;
    expect(component.convertExpense(_expenses[0])).toBeGreaterThan(0);
  });

  it('should convert to zero if rateData is zero or less when expense is in naira and convert is called', () => {
    (<any>component).rateData = -2;
    expect(component.convertExpense(_expenses[1])).toBe(0);
  });
  it('should convert greater than zero if rateData is greater than zero when expense is in naira and convert is called', () => {
    (<any>component).rateData = 24;
    expect(component.convertExpense(_expenses[1])).toBeGreaterThan(0);
  });

  it('should convert to dollars if rateData is greater than zero when sumExpense is called', () => {
    expect(component.sumExpenses(_expenses, _currencyNaira)).toBe(54000)

  });

  it('should convert to naira if rateData is greater than zero when sumExpense is called', () => {
    let sumValue = component.sumExpenses(_expenses, _currencyDollar);
    expect(sumValue).toBe(5400);
  });
  it('should change the value of the dollar as true if it was set as false when toggleCurrency is called', () => {
    (<any>component).isDollar = true;
    component.toggleCurrency();
    expect((<any>component).isDollar).toBe(false);
  })
  it('should call the calculateExpense to dollar when toggleCurrency is called', () => {
    spyOn(component, 'calculateExpense');
    component.toggleCurrency();
    expect(component.calculateExpense).toHaveBeenCalled();
  })
  it('should change the value of the dollar as false if it was set as true when toggleCurrency is called', () => {
    (<any>component).isDollar = false;
    component.toggleCurrency();
    expect((<any>component).isDollar).toBe(true);
  })
*/
});
