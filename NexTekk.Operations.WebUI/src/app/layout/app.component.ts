import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { AuthService, NotificationService, SpinnerService, UserService } from '../shared';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    currentYear = new Date().getFullYear();
    baseSiteUrl = environment.nextekkSite;
    title: string;
    isManager = false;
    isEmployee = false;
    isCollapsed = true;

    constructor(
        private _router: Router,
        private _authService: AuthService,
        private _userService: UserService,
        private _spinnerService: SpinnerService,
        notificationService: NotificationService,
        vcr: ViewContainerRef) {

        notificationService.setRootViewContainerRef(vcr);
    }

    private getTitle(path): string {
        let value = '';

        switch (path) {
            case '/time-entries':
                value = 'My Time Entries';
                break;

            case '/activities':
                value = 'Activities List';
                break;

            case '/pending-approval':
                value = 'Pending Approval';
                break;

            case '/approved':
                value = 'Approved';
                break;

            case '/projects':
                value = 'Projects';
                break;

            case '/clients':
                value = 'Clients';
                break;

            case '/contacts':
                value = 'Contact Persons';
                break;

            case '/vacations':
                value = 'My Vacations';
                break;

            case '/users-vacations':
                value = 'All Employees Vacations';
                break;

            case '/expenses':
                value = 'Expenses';
                break;

            case '/revenue':
                value = 'Revenue';
                break;

            case '/assets':
                value = 'Assets';
                break;

            default:
                value = 'Current Page';
        }

        return value;
    }

    get isAuthenticated(): boolean {
        return this._authService.isAuthenticated;
    }

    get userFirstName(): Observable<string> {
        return this._userService.getCurrentUser().map(x => {
            this.isManager = x.isManager();
            this.isEmployee = x.isEmployee();
            return x.firstName;
        });
    }

    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }

    logOut() {
        this._spinnerService.show();
        this._authService.logOut();
    }

    ngOnInit() {
        this._router.events.subscribe((data: any) => {
                this.title = this.getTitle(data.url);
            });
    }
}
