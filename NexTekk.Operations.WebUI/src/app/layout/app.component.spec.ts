import { ViewContainerRef } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DialogModalComponent } from '../shared/widgets/dialogModal/dialogModal.component';
import {
    SpinnerComponent, AuthService, SpinnerService,
    NotificationService, UserService, DialogService, ModalComponent
} from '../shared';
import { CollapseDirective } from 'ngx-bootstrap';
import { Observable } from 'rxjs/Observable';
import { AppModule } from '../app.module';

describe('AppComponent', () => {
    let _fixture: ComponentFixture<AppComponent>;
    let appComponent: AppComponent;
    let mockAuthService: any = {
        isAuthenticated: false,
        logOut: jasmine.createSpy('logOut')
    };

    let mockUserService: any = {
        getCurrentUser: jasmine.createSpy('getCurrentUser').and.returnValue(Observable.of({
            isManager: function () {
                return true;
            },
            firstName: 'Ayo'
        }))
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes([]), AppModule
            ],
            providers: [
                SpinnerService,
                DialogService,
                NotificationService,
                { provide: AuthService, useValue: mockAuthService },
                { provide: NotificationService, useValue: { setRootViewContainerRef: function () { } } },
                { provide: UserService, useValue: mockUserService },
                { provide: ViewContainerRef, useValue: {} }
            ]
        });

        _fixture = TestBed.createComponent(AppComponent);
        appComponent = <AppComponent>_fixture.debugElement.componentInstance;
    });

    it('can be instantiated succesfully', () => {
        expect(appComponent).toBeTruthy();
    });

    it('can check for authentication', () => {
        let isAuthenticated = appComponent.isAuthenticated;

        (<any>appComponent)._authService = mockAuthService;

        expect(isAuthenticated).toBe(false);
    });

    it('can get user firstName', () => {
        let firstName = appComponent.userFirstName;

        expect(mockUserService.getCurrentUser).toHaveBeenCalled();
    });

    it('can logout user', () => {
        appComponent.logOut();

        expect(mockAuthService.logOut).toHaveBeenCalled();
    });

    it('can toggleCollapse', () => {
        expect(appComponent.isCollapsed).toBe(true);

        appComponent.toggleCollapse();

        expect(appComponent.isCollapsed).toEqual(false);
    });

    it('can get title when url is time /time-entries', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/time-entries'
            })
        };

        appComponent.ngOnInit();

        expect(appComponent.title).toEqual('My Time Entries');
    });

    it('title is "Activities List" when url is "/activities"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/activities'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('Activities List');
    });

    it('title is "Pending Approval" when url is "/pending-approval"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/pending-approval'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('Pending Approval');
    });

    it('title is "Pending Approval" when url is "/approved"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/approved'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('Approved');
    });

    it('title is "Projects" when url is "/projects"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/projects'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('Projects');
    });

    it('title is "Clients" when url is "/clients"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/clients'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('Clients');
    });

    it('title is "Contact Persons" when url is "/contacts"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/contacts'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('Contact Persons');
    });

    it('title is "My Vacations" when url is "/vacations"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/vacations'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('My Vacations');
    });

    it('title is "All Employees Vacations" when url is "/users-vacations"', () => {
        (<any>appComponent)._router = {
            events: Observable.of({
                url: '/users-vacations'
            })
        };

        appComponent.ngOnInit();
        expect(appComponent.title).toEqual('All Employees Vacations');
    });
});
