import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './layout/app.component';
import { AuthComponent, SharedModule } from './shared';
import { TimeTrackingModule } from './timeTracking/timeTracking.module';
import { ProjectModule } from './projectManagement/project.module';
import { VacationModule } from './vacations/vacations.module';
import { CollapseModule } from 'ngx-bootstrap';
import { CashFlowModule } from './cashflow/cashflow.module';

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        TimeTrackingModule,
        ProjectModule,
        VacationModule,
        CollapseModule,
        CashFlowModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'auth', pathMatch: 'full' },
            { path: 'auth', component: AuthComponent },
            { path: '**', redirectTo: 'auth' }
        ]),
        SharedModule.forRoot()
    ],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }]
})
export class AppModule {
}
