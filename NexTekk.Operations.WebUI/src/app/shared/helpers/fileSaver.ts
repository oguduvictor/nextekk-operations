import * as fileSaver from 'file-saver';

export class FileSaver {
    static saveFile(data: Response, name: string = 'file'): void {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        let date = new Date();
        fileSaver.saveAs(blob, `${name}_report-${date.toDateString()}.xlsx`);
    }
}
