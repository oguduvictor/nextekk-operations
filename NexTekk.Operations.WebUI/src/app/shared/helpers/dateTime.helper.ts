﻿import { DatePipe } from '@angular/common';
import * as _ from 'lodash';

export class DateTimeHelper {
    private static datePipe: DatePipe = new DatePipe('en-US');

    public static format(date: any, dateFormat: string): string {
        return this.datePipe.transform(date, dateFormat);
    }

    public static shortDate(date: Date): string {
        return this.format(date, 'M/d/y');
    }

    public static getTime(date: Date): string {
        let hour = _.padStart(date.getHours().toString(), 2, '0');
        let mins = _.padStart(date.getMinutes().toString(), 2, '0');
        let secs = _.padStart(date.getSeconds().toString(), 2, '0');

        return `${hour}:${mins}:${secs}`;
    }

    public static getWeekFirstDate(date: Date = null): Date {
        let today = date || new Date();

        today.setHours(0, 0, 0, 0);

        let firstDayOfWeek = today.valueOf() - ((today.getDay() - 1) * this.microsecondsPerDay);

        return new Date(firstDayOfWeek);
    }

    public static getWeekLastDate(date: Date = null): Date {
        let today = date || new Date();
        let lastDayOfWeek = today.valueOf() + ((7 - today.getDay()) * this.microsecondsPerDay) - 1000; // minus 1 sec
        let lastDayDate = new Date(lastDayOfWeek);

        lastDayDate.setHours(23, 59, 59, 0);

        return lastDayDate;
    }

    public static getWeekStartAndEndDates(date: Date = null): Date[] {
        return [
            this.getWeekFirstDate(date),
            this.getWeekLastDate(date)
        ];
    }

    public static get microsecondsPerDay(): number {
        return 24 * 60 * 60 * 1000;
    }

    public static setTime(date: Date, time: string) {
        let timeParts = time.split(':');

        date.setHours(parseInt(timeParts[0], 10), parseInt(timeParts[1], 10));

        return date;
    }

    public static setDate(date: Date, dateString: string) {
        let timeParts = dateString.split('-');

        date.setFullYear(parseInt(timeParts[0], 10), parseInt(timeParts[1], 10) - 1, parseInt(timeParts[2], 10));

        return date;
    }
}
