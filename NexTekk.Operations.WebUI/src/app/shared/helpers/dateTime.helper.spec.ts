import { DateTimeHelper } from './dateTime.helper';

describe('DateTimeHelper', () => {

    it('should format date according to the format string', () => {
        let date = new Date('2017-01-16 12:15');
        let formatedDate = DateTimeHelper.format(date, 'MM-dd-yyyy');

        expect(typeof formatedDate).toBe('string');
        expect(formatedDate).toBe('01-16-2017');
    });

    it('should format date as short date', () => {
        let date = new Date('2017-01-16 12:15');
        let shortDateFormat = DateTimeHelper.shortDate(date);

        expect(typeof shortDateFormat).toBe('string');
        expect(shortDateFormat).toBe('1/16/2017');
    });

    it('should be able to get the first day of the week', () => {
        let date = new Date('2017-01-18 12:15');
        let weekFirstDay = DateTimeHelper.getWeekFirstDate(date);

        expect(weekFirstDay.getDate()).toBe(16);
        expect(weekFirstDay.getFullYear()).toBe(2017);
        expect(weekFirstDay.getMonth()).toBe(0);
        expect(weekFirstDay.getHours()).toBe(0);
        expect(weekFirstDay.getMinutes()).toBe(0);
        expect(weekFirstDay.getSeconds()).toBe(0);
    });

    it('should be able to get the last day of the week', () => {
        let date = new Date('2017-01-18 12:15');
        let weekLastDay = DateTimeHelper.getWeekLastDate(date);

        expect(weekLastDay.getDate()).toBe(22);
        expect(weekLastDay.getFullYear()).toBe(2017);
        expect(weekLastDay.getMonth()).toBe(0);
        expect(weekLastDay.getHours()).toBe(23);
        expect(weekLastDay.getMinutes()).toBe(59);
        expect(weekLastDay.getSeconds()).toBe(59);
    });

    it('should get first and last date of the week', () => {
        let date = new Date('2017-01-18 12:15');
        let firstAndLastDates = DateTimeHelper.getWeekStartAndEndDates(date);

        expect(firstAndLastDates[0]).not.toBeFalsy();
        expect(firstAndLastDates[1]).not.toBeFalsy();
        expect(firstAndLastDates[0].valueOf()).toBeLessThan(firstAndLastDates[1].valueOf());
    });

    it('should get number of microseconds in a day', () => {
        expect(DateTimeHelper.microsecondsPerDay).toBe(86400000);
    });

    it('should get time in string format', () => {
        let date = new Date('2017-01-18 12:15');
        let time = DateTimeHelper.getTime(date);

        expect(time).toBe('12:15:00');
    });

    it('should set time for a dateTime object', () => {
        let date = new Date('2017-01-18 00:00:00');
        DateTimeHelper.setTime(date, '12:15');

        expect(date.getHours()).toBe(12);
        expect(date.getMinutes()).toBe(15);

        expect(date.getFullYear()).toBe(2017);
        expect(date.getMonth()).toBe(0);
        expect(date.getDate()).toBe(18);
    });

    it('should set date for a dateTime object', () => {
        let date = new Date('2017-01-18 12:15');
        DateTimeHelper.setDate(date, '2016-05-28');

        expect(date.getHours()).toBe(12);
        expect(date.getMinutes()).toBe(15);

        expect(date.getFullYear()).toBe(2016);
        expect(date.getMonth()).toBe(4);
        expect(date.getDate()).toBe(28);
    });
});
