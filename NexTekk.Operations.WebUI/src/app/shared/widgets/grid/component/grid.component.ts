import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DropDownFilterComponent } from '../filters/dropDownFilter/dropDownFilter.component';
import { DateEditorComponent } from '../dateEditor/dateEditor.component';
import { InputEditorComponent } from '../inputEditor/inputEditor.component';
import { SelectEditorComponent } from '../select/selectEditor.component';
import { DropDownFilterParams } from '../models/dropDownFilterParams.model';
import { IdValuePair } from '../models/idValuePair.model';
import { GridApi, IDatasource, IGetRowsParams, ColDef } from 'ag-grid';
import { InputRendererComponent } from '../inputRenderer/inputRenderer.component';
import { ColumnDefinitions } from '../../..';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

@Component({
    selector: 'app-grid',
    templateUrl: './grid.component.html'
})
export class GridComponent {
    rowModelType: string;
    columnDefs: ColDef[];

    @Input()
    dataList: any[];

    @Input()
    gridConfig: ColumnDefinitions[];

    @Output()
    onFilterChanged: EventEmitter<object>;

    @Output()
    onSearchCriteriaChanged: EventEmitter<any>;

    @Output()
    onRowSelected: EventEmitter<object>;

    @Output()
    onCellValueChanged: EventEmitter<object>;

    private gridApi: GridApi
    private params: IGetRowsParams;

    constructor() {
        this.rowModelType = this.rowModelType || 'infinite';
        this.onFilterChanged = new EventEmitter<object>();
        this.onRowSelected = new EventEmitter<object>();
        this.onCellValueChanged = new EventEmitter<object>();
        this.onSearchCriteriaChanged = new EventEmitter<object>();
    }

    onGridReady($event) {
        this.gridApi = $event.api;
    }

    cellValueChanged($event) {
        const eventObj = {
            newValue: $event['newValue'],
            oldValue: $event['oldValue'],
            data: $event['data']
        };

        this.onCellValueChanged.emit(eventObj);
    }

    addRow<T>(model: T): void {
        this.gridApi.updateRowData({ add: [model], addIndex: 0 });
        this.gridApi.deselectIndex(0);
    }

    deleteSelectedRows(): void {
        this.gridApi.deselectAll();
        this.gridApi.refreshInfiniteCache();
    }

    ngOnChanges(simpleChanges): void {
        if(simpleChanges['gridConfig'] && this.gridConfig.length) {
            let timeOutId = setTimeout(() => {
                this.setColumnDefinitions();
                this.gridApi.sizeColumnsToFit();
                this.gridApi.setDatasource(this.setGridDataSource());
                clearTimeout(timeOutId);
            }, 300);
        }

        if (simpleChanges['dataList'] && !simpleChanges['dataList'].firstChange) {
            let lastRow = -1;

            if (this.dataList.length == 0 || this.dataList.length % 100 != 0) {
                lastRow = this.dataList.length;
            }

            this.params.successCallback(this.dataList, lastRow);
        }
    }

    rowSelected = ($event): void => this.onRowSelected.emit(this.gridApi.getSelectedRows());

    deSelectAll = (): void => this.gridApi.deselectAll();

    getSelectedRows = <T>(): T[] => this.gridApi.getSelectedRows();

    getRenderedRows = <T>(): T[] => this.gridApi.getRenderedNodes().map(node => node.data);

    getSearchCriteria() {
        const searchCriteria = {};
        const filterModel = this.gridApi.getFilterModel();

        for (let prop in filterModel) {
            if (filterModel[prop]['filterType'] === 'number') {
                searchCriteria['minAmount'] = filterModel[prop]['filter'];
                searchCriteria['maxAmount'] = filterModel[prop]['filterTo'];

                continue;
            }

            if (filterModel[prop]['filterType'] === 'date') {
                searchCriteria[prop] = {};
                searchCriteria[prop]['beginDateTime'] = filterModel[prop]['dateFrom'];
                searchCriteria[prop]['endDateTime'] = filterModel[prop]['dateTo'];

                continue;
            }

            searchCriteria[prop] = filterModel[prop]['value'] || filterModel[prop]['filter'];
        }

        searchCriteria['pageNumber'] = this.params.endRow / 100;
        
        return searchCriteria;
    }

    refreshData() {
        this.gridApi.setDatasource(this.setGridDataSource());
    }

    onFilter() {
        this.onFilterChanged.emit();
    }

    private setColumnDefinitions(): void {
        this.columnDefs = [
            { headerName: '', field: 'selected', width: 30, checkboxSelection: true, suppressMenu: true, suppressFilter: true },
            ...this.gridConfig.map(header => {
                let gridHeaders = {};

                gridHeaders = this.getFilterFrameWork(header.filter);

                gridHeaders = {...gridHeaders, ...this.getCellFrameWork(header.cellFormat)};

                gridHeaders['headerName'] = header.headerName;
                gridHeaders['field'] = header.field;
                gridHeaders['colId'] = header.colId;
                gridHeaders['width'] = header.width;

                return gridHeaders;
            })
        ];
    }

    private setGridDataSource(): IDatasource {
        return {
            rowCount: null,
            getRows: (params: IGetRowsParams) => {
                this.params = params;
                this.getRowBlock(params);
            }
        };
    }

    private getRowBlock(params: IGetRowsParams) {
        const searchCriteria = this.getSearchCriteria();

        searchCriteria['pageNumber'] = this.params.endRow / 100;

        this.onSearchCriteriaChanged.emit(searchCriteria);
    }

    private getFilterFrameWork(filter: object) {
        if (!filter || filter['suppressMenu']) return { suppressMenu: true };
        const filterObj = {};

        switch(filter['type']) {
            case 'dropdown':
                filterObj['filterFramework'] = DropDownFilterComponent;
                filterObj['filterParams'] = new DropDownFilterParams(filter['options']);
                break;
            case 'date':
            case 'number':
            case 'text':
                filterObj['filter'] = filter['type'];
                filterObj['filterParams'] = { filterOptions: [filter['options']] };
                break;
        }

        return filterObj;
    }

    private getCellFrameWork(cell: object) {
        if(!cell) return {};
        const cellObj = {};

        cellObj['editable'] = cell['editable'];

        switch(cell['editor']) {
            case 'date':
                cellObj['cellEditorFramework'] = DateEditorComponent;
                break;
            case 'input':
                cellObj['cellEditorFramework'] = InputEditorComponent;
                break;
            case 'dropdown':
                cellObj['cellEditorFramework'] = SelectEditorComponent;
                cellObj['cellEditorParams'] = new DropDownFilterParams(cell['options']);
                break;
        }

        cellObj['cellRendererFramework'] = cell['renderer'] && InputRendererComponent;

        return cellObj;
    }
}