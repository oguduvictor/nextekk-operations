import { TestBed, fakeAsync, tick, ComponentFixture, flushMicrotasks, resetFakeAsyncZone } from "@angular/core/testing";
import { SharedModule, GridComponent, ColumnDefinitions, FilterDefinitions, CellDefinition } from "../../..";
import { GridApi } from "ag-grid";

describe('Grid Component', () => {
    let _fixture: ComponentFixture<GridComponent>;;
    let _gridComponent: GridComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ SharedModule ]
        }).compileComponents();

        _fixture = TestBed.createComponent(GridComponent);
        _gridComponent = <GridComponent>_fixture.debugElement.componentInstance;
    });

    it('can be instantiated', () => {
        expect(_gridComponent).toBeTruthy();
    });

    xit('should set columnDefinitions when it receives gridConfig', fakeAsync(() => {
        _fixture.autoDetectChanges();
        
        const gridConfig = [
            new ColumnDefinitions('Item', 'item', 105, 'item', new FilterDefinitions(false, 'text', 'contains')),
            new ColumnDefinitions('Serial #', 'serialNo', 123, 'serialNo', new FilterDefinitions(false, 'text', 'contains'))
        ];
        const simpleChanges = { gridConfig };

        _gridComponent.gridConfig = gridConfig;
        
        _gridComponent.ngOnChanges(simpleChanges);
        
        tick(300);

        expect(_gridComponent.columnDefs).toBeTruthy();
        expect(_gridComponent.columnDefs.length).toBe(3);

        flushMicrotasks();
    }));

    it('emits event when cell value changes', () => {
        const eventObj = {
            newValue: 'newValue',
            oldValue: 'oldValue',
            data: 'data'
        };
        
        spyOn(_gridComponent.onCellValueChanged, 'emit');

        _gridComponent.cellValueChanged(eventObj);

        expect(_gridComponent.onCellValueChanged.emit).toHaveBeenCalledWith(eventObj);
    });

    it('should emit event when filter occurs', () => {
        spyOn(_gridComponent.onFilterChanged, 'emit');

        _gridComponent.onFilter();

        expect(_gridComponent.onFilterChanged.emit).toHaveBeenCalledTimes(1);

    });

});
