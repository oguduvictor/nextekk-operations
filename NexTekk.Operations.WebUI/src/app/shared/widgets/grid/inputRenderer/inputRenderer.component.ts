import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import * as _ from 'lodash';
import { DateTimeHelper } from '../../..';

@Component({
    selector: 'app-input-renderer',
    templateUrl: './inputRenderer.component.html',
    styleUrls: ['./inputRenderer.component.css']
})
export class InputRendererComponent implements ICellRendererAngularComp {
    inputValue: any;
    isNumber: boolean;

    agInit(params: any) {
        this.inputValue = params.value;
        this.isNumber = _.isNumber(params.value);
    }

    refresh(params: any): boolean {
        this.inputValue = params.value;
        this.isNumber = _.isNumber(params.value);
        return true;
    }
}
