import { InputRendererComponent } from './inputRenderer.component';

describe('InputRendererComponent', () => {
    let _inputRenderer: InputRendererComponent;
    let _params;

    beforeEach(() => {
        _inputRenderer = new InputRendererComponent();

        _params = { value: ' Missy Dreen', column: { colId: 'currency' } };
    });

    it('can be instantiated successfully', () => {
        expect(_inputRenderer).toBeTruthy();
    });

    it('sets value of inputValue when agInit is called', () => {
        expect(_inputRenderer.inputValue).toBeUndefined();
        _inputRenderer.agInit(_params);

        _inputRenderer.inputValue = _params.value;
    });

    it('sets value of inputValue when refresh is called', () => {
        expect(_inputRenderer.inputValue).toBeUndefined();

        _inputRenderer.refresh(_params);

        expect(_inputRenderer.inputValue).toEqual(_params.value);
    });
});
