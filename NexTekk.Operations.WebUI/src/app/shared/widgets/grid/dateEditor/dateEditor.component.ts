import { Component } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { DateTimeHelper } from '../../../../shared';

@Component({
    selector: 'app-date-editor',
    templateUrl: './dateEditor.component.html',
    styleUrls: ['./dateEditor.component.css']
})
export class DateEditorComponent implements ICellEditorAngularComp {
    date: Date;
    maxDate = new Date();
    minDate = new Date();
    bsConfig: Partial<BsDatepickerConfig> = { containerClass: 'theme-default', maxDate: this.maxDate, minDate: this.minDate };

    constructor() {
        this.minDate.setMonth(this.minDate.getMonth() - 11, 1);
    }

    getValue() {
        return this.date;
    }

    agInit(params) {
        this.date = params.value;
    }
}
