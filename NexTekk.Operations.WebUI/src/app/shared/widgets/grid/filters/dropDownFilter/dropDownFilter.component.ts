import { Component } from '@angular/core';
import {IAfterGuiAttachedParams, IDoesFilterPassParams, IFilterParams, RowNode} from 'ag-grid';
import { AgFilterComponent } from 'ag-grid-angular';
import { GridFilterValue } from '../../models/gridFilterValue.model';
import { IdValuePair } from '../../models/idValuePair.model';

@Component({
    selector: 'app-grid-dropdown-filter',
    templateUrl: './dropDownFilter.component.html',
    styleUrls: ['./dropDownFilter.component.css']
})
export class DropDownFilterComponent implements AgFilterComponent {
    params: IFilterParams;
    selectedOption: string;
    dropDownOptions: any[];
    isIdValuePairOptions: boolean;
    
    constructor() {
        this.selectedOption = '';
        this.dropDownOptions = [];
        this.isIdValuePairOptions = false;
    }

    isFilterActive(): boolean {
        return !!this.selectedOption;
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        return !!this.selectedOption;
    }

    getModel() {
        return new GridFilterValue(this.selectedOption);
    }

    setModel(model) {
        console.log(model);
    }

    onChange(value) {
        this.selectedOption = value;
        this.params.filterChangedCallback();
    }

    agInit(params: any): void {
        this.params = params;

        this.dropDownOptions = params.dropDownOptions;
        this.isIdValuePairOptions = typeof(params.dropDownOptions[0]) !== 'string';
    }
}
