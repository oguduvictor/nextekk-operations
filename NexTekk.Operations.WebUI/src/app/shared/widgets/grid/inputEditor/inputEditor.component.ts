import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import * as _ from 'lodash';

@Component({
    selector: 'app-input-editor',
    templateUrl: './inputEditor.component.html',
    styleUrls: ['./inputEditor.component.css']
})
export class InputEditorComponent implements ICellEditorAngularComp, AfterViewInit {
    inputValue: any;
    maxLength: number = 140;
    multilineInput = true;

    private KEY_LEFT = 37;
    private KEY_UP = 38;
    private KEY_RIGHT = 39;
    private KEY_DOWN = 40;

    @ViewChild('inputElement')
    inputElement: ElementRef;

    inputType = 'text';

    getValue() {
        if (this.inputType === 'number') {
            this.inputValue = _.toNumber(this.inputValue);
        }

        return this.inputValue;
    }

    onKeyDown(event): void {
        let key = event.which || event.keyCode;
        if (key === this.KEY_LEFT ||  key === this.KEY_RIGHT || key === this.KEY_UP || key === this.KEY_DOWN) {
            event.stopPropagation();
        }
    }

    agInit(params: any) {
        this.inputValue = params.value;

        if (_.isNumber(this.inputValue)) {
            this.inputType = 'number';
            this.multilineInput = false;
            this.maxLength = 100;
        }
    }

    ngAfterViewInit() {
        this.inputElement.nativeElement.focus();
    }
}
