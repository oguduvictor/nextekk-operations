import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputEditorComponent } from './inputEditor.component';
import { ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { exec } from 'child_process';

describe('inputEditorComponent', () => {
    let inputEditorComponent: InputEditorComponent;
    let fixture: ComponentFixture<InputEditorComponent>;

    let _params = {value: ' Missy Dreen', column: { colId: 'currency'}};

    let _event = {
        keyCode: 39,
        stopPropagation: () => {}
    }

    let _event2 = {
        keyCode: 99,
        stopPropagation: () => {}
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [InputEditorComponent],
            providers: []
        });

        fixture = TestBed.createComponent(InputEditorComponent);

        inputEditorComponent = <InputEditorComponent>fixture.componentInstance;

        spyOn(_event, 'stopPropagation');
        spyOn(_event2, 'stopPropagation');

        inputEditorComponent.onKeyDown(_event);
        inputEditorComponent.onKeyDown(_event2);
    });

    it('can be instantiated successfully', () => {
        expect(inputEditorComponent).toBeTruthy();
    });

    it('inputType should be text by default', () => {
        expect(inputEditorComponent.inputType).toEqual('text');
    });

    it('returns inputValue when getValue() is called', () => {
        inputEditorComponent.inputValue = 'Missy Dreen';

        let inputValue = inputEditorComponent.inputValue;

        expect(inputValue).toEqual(inputEditorComponent.getValue());
    });

    it('sets inputValue when agInit is called', () => {
        let params = {value: ' Missy Dreen', column: { colId: 'currency'}};

        inputEditorComponent.agInit(params);

        expect(inputEditorComponent.inputValue).toEqual(params.value);
    });

    it('should set focus on inputElement once ngAfterViewInit is called', () => {

        inputEditorComponent.inputElement = <ElementRef>{ nativeElement: { focus: jasmine.createSpy('focus').and.returnValue('')}};

        inputEditorComponent.ngAfterViewInit();

        expect(inputEditorComponent.inputElement.nativeElement.focus).toHaveBeenCalled();
    });

    it('"onKeyDown()" event should trigger call to event.stopPropagation()', () => {
        expect(_event.stopPropagation).toHaveBeenCalled();
    });

    it('"onKeyDown()" event should not trigger call to event.stopPropagation()', () => {
        expect(_event2.stopPropagation).not.toHaveBeenCalled();
    });
});
