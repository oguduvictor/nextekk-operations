import { Component } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { User, UserService } from '../../../../../app/shared/';
import { IdValuePair } from '../models/idValuePair.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-select-editor',
  templateUrl: './selectEditor.component.html',
  styleUrls: ['./selectEditor.component.css']
})
export class SelectEditorComponent implements ICellEditorAngularComp {
  options: IdValuePair[];
  selectedValue: string | number;

  constructor() {
    this.options = [];
  }

  getValue(): string | number {
    const selectedOption: IdValuePair = this.options.find(x => x.id === this.selectedValue)
                  || _.head(this.options) || new IdValuePair('', '');

    return selectedOption.value;
  }

  agInit(params: any) {
    this.options = params.dropDownOptions;

    this.selectedValue = this.options.find(x => x.value === params.value).id;
  }
}
