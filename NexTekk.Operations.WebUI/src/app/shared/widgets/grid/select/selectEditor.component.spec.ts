import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SelectEditorComponent } from './selectEditor.component';
import { Observable } from 'rxjs/Observable';
import { User, IdValuePair } from '../../../../shared/index';
import { FormsModule } from '@angular/forms';
import { Expense } from '../../../../cashflow/common/models/expense.model';
import { Currency } from '../../../../cashflow/common/models/currency.model';

describe('SelectEditorComponent', () => {
  let selectEditorComponent: SelectEditorComponent;
  let fixture: ComponentFixture<SelectEditorComponent>;
  const _user1 = new User('839nd8-njsi892-9n92n', 'Gbolahan', 'Allen', '', []);
  const _user2 = new User('my-user-id', 'Joe', 'Bloe');

  const _expense: Expense = new Expense(_user2, Currency.naira,
    'expense_id', new Date('12-09-2017'), 5000, 'for dinner party', 'want to have fun'
  );

  let userService = {
    getAllUsers: () => {
      return Observable.of([
        new User('', 'Gbolahan', 'Allen', '', [])
      ]);
    }
  };

  let _params: any;
  let _params2: any;

  beforeEach(() => {
    _params = {
      value: Currency.naira,
      column: { colId: 'currency' },
      dropDownOptions: Currency.toIdValuePair()
    };

    _params2 =  {
      value: _user2.fullName,
      column: { colId: 'incurredById' },
      dropDownOptions: [
        new IdValuePair(_user1.id, _user1.fullName)
      ],
      node: {
        data: _expense
      }
    };

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [SelectEditorComponent]
    });

    fixture = TestBed.createComponent(SelectEditorComponent);

    selectEditorComponent = <SelectEditorComponent>fixture.componentInstance;
  });

  it('should have a defined component', () => {
    expect(selectEditorComponent).toBeTruthy();
    expect(selectEditorComponent).toBeDefined();
  });

  it('can set options for dropdown when initialized', () => {
    selectEditorComponent.agInit(_params);

    expect(selectEditorComponent.options[0] instanceof IdValuePair).toBeTruthy();
    expect(selectEditorComponent.options.length).toBe(2);
  });

  it('can return right value on use of "getValue()"', () => {
    selectEditorComponent.agInit(_params);

    expect(selectEditorComponent.getValue()).toEqual(Currency.naira);
  });

  xit('sets value of options[] when used in Person column', () => {
    selectEditorComponent.agInit(_params2);

    expect(selectEditorComponent.options[0].value).toEqual(_user1.fullName);
    expect(selectEditorComponent.options[0].id).toEqual(_user1.id);
    expect(selectEditorComponent.options.length).toBe(1);
  });

  it('sets selectedValue to "naira" if params.value is ₦', () => {
    _params.value = Currency.naira;
    selectEditorComponent.agInit(_params);

    expect(selectEditorComponent.selectedValue).toEqual('naira');
  });

  it('sets selectedValue to "dollar" if params.value is $', () => {
    _params.value = Currency.dollar;
    selectEditorComponent.agInit(_params);

    expect(selectEditorComponent.selectedValue).toEqual('dollar');
  });
});
