export class CellDefinition {
    constructor(public editable: any, public editor: string, public options?: any, public renderer?: string) {}
    
    static enableCellEdit = (params): boolean => params.node.data && params.node.data.canDelete;
}