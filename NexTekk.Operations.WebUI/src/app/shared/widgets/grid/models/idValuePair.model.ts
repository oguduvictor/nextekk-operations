export class IdValuePair {
    constructor(
        public id: string | number,
        public value: string
    ) {}

    static GenerateYesNoOption(): IdValuePair[] {
        return Array.of(new IdValuePair('true', 'Yes'), new IdValuePair('false', 'No'));
    }
}
