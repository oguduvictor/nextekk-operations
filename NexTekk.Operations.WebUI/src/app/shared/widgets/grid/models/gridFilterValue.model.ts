export class GridFilterValue {
    constructor(
        public value: string
    ) {}

    public get isValid() {
        return this.value !== undefined && this.value !== null;
    }
}
