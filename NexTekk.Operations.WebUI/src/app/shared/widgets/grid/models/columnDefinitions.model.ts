import { FilterDefinitions } from "./filterConfig.model";
import { CellDefinition } from "./cellDefinitions.model";

export class ColumnDefinitions {
    constructor(
        public headerName?: string,
        public field?: string,
        public width?: number,
        public colId?: string,
        public filter?: FilterDefinitions,
        public cellFormat?: CellDefinition
    ) {}
}