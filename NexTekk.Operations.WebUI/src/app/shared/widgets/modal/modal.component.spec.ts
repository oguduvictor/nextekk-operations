import { ModalComponent } from './modal.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SharedModule } from '../../shared.module';

describe('ModalComponent', () => {
    let _modal: ModalComponent = null;
    let _fixture: ComponentFixture<ModalComponent>;
    let modal: ModalDirective;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule]
        });

        _fixture = TestBed.createComponent(ModalComponent);
        _modal = <ModalComponent>_fixture.debugElement.componentInstance;
        modal = (<any>_modal).modal;

    });

    it('should instantiate component', () => {
        expect(_modal).toBeTruthy();
    });

    it('should instantiate with initial conditions', () => {
        expect(_modal.title).toBe('[Title goes here]');
        expect(_modal.closable).toBe(true);
        expect(_modal.size).toBe('medium');
    });

    it('can be displayed by calling show', () => {
        spyOn(modal, 'show');

        _fixture.detectChanges();
        _modal.show();

        expect(modal.show).toHaveBeenCalled();
    });

    it('can be closed by calling close', () => {
        spyOn(_modal.onClosed, 'emit');
        spyOn(modal, 'hide');

        _modal.close();

        _fixture.detectChanges();

        expect(_modal.onClosed.emit).toHaveBeenCalledWith(true);
        expect(modal.hide).toHaveBeenCalled();
    });
});
