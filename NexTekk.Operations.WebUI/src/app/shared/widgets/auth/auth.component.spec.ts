import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { UserService } from '../../services/user.service';
import { AuthComponent } from './auth.component';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models/user.model';
import { SpinnerService } from '../spinner/spinner.service';

describe('AuthComponent', () => {
    let _authService: AuthService;
    let _userService: UserService;
    let _spinnerService: SpinnerService;
    let _fixture: ComponentFixture<AuthComponent>;
    let _authComponent: AuthComponent;
    let _window = <any>{};

    beforeEach(async(() => {
        _spinnerService = jasmine.createSpyObj('SpinnerService', ['show', 'hide']);
        _authService = jasmine.createSpyObj('AuthService',
            ['setAccessToken', 'getAccessToken', 'redirectToLastPage', 'triggerAuthentication', 'redirectToHome'] );
        _userService = <any>{getCurrentUser: jasmine.createSpy('getCurrentUser').and.returnValue(Observable.of(new User()))};

        _window.location = { href: 'http://localhost:1000' };
        TestBed.configureTestingModule({
            declarations: [AuthComponent],
            providers: [
                { provide: UserService, useValue: _userService  },
                { provide: AuthService, useValue:  _authService },
                { provide: SpinnerService, useValue: _spinnerService }
            ]
        });

        _fixture = TestBed.createComponent(AuthComponent);
        _authComponent = <AuthComponent>_fixture.debugElement.componentInstance;
        (<any>_authComponent)._window = _window;
    }));

    it('should instantiate successfully', () => {
        expect(_authComponent).toBeTruthy();
    });

    it('should get access token from url if available', () => {
        let token = 'tokelet2345';
        _window.location.href = `http://localhost:1000?access_token=${token}`;

        _authComponent.ngOnInit();

        expect(_authService.setAccessToken).toHaveBeenCalledWith(token);
        expect(_userService.getCurrentUser).toHaveBeenCalled();
        expect(_authService.redirectToLastPage).toHaveBeenCalled();
    });

    it('should trigger authentication if no token found', () => {
        _authService.getAccessToken = jasmine.createSpy('getAccessToken').and.returnValue('');
        _authComponent.ngOnInit();
        expect(_authService.triggerAuthentication).toHaveBeenCalledTimes(1);
    });

    it('should redirect to home if token found in session', () => {
        (<any>_authComponent)._authService.getAccessToken = jasmine.createSpy('getAccessToken').and.returnValue('token-1234');

        _authComponent.ngOnInit();

        expect(_authService.redirectToHome).toHaveBeenCalledTimes(1);
    });

    it('should show spinner on initalizing', () => {
        _authComponent.ngOnInit();
        expect(_spinnerService.show).toHaveBeenCalled;
    });

    it('should hide spinner service', () => {
        _authComponent.ngOnDestroy();
        expect(_spinnerService.hide).toHaveBeenCalled();
    });
});
