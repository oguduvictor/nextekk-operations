import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { UserService } from '../../services/user.service';
import { SpinnerService } from '../spinner/spinner.service';

@Component({
    template: '<p>authenticating...</p>'
})
export class AuthComponent implements OnInit, OnDestroy {
    private _window: Window = window;

    constructor(
        private _authService: AuthService,
        private _userService: UserService,
        private _spinnerService: SpinnerService) {
    }

    private getTokenFromUrl(): string {
        let urlParts = this._window.location.href.split('?');

        if (urlParts.length < 2) {
            return null;
        }

        let queryParts = urlParts[1].split('&');

        for (let queryPart of queryParts) {
            let tokenKeyIndex = queryPart.indexOf('access_token');

            if (tokenKeyIndex > -1) {
                return queryPart.split('=')[1];
            }
        }

        return null;
    }

    ngOnInit(): void {
        this._spinnerService.show();
        let token = this.getTokenFromUrl();

        if (token) {
            this._authService.setAccessToken(token);
            this._userService.getCurrentUser().subscribe(() => {
                this._spinnerService.hide();
                this._authService.redirectToLastPage();
            },
            () => {
                this._spinnerService.hide();
            });

            return;
        }

        if (!this._authService.getAccessToken()) {
            this._authService.triggerAuthentication();
        } else {
            this._authService.redirectToHome();
        }
    }

    ngOnDestroy(): void {
        this._spinnerService.hide();
    }
}
