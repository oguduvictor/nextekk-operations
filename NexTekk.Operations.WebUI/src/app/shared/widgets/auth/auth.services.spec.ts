import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Http} from '@angular/http';
import { AuthService } from './auth.service';
import { NotificationService } from '../../services/notification.service';
import { User } from '../../models/user.model';
import { environment} from '../../../../environments/environment';

describe('AuthService', () => {
    let _tokenKey = 'NexTekk:access_token';
    let _userKey = 'NexTekk:user_info';
    let _homeUrl = '/time-entries';
    let _user = new User('12345', 'Joe');
    let _redirectUrlKey = 'NexTekk:redirect_uri';
    let _authService: AuthService;
    let _notificationService: NotificationService = <any>{showError: function() {}};
    let _http;
    let _router: Router = <any>{
            createUrlTree: () => '/auth',
            url: '/link',
            navigateByUrl: jasmine.createSpy('navigateByUrl')
        };
    let _window = {
            sessionStorage: {
                getItem: () => 'item value',
                setItem: () => {},
                removeItem: () => {}
            },
            location: {
                href: 'http://localhost:1000'
            }
        };

    beforeEach(() => {
        _http = <any>{};
        TestBed.configureTestingModule({
            providers: [
                AuthService,
                {provide: Http, useValue: _http},
                { provide: Router, useValue: _router },
                { provide: NotificationService, useValue: _notificationService }
            ]
        });

        _authService = TestBed.get(AuthService);
        (<any>_authService)._window = _window;
    });

    it('should instantiate succesfully', () => {
        expect(_authService).not.toBeFalsy();
    });

    it('should provide authentication status', () => {
        expect(_authService.isAuthenticated).toBe(true);
    });

    it('should return access token from sessionStorage', () => {
        spyOn(_window.sessionStorage, 'getItem').and.returnValue('token-A');

        let token = _authService.getAccessToken();

        expect(token).toBe('token-A');
        expect(_window.sessionStorage.getItem).toHaveBeenCalledWith(_tokenKey);
    });

    it('should save access token in sessionStorage', () => {
        spyOn(_window.sessionStorage, 'setItem');

        _authService.setAccessToken('token-A');

        expect(_window.sessionStorage.setItem).toHaveBeenCalledWith(_tokenKey, 'token-A');
    });

    it('should save user info in sessionStorage as json', () => {
        spyOn(_window.sessionStorage, 'setItem');

        _authService.setUserInfo(_user);

        let userJson = JSON.stringify(_user);

        expect(_window.sessionStorage.setItem).toHaveBeenCalledWith(_userKey, userJson);
    });

    it('triggerAuthentication should reset all items but redirect url', () => {
        spyOn(_window.sessionStorage, 'removeItem');
        spyOn(_window.sessionStorage, 'setItem');

        _authService.triggerAuthentication(false);

        expect((<any>_window.sessionStorage.removeItem).calls.allArgs()).toEqual([[_tokenKey], [_userKey]]);
        expect(_window.sessionStorage.removeItem).not.toHaveBeenCalledWith(_redirectUrlKey, _router.url);
        expect(_window.sessionStorage.setItem).toHaveBeenCalledWith(_redirectUrlKey, _router.url);
    });

    it('triggerAuthentication should reset all sessionStorage for logout', () => {
        spyOn(_window.sessionStorage, 'removeItem');
        spyOn(_window.sessionStorage, 'setItem');

        _authService.triggerAuthentication(true);

        expect((<any>_window.sessionStorage.removeItem).calls.allArgs()).toEqual([[_tokenKey], [_userKey], [_redirectUrlKey]]);
        expect(_window.sessionStorage.setItem).toHaveBeenCalledWith(_redirectUrlKey, _router.url);
    });

    it('triggerAuthentication redirect app to the login page', () => {
        _window.location.href = 'http://localhost:1000';

        let expectedUrl = `${environment.loginEndPoint}?redirect_uri=${encodeURIComponent('http://localhost:1000#/auth')}`;

        _authService.triggerAuthentication();

        expect(_window.location.href).toBe(expectedUrl);
    });

    it('should be able to redirect to the last page if available', () => {
        spyOn(_window.sessionStorage, 'getItem').and.returnValue('/link1');
        spyOn(_window.sessionStorage, 'removeItem');

        _authService.redirectToLastPage();

        expect(_window.sessionStorage.getItem).toHaveBeenCalledWith(_redirectUrlKey);
        expect(_window.sessionStorage.removeItem).toHaveBeenCalledWith(_redirectUrlKey);
        expect(_router.navigateByUrl).toHaveBeenCalledWith('/link1');
    });

    it('should redirect to home when no last page exists', () => {
        _window.sessionStorage.getItem = () => '';
        _authService.redirectToLastPage();
        expect(_router.navigateByUrl).toHaveBeenCalledWith(_homeUrl);
    });

    it('redirectToHome should redirect to home', () => {
        _authService.redirectToHome();
        expect(_router.navigateByUrl).toHaveBeenCalledWith(_homeUrl);
    });

    it('logOut should trigger authentication request', () => {
        spyOn(_authService, 'triggerAuthentication');

        _authService.logOut();

        expect(_authService.triggerAuthentication).toHaveBeenCalledWith(true);
    });

    it('can get user information', () => {
        (<any>_authService)._userInfoKey = 'testKey';
        spyOn(_window.sessionStorage, 'getItem');

        _authService.getUserInfo();

        expect(_window.sessionStorage.getItem).toHaveBeenCalledWith('testKey');
    });
});
