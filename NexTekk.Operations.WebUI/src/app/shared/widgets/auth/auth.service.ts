import { Injectable } from '@angular/core';
import { environment} from '../../../../environments/environment';
import { Router } from '@angular/router';
import { User } from '../../models/user.model';

@Injectable()
export class AuthService {
    private _redirectUriKey = 'NexTekk:redirect_uri';
    private _tokenKey = 'NexTekk:access_token';
    private _userInfoKey = 'NexTekk:user_info';
    private _homeUrl = '/time-entries';
    private _window: Window = window; // TODO: find a way to safely inject it into the constructor

    constructor(
        private _router: Router
    ) {}

    get isAuthenticated() {
        return !!this.getAccessToken();
    }

    getAccessToken() {
        return this._window.sessionStorage.getItem(this._tokenKey);
    }

    setAccessToken(token: string) {
        this._window.sessionStorage.setItem(this._tokenKey, token);
    }

    setUserInfo(user: User) {
        this._window.sessionStorage.setItem(this._userInfoKey, JSON.stringify(user));
    }

    getUserInfo() {
        return this._window.sessionStorage.getItem(this._userInfoKey);
    }

    triggerAuthentication(clearSessionData: boolean = false): void {
        let urlBase = this._window.location.href.split('#/')[0];
        let authUrl = this._router.createUrlTree(['auth']).toString();
        let identityRedirectUrl = `${urlBase}#${authUrl}`;
        let redirectUrl = (this._router.url === authUrl) ? this._homeUrl : this._router.url;

        this._window.sessionStorage.setItem(this._redirectUriKey, redirectUrl);
        this._window.sessionStorage.removeItem(this._tokenKey);
        this._window.sessionStorage.removeItem(this._userInfoKey);

        if (clearSessionData) {
            this._window.sessionStorage.removeItem(this._redirectUriKey);
        }

        this._window.location.href = `${environment.loginEndPoint}?redirect_uri=${encodeURIComponent(identityRedirectUrl)}`;
    }

    redirectToLastPage(): void {
        let url = this._window.sessionStorage.getItem(this._redirectUriKey) || this._homeUrl;

        this._window.sessionStorage.removeItem(this._redirectUriKey);
        this._router.navigateByUrl(url);
    }

    redirectToHome(): void {
        this._router.navigateByUrl(this._homeUrl);
    }

    logOut() {
        this.triggerAuthentication(true);
    }
}
