import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Dialog } from './dialog.model';

@Injectable()
export class DialogService {
    event: Subject<any> = new Subject();
    resolve: (x: boolean) => void;
    reject: (x: boolean) => void;

    showConfirm(title: string, message: string): Promise<boolean> {
        this.event.next(new Dialog(title, message));

        return new Promise<boolean>((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }

    showAlert(title: string, message: string): Promise<boolean> {
        this.event.next(new Dialog(title, message, false));

        return new Promise<boolean>((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }
}
