import { TestBed, ComponentFixture } from '@angular/core/testing';
import { DialogModalComponent } from './dialogModal.component';
import { DialogService } from './dialogModal.service';
import { ModalComponent } from '../modal/modal.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared.module';

describe('DialogModalComponent', () => {
    let _dialogModal: DialogModalComponent;
    let _modal: ModalComponent;
    let _fixture: ComponentFixture<DialogModalComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule],
            providers: [DialogService]
        });

        _fixture = TestBed.createComponent(DialogModalComponent);
        _dialogModal = <DialogModalComponent>_fixture.debugElement.componentInstance;
        _modal = <ModalComponent>_fixture.debugElement.componentInstance;
    });

    it('can be instantiated successfully', () => {
        expect(_dialogModal instanceof DialogModalComponent).toBeTruthy();
    });

    it('should instantiate with initial conditions', () => {
        expect(_dialogModal.isConfirm).toBe(false);
        expect(_dialogModal.title).toBe('Alert!');
        expect(_dialogModal.message).toBe('Something bad happened');
    });

    it('should display cancel button if isConfirm is true', () => {
        let compiled = _fixture.debugElement.nativeElement;

        _dialogModal.isConfirm = true;
        _fixture.detectChanges();

        expect(compiled.querySelector('button.btn-default')).toBeTruthy();
    });
});
