export class Dialog {
    constructor (
        public dialogTitle: string,
        public dialogMsg: string,
        public isConfirm = true
    ) {}
}
