﻿import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';
import { DialogService } from './dialogModal.service';

@Component({
    selector: 'app-dialog-modal',
    templateUrl: './dialogModal.component.html'
})
export class DialogModalComponent {
    isConfirm = false;
    title = 'Alert!';
    message = 'Something bad happened';

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    constructor(private _dialogService: DialogService) {
        this._dialogService.event.subscribe(dialog => {
            this.title = dialog.dialogTitle;
            this.message = dialog.dialogMsg;
            this.isConfirm = dialog.isConfirm;
            this.modal.show();
        });
    }

    ok(): void {
        this.modal.close();
        this._dialogService.resolve(true);
    }

    cancel(): void {
        this.modal.close();
        this._dialogService.resolve(false);
    }
}
