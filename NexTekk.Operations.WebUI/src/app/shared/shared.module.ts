﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './widgets/modal/modal.component';
import { DialogModalComponent } from './widgets/dialogModal/dialogModal.component';
import { DialogService } from './widgets/dialogModal/dialogModal.service';
import { SpinnerComponent } from './widgets/spinner/spinner.component';
import { SpinnerService } from './widgets/spinner/spinner.service';
import { AuthService } from './widgets/auth/auth.service';
import { UserService } from './services/user.service';
import { AuthComponent } from './widgets/auth/auth.component';
import { HttpService } from './services/http.service';
import { ManagerGuardService } from './services/managerGuard.service';
import { FilterByPipe } from './pipes/filterBy.pipe';
import { CamelSeparatePipe } from './pipes/camelSeparate.pipe';
import { NotificationService } from './services/notification.service';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { ModalModule } from 'ngx-bootstrap/modal';
import { OrderByPipe } from './pipes/orderBy.pipe';
import { EmployeeManagerGuardService } from './services/employeeManagerGuard.service';
import { GridComponent } from './widgets/grid/component/grid.component';
import { AgGridModule } from 'ag-grid-angular';
import { DropDownFilterComponent } from './widgets/grid/filters/dropDownFilter/dropDownFilter.component';
import { FormsModule } from '@angular/forms';
import { InputRendererComponent } from './widgets/grid/inputRenderer/inputRenderer.component';
import { DateEditorComponent } from './widgets/grid/dateEditor/dateEditor.component';
import { InputEditorComponent } from './widgets/grid/inputEditor/inputEditor.component';
import { SelectEditorComponent } from './widgets/grid/select/selectEditor.component';
import { NumberPipe } from './pipes/number.pipe';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AsideComponent } from './widgets/aside/aside.component';

let options: ToastOptions = <ToastOptions>{
  positionClass: 'toast-bottom-right',
  newestOnTop: true,
  toastLife: 3000,
  showCloseButton: true
};

@NgModule({
    declarations: [
        ModalComponent,
        DialogModalComponent,
        SpinnerComponent,
        FilterByPipe,
        NumberPipe,
        CamelSeparatePipe,
        AuthComponent,
        OrderByPipe,
        GridComponent,
        InputEditorComponent,
        DateEditorComponent,
        SelectEditorComponent,
        InputRendererComponent,
        DropDownFilterComponent,
        AsideComponent
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ToastModule.forRoot(),
        ModalModule.forRoot(),
        FormsModule,
        AgGridModule.withComponents([
            GridComponent,
            InputEditorComponent,
            DateEditorComponent,
            SelectEditorComponent,
            InputRendererComponent,
            DropDownFilterComponent
        ]),
        BsDatepickerModule.forRoot()
    ],
    exports: [
        ModalComponent,
        DialogModalComponent,
        SpinnerComponent,
        FilterByPipe,
        CamelSeparatePipe,
        AuthComponent,
        OrderByPipe,
        GridComponent,
        NumberPipe,
        InputEditorComponent,
        DateEditorComponent,
        SelectEditorComponent,
        InputRendererComponent,
        DropDownFilterComponent,
        AsideComponent
    ]
    ,
    entryComponents: [
        InputRendererComponent,
        InputEditorComponent,
        DateEditorComponent,
        SelectEditorComponent,
        DropDownFilterComponent
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                AuthService,
                HttpService,
                SpinnerService,
                NotificationService,
                ManagerGuardService,
                EmployeeManagerGuardService,
                UserService,
                DialogService,
                {provide: ToastOptions, useValue: options}
            ]
        };
    }
}
