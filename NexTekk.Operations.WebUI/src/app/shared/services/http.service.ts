import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthService } from '../widgets/auth/auth.service';
import { NotificationService } from './notification.service';
import { environment} from '../../shared';
import * as _ from 'lodash';

@Injectable()
export class HttpService extends Http {
  private _authService: AuthService;
  private _notificationService: NotificationService;
  private _apiRoot = environment.apiUrl;

  constructor (
    backend: XHRBackend,
    options: RequestOptions,
    authService: AuthService,
    notificationService: NotificationService) {

    super(backend, options);
    this._authService = authService;
    this._notificationService = notificationService;
  }

  getData<T>(url: string, returnType: { new(): T ; } = null, option?: RequestOptionsArgs): Observable<T> {
    return this.get(this._apiRoot + url, option)
      .catch(this.handleError)
      .map(res => this.parSeData(res, returnType));
  }

  getMultiple<T>(url: string, returnType: { new(): T ; } = null, option?: RequestOptionsArgs): Observable<Array<T>> {
    return this.get(this._apiRoot + url, option)
      .catch(this.handleError)
      .map(res => this.parSeData(res, returnType));
  }

  postData<T>(url: string, body: any, returnType: { new(): T ; } = null, option?: RequestOptionsArgs): Observable<T> {
    return this.post(this._apiRoot + url, body, option)
      .catch(this.handleError)
      .map(res => this.parSeData(res, returnType));
  }

  putData<T>(url: string, body: any, returnType: { new (): T ; } = null, option?: RequestOptionsArgs): Observable<T> {
    return this.put(this._apiRoot + url, body, option)
      .catch(this.handleError)
      .map(res => this.parSeData(res, returnType));
  }

  deleteData<T>(url: string, returnType: { new (): T ; } = null, option?: RequestOptionsArgs): Observable<T> {
    return this.delete(this._apiRoot + url, option)
      .catch(this.handleError)
      .map(res => this.parSeData(res, returnType));
  }

  request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    let token = this._authService.getAccessToken();

    if (token) {
      let headers = (typeof url === 'string')
                    ? (options || { headers: new Headers() }).headers
                    : url.headers;

      headers.set('Authorization', `Bearer ${token}`);
    }

    return super.request(url, options)
                .catch(this.catchAuthError(this));
  }

  private catchAuthError (self: HttpService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      if (res.status === 401) {
        this._authService.triggerAuthentication();
      }

      if (res.status === 403) {
        let noPermissionMsg = 'You have no permission to access the requested resource.';

        (<any>res)._body = {message: noPermissionMsg};
        this._notificationService.showError(noPermissionMsg);
      }

      return Observable.throw(res);
    };
  }

  private handleError(error: any) {
    let errorData = error.json();
    let errMsg = errorData.message || 'Internal server error';

    if (!environment.production) {
        console.error(errorData);
    }

    return Observable.throw(errMsg);
  }

  private parSeData<T>(response: Response, type: { new (): T ; } = null): any {
    if (response.status < 200 || response.status >= 300) {
        throw new Error('Bad response status: ' + response.status);
    }

    let data: string;

    try {
      data = <any>response.json();
    } catch (e) {
      return (<any>response)._body;
    }

    if (!type || type.hasOwnProperty('fromDto')) {
      return data;
    }

    if (!_.isArray(data)) {
      let typedData = new type();
      (<any>typedData).fromDto(data);

      return typedData;
    }

    let result = [];

    for (let item of data) {
      let typedData = new type();
      (<any>typedData).fromDto(item);

      result.push(typedData);
    }
    return result;
  }
}
