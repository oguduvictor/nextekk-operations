import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NotificationService } from './notification.service';

describe('ToastsManager', () => {
    let _toaster: ToastsManager;
    let _notificationService: NotificationService;

    beforeEach(() => {
        _toaster = jasmine.createSpyObj('ToastsManager', ['info', 'success', 'error', 'warning']);

        _notificationService = new NotificationService(_toaster);
    });

    it('can be instantiated successfully', () => {
        expect(_notificationService).toBeTruthy();
    });

    it('can show error message without title', () => {
        let msg = 'sorry an error occurred';
        _notificationService.showError(msg);

        expect(_toaster.error).toHaveBeenCalledWith(msg, undefined);
    });

    it('can show error msg with title', () => {
        let msg = 'Sorry an error occurred';
        let title = 'Search error';

        _notificationService.showError(msg, title);
        expect(_toaster.error).toHaveBeenCalledWith(msg, title);
    });

    it('can show info without title', () => {
        let info = 'You have new messages';
        _notificationService.showInfo(info);

        expect(_toaster.info).toHaveBeenCalledWith(info, undefined);
    });

    it('can show info with title', () => {
        let info = 'You have new messages';
        let title = 'New Messages';

        _notificationService.showInfo(info, title);

        expect(_toaster.info).toHaveBeenCalledWith(info, title);
    });

    it('can show warning without title', () => {
        let warning = 'Deleting will remove time entries permanently';
        _notificationService.showWarning(warning);

        expect(_toaster.warning).toHaveBeenCalledWith(warning, undefined);
    });

    it('can show warning with title', () => {
        let warning = 'Deleting will remove time entries permanently';
        let title = 'warning';

        _notificationService.showWarning(warning, title);

        expect(_toaster.warning).toHaveBeenCalledWith(warning, title);
    });

    it('can show success without warning', () => {
        let success = 'Submission was success';

        _notificationService.showSuccess(success);

        expect(_toaster.success).toHaveBeenCalledWith(success, undefined);
    });

    it('can show success with title', () => {
        let success = 'Submission was successfully';
        let title = 'Time Entries';

        _notificationService.showSuccess(success, title);

        expect(_toaster.success).toHaveBeenCalledWith(success, title);
    });
});
