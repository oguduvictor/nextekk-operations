import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../widgets/auth/auth.service';
import { UserService } from './user.service';
import { NotificationService } from './notification.service';

@Injectable()
export class EmployeeManagerGuardService implements CanActivate {
    constructor(
        private _authService: AuthService,
        private _userService: UserService,
        private _notification: NotificationService) { }

    canActivate() {
        return this._userService.getCurrentUser().map(user => {
            let isEmployeeOrManager = user.isEmployee() || user.isManager();

            if (!isEmployeeOrManager) {
                this._notification.showError('You have no access to the requested page.');
                this._authService.redirectToHome();
            }

            return isEmployeeOrManager;
        });
    }
}
