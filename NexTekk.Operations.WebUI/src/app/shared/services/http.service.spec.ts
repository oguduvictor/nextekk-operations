import { getTestBed, TestBed } from '@angular/core/testing';
import { BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpService } from './http.service';
import { AuthService } from '../widgets/auth/auth.service';
import { NotificationService } from './notification.service';
import { User } from '../models/user.model';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

describe('HttpService', () => {
    let _backend: MockBackend;
    let _httpservice: HttpService;
    let _notificationService: NotificationService = <any>{showError: function() {}};
    let _authService = {
        getAccessToken: () => 'token_0001'
    };

    let setupConnections = (responseBodyData?: any, status?: number): any => {
        let options = {
          body: responseBodyData === undefined ? {id: '12345'} : responseBodyData,
          status: status || 200
        };

        _backend.connections.subscribe((connection: MockConnection) => {
            let responseOptions = new ResponseOptions(options);
            let response = new Response(responseOptions);
            connection.mockRespond(response);
        });

        return options.body;
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                AuthService,
                { provide: AuthService, useValue: _authService },
                { provide: NotificationService, useValue: _notificationService},
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions,
                        AuthService,
                        NotificationService
                    ],
                    provide: HttpService,
                    useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions, authService: AuthService, notification) => {
                        return new HttpService(<any>backend, defaultOptions, authService, notification);
                    }
                }
            ]
        });

        let testbed = getTestBed();
        _backend = testbed.get(MockBackend);
        _httpservice = TestBed.get(HttpService);
    });

    it('should instantiate successfully', () => {
        expect(_httpservice).not.toBeNull();
    });

    it('getData should get data in the right type', () => {
        setupConnections();

        _httpservice.getData('api/link', User).subscribe(res => {
            expect(res).not.toBeNull();
            expect(res instanceof User).toBe(true);
            expect(res.id).toBe('12345');
        });
    });

    it('getData should call Http.get', () => {
        let responseData = { _body: 12345 };
        (<any>_httpservice).get = () => Observable.of(responseData);

        setupConnections();

        _httpservice.getData('api/link').subscribe(res => {
            expect(res).toBe(12345);
        });
    });

    it('getData should return json if type is not specified', () => {
        let responseData = setupConnections();
        _httpservice.getData('api/link').subscribe(res => {
            expect(res).toBe(responseData);
            expect(res instanceof Object).toBe(true);
            expect((<any>res).id).toBe('12345');
        });
    });

    it('getData should return response body if not json formatted', () => {
        setupConnections(12345);

        _httpservice.getData('api/link').subscribe(res => {
            expect(res).toBe(12345);
        });
    });

    it('getData should return empty if respond body is empty', () => {
        setupConnections('');

        _httpservice.getData('api/link').subscribe(res => {
            expect(res).toBeFalsy();
        });
    });

    it('service should throw exception if api failed', () => {
        setupConnections('', 500);

        _httpservice.getData('api/link').catch(err => {
            expect(err).toBe('Internal server error');
            return Observable.throw(err);
        });
    });

    it('getMultiple should get an array', () => {
        setupConnections([ {id: '12345'} ]);

        _httpservice.getMultiple('api/link', User).subscribe(res => {
            expect(res).not.toBeNull();
            expect(_.isArray(res)).toBe(true);
            expect(res[0] instanceof User).toBe(true);
            expect(res[0].id).toBe('12345');
        });
    });

    it('getMultiple should call Http.get', () => {
        let responseData = { _body: [12345] };
        (<any>_httpservice).get = () => Observable.of(responseData);

        setupConnections();

        _httpservice.getMultiple('api/link').subscribe(res => {
            expect(res).toBe(responseData._body);
        });
    });

    it('PostData should return the right data type', () => {
        setupConnections();

        _httpservice.postData('api/link', {id: 1}, User).subscribe(res => {
            expect(res).not.toBeNull();
            expect(res instanceof User).toBe(true);
            expect(res.id).toBe('12345');
        });
    });

    it('postData should call Http.post', () => {
        let responseData = { _body: 12345 };
        (<any>_httpservice).post = () => Observable.of(responseData);

        setupConnections();

        _httpservice.postData('api/link', {}).subscribe(res => {
            expect(res).toBe(12345);
        });
    });

    it('DeleteData should return the right data type', () => {
        setupConnections();

        _httpservice.deleteData('api/link', User).subscribe(res => {
            expect(res).not.toBeNull();
            expect(res instanceof User).toBe(true);
            expect(res.id).toBe('12345');
        });
    });

    it('deleteData should call Http.delete', () => {
        let responseData = { _body: 12345 };
        (<any>_httpservice).delete = () => Observable.of(responseData);

        setupConnections();

        _httpservice.deleteData('api/link').subscribe(res => {
            expect(res).toBe(12345);
        });
    });

    it('PutData should return the right data type', () => {
        setupConnections();

        _httpservice.putData('api/link', {id: 1}, User).subscribe(res => {
            expect(res).not.toBeNull();
            expect(res instanceof User).toBe(true);
            expect(res.id).toBe('12345');
        });
    });

    it('putData should call Http.put', () => {
        let responseData = { _body: 12345 };
        (<any>_httpservice).put = () => Observable.of(responseData);

        setupConnections();

        _httpservice.putData('api/link', {}).subscribe(res => {
            expect(res).toBe(12345);
        });
    });
});
