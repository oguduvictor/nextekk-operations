import { EmployeeManagerGuardService } from './employeeManagerGuard.service';
import { TestBed } from '@angular/core/testing';
import { User } from '../models/user.model';
import { UserService } from './user.service';
import { AuthService } from '../widgets/auth/auth.service';
import { NotificationService } from './notification.service';
import { Observable } from 'rxjs/Observable';

describe('EmployeeManagerGuardService', () => {
    let _user = new User();
    let _employeeManagerGuardService: EmployeeManagerGuardService;
    let _authService: AuthService = <any>{
        redirectToHome: jasmine.createSpy('redirectToHome')
    };

    let _mockUserService;

    beforeEach(() => {
        _user = <User>{
            isEmployee: () => false,
            isManager: () => false
        };

        _mockUserService = {
            getCurrentUser: jasmine.createSpy('getCurrentUser').and.returnValue(Observable.of(_user))
        };

        TestBed.configureTestingModule({
            providers: [
                EmployeeManagerGuardService,
                { provide: AuthService, useValue: _authService },
                { provide: NotificationService, useValue: { showError: function () { } } },
                {
                    provide: UserService, useValue: _mockUserService
                }
            ]
        });

        _employeeManagerGuardService = TestBed.get(EmployeeManagerGuardService);
    });

    it('canActivate() should return true if logged in user is an Manager', () => {
        _user.isManager = () => true;

        _employeeManagerGuardService.canActivate()
            .subscribe(x => {
                expect(x).toBe(true);
            });

    });

    it('canActivate() should return false if currrent logged in user is an Employee', () => {
        _user.isEmployee = () => true;

        _employeeManagerGuardService.canActivate()
            .subscribe(x => {
                expect(x).toBe(true);
            });
    });

    it('canActivate() return false if current logged in user is neither an Employee or Manager', () => {
        _employeeManagerGuardService.canActivate()
            .subscribe(x => {
                expect(x).toBe(false);
            });
    });

    it('canActivate() gets current logged in user from user service', () => {

        _employeeManagerGuardService.canActivate();

        expect(_mockUserService.getCurrentUser).toHaveBeenCalledTimes(1);
    });
})
