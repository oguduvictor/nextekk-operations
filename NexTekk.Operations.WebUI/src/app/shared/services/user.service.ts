import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user.model';
import { HttpService } from './http.service';
import { AuthService } from '../widgets/auth/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class UserService implements Resolve<User> {
    private _users: User[];

    constructor(
        private _httpService: HttpService,
        private _authService: AuthService
    ) { }

    getCurrentUser(): Observable<User> {
        let userJson = this._authService.getUserInfo();

        if (userJson) {
            let user = new User();
            user.fromDto(JSON.parse(userJson));
            return Observable.of(user);
        }

        return this.fetchLoggedInUser();
    }

    isUserManager(): Observable<boolean> {
        return this.getCurrentUser().map(user => user.isManager());
    }

    getAllUsers(): Observable<User[]> {
        if (this._users) {
            return Observable.of(this._users);
        }

        return this.fetchAllUsers();
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
        if (!this._authService.isAuthenticated) {
            return Observable.of(new User());
        }

        return this.getCurrentUser();
    }

    private fetchLoggedInUser(): Observable<User> {
        return this._httpService.getData('api/user/getLoggedInUser', User)
            .map(user => {
                this._authService.setUserInfo(user);
                return user;
            });
    }

    private fetchAllUsers(): Observable<User[]> {
        return this._httpService.getMultiple('api/user/getAllUsers', User)
            .map(users => {
                this._users = users;
                return this._users;
            });
    }
}
