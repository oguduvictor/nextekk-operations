import { Injectable, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class NotificationService {
    constructor(public _toasterService: ToastsManager) { }

    showError(message: string, title?: string) {
        this._toasterService.error(message, title);
    }

    showInfo(message: string, title?: string) {
        this._toasterService.info(message, title);
    }

    showWarning(message: string, title?: string) {
        this._toasterService.warning(message, title);
    }

    showSuccess(message: string, title?: string) {
        this._toasterService.success(message, title);
    }

    /**
     * This needs to be set before any of the toastr can be called
     * @param vcr
     */
    setRootViewContainerRef(vcr: ViewContainerRef) {
        this._toasterService.setRootViewContainerRef(vcr);
    }
}
