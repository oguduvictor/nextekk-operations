import { TestBed } from '@angular/core/testing';
import { AuthService } from '../widgets/auth/auth.service';
import { ManagerGuardService } from '../services/managerGuard.service';
import { NotificationService } from '../services/notification.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Observable } from 'rxjs/Observable';

describe('ManagerGuardService', () => {
    let _user = new User();
    let _managerGuardService;
    let _authService: AuthService = <any>{
        redirectToHome: jasmine.createSpy('redirectToHome')
    };

    _user.roles = ['Manager'];

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ManagerGuardService,
                { provide: AuthService, useValue: _authService },
                { provide: NotificationService, useValue: {showError: function() {}}},
                { provide: UserService, useValue: {getCurrentUser: function() {
                    return Observable.of(_user);
                }}}
            ]
        });

        _managerGuardService = TestBed.get(ManagerGuardService);
    });

    it('should instantiate succesfully', () => {
        expect(_managerGuardService).toBeTruthy();
    });

    it('canActivate should get the current user', () => {
        _managerGuardService.canActivate().subscribe(res => {
            expect(res).toBe(true);
        });
    });

    it('should not redirect if user is a manager', () => {
        _managerGuardService.canActivate().subscribe(res => {
            expect(res).toBe(true);
            expect(_authService.redirectToHome).not.toHaveBeenCalled();
        });
    });

    it('should redirect if user is not a manager', () => {
        _user.roles = ['Employee'];

        _managerGuardService.canActivate().subscribe(res => {
            expect(res).toBe(false);
            expect(_authService.redirectToHome).toHaveBeenCalled();
        });
    });
});
