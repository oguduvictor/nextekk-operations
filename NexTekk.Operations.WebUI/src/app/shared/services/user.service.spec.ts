import { UserService } from './user.service';
import { User } from '../models/user.model';
import { HttpService } from './http.service';
import { AuthService } from '../widgets/auth/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

describe('UserService', () => {
    let _userService: UserService;
    let _mockHttpService: HttpService;
    let _mockAuthService: AuthService;
    let _user: User;

    beforeEach(() => {
        _user = <User>{
            id: '1',
            firstName: 'Joe',
            lastName: 'Blow',
            email: 'joe@doe.com',
            roles: ['developer']
        };

        _mockHttpService = <any>{
            getData: jasmine.createSpy('getData').and.returnValue(Observable.of(_user)),
            getMultiple: jasmine.createSpy('getMultiple').and.returnValue(Observable.of([_user]))
        };

        _mockAuthService = <any>{
            getUserInfo: () => JSON.stringify(_user),
            setUserInfo: () => {},
            isAuthenticated: true
        };

        _userService = new UserService(_mockHttpService, _mockAuthService);
    });

    it('can be instantiated successfully', () => {
        expect(_userService).toBeTruthy();
    });

    it('can get current user from auth service', () => {
        _userService.getCurrentUser().subscribe(data => {
            expect(data.firstName === 'Joe');
            expect(data.roles === ['developer']);
        });

        expect(_mockHttpService.getData).not.toHaveBeenCalled();
    });

    it('can get current user from http service', () => {
        (<any>_userService)._authService.getUserInfo = () => null;

        _userService.getCurrentUser().subscribe(data => {
            expect(data.firstName === 'Joes');
        });

        expect(_mockHttpService.getData).toHaveBeenCalledWith('api/user/getLoggedInUser', User);
    });

    it('should return false if current user is not a manager', () => {
        _userService.isUserManager().subscribe(x => {
            expect(x).toBe(false);
        });
    });

    it('should return true if current user is a manager', () => {
        _userService.getCurrentUser = () => {
            let user = new User();
            user.roles = ['Manager'];
            return Observable.of(user);
        };

        _userService.isUserManager().subscribe(x => {
            expect(x).toBe(true);
        });
    });

    it('can get all users from http service', () => {
        let users = (<any>_userService)._users;
        expect(users).toBeUndefined();

        _userService.getAllUsers().subscribe(data => {
            expect(data.length).toEqual(1);
        });

        users = (<any>_userService)._users;

        expect(users.length).toEqual(1);

        expect(_mockHttpService.getMultiple).toHaveBeenCalledWith('api/user/getAllUsers', User);
    });

    it('can get all saved cached users', () => {
        (<any>_userService)._users = [_user];

        _userService.getAllUsers().subscribe(data => {
            expect(data.length).toBe(1);
        });

        expect(_mockHttpService.getMultiple).not.toHaveBeenCalled();
    });

    it('resolve returns current user if user is authenticated', () => {
        let route = <any>{};
        let state = <any>{};

        _userService.resolve(route, state).subscribe(data => {
            expect(data.firstName).toEqual(_user.firstName);
            expect(data.lastName).toEqual(_user.lastName);
            expect(data.id).toEqual(_user.id);
        });
    });

    it('resolve should return new user if user is not authenticated', () => {
        (<any>_userService)._authService.isAuthenticated = false;

        _userService.resolve(<any>{}, <any>{}).subscribe(data => {
            expect(data.id).toBeNull();
            expect(data.firstName).toBeNull();
            expect(data.firstName).toBeNull();
        });
    });
});
