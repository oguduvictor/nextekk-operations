import { CamelSeparatePipe} from './camelSeparate.pipe';

describe('CamelSeparatePipe', () => {
    let filter: CamelSeparatePipe;

    beforeEach(() => {
        filter = new CamelSeparatePipe();
    });

    it('should return a space seperated string', () => {
        let result =  filter.transform('inProgress');

        expect(result).toBe('In Progress');
    });

    it('should return passed value when passed a null or empty string', () => {
        expect (filter.transform('')).toBe('');
        expect (filter.transform(null)).toBe(null);
    });

    it('should capitalize the first letter when all lowercase string is passed', () => {
        let result =  filter.transform('inprogress');

        expect(result).toBe('Inprogress');
    });

    it('should return the string if its all uppercase', () => {
        let result =  filter.transform('EMEKA');

        expect(result).toBe('EMEKA');
    });

    it('should return a space separated words when a pascalCased string is passed', () => {
        let result =  filter.transform('NextekkIsAwesome');

        expect(result).toBe('Nextekk Is Awesome');
    });

    it('should capitalize the letter when a lowercase letter is passed', () => {
        let result =  filter.transform('i');

        expect(result).toBe('I');
    });

});
