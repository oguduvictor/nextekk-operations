import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({ name: 'numberFormat' })
export class NumberPipe extends DecimalPipe implements PipeTransform {
    transform(value: any, digits?: string): string | null {
        const transformedValue = super.transform(Math.abs(value));

        if (isNaN(value)) {
            return value;
        }

        if (value >= 0 ) {
            return transformedValue;
        }

        return `(${transformedValue})`;
    }
}
