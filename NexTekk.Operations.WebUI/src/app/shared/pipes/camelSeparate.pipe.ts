import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'camelSeparate' })
export class CamelSeparatePipe implements PipeTransform {
    transform(source: string): string {

        if (!source || source === source.toUpperCase()) {
            return source;
        }

        let newWord = source[0].toUpperCase();

        for ( let i = 1; i < source.length; i++) {
            let thisChar = source.charAt(i);

            if (thisChar === thisChar.toUpperCase()) {
                newWord += ' ';
                newWord += thisChar;
                continue;
            }
            newWord += thisChar;
        }
        return newWord;
    }
}
