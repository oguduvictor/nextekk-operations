import { Pipe, PipeTransform } from '@angular/core';
import * as  _ from 'lodash';

@Pipe({
    name: 'orderBy',
    pure: false
})
export class OrderByPipe implements PipeTransform {
    transform<T>(source: T[], field: any, nestedField: any = null): T[] {
        let result: any[];

        if (!field || !source || source.length <= 1 ) {
            return source;
        }

        if (nestedField) {
            result = <any> _.orderBy(source, item => item[field][nestedField]);

        } else {
            result = <any>_.orderBy(source, [field]);
        }

        return <T[]>result;
    }
}
