import { OrderByPipe } from './orderBy.pipe';

describe('OrderByPipe', () => {

    let filter = new OrderByPipe();
    let employees = [
            {id: 'id0', firstName: 'joe', lastName: 'emeka'},
            {id: 'id1', firstName: 'joe', lastName: 'emeka'},
            {id: 'id1', firstName: 'jane', lastName: 'emmanuel'},
            {id: 'id2', firstName: 'jude', lastName: 'ogudu'},
            {id: 'id2', firstName: 'judy', lastName: 'moses'}
        ];

    let projects = [
        {id: 'id1', title: 'TimeTracking'},
        {id: 'id2', title: 'Testing'},
        {id: 'id2', title: 'clientManagement'}
    ];

    let vacations = [
        {id: 'id1', employee: {id: 'id1', firstName: 'joe', lastName: 'emeka'}, hoursPerPeriod: 5, yearBeginBalance: 10 },
        {id: 'id2', employee: {id: 'id1', firstName: 'jane', lastName: 'emmanuel'}, hoursPerPeriod: 5, yearBeginBalance: 10 },
        {id: 'id3', employee: {id: 'id2', firstName: 'judy', lastName: 'moses'}, hoursPerPeriod: 5, yearBeginBalance: 10 }
    ];

    it('orders employees base on firstName', () => {
        let result = filter.transform(employees, 'firstName');

        expect(result instanceof Array).toBe(true);
        expect(result[0].firstName).toBe('jane');
    });

    it('orders lowercase last', () => {
        let result = filter.transform(projects, 'title');

        expect(result instanceof Array).toBe(true);
        expect(result[2].title).toBe('clientManagement');
    });

    it('orders vacations based on employee firstname', () => {
        let result  = filter.transform(vacations, 'employee', 'firstName');

        expect(result instanceof Array).toBe(true);
        expect(result[0].employee.firstName).toBe('jane');
        expect(result[0].id).toBe('id2');
    });
});
