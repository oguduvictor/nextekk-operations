import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({ name: 'filterBy' })
export class FilterByPipe implements PipeTransform {
    transform<T>(source: T[], filterByQuery: any): T[] {
        if (_.isEmpty(filterByQuery)) {
            return source;
        }

        let query: any = {};
        let userProjects: any;

        // rewrite the query in order to eliminate undefined from the query
        for (let key in filterByQuery) {
            if (filterByQuery[key] === undefined || filterByQuery[key] === 'undefined') {
                continue;
            }

            query[key] = filterByQuery[key];
        }

        if (_.isEmpty(query)) {
            return source;
        }

        return source.filter((item: T) => {
            let result = true;

            for (let key in query) {
                let value = query[key];
                let itemValue = item[key];

                if (itemValue === value) {
                    continue;
                }

                if (_.isArray(itemValue) && itemValue.indexOf(value) > -1) {
                    continue;
                }

                result = false;
                break;
            }

            return result;
        });
    }
}
