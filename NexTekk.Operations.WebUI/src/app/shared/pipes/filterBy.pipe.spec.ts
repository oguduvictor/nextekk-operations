import {FilterByPipe} from './filterBy.pipe';

describe('FilterBy', () => {
    let filter = new FilterByPipe();
    let timeEntries = [
            {id: 'id1', name: 'joe', interests: ['sports', 'music', 'driving']},
            {id: 'id1', name: 'jane', interests: ['music', 'driving']},
            {id: 'id2', name: 'jude', interests: ['sports']},
            {id: 'id2', name: 'judy', interests: ['music']}
        ];

    it('filters elements in an array by specified parameters', () => {
        let result = filter.transform(timeEntries, {id: 'id1'});

        expect(result instanceof Array).toBe(true);
        expect(result.length).toBe(2);
    });

    it('filters based on multiple fields', () => {
        let result = filter.transform(timeEntries, {id: 'id1', name: 'jane'});

        expect(result instanceof Array).toBe(true);
        expect(result.length).toBe(1);
    });

    it('returns all items if filtered by undefined values', () => {
        let result = filter.transform(timeEntries, {id: undefined, name: undefined});

        expect(result instanceof Array).toBe(true);
        expect(result.length).toBe(4);
    });

    it('returns items based on values that are not undefined', () => {
        let result = filter.transform(timeEntries, {id: 'id1', name: undefined});

        expect(result instanceof Array).toBe(true);
        expect(result.length).toBe(2);
    });

    it('should be able to filter on array fields', () => {
        let musicResult = filter.transform(timeEntries, {interests: 'music'});
        let sportsResult = filter.transform(timeEntries, {interests: 'sports'});
        let sportsAndId2Result = filter.transform(timeEntries, {id: 'id2', interests: 'sports'});

        expect(musicResult.length).toBe(3);
        expect(sportsResult.length).toBe(2);
        expect(sportsAndId2Result.length).toBe(1);
    });
});
