export interface IValueDisplay {
    value: string;
    display: string;
}
