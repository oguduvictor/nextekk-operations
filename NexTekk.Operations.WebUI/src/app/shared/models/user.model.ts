import * as _ from 'lodash';

export class User {
    private MANAGER_ROLE = 'Manager';
    private EMPLOYEE_ROLE = 'Employee';

    constructor(
        public id: string = null,
        public firstName: string = null,
        public lastName: string = null,
        public email: string = null,
        public roles: string[] = null
    ) {}

    get fullName(): string {
        if (!this.firstName || !this.lastName) {
            return undefined;
        }

        return `${this.firstName} ${this.lastName}`.trim();
    }

    isManager() {
        if (_.isEmpty(this.roles)) {
            return false;
        }

        return this.roles.indexOf(this.MANAGER_ROLE) > -1;
    }

    isEmployee() {
        if (_.isEmpty(this.roles)) {
            return false;
        }

        return this.roles.indexOf(this.EMPLOYEE_ROLE) > -1;
    }

    fromDto(dto: any) {
        this.id = dto.id;
        this.firstName = dto.firstName;
        this.lastName = dto.lastName;
        this.email = dto.email;
        this.roles = dto.roles;
    }
}
