import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ClientComponent } from './clients.component';
import { Client } from '../common/models/client.model';
import { Contact } from '../common/models/contact.model';
import { Project } from '../common/models/project.model';
import { ClientContact } from '../common/models/clientContact.model';
import { TeamModalComponent } from '../common/widgets/teamMembersModal/teamMemberModal.component';
import { ProjectDataService } from '../common/services/projectData.service';
import { SpinnerService, NotificationService, UserService, User, ModalComponent,
         DialogService, CamelSeparatePipe, FilterByPipe } from '../../shared';
import { ClientModalComponent } from '../common/widgets/clientModalComponent/clientModal.component';
import { ContactPersonModalComponent } from '../common/widgets/contactPersonModal/contactPersonModal.component';
import { ProjectEntryComponent } from '../common/widgets/projectEntryComponent/projectEntry.component';
import { Observable } from 'rxjs/Observable';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ProjectModule } from '../project.module';

describe('ClientComponent', () => {
    let _clientComp: ClientComponent;
    let _fixture: ComponentFixture<ClientComponent>;
    let _mockSpinnerService: SpinnerService;
    let clientModal: ClientModalComponent;
    let contactModal: ContactPersonModalComponent;
    let _mockDataService;
    let mockUserService = {
        getAllUsers: function () {
            return Observable.of([new User()]);
        }
    };

    beforeEach(() => {
        _mockSpinnerService = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);

        _mockDataService = {
            getClient: (clientId: string) => {
                return Observable.of(new Client(clientId, 'Microsoft'));
            },
            getContactSummaries: jasmine.createSpy('getContactSummaries').and
                .returnValue(Observable.of(
                    [
                        new Contact(1, 'Joe', 'Blow'),
                        new Contact(2, 'Amy', 'Suns'),
                        new Contact(3, 'Luci', 'Williams')
                    ]
                )),

            getClients: jasmine.createSpy('getClients').and.returnValue(Observable.of(
                [
                    <Client>{
                        name: 'client1',
                        id: 'id1',
                        contacts: [
                            new Contact(1, 'Joe', 'Blow'),
                            new Contact(2, 'Amy', 'Suns'),
                            new Contact(3, 'Luci', 'Williams')
                        ]
                    },
                    <Client>{
                        name: 'client2',
                        id: 'id2',
                        contacts: [
                            new Contact(1, 'Joe', 'Blow'),
                            new Contact(3, 'Luci', 'Williams')
                        ]
                    }
                ]
            )),

            getAllProjects: function () {
                return Observable.of([new Project()]);
            },

            getContacts: function () {
                return Observable.of([new ClientContact]);
            }
        };

        TestBed.configureTestingModule({
            imports: [ProjectModule, RouterTestingModule.withRoutes([]), ToastModule.forRoot()],
            providers: [
                DialogService,
                NotificationService,
                { provide: ProjectDataService, useValue: _mockDataService },
                { provide: SpinnerService, useValue: _mockSpinnerService },
                { provide: UserService, useValue: mockUserService }
            ]
        });

        _fixture = TestBed.createComponent(ClientComponent);
        _clientComp = <ClientComponent>_fixture.debugElement.componentInstance;
        clientModal = (<any>_clientComp)._clientModal;
        contactModal = (<any>_clientComp)._contactModal;

    });

    it('can be successfully instantiated', () => {
        expect(_clientComp).toBeTruthy();
    });

    it('fetches all clients during initialization', () => {
        expect(_clientComp.clients).toBeDefined();

        _clientComp.ngOnInit();

        expect(_clientComp.clients.length).toEqual(2);
    });

    it('calls api to fetch client during initialization', () => {
        _clientComp.ngOnInit();

        expect(_mockDataService.getClients).toHaveBeenCalled();
    });

    it('shows spinner service when being initialized', () => {
        _clientComp.ngOnInit();

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('hides spinner service when being initialized', () => {
        _clientComp.ngOnInit();

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('can display modal for client details ', () => {
        let client = new Client('1');

        spyOn(clientModal, 'showById');

        _clientComp.displayClient(client);

        expect(clientModal.showById).toHaveBeenCalledWith(client.id);
    });

    it('can display modal for new client', () => {
        let client = new Client();

        spyOn(clientModal, 'show');

        _clientComp.openNewClientModal();

        expect(clientModal.show).toHaveBeenCalledWith(client);
    });

    it('should add client when onClientSaved() is called', () => {
        let client = new Client();

        _clientComp.ngOnInit();

        expect(_clientComp.clients.length).toEqual(2);
        _clientComp.onClientSaved(client);

        expect(_clientComp.clients.length).toBe(3);
    });

    it('shows spinner service when client is saved', () => {
        _clientComp.onClientSaved(new Client());

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('hides spinner service when client is saved', () => {
        _clientComp.onClientSaved(new Client());

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('should remove client when onDeleted() is called', () => {

        _clientComp.ngOnInit();
        expect(_clientComp.clients.length).toEqual(2);

        let client = _clientComp.clients[0];

        _clientComp.onClientDeleted(client);

        expect(_clientComp.clients.length).toBe(1);
    });

    it('can call displayContact', () => {
        let contact = new Contact(1, 'Ajayi', 'Francis');

        spyOn(contactModal, 'showById');

        _clientComp.displayContact(contact);

        expect(contactModal.showById).toHaveBeenCalledWith(contact.id);
    });

    it('gets all contacts summaries when initialized', () => {
        _clientComp.ngOnInit();

        expect(_mockDataService.getContactSummaries).toHaveBeenCalled();
    });

    it('should updated contact list after a contact is edited', () => {
        let client  = new Client();
        let contact = new Contact(1);
        client.contacts = [contact];
        contact.firstName = 'Emeka';
        let contact2 = new Contact(1, 'Emeka');
        _clientComp.clients = [client];

        _clientComp.onContactSaved(contact2);

        expect(client.contacts[0].firstName).toBe('Emeka');
    });

    it('should removed contact from contactlist after contact has been deleted', () => {
        let client  = new Client();
        let contact = new Contact(1);
        client.contacts = [contact];
        _clientComp.clients = [client];

        _clientComp.onContactDeleted(contact);

        expect(client.contacts.length).toBe(0);
    });

    it('should should update an existing client after it is saved', () => {
        let client  = new Client('1');
        let client2 = new Client('1', 'Microsoft');
        _clientComp.clients = [client];

        _clientComp.onClientSaved(client2);

        expect(_clientComp.clients[0].name).toBe('Microsoft');
    });

});
