import { Component, ViewChild, OnInit } from '@angular/core';
import { Client } from '../common/models/client.model';
import { Project } from '../common/models/project.model';
import { ProjectDataService } from '../common/services/projectData.service';
import { SpinnerService, ModalComponent } from '../../shared';
import { ClientModalComponent } from '../common/widgets/clientModalComponent/clientModal.component';
import { ContactPersonModalComponent } from '../common/widgets/contactPersonModal/contactPersonModal.component';
import { Contact } from '../common/models/contact.model';
import { ProjectEntryComponent } from '../common/widgets/projectEntryComponent/projectEntry.component';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

@Component({
    selector: 'app-client-component',
    templateUrl: './clients.component.html'
})
export class ClientComponent implements OnInit {
    selectedClient: Client;
    clients: Client[] = [];
    projects: Project[];
    allContacts: Contact[] = [];

    selectedContactId: number;

    canAddContact = false;

    private _projects: Project[];

    @ViewChild(ClientModalComponent)
    private _clientModal: ClientModalComponent;

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild(ProjectEntryComponent)
    private _projectModal: ProjectEntryComponent;

    @ViewChild(ContactPersonModalComponent)
    private _contactModal: ContactPersonModalComponent;

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }

        return this.form.valid;
    }

    constructor(
        private _projectDataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _route: ActivatedRoute
    ) {
        let contactId = _route.snapshot.params['id'];
        this.selectedContactId = !(contactId) ? undefined : Number(contactId);
    }

    displayClient(clientEntry: Client): void {
        this._clientModal.showById(clientEntry.id);
    }

    openNewClientModal(): void {
        this._clientModal.show(new Client());
    }

    displayContact(contactEntry: Contact) {
        this._contactModal.showById(contactEntry.id);
    }

    onContactSaved(contact: Contact): void {
        this.clients.forEach(client => {
            let existingContact = client.contacts.find( x => x.id === contact.id);
            if (existingContact) {
                _.merge(existingContact, contact);
            }
        });
    }

    onContactDeleted(contact: Contact): void {
        this.clients.forEach(client => {
            _.remove(client.contacts, x => x.id === contact.id);
        });
    }

    onClientSaved(client: Client) {
        let existingClient = this.clients.find(x => x.id === client.id);

        if (existingClient) {
            _.merge(existingClient, client);
            existingClient.contacts = client.contacts;
            return;
        }

        this._spinnerService.show();

        this._projectDataService.getClient(client.id).subscribe(x => {
            this._spinnerService.hide();
            this.clients.push(x);
        });
    }

    onClientDeleted(client: Client) {
        _.remove(this.clients, x => x.id === client.id);
    }

    ngOnInit() {
        this._spinnerService.show();
        this._projectDataService.getClients().subscribe(x => {
            this.clients = x;
        },
        err => {
            this._spinnerService.hide();
        });

        this._projectDataService.getContactSummaries().subscribe(x => {
            this._spinnerService.hide();
            this.allContacts = x;
        },
        err => {
            this._spinnerService.hide();
        });
    }
}
