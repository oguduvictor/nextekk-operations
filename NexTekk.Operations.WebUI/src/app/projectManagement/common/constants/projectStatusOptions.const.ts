export class ProjectStatusOptions {
    static inProgress = 'inProgress';
    static completed = 'completed';
    static notStarted = 'notStarted';
    static suspended = 'suspended';
}
