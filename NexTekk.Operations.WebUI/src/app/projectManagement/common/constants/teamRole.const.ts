export class TeamRole {
    static developer = 'developer';
    static architect = 'architect';
    static qaAnalyst = 'qaAnalyst';
    static businessAnalyst = 'businessAnalyst';
    static projectManager = 'projectManager';
}
