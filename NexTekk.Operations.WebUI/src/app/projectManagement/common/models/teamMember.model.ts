import { User } from '../../../shared';

export class Team {
    constructor(
        public member: User = null,
        public memberId: string = null,
        public role: string = null
    ) {
        this.member = new User();
    }

    fromDto(dto: any): void {
        let member  = new User();
        member.fromDto(dto.user);
        this.member = member;
        this.memberId = dto.user.id;
        this.role = dto.role;
    }

    toDto(id: string): any {
        let dto = <any>{};
        dto.userId = this.memberId;
        dto.role = this.role;
        dto.projectId = id;

        return dto;
    }
}
