import {User} from '../../../shared/models/user.model';
import { DateTimeHelper } from '../../../shared';
import { Client } from '../models/client.model';

export class Contact {
    constructor(
        public id: number = null,
        public firstName: string = null,
        public lastName: string = null,
        public email: string = null,
        public phoneNumber1: string = null,
        public phoneNumber2: string = null,
        public createdDate = null,
        public createdBy: string = null,
        public creator: User = null,
        public editorId: string = null,
        public editor: User = null,
        public clients: Client[] = []
    ) {
        this.creator = new User();
     }

    get fullName(): string {
        return this.firstName + ' ' + this.lastName;
    }

    get phoneNumbers(): string {
        if (!this.phoneNumber1 || !this.phoneNumber2) {
            return this.phoneNumber1 || this.phoneNumber2;
        }
        return this.phoneNumber1 + ', ' + this.phoneNumber2;
    }

    fromDto(dto: any) {
        this.id = dto.id;
        this.firstName = dto.firstName;
        this.lastName = dto.lastName;
        this.email = dto.email;
        this.phoneNumber1 = dto.phoneNumber1;
        this.phoneNumber2 = dto.phoneNumber2;
        this.createdDate = DateTimeHelper.shortDate(dto.created);
        this.createdBy = dto.creatorId; // Todo get the name of the creator;

        if (dto.creator) {
            this.creator = new User(dto.creator.id, dto.creator.firstName, dto.creator.lastName);
            this.editor = dto.editor;
        }
        this.clients = dto.clients;
    }

    toDto(): any {
        let dto = <any>{};

        dto.id = this.id;
        dto.firstName = this.firstName;
        dto.lastName = this.lastName;
        dto.email = this.email;
        dto.phoneNumber1 = this.phoneNumber1;
        dto.phoneNumber2 = this.phoneNumber2;

        return dto;
    }
}
