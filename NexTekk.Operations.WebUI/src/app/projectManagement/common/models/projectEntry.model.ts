import { ProjectDataService } from '../services/projectData.service';
import { Project } from './project.model';
import { Contact } from './contact.model';
import { SpinnerService, User, UserService } from '../../../shared';
import { Client } from '../models/client.model';
import * as _ from 'lodash';

export class ProjectEntry {
    allClients: Client[] = [];
    managers: User[];
    contacts: Contact[] = [];
    projects: Project[] = [];
    users: User[] = [];

    constructor(
        private _dataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _userService: UserService
    ) {
        this.loadData();
    }

    addProject(project: Project): void {
        let existingProject = this.projects.find(x => x.id === project.id);

        if (existingProject) {
            _.merge(existingProject, project);
            return;
        }
        this.projects.push(project);
    }

    getProjects(): void {
        this._spinnerService.show();
        this._dataService.getAllProjects().subscribe(data => {
            this.projects = data;
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
        });
    }

    removeProject(project: Project): void {
        _.remove(this.projects, x => {
            return (x.id === project.id);
        });
    }

    getManagers(): User[] {
        let managers: User[] = [];
        this.projects.forEach(x => {
            if (x.projectManager) {
                managers.push(x.projectManager);
            }
        });
        return _.uniqBy(managers, 'id');
    }

    getProject(id: string): Project {
        return this.projects.find(x => x.id === id);
    }

    private clients(): void {
        this._spinnerService.show();
        this._dataService.getClientSummaries().subscribe(clients => {
            this.allClients = clients;
            this._spinnerService.hide();
        },
        err => {
            this._spinnerService.hide();
        });
    }

    private loadData(): void {
        this.getProjects();
        this.getAllUsers();
        this.clients();
    }

    private getAllUsers(): void {
        this._spinnerService.show();
        this._userService.getAllUsers().subscribe(users => {
            this.users = users;
            this._spinnerService.hide();
        }, err => {
            this._spinnerService.hide();
        });
    }
}
