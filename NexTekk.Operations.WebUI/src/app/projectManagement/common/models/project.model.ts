import { DateTimeHelper, User } from '../../../shared';
import { ProjectStatusOptions } from '../constants/projectStatusOptions.const';
import { Client } from './client.model';
import { Team } from './teamMember.model';
import { TeamRole } from '../../common/constants/teamRole.const';
import * as _ from 'lodash';

export class Project {
    constructor(
        public id: string = null,
        public title: string = null,
        public client: Client = null,
        public clientId: string = null,
        public status: string = null,
        public created: String = null,
        public createdBy: User = null,
        public teamMembers: Team[] = null,
        public projectManager: User = null

    ) {
        this.status = ProjectStatusOptions.notStarted;
        this.client = new Client();
        this.teamMembers = [];
    }

    get projectManagerId(): any {
        if (this.projectManager) {
            return this.projectManager.id;
        }

        return null;
    }

    get members(): User[] {
        return this.teamMembers.map(x => x.member);
    }

    get membersIds(): any {
        let ids = this.teamMembers.map(x => x.memberId);

        if (ids.length > 0) {
            return ids;
        }

        return null;
    }

    get mbrIds(): string[] {
        return this.teamMembers.map(x => x.memberId);
    }

    addTeamMember(member: Team): void {
        let existingMember = this.teamMembers.find(x => x.memberId === member.memberId);
        if (!existingMember) {
            this.teamMembers.push(member);
        }
    }

    removeTeamMember(memberid: string): void {
        _.remove(this.teamMembers, x => {
            return x.memberId === memberid;
        });
    }

    fromDto(dto: any): void {
        this.id = dto.id;
        this.title = dto.title;
        this.status = dto.status;
        this.created = DateTimeHelper.format(dto.created, 'yyyy-MM-dd');

        dto.teamMembers.forEach(x => {
            let team = new Team();
            team.fromDto(x);
            if (team.role !== TeamRole.projectManager) {
                this.teamMembers.push(team);
            } else {
                this.projectManager = team.member;
            }

        });

        if (dto.client) {
            this.client = new Client(dto.client.id, dto.client.name);
            this.clientId = this.client.id;
        }

        if (dto.creator) {
            this.createdBy = new User();
            this.createdBy.fromDto(dto.creator);
        }
    }

    toDto(): any {
        let dto = <any>{};
        dto.id = this.id;
        dto.title = this.title;
        dto.status = this.status;
        dto.clientId = this.client.id;

        return dto;
    }

}
