export class ClientContact {
    constructor(
        public clientId: string = null,
        public contactId: number = null
    ) { }

    fromDto(dto: any) {
        this.clientId = dto.clientId;
        this.contactId = dto.contactId;
    }
}
