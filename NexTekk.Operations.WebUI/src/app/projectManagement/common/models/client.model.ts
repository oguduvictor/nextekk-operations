import { Contact } from './contact.model';
import { Project } from './project.model';
import { User, DateTimeHelper } from '../../../shared';

export class Client {
    constructor(
        public id: string = null,
        public name: string = null,
        public createdBy: string = null,
        public createdDate: string = null,
        public editedDate: string = null,
        public creator: User = null,
        public contacts: Contact[] = null,
        public projects: Project[] = null
    ) {
        this.creator = new User();
        this.contacts = [];
        this.projects = [];
    }

    fromDto(dto: any) {
        this.id = dto.id;
        this.name = dto.name;
        this.createdBy = dto.creatorId;
        this.createdDate = DateTimeHelper.format(dto.created, 'yyyy-MM-dd');
        this.editedDate = DateTimeHelper.format(dto.edited, 'yyyy-MM-dd');
        if (dto.creator) {
            this.creator = new User();
            this.creator.fromDto(dto.creator);
        }

        if (dto.contacts) {
            dto.contacts.forEach(x => {
                let contact = new Contact();
                contact.fromDto(x);
                this.contacts.push(contact);
            });
        }

        if (dto.projects) {
            dto.projects.forEach(x => {
                let project = new Project();
                project.fromDto(x);
                this.projects.push(project);
            });
        }

    }

    get contactIds(): number[] {
        return this.contacts.map(x => x.id);
    }

    toDto(): any {
        let dto = <any>{};

        dto.id = this.id;
        dto.name = this.name;
        dto.contactIds = [];
        this.contacts.forEach(contact => {
            dto.contactIds.push(contact.id);
        });

        return dto;
    }
}
