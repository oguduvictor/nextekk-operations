import { ProjectEntry } from './projectEntry.model';
import { ProjectDataService } from '../services/projectData.service';
import { Project } from './project.model';
import { SpinnerService, User, UserService } from '../../../shared';
import { Team } from '../models/teamMember.model';
import { Client } from '../models/client.model';
import { Observable } from 'rxjs/Observable';

describe('ProjectEntry', () => {
    let userService = {
        getAllUsers: function() {
            return Observable.of([new User()]);
        }
    };

    let mockDataService = <any> {
        _projectApiUrl: 'string',

        getAllProjects: function(){
            return Observable.of([new Project()]);
        },

        getTeam: function() {
            return Observable.of([new Team()]);
        },

        getAllUsers: function() {
            return Observable.of([new User('1', 'emaka', 'emma')]);
        },

        getClientSummaries: function() {
            return Observable.of([new Client()]);
        }
    };
    let mock: ProjectDataService;
    let projectEntry: ProjectEntry;
    let spinner: SpinnerService;
    let mockUserService: UserService;

    beforeEach( () => {
        spinner = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);
        mock = mockDataService;
        mockUserService = <UserService>userService;
        projectEntry = new ProjectEntry(mock, spinner, mockUserService);
    });

    it('can be initialized successfully', () => {
        expect(projectEntry).toBeTruthy();
    });

    it('should be able to add unique project to project list', () => {
        let project = new Project();

        projectEntry.addProject(project);

        expect(projectEntry.projects.length).toBe(1);
    });

    it('should be able to update project already in project list', () => {
        let project = new Project();
        project.id = '1';
        projectEntry.projects = [project];

        project.title = 'Time Tracking';
        projectEntry.addProject(project);

        expect(projectEntry.projects[0].title).toBe('Time Tracking');
    });

    it('should be able to get all projects from database', () => {
        projectEntry.getProjects();

        expect(projectEntry.projects.length).toBe(1);
    });

    it('should be able to remove project from project list', () => {
        let project = new Project();
        projectEntry.projects = [project];

        projectEntry.removeProject(project);

        expect(projectEntry.projects.length).toBe(0);
    });
});
