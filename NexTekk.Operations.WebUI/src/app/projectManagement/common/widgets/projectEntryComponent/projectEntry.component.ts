import { Component, ViewChild, EventEmitter, Output } from '@angular/core';
import { Project } from '../../models/project.model';
import { SpinnerService, ModalComponent, DialogService, NotificationService } from '../../../../shared';
import { ProjectDataService } from '../../services/projectData.service';
import { Client } from '../../models/client.model';
import { ContactPersonModalComponent } from '../contactPersonModal/contactPersonModal.component';
import { TeamModalComponent } from '../teamMembersModal/teamMemberModal.component';
import { ProjectEntry } from '../../models/projectEntry.model';
import { ProjectStatusOptions } from '../../constants/projectStatusOptions.const';

@Component({
    selector: 'app-project-entry-component',
    templateUrl: './projectEntry.component.html'
})
export class ProjectEntryComponent {
    clients: Client[];
    allStatus = [ProjectStatusOptions.notStarted, ProjectStatusOptions.inProgress,
    ProjectStatusOptions.suspended, ProjectStatusOptions.completed];
    projectEntry: ProjectEntry;
    title: string;
    project: Project;
    canDelete = false;
    canSave = false;
    isNew = false;

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild(TeamModalComponent)
    private teamModal: TeamModalComponent;

    @Output()
    onSaved = new EventEmitter<Project>();

    @Output()
    onDeleted = new EventEmitter<Project>();

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }
        return this.form.valid;
    }

    constructor(
        private _dataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _dialogService: DialogService,
        private _notificationService: NotificationService
    ) {
    }

    show(projectEntry: Project): void {
        if (!this.clients) {
            this._spinnerService.show();
            this._dataService.getClientSummaries().subscribe(clients => {
                this._spinnerService.hide();
                this.clients = clients;
            },
                err => {
                    this._spinnerService.hide();
                });
        }

        this.isNew = !projectEntry.id;
        this.title = this.isNew ? 'New Project' : 'Edit Project';
        this.canSave = true;
        this.canDelete = !this.isNew;
        this.project = projectEntry;
        this.modal.show();
    }

    showById(projectId: string): void {
        this._spinnerService.show();
        this._dataService.getProject(projectId).subscribe(project => {
            this._spinnerService.hide();
            this.show(project);
        },
            err => {
                this._spinnerService.hide();
                this._notificationService.showError('An error occurred while retrieving the project', 'Error');
            });
    }

    close(): void {
        this.modal.close();
    }

    displayTeam(project: Project): void {
        this.teamModal.show(project);
    }

    save(): void {
        this._spinnerService.show();
        this._dataService
            .saveProject(this.project)
            .subscribe(x => {
                this._spinnerService.hide();
                this.onSaved.emit(x);
                this.close();
                this._notificationService.showSuccess('The project was saved successfully', 'Success');
            }, err => {
                this._notificationService.showError('An error occurred while saving the project', 'Error');
                this._spinnerService.hide();
            });
    }

    delete(): void {
        let msg = '"' + this.project.title + '" will be permanently deleted. Click OK to continue.';

        this._dialogService.showConfirm('Delete Project?', msg).then(isOk => {
            if (!isOk) {
                return;
            }
            this._spinnerService.show();
            this._dataService.deleteProject(this.project.id)
                .subscribe(x => {
                    this._spinnerService.hide();
                    this.onDeleted.emit(this.project);
                    this.close();
                    this._notificationService.showSuccess('The project was deleted successfully', 'Success');
                },
                err => {
                    this._spinnerService.hide();
                    this._notificationService.showError('An error occurred while deleting the project', 'Error');
                });

        });
    }

}
