import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ProjectEntry } from '../../models/projectEntry.model';
import { Project } from '../../models/project.model';
import {
    DialogService, SpinnerService, NotificationService,
    User, UserService
} from '../../../../shared';
import { TeamModalComponent } from '../teamMembersModal/teamMemberModal.component';
import { ProjectDataService } from '../../services/projectData.service';
import { Client } from '../../models/client.model';
import { Contact } from '../../models/contact.model';
import { Team } from '../../models/teamMember.model';
import { ProjectEntryComponent } from './projectEntry.component';
import { Observable } from 'rxjs/Observable';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ProjectModule } from '../../../project.module';

describe('ProjectEntryComponent', () => {
    let _fixture: ComponentFixture<ProjectEntryComponent>;
    let projectEntryComponent: ProjectEntryComponent;
    let teamModal: TeamModalComponent;
    let mockProjectEntry: ProjectEntry;

    let mockDialogService = {
        showConfirm(title: string, message: string) {
            return Promise.resolve(true);
        }
    };

    let mockUserService = {
        getAllUsers: function () {
            return Observable.of([new User()]);
        }
    };
    let mockDataService;

    beforeEach(() => {
        mockProjectEntry = <ProjectEntry>jasmine.createSpyObj('projectEntry', ['addProject',
            'getProjects', 'removeProject', 'removeContact', 'addContact', 'getContacts, getAllUsers']);
        mockDataService = {
            getContacts: function () {
                return Observable.of([new Contact()]);
            },

            getTeam: function () {
                return Observable.of([new Team()]);
            },

            getAllProjects: function () {
                return Observable.of([new Project()]);
            },

            getClientSummaries: function () {
                return Observable.of([new Client()]);
            },

            saveProject: function (project: Project) {
                return Observable.of(project);
            },

            deleteProject: function () {
                return Observable.of(new Project());
            },

            getProject: function (id: string) {
                return Observable.of(new Project());
            }
        };

        TestBed.configureTestingModule({
            imports: [ProjectModule, ToastModule.forRoot()],
            providers: [
                SpinnerService, DialogService, NotificationService,
                { provide: ProjectDataService, useValue: mockDataService },
                { provide: UserService, useValue: mockUserService },
                { provide: DialogService, useValue: mockDialogService }
            ]
        });

        _fixture = TestBed.createComponent(ProjectEntryComponent);
        projectEntryComponent = <ProjectEntryComponent>_fixture.debugElement.componentInstance;
        teamModal = (<any>projectEntryComponent).teamModal;

        projectEntryComponent.projectEntry = mockProjectEntry;
    });

    it('can be instantiated successfully', () => {
        expect(projectEntryComponent).toBeTruthy();
    });

    it('can display show modal with edit properties', () => {
        let project = new Project();
        project.id = '1';
        let member = new Team();
        member.memberId = '1';
        member.member.firstName = 'emeka';
        member.member.lastName = 'emmanuel';
        project.teamMembers = [member];

        _fixture.detectChanges();
        projectEntryComponent.show(project);

        expect(projectEntryComponent.title).toBe('Edit Project');
        expect(projectEntryComponent.canDelete).toBe(true);
        expect(projectEntryComponent.canSave).toBe(true);
    });

    it('can display project by id', () => {
        let project = new Project();

        spyOn(projectEntryComponent, 'show');
        _fixture.detectChanges();
        projectEntryComponent.showById('1');

        expect(projectEntryComponent.show).toHaveBeenCalledWith(project);
    });

    it('can display teamMember modal', () => {
        let project = new Project('1', 'TimeTracking');

        spyOn(teamModal, 'show');
        projectEntryComponent.displayTeam(project);

        //expect(teamModal.show).toHaveBeenCalledWith(project);
    });

    it('can delete a project', () => {

        setTimeout(function () {
            spyOn(mockDataService, 'deleteProject');
            projectEntryComponent.project = new Project('id', 'new project');
            projectEntryComponent.delete();
            setTimeout(expect(() => mockDataService.deleteProject).toHaveBeenCalledWith(), 0);
        }, 0);

    });

    it('can display show modal with new properties', () => {
        let project = new Project();

        _fixture.detectChanges();
        projectEntryComponent.show(project);
        _fixture.detectChanges();

        expect(projectEntryComponent.title).toBe('New Project');
        expect(projectEntryComponent.canDelete).toBe(false);
        expect(projectEntryComponent.canSave).toBe(true);
    });

    it('can save project', () => {
        let project = projectEntryComponent.project = new Project();

        spyOn(projectEntryComponent.onSaved, 'emit');

        projectEntryComponent.save();

        expect(projectEntryComponent.onSaved.emit).toHaveBeenCalledWith(project);
    });
});
