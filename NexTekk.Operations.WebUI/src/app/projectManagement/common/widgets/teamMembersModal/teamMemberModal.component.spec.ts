import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule} from '@angular/forms';
import { Team } from '../../models/teamMember.model';
import { Project } from '../../models/project.model';
import { User, ModalComponent, CamelSeparatePipe, SpinnerService, UserService } from '../../../../shared';
import { ProjectDataService } from '../../services/projectData.service';
import { TeamModalComponent } from './teamMemberModal.component';
import { Observable } from 'rxjs/Observable';
import { ProjectModule } from '../../../project.module';

describe('TeamModalComponent', () => {
    let _fixture: ComponentFixture<TeamModalComponent>;
    let teamModal: TeamModalComponent;
    let modal: ModalComponent;
    let mockUserService = {
         getAllUsers: function() {
            return Observable.of([new User()]);
        }
    };

    let mockDataService = {
        getTeam: function () {
            /*
            this return 5 because of the temporary measure to give the
            user list ids.
            */
            return Observable.of([new Team(), new Team(), new Team(), new Team(), new Team()]);
        },

        getProject: function(id: string) {
            return Observable.of(new Project(id));
        },

        getAllUsers: function() {
            return Observable.of([new User()]);
        },

        saveTeamMembers: function(projectId: string, team: Team[]) {
            return Observable.of('success');
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ProjectModule],
            providers: [{ provide: ProjectDataService, useValue: mockDataService },
            { provide: UserService, useValue: mockUserService}, SpinnerService ]
        });

        _fixture = TestBed.createComponent(TeamModalComponent);
        teamModal = <TeamModalComponent>_fixture.debugElement.componentInstance;
        modal = (<any>teamModal).modal;
        teamModal.users = [new User('1', 'emeka' , 'emma')];
        teamModal.project = new Project('1');
        teamModal.projectCopy = teamModal.project;
        teamModal.project.teamMembers = [];
        teamModal.projectCopy.teamMembers = teamModal.project.teamMembers;

    });

    it('can be instatiated successfully', () => {
        expect(teamModal).toBeTruthy();
    });

    it('should display new input section for a new member to be added', () => {
        teamModal.project.teamMembers = [];
        teamModal.displayNewInput();

        expect(teamModal.isNew).toBe(true);
        expect(teamModal.newMember instanceof Team).toBeTruthy();
    });

    it('should populate the teamlist before displaying the modal', () => {
        let project = new Project();

        _fixture.detectChanges();
        teamModal.show(project);

        expect(teamModal.project instanceof Project).toBeTruthy();
        expect(teamModal.errorMessage).toBe(null);
        expect(teamModal.isNew).toBe(false);
    });

    it('should be able to add a unique member to the team', () => {
        teamModal.project.teamMembers = [];
        let member  = new Team();
        member.memberId = '1';
        teamModal.newMember = member;

        teamModal.displayNewInput();

        expect(teamModal.project.teamMembers.length).toBe(1);
    });

    it('should not be able to add member who is already in the team list ', () => {
        let member  = new Team();
        member.member.firstName = 'emeka';
        member.member.lastName = 'emma';
        member.memberId = '1';
        teamModal.project.teamMembers = [member];
        teamModal.newMember = member;
        teamModal.displayNewInput();

        expect(teamModal.project.teamMembers.length).toBe(1);
    });

    xit('should return list of unassigned users', () => {
        teamModal.users.push(new User('2', 'victor', 'moses'));
        let uniqueUsers = teamModal.userOptions('1');
        expect(uniqueUsers.length).toBe(2);
    });

    it('should return empty users list if no users in the system', () => {
        teamModal.users = [];
        let uniqueUsers = teamModal.userOptions('1');

        expect(uniqueUsers.length).toBe(0);
    });

    it('should be able to hide the new member imput section', () => {
        teamModal.removeMember();

        expect(teamModal.isNew).toBeFalsy();
        expect(teamModal.newMember).toBeNull();
    });

    it('should be able to set a teamMember as projectManager', () => {
        teamModal.users = [new User('1', 'Emeka')];
        teamModal.projectManagerId = '1';

        expect(teamModal.project.projectManager.firstName).toBe('Emeka');

    });

    it('should be able to close the modal', () => {
        teamModal.project.teamMembers = [new Team(new User())];
        teamModal.project.projectManager = teamModal.projectCopy.projectManager = teamModal.users[0];
        spyOn(modal, 'close');
        teamModal.close();

        expect(modal.close).toHaveBeenCalled();
    });

    it('should set project to null after save', () => {
        teamModal.project.teamMembers = [new Team(new User())];
        teamModal.project.projectManager = teamModal.projectCopy.projectManager = teamModal.users[0];

        spyOn(mockDataService, 'saveTeamMembers');
        teamModal.save();

        expect(teamModal.project).toBeNull();
    });

});
