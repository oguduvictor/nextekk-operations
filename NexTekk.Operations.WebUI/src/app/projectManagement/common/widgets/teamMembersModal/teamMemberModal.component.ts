import { Component, ViewChild } from '@angular/core';
import { Team } from '../../models/teamMember.model';
import { Project } from '../../models/project.model';
import { TeamRole } from '../../constants/teamRole.const';
import { User, UserService, ModalComponent, SpinnerService } from '../../../../shared';
import { ProjectDataService } from '../../services/projectData.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-team-members-modal-component',
    templateUrl: './teamMemberModal.component.html',
    styleUrls: ['./teamMemberModal.component.css']

})
export class TeamModalComponent {
    title = 'Manage Team Members';
    isNew = false;
    errorMessage: string = null;
    newMember: Team;
    project: Project;
    projectCopy: Project;
    roles = [TeamRole.architect, TeamRole.businessAnalyst, TeamRole.developer,
    TeamRole.qaAnalyst];
    users: User[];
    private _projectManagerId: string = null;

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }
        return this.form.valid;
    }

    constructor(
        private _dataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _userService: UserService
    ) {}

    get projectManagerId(): string {
        return this._projectManagerId;
    }

    set projectManagerId(id: string) {
        let manager = this.getUser(id);
        this.project.projectManager = manager;
        this._projectManagerId = id;
    }

    displayNewInput(): void {
        this.errorMessage = null;

        if (this.newMember) {
            this.newMember.member = this.getUser(this.newMember.memberId);
            this.project.addTeamMember(this.newMember);
            this.newMember = null;
        }

        if (this.userOptions(null).length <= 0) {
            this.errorMessage = 'All users are alreay assigned to this project';
            this.isNew = false;
            return;
        }

        this.isNew = true;
        this.newMember = new Team();
    }

    show(project: Project): void {
        this._spinnerService.show();
        this._userService.getAllUsers().subscribe(users => {
            this._spinnerService.hide();
            this.users = users;
        },
        err => {
            this._spinnerService.hide();
        });

        this.project = project;
        this.projectCopy = _.merge(new Project(), project);

        if (project.projectManagerId !== null) {
            this._projectManagerId = project.projectManagerId;
        }

        this.newMember = null;
        this.errorMessage = null;
        this.isNew = false;
        this.modal.show();
    }

    removeMember(): void {
        this.errorMessage = null;
        this.isNew = false;
        this.newMember = null;
    }

    close(): void {
        this.project.teamMembers = this.projectCopy.teamMembers;
        this.project.projectManager = this.projectCopy.projectManager;
        this.modal.close();
    }

    getUser(userId: string): User {
        return this.users.find(x => x.id === userId);
    }

    userOptions(val: string): User[] {
        if (_.isEmpty(this.users)) {
            return [];
        }

        let userOptions =  this.users.filter((user: User) => {
            return  user.id === val || ( user.id !== this.projectManagerId && this.project.mbrIds.indexOf(user.id) < 0);
        });

        return userOptions;
    }

    private resetTeamMembers(): void {
        this.project.teamMembers.forEach(x => {
            x.member = this.users.find(y => y.id === x.memberId);
        });
    }

    save(): void {
        if (this.newMember) {
            this.newMember.member = this.getUser(this.newMember.memberId);
            this.project.addTeamMember(this.newMember);
        }

        let dtoTeam = [];

        this.project.teamMembers.forEach(x => {
            dtoTeam.push(x.toDto(this.project.id));
        });

        let projectManager = new Team();
        projectManager.memberId = this.project.projectManager.id;
        projectManager.role = TeamRole.projectManager;

        dtoTeam.push(projectManager.toDto(this.project.id));

        this._spinnerService.show();
        this._dataService.saveTeamMembers(this.project.id, dtoTeam)
            .subscribe(x => {
                this._spinnerService.hide();
                this.resetTeamMembers();
                this.modal.close();
                this.project = null;

            },
             err => {
                this._spinnerService.hide();
            });
    }
}
