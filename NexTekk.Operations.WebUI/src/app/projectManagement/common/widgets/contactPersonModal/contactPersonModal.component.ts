import { Component, ViewChild, EventEmitter, Output } from '@angular/core';
import { Contact } from '../../models/contact.model';
import { SpinnerService, ModalComponent } from '../../../../shared';
import { ProjectDataService } from '../../services/projectData.service';
import { DialogService } from '../../../../shared';

@Component({
    selector: 'app-contact-person-modal',
    templateUrl: './contactPersonModal.component.html'
})
export class ContactPersonModalComponent {
    contact: Contact;
    title: string;
    canDelete = false;
    canSave = false;
    isReadOnly = false;
    isNew = false;

    phoneNumberPattern = '[- 0-9 + ()]+$';

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @Output()
    onSaved = new EventEmitter<Contact>();

    @Output()
    onDeleted = new EventEmitter<Contact>();

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }
        return this.form.valid;
    }

    constructor(
        private _dataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _dialogService: DialogService
    ) { }

    show(entry: Contact, isNew): void {
        if (isNew) {
            this.title = 'New Contact';
            this.canSave = true;
            this.canDelete = false;
            this.isNew = true;

            this.contact = new Contact();
            this.modal.show();
            return;
        }

        this.title = 'Edit Contact';
        this.canSave = true;
        this.canDelete = true;
        this.isNew = false;

        this.contact = entry;
        this.modal.show();
    }

    showById(contactId: number) {
        this._spinnerService.show();
        this._dataService.getContact(contactId).subscribe(contact => {
            this._spinnerService.hide();
            this.show(contact, false);
        },
        err => {
            this._spinnerService.hide();
        });
    }

    close(): void {
        this.modal.close();
    }

    save(): void {
        this._spinnerService.show();
        this._dataService
            .saveContact(this.contact)
            .subscribe(x => {
                this._spinnerService.hide();
                this.onSaved.emit(x);
                this.close();
            },
            err => {
                this._spinnerService.hide();
            });
    }

    delete(): void {
        let msg = '"' + this.contact.firstName + '" will be permanently deleted. Click OK to continue.';

        this._dialogService.showConfirm('Delete Contact?', msg).then(isOk => {
            if (!isOk) {
                return;
            }

            this._spinnerService.show();

            this._dataService.deleteContact(this.contact.id)
                .subscribe(x => {
                    this._spinnerService.hide();
                    this.onDeleted.emit(this.contact);
                },
                err => {
                    this._spinnerService.hide();
                });

            this.close();
        });
    }
}
