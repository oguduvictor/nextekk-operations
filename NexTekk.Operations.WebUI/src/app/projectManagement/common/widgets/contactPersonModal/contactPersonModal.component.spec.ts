import { Contact } from '../../models/contact.model';
import { ProjectDataService } from '../../services/projectData.service';
import { ContactPersonModalComponent } from './contactPersonModal.component';

import { SpinnerService, ModalComponent, DialogService } from '../../../../shared';

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ProjectModule } from '../../../project.module';

describe('ContactPersonModalComponent', () => {
    let contactModal: ContactPersonModalComponent;
    let fixture: ComponentFixture<ContactPersonModalComponent>;
    let mockSpinnerService: SpinnerService;

    let mockDialogService = {
        showConfirm(title: string, message: string) {
            return Promise.resolve(true);
        }
    };

    let mockDataService = {
        getContacts: () => {
            return Observable.of([new Contact(1, 'Frank', 'El')]);
        },

        getContact: (contactId: number) => {
            return Observable.of(new Contact(contactId, 'Helin', 'Jay'));
        },

        saveContact: (contact: Contact) => {
            return Observable.of(contact);
        },

        deleteContact: () => {
            return Observable.of(new Contact());
        }
    };

    beforeEach(() => {
        mockSpinnerService = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);
        TestBed.configureTestingModule({
            imports: [ProjectModule, RouterTestingModule.withRoutes([])],
            providers: [
                DialogService,
                { provide: ProjectDataService, useValue: mockDataService },
                { provide: SpinnerService, useValue: mockSpinnerService },
                { provide: DialogService, useValue: mockDialogService }

            ]
        });

        fixture = TestBed.createComponent(ContactPersonModalComponent);

        contactModal = fixture.debugElement.componentInstance;
    });

    it(' can be instatiated successfully', () => {
        expect(contactModal).toBeTruthy();
    });

    it('sets title to New Contact when isNew is true', () => {
        expect(contactModal.isNew).toBe(false);
        expect(contactModal.title).toBeUndefined();

        fixture.detectChanges();
        contactModal.show(new Contact(), true);

        expect(contactModal.title).toEqual('New Contact');
    });

    it('sets canSave to true when show is called with isNew equal to true', () => {
        contactModal.canSave = false;

        expect(contactModal.canSave).toEqual(false);

        fixture.detectChanges();
        contactModal.show(new Contact(), true);

        expect(contactModal.canSave).toBe(true);
    });

    it('sets canDelete to false when show is called with isNew equal to true', () => {
        contactModal.canDelete = true;

        expect(contactModal.canDelete).toBe(true);

        fixture.detectChanges();
        contactModal.show(new Contact(), true);

        expect(contactModal.canDelete).toEqual(false);
    });

    it('sets title to Edit Contact when show is called with isNew equal to false', () => {
        expect(contactModal.title).toBeUndefined();

        fixture.detectChanges();
        contactModal.show(new Contact(1, 'Jane', 'Jery'), false);

        expect(contactModal.title).toEqual('Edit Contact');
    });

    it('sets canSave to equal to true when show is called with isNew equal to false', () => {
        contactModal.canSave = false;

        fixture.detectChanges();
        contactModal.show(new Contact(1, 'Mark', 'Len'), false);

        expect(contactModal.canSave).toBe(true);
    });

    it('sets canDelete to true when show is called with isNew equal to false', () => {

        expect(contactModal.canDelete).toBe(false);

        fixture.detectChanges();
        contactModal.show(new Contact(), false);

        expect(contactModal.canDelete).toEqual(true);
    });

    it('show is called when showContact is invoked', () => {
        spyOn(contactModal, 'show');

        fixture.detectChanges();
        contactModal.showById(1);

        expect(contactModal.show).toHaveBeenCalledWith(new Contact(1, 'Helin', 'Jay'), false);
    });

    it('save() shows spinner service', () => {
        contactModal.contact = new Contact(1, 'Maro', 'Jake');

        contactModal.save();

        expect(mockSpinnerService.show).toHaveBeenCalled();
    });

    it('can delete a contact', () => {
            spyOn(mockDataService, 'deleteContact');

            contactModal.contact = new Contact(1, 'Emeka');
            contactModal.delete();

            setTimeout(() => expect(mockDataService.deleteContact).toHaveBeenCalled(), 0);
    });

    it('save() hides shown spinner service', () => {
        contactModal.contact = new Contact(1, 'Maro', 'Jake');

        contactModal.save();

        expect(mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('save() calls dataservice.saveContact', () => {
        let contactEntry = new Contact(1, 'Maro', 'Jake');
        contactModal.contact = contactEntry;

        spyOn(contactModal.onSaved, 'emit');
        contactModal.save();

        expect(contactModal.onSaved.emit).toHaveBeenCalledWith(contactEntry);
    });

    it('save calls close if call to dataService was successful', () => {
        let contactEntry = new Contact(1, 'Maro', 'Jake');
        contactModal.contact = contactEntry;

        spyOn(contactModal, 'close');
        contactModal.save();

        expect(contactModal.close).toHaveBeenCalled();
    });
});
