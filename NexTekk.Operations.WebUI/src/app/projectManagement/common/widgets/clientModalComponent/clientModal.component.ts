import { Component, ViewChild, EventEmitter, Output, OnInit } from '@angular/core';
import { Client } from '../../models/client.model';
import { SpinnerService, ModalComponent, DialogService } from '../../../../shared';
import { ProjectDataService } from '../../services/projectData.service';
import { ContactPersonModalComponent } from '../contactPersonModal/contactPersonModal.component';
import { ProjectEntryComponent } from '../projectEntryComponent/projectEntry.component';
import { Project } from '../../models/project.model';
import { Contact } from '../../models/contact.model';
import * as _ from 'lodash';

@Component({
    selector: 'app-client-modal',
    templateUrl: './clientModal.component.html'
})
export class ClientModalComponent implements OnInit {
    client: Client;
    title: string;
    canSave = false;
    isNew = false;
    selectedContactds: number[];

    private _allContacts: Contact[];

    @ViewChild(ModalComponent)
    private modal: ModalComponent;

    @ViewChild(ContactPersonModalComponent)
    private _contactModal: ContactPersonModalComponent;

    @ViewChild(ProjectEntryComponent)
    private _projectModal: ProjectEntryComponent;

    @Output()
    onSaved = new EventEmitter<Client>();

    @Output()
    onDeleted = new EventEmitter<Client>();

    @Output()
    contactDeleted = new EventEmitter<Contact>();

    @ViewChild('frm') form;
    get isValid(): boolean {
        if (!this.form) {
            return true;
        }
        return this.form.valid;
    }

    constructor(
        private _dataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _dialogService: DialogService
    ) { }

    contactOptions(val: number): Contact[] {
        if (_.isEmpty(this._allContacts)) {
            return [];
        }

        return this._allContacts.filter((contact: Contact) => {
            return  contact.id === val || this.selectedContactds.indexOf(contact.id) < 0;
        });
    }

    show(client: Client): void {
        this.resetSelectedClients();

        this.isNew = !(client && client.id);
        this.title = this.isNew ? 'New Client' : 'Edit Client';
        this.canSave = true;
        this.isNew = this.isNew;
        this.client = client || new Client();

        for (let i = 0; i < this.client.contacts.length; i++) {
            if ( i >= this.selectedContactds.length) {
                break;
            }

            this.selectedContactds[i] = this.client.contacts[i].id;
        }

        this.modal.show();
    }

    showById(clientId: string) {
        this._spinnerService.show();
        this._dataService.getClient(clientId).subscribe(x => {
            this._spinnerService.hide();
            this.show(x);
        },
        () => {
            this._spinnerService.hide();
        });
    }

    displayContact(contactId) {
        this._contactModal.showById(contactId);
    }

    displayProject(projectId: string) {
        this._projectModal.showById(projectId);
    }

    close(): void {
        this.modal.close();
    }

    delete(): void {
        let hasActiveProjects = this.client.projects.length > 0;
        let delMsg = 'will be permanently deleted. Click OK to continue.';
        let hasActiveProjectsMsg = 'cannot be deleted because it has projects that are linked to it.';

        let msg = `${this.client.name} ${hasActiveProjects ? hasActiveProjectsMsg : delMsg}`;

        this._dialogService.showConfirm('Delete Client?', msg).then(isOk => {

            if (hasActiveProjects || !isOk) {
                return;
            }

            this._spinnerService.show();
            this._dataService.deleteClient(this.client.id)
                .subscribe(() => {
                    this.onDeleted.emit(this.client);
                    this._spinnerService.hide();
                },
                () => {
                    this._spinnerService.hide();
                });
            this.close();
        });
    }

    save(): void {
        this.client.contacts = this._allContacts.filter(x => {
            return this.selectedContactds.indexOf(x.id) > -1;
        });

        this._spinnerService.show();
        this._dataService.saveClient(this.client)
            .subscribe(x => {
                this.client.id = x.id;
                this.onSaved.emit(this.client);
                this._spinnerService.hide();
                this.close();
            },
            () => {
                this._spinnerService.hide();
            });
    }

    onProjectSaved(project: Project): void {
        let existingProject = this.client.projects.find( x => x.id === project.id);

        if (existingProject) {
            _.merge(existingProject, project);
        }
    }

    onProjectDeleted(project: Project): void {
        _.remove(this.client.projects, x => x.id === project.id);
    }

    onContactDeleted(contact: Contact) {
        _.remove(this.client.contacts, x => x.id === contact.id);
        _.remove(this._allContacts, x => x.id === contact.id);
        this.resetSelectedClients();
        this.contactDeleted.emit(contact);
    }

    onContactSaved(contact: Contact): void {
        let existingContact = this._allContacts.find(x => x.id === contact.id);

        if (existingContact) {
            _.merge(existingContact, contact);
            return;
        }

        this._allContacts.push(existingContact);
    }

    ngOnInit() {
        this._dataService.getContacts().subscribe(x => {
            this._allContacts = x;
        });

        this.resetSelectedClients();
    }

    private resetSelectedClients() {
        this.selectedContactds = [undefined, undefined, undefined];
    }
}
