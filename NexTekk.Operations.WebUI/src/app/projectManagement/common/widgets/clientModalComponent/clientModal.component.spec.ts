import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Client } from '../../models/client.model';
import { Contact } from '../../models/contact.model';
import { Project } from '../../models/project.model';
import { ClientContact } from '../../models/clientContact.model';
import { ProjectDataService } from '../../services/projectData.service';

import {
    User, UserService, NotificationService, SpinnerService,
    ModalComponent, CamelSeparatePipe, DialogService
} from '../../../../shared';

import { ClientModalComponent } from './clientModal.component';
import { ContactPersonModalComponent } from '../contactPersonModal/contactPersonModal.component';
import { ProjectEntryComponent } from '../projectEntryComponent/projectEntry.component';
import { TeamModalComponent } from '../teamMembersModal/teamMemberModal.component';
import { Observable } from 'rxjs/Observable';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ProjectModule } from '../../../project.module';

describe('ClientModalComponent', () => {
    let _clientModal: ClientModalComponent;
    let _fixture: ComponentFixture<ClientModalComponent>;
    let _mockSpinnerService: SpinnerService;
    let _contactModal: ContactPersonModalComponent;
    let _projectModal: ProjectEntryComponent;
    let allContacts: Contact[];

    let mockDialogService = {
        showConfirm(title: string, message: string) {
            return Promise.resolve(true);
        }
    };

    let mockUserService = {
        getAllUsers: function () {
            return Observable.of([new User()]);
        }
    };

    let _mockDataService = {
        getClients: function () {
            return Observable.of([new Client()]);
        },

        getClient: (clientId: string) => {
            return Observable.of(new Client(clientId, 'Maro', 'Jake'));
        },

        saveClient: function (client: Client) {
            return Observable.of(client);
        },

        getContacts: function () {
            return Observable.of([
                new Contact(1, 'Maro', 'Jake'),
                new Contact(2, 'Cynthia', 'Rock'),
                new Contact(3, 'Jenefer', 'Champ')
            ]);
        },

        getClientsContacts: function () {
            return Observable.of([new ClientContact()]);
        },

        deleteClient: function () {
            return Observable.of(new Client());
        }

    };

    beforeEach(() => {
        _mockSpinnerService = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['show', 'hide']);
        TestBed.configureTestingModule({
            imports: [ProjectModule, RouterTestingModule.withRoutes([]), ToastModule.forRoot()],
            providers: [
                DialogService,
                NotificationService,
                { provide: ProjectDataService, useValue: _mockDataService },
                { provide: SpinnerService, useValue: _mockSpinnerService },
                { provide: UserService, useValue: mockUserService },
                { provide: DialogService, useValue: mockDialogService }
            ]
        });

        _fixture = TestBed.createComponent(ClientModalComponent);
        _clientModal = <ClientModalComponent>_fixture.debugElement.componentInstance;
        _contactModal = (<any>_clientModal)._contactModal;
        _projectModal = (<any>_clientModal)._projectModal;
        allContacts = (<any>_clientModal)._allContacts = [];

    });

    it('can be successfully instantiated', () => {
        expect(_clientModal).toBeTruthy();
    });

    it('isNew is true when new client is passed into show', () => {
        let entry = new Client();

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.isNew).toBe(true);
    });

    it('isNew is false when existing client is passed into show', () => {
        let entry = new Client('id1', 'Silverbird');

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.isNew).toBe(false);
    });

    it('canSave is true when new client is passed into show', () => {
        let entry = new Client();

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.title).toBe('New Client');
        expect(_clientModal.canSave).toBe(true);
    });

    it('canSave is true when existing client is passed into show', () => {
        let entry = new Client('id1', 'Silverbird');

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.canSave).toBe(true);
    });

    it('title is "New Client" when new client is passed into show', () => {
        let entry = new Client();

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.title).toBe('New Client');
    });

    it('title is "Edit Client" when existing client is passed into show', () => {
        let entry = new Client('id1', 'Silverbird');

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.title).toBe('Edit Client');
    });

    it('client property has a value after show is called', () => {
        expect(_clientModal.client).toBeUndefined();
        let entry = new Client();

        _fixture.detectChanges();
        _clientModal.show(entry);

        expect(_clientModal.client).toBeDefined();
    });

    it('should assign values to selectedContacts when show is called', () => {
        let client = <Client>{
            name: 'client1',
            id: 'id1',
            contacts: [
                new Contact(1, 'Joe', 'Blow'),
                new Contact(2, 'Amy', 'Suns'),
                new Contact(3, 'Luci', 'Williams')
            ]
        };

        expect(_clientModal.selectedContactds).toBeUndefined();

        _fixture.detectChanges();
        _clientModal.show(client);

        expect(_clientModal.selectedContactds[0]).toEqual(client.contacts[0].id);
        expect(_clientModal.selectedContactds[1]).toEqual(client.contacts[1].id);
        expect(_clientModal.selectedContactds[2]).toEqual(client.contacts[2].id);
    });

    it('resets selectedContactds when show is called', () => {
        expect(_clientModal.selectedContactds).toBeUndefined();

        _fixture.detectChanges();
        _clientModal.show(new Client());

        expect(_clientModal.selectedContactds.length).toEqual(3);

        expect(_clientModal.selectedContactds[0]).toEqual(undefined);
        expect(_clientModal.selectedContactds[1]).toEqual(undefined);
        expect(_clientModal.selectedContactds[2]).toEqual(undefined);
    });

    it('can show client by id', () => {
        spyOn(_clientModal, 'show');

        _fixture.detectChanges();
        _clientModal.showById('clientId');

        expect(_clientModal.show).toHaveBeenCalled();
    });

    it('shows SpinnerService when showById is called', () => {
        _fixture.detectChanges();
        _clientModal.showById('clientId');

        expect(_mockSpinnerService.show).toHaveBeenCalled();
    });

    it('can delete a client', () => {

            spyOn(_mockDataService, 'deleteClient');

            _clientModal.client = new Client('1', 'Emeka');
            _clientModal.delete();

            setTimeout(() => expect(_mockDataService.deleteClient).toHaveBeenCalled(), 0);
    });

    it('hides SpinnerService when showById is called', () => {

        _fixture.detectChanges();
        _clientModal.showById('clientId');

        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('can call displayClient', () => {
        spyOn(_contactModal, 'showById');

        _clientModal.displayContact(1);

        expect(_contactModal.showById).toHaveBeenCalledWith(1);
    });

    it('can call displayProject', () => {
        spyOn(_projectModal, 'showById');

        _clientModal.displayProject('projectId');

        expect(_projectModal.showById).toHaveBeenCalledWith('projectId');
    });

    it('adds selected contacts to client when saving', () => {
        _clientModal.ngOnInit();

        _fixture.detectChanges();
        _clientModal.show(new Client());

        expect(_clientModal.client.contacts.length).toEqual(0);

        _clientModal.selectedContactds = [1, 2, 3];

        _clientModal.save();

        let clientContacts = _clientModal.client.contacts;

        expect(clientContacts.length).toEqual(3);

        expect(clientContacts[0].fullName).toEqual('Maro Jake');

        expect(clientContacts[1].fullName).toEqual('Cynthia Rock');
        expect(clientContacts[2].fullName).toEqual('Jenefer Champ');
    });

    it('can unselect contacts from an existing client', () => {
        let client = <Client>{
            name: 'client1',
            id: 'id1',
            contacts: [
                new Contact(1, 'Joe', 'Blow'),
                new Contact(2, 'Amy', 'Suns'),
                new Contact(3, 'Luci', 'Williams')
            ]
        };

        _clientModal.ngOnInit();
        _fixture.detectChanges();
        _clientModal.show(client);

        expect(_clientModal.client.contacts.length).toEqual(3);

        _clientModal.selectedContactds = [1];

        _clientModal.save();

        let clientContacts = _clientModal.client.contacts;

        expect(clientContacts.length).toEqual(1);

        expect(clientContacts[0].fullName).toEqual('Maro Jake');
    });

    it('emits saved client once save is successfully', () => {
        spyOn(_clientModal.onSaved, 'emit');

        let client = new Client('clientId', 'Maro', 'Jake');
        _clientModal.ngOnInit();

        _fixture.detectChanges();
        _clientModal.show(client);

        _clientModal.save();

        expect(_clientModal.onSaved.emit).toHaveBeenCalledWith(client);
    });

    it('shows spinner service when saving client', () => {
        spyOn(_clientModal.onSaved, 'emit');

        let client = new Client('clientId', 'Maro', 'Jake');
        _clientModal.ngOnInit();

        _fixture.detectChanges();
        _clientModal.show(client);

        _clientModal.save();

        expect(_mockSpinnerService.show).toHaveBeenCalled();
        expect(_mockSpinnerService.hide).toHaveBeenCalled();
    });

    it('returns list of contacts not selected', () => {
        _clientModal.ngOnInit();
        _fixture.detectChanges();
        _clientModal.show(new Client());

        expect(_clientModal.client.contacts.length).toEqual(0);

        _clientModal.selectedContactds = [1, 2, undefined];

        let clientContacts = _clientModal.contactOptions(1);

        expect(clientContacts.length).toEqual(2);

        expect(clientContacts[0].id).toEqual(1);
        expect(clientContacts[1].id).toEqual(3);
    });

    it('can updated project after the project is updated', () => {
        _clientModal.client = new Client();
        _clientModal.client.projects = [new Project('1')];
        let project = new Project('1', 'TimeTrackingTesting');

        _clientModal.onProjectSaved(project);

        expect(_clientModal.client.projects[0].title).toBe('TimeTrackingTesting');
    });

    it('can remove project after the project is deleted', () => {
        _clientModal.client = new Client();
        _clientModal.client.projects = [new Project('1')];
        let project = new Project('1', 'TimeTrackingTesting');

        _clientModal.onProjectDeleted(project);

        expect(_clientModal.client.projects.length).toBe(0);
    });

    it('can remove contact after the contact is deleted', () => {
        _clientModal.client = new Client();
        _clientModal.client.contacts = [new Contact(1)];
        let contact = new Contact(1, 'Emeka');

        _clientModal.onContactDeleted(contact);

        expect(_clientModal.client.contacts.length).toBe(0);
    });

    it('can updated contact after the contact is updated', () => {
        allContacts.push(new Contact(1));
        let contact = new Contact(1, 'Emeka');

        _clientModal.onContactSaved(contact);

        expect(allContacts[0].firstName).toBe('Emeka');
        expect(allContacts.length).toBe(1);
    });

    it('can add contact to clientContacts when a new contact is selected', () => {
        allContacts.push(new Contact(1));
        let contact = new Contact(2, 'Emeka');

        _clientModal.onContactSaved(contact);

        expect(allContacts.length).toBe(2);
    });

});
