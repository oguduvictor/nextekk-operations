import { getTestBed, TestBed } from '@angular/core/testing';
import { BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AuthService, HttpService } from '../../../shared';
import { ProjectDataService } from './projectData.service';
import { Project } from '../models/project.model';
import { Contact } from '../models/contact.model';
import { Client } from '../models/client.model';
import { Team } from '../models/teamMember.model';

describe('ProjectDataService', () => {
    let _backend: MockBackend;
    let _service: ProjectDataService;
    let _url: string;
    let _authService = {
        getAccessToken: () => 'token_0001'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseRequestOptions,
                MockBackend,
                ProjectDataService,
                { provide: AuthService, useValue: _authService },
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions,
                        AuthService
                    ],
                    provide: HttpService,
                    useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions, authService: AuthService) => {
                        let notification = <any>{ showError: function() {} };
                        return new HttpService(<any>backend, defaultOptions, authService, notification);
                    }
                }
            ]
        });

        _url = 'http://localhost:33895/api/';

        let testbed = getTestBed();
        _backend = testbed.get(MockBackend);
        _service = TestBed.get(ProjectDataService);
    });

    function setupConnections(backend: MockBackend, options: any, url?) {
        backend.connections.subscribe((connection: MockConnection) => {
            if (connection.request.url === url) {
                let responseOptions = new ResponseOptions(options);
                let response = new Response(responseOptions);
                connection.mockRespond(response);
            } else {
                throw new Error('Invalid Url');
            }
        });
    }

    it('getAllProjects() should return list of all projects', () => {
        _url += 'Project/Search';

        setupConnections(_backend, {
            body: [new Project('1', 'new project')],
            status: 200
        }, _url);

        _service.getAllProjects().subscribe((data: Project[]) => {
            expect(data instanceof Array).toBeTruthy();
            expect(data[0].title).toBe('new project');
        });
    });

    it('saveProject() should  send and create project', () => {
        _url += 'Project/create';

        setupConnections(_backend, {
            body: new Project('1', 'new project'),
            status: 200
        }, _url);

        let project = new Project();

        _service.saveProject(project).subscribe((data: Project) => {
            expect(data instanceof Project).toBeTruthy();
        });
    });

    it('getProject() should return project', () => {
        let _id = '1';
        _url += `Project/Get/${_id}`;

        setupConnections(_backend, {
            body: new Project('1', 'new project'),
            status: 200
        }, _url);

        _service.getProject(_id).subscribe((data: Project) => {
            expect(data instanceof Project).toBeTruthy();
        });
    });

    it('deleteProject() should delete project', () => {
        let project = new Project('1');
        _url += `Project/Delete/${project.id}`;

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.deleteProject(project.id).subscribe((data: string) => {
            expect(data).toBe('success');
        });
    });

    it('getContact() should get Contact ', () => {
        let _id = 1;
        _url += `Contact/Get/${_id}`;

        setupConnections(_backend, {
            body: new Contact(),
            status: 200
        }, _url);

        _service.getContact(_id).subscribe((data: Contact) => {
            expect(data instanceof Contact).toBeTruthy();
        });
    });

    it('getContacts() should get all Contacts ', () => {
        _url += `Contact/GetAll`;

        setupConnections(_backend, {
            body: [new Contact()],
            status: 200
        }, _url);

        _service.getContacts().subscribe((data: Contact[]) => {
            expect(data instanceof Array).toBeTruthy();
        });
    });

    it('saveContact() should  send and create contact', () => {
        _url += 'Contact/Create';

        setupConnections(_backend, {
            body: new Contact(1, 'emeka'),
            status: 200
        }, _url);

        let contact = new Contact();

        _service.saveContact(contact).subscribe((data: Contact) => {
            expect(data instanceof Contact).toBeTruthy();
        });
    });

    it('deleteContact() should delete contact', () => {
        let contact = new Contact(1);
        _url += `Contact/Delete/${contact.id}`;

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.deleteContact(contact.id).subscribe((data: string) => {
            expect(data).toBe('success');
        });
    });

    it('getClient() should get Client ', () => {
        let _id = '1';
        _url += `Client/Get/${_id}`;

        setupConnections(_backend, {
            body: new Client(),
            status: 200
        }, _url);

        _service.getClient(_id).subscribe((data: Client) => {
            expect(data instanceof Client).toBeTruthy();
        });
    });

    it('getClients() should get all clients ', () => {
        _url += `Client/GetAll`;

        setupConnections(_backend, {
            body: [new Client()],
            status: 200
        }, _url);

        _service.getClients().subscribe((data: Client[]) => {
            expect(data instanceof Array).toBeTruthy();
        });
    });

    it('saveClient() should  send and create client', () => {
        _url += 'Client/Create';

        setupConnections(_backend, {
            body: new Client('1', 'Microsoft'),
            status: 200
        }, _url);

        let client = new Client();

        _service.saveClient(client).subscribe((data: Client) => {
            expect(data instanceof Client).toBeTruthy();
        });
    });

    it('deleteClient() should delete client', () => {
        let client = new Client('1');
        _url += `Client/Delete/${client.id}`;

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        _service.deleteClient(client.id).subscribe((data: string) => {
            expect(data).toBe('success');
        });
    });

    it('saveTeamMembers() should send and save teamMembers of a project', () => {
        let project = new Project('1');
        _url += `Project/SaveTeamMember/${project.id}`;

        setupConnections(_backend, {
            body: 'success',
            status: 200
        }, _url);

        let team = [new Team()];

        _service.saveTeamMembers(project.id, team).subscribe(data => {
            expect(data).toBe('success');
        });
    });
});
