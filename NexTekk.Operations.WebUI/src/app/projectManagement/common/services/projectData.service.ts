import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared';
import { Project } from '../models/project.model';
import { Contact } from '../models/contact.model';
import { Team } from '../models/teamMember.model';
import { Client } from '../models/client.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProjectDataService {
    private _projectApiUrl = 'api/Project/';
    private _contactApiUrl = 'api/Contact/';
    private _clientApiUrl = 'api/Client/';

    constructor (private _http: HttpService) {}

    getAllProjects(): Observable<Project[]> {
        return this._http.getMultiple(this._projectApiUrl + 'Search', Project);
    }

    saveProject(project: Project): Observable<Project> {
        return !!project.id
            ? this._http.putData(this._projectApiUrl + 'Update/' + project.id, project.toDto(), Project)
            : this._http.postData(this._projectApiUrl + 'create', project.toDto(), Project);
    }

    getProject(projectId: string): Observable<Project> {
         return this._http.getData(this._projectApiUrl + 'Get/' + projectId, Project);
    }

    getTeamMembers(projectId: string): Observable<Team[]> {
        return this._http.getMultiple(this._projectApiUrl + 'GetTeamMembers/' + projectId);
    }

    deleteProject(projectId: string): Observable<string> {
        return this._http.deleteData(this._projectApiUrl + 'Delete/' + projectId )
                .map(() => 'success');
    }

    getContact(contactId: number): Observable<Contact> {
        return this._http.getData(this._contactApiUrl + 'Get/' + contactId, Contact);
    }

    getContacts(): Observable<Contact[]> {
        return this._http.getMultiple(this._contactApiUrl + 'GetAll', Contact);
    }

    getContactSummaries(): Observable<Contact[]> {
        return this._http.getMultiple(this._contactApiUrl + 'GetContactSummaries', Contact);
    }

    saveContact(contact: Contact): Observable<Contact> {
        return !!contact.id
            ? this._http.putData(this._contactApiUrl + 'Update/' + contact.id, contact.toDto(), Contact)
            : this._http.postData(this._contactApiUrl + 'Create', contact.toDto(), Contact);
    }

    deleteContact(contactId: number): Observable<string> {
        return this._http.deleteData(this._contactApiUrl + 'Delete/' + contactId )
                .map(() => 'success');
    }

    getClient(clientId: string): Observable<Client> {
        return this._http.getData(this._clientApiUrl + 'Get/' + clientId, Client);
    }

    getClients(): Observable<Client[]> {
        return this._http.getMultiple(this._clientApiUrl + 'GetAll', Client);
    }

    getClientSummaries(): Observable<Client[]> {
        return this._http.getMultiple(this._clientApiUrl + 'GetClientSummaries', Client);
    }

    saveClient(client: Client): Observable<Client> {
         return !!client.id
            ? this._http.putData(this._clientApiUrl + 'Update/' + client.id, client.toDto(), Client)
            : this._http.postData(this._clientApiUrl + 'Create', client.toDto(), Client);
    }

    deleteClient(clientId: string): Observable<string> {
        return this._http.deleteData(this._clientApiUrl + 'Delete/' + clientId )
                .map(() => 'success');
    }

    saveTeamMembers(projectId: string, team: Team[]): Observable<string> {
        return this._http.putData(this._projectApiUrl + 'SaveTeamMember/' + projectId, team)
                .map(() => 'success');
    }
}
