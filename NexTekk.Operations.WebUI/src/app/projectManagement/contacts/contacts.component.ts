import { Component, ViewChild, OnInit } from '@angular/core';
import { Contact } from '../common/models/contact.model';
import { Client } from '../common/models/client.model';
import { ProjectDataService } from '../common/services/projectData.service';
import { SpinnerService, ModalComponent, NotificationService } from '../../shared';
import { ContactPersonModalComponent } from '../common/widgets/contactPersonModal/contactPersonModal.component';

import * as _ from 'lodash';

@Component({
    selector: 'app-contact-component',
    templateUrl: './contacts.component.html'
})
export class ContactComponent implements OnInit {
    contacts: Contact[];
    clients: Client[];

    @ViewChild(ContactPersonModalComponent)
    private _contactModal: ContactPersonModalComponent;

    @ViewChild(ModalComponent)
    private _modal: ModalComponent;

    constructor(
        private _projectDataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _notificationService: NotificationService
    ) { }

    openNewContactModal() {
        this._contactModal.show(new Contact(), true);
    }

    onContactSaved(contact: Contact) {
        let existingContact = this.contacts.find(x => x.id === contact.id);

        if (existingContact) {
            _.merge(existingContact, contact);
            return;
        }

        this.contacts.push(contact);
    }

    onContactDeleted(contact: Contact) {
        _.remove(this.contacts, x => x.id === contact.id);
    }

    displayContact(contactId: number) {
        this._spinnerService.show();
        this._projectDataService.getContact(contactId)
            .subscribe(contact => {
                this._spinnerService.hide();
                this._contactModal.show(contact, false);
            },
            () => {
                this._spinnerService.hide();
            });
    }

    ngOnInit() {
        this._spinnerService.show();

        this._projectDataService.getContacts().subscribe(x => {
            this.contacts = x;
            this._spinnerService.hide();
            },
            () => {
                this._spinnerService.hide();
            });
    }
}
