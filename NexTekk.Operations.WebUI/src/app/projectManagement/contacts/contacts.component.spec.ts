import {ContactComponent} from './contacts.component';
import {Contact} from '../common/models/contact.model';
import { ProjectDataService } from '../common/services/projectData.service';
import { SpinnerService, ModalComponent, NotificationService, DialogService } from '../../shared';
import {ContactPersonModalComponent} from '../common/widgets/contactPersonModal/contactPersonModal.component';

import {ComponentFixture, TestBed} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { ProjectModule } from '../project.module';

let options: ToastOptions = <ToastOptions>{
  positionClass: 'toast-bottom-right',
  newestOnTop: true,
  toastLife: 3000,
  showCloseButton: true
};

describe('ContactComponent', () => {
    let fixture: ComponentFixture<ContactComponent>;
    let contactComp: ContactComponent;
    let mockSpinnerService: SpinnerService;
    let mockDataService: ProjectDataService = <ProjectDataService> {
        getContacts: () => {
            return Observable.of([new Contact(1, 'Frank', 'El' )]);
        },

        getContact: (contactId: number) => {
            return Observable.of(new Contact(contactId, 'Helin', 'Jay'));
        }
    };

    beforeEach(() => {
        mockSpinnerService = <SpinnerService>jasmine.createSpyObj('SpinnerService', ['hide', 'show']);

        TestBed.configureTestingModule({
            imports: [ProjectModule, RouterTestingModule.withRoutes([])],
            providers: [
                NotificationService, ToastsManager, DialogService,
                { provide: ProjectDataService, useValue: mockDataService },
                { provide: SpinnerService, useValue: mockSpinnerService },
                { provide: ToastOptions, useValue: options}
            ]
        });

        fixture = TestBed.createComponent(ContactComponent);
        contactComp = <ContactComponent>fixture.debugElement.componentInstance;
    });

    it('can be instantiated successfully', () => {
        expect(contactComp).toBeTruthy();
    });

    it('should add new contact to contacts array onContactSaved', () => {
        contactComp.ngOnInit();
        expect(contactComp.contacts.length).toEqual(1);
        let contact = new Contact(2, 'Maro', 'Jake');

        contactComp.onContactSaved(contact);

        expect(contactComp.contacts.length).toEqual(2);
    });

    it('should modify exiting contact in contacts on contact saved', () => {
        contactComp.ngOnInit();
        expect(contactComp.contacts.length).toEqual(1);

        let prevContact = contactComp.contacts[0];
        expect(prevContact.fullName).toEqual('Frank El');

        prevContact.firstName = 'Kingston';
        prevContact.lastName = 'Aly';

        contactComp.onContactSaved(prevContact);

        expect(contactComp.contacts.length).toEqual(1);

        expect(contactComp.contacts[0].fullName).toEqual('Kingston Aly');
    });

    it('can open contact Modal', () => {
        spyOn(contactComp, 'openNewContactModal');

        contactComp.openNewContactModal();
        expect(contactComp.openNewContactModal).toHaveBeenCalled();
    });

    it('removes contact from contact list onContactDeleted', () => {
        contactComp.ngOnInit();
        expect(contactComp.contacts.length).toEqual(1);

        let prevContact = contactComp.contacts[0];
        expect(prevContact.fullName).toEqual('Frank El');

        contactComp.onContactDeleted(prevContact);

        expect(contactComp.contacts.length).toBe(0);
    });

    it('shows spinner service when displaying contact', () => {
        let contact = new Contact(1, 'Frank', 'El');

        fixture.detectChanges();
        contactComp.displayContact(contact.id);

        expect(mockSpinnerService.show).toHaveBeenCalled();
    });

    it('hides spinner service when displaying contact', () => {
        let contact = new Contact(1, 'Frank', 'El');

        fixture.detectChanges();
        contactComp.displayContact(contact.id);

        expect(mockSpinnerService.hide).toHaveBeenCalled();
    });
});
