﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule, ManagerGuardService } from '../shared';
import { ProjectComponent } from './projects/projects.component';
import { ContactPersonModalComponent } from './common/widgets/contactPersonModal/contactPersonModal.component';
import { ProjectEntryComponent } from './common/widgets/projectEntryComponent/projectEntry.component';
import { TeamModalComponent } from './common/widgets/teamMembersModal/teamMemberModal.component';
import { ProjectDataService } from './common/services/projectData.service';
import { ClientComponent } from './clients/clients.component';
import { ClientModalComponent } from './common/widgets/clientModalComponent/clientModal.component';
import { ContactComponent } from './contacts/contacts.component';

@NgModule({
    declarations: [
        ProjectComponent,
        ContactPersonModalComponent,
        ProjectEntryComponent,
        TeamModalComponent,
        ClientComponent,
        ClientModalComponent,
        ContactComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        RouterModule.forChild([
            { path: 'projects/:id', component: ProjectComponent, canActivate: [ManagerGuardService]},
            { path: 'projects', component: ProjectComponent, canActivate: [ManagerGuardService]},
            { path: 'clients', component: ClientComponent, canActivate : [ManagerGuardService] },
            { path: 'clients/:id', component: ClientComponent, canActivate: [ManagerGuardService]},
            { path: 'contacts', component: ContactComponent, canActivate: [ManagerGuardService] }
        ])
    ],
    providers: [ProjectDataService]
})
export class ProjectModule {
}
