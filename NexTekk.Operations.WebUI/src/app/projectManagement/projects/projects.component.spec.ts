import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule} from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ProjectComponent } from './projects.component';
import { ProjectEntry } from '../common/models/projectEntry.model';
import { ProjectDataService } from '../common/services/projectData.service';
import { SpinnerService, NotificationService, FilterByPipe,
        DialogService, ModalComponent, CamelSeparatePipe, UserService, User} from '../../shared';
import { TeamModalComponent } from '../common/widgets/teamMembersModal/teamMemberModal.component';
import { ClientModalComponent } from '../common/widgets/clientModalComponent/clientModal.component';
import { ContactPersonModalComponent } from '../common/widgets/contactPersonModal/contactPersonModal.component';
import { ProjectEntryComponent } from '../common/widgets/projectEntryComponent/projectEntry.component';
import { Contact } from '../common/models/contact.model';
import { Project } from '../common/models/project.model';
import { Team } from '../common/models/teamMember.model';
import { Client } from '../common/models/client.model';
import { Observable } from 'rxjs/Observable';
import { ProjectModule } from '../project.module';

describe('ProjectComponent', () => {
    let _fixture: ComponentFixture<ProjectComponent>;
    let projectComponent: ProjectComponent;
    let teamModal: TeamModalComponent;
    let clientModal: ClientModalComponent;
    let projectModal: ProjectEntryComponent;
    let mockProjectEntry: ProjectEntry;
    let mockUserService = {
         getAllUsers: function() {
            return Observable.of([new User()]);
        }
    };

    let mockDataService = {
        getContacts: function () {
            return Observable.of([new Contact()]);
        },

        getTeam: function () {
            return Observable.of([new Team()]);
        },

        getAllProjects: function () {
            return Observable.of([new Project()]);
        }
    };

    beforeEach(() => {
        mockProjectEntry = <ProjectEntry>jasmine.createSpyObj('projectEntry', ['addProject',
            'getProjects', 'removeProject', 'removeContact', 'addContact', 'getContacts']);

        TestBed.configureTestingModule({
            imports: [ProjectModule, RouterTestingModule.withRoutes([])],
            providers: [DialogService, SpinnerService, NotificationService,
            { provide: ProjectDataService, useValue: mockDataService },
            { provide: UserService, useValue: mockUserService}]
        });

        _fixture = TestBed.createComponent(ProjectComponent);
        projectComponent = <ProjectComponent>_fixture.debugElement.componentInstance;
        teamModal = <TeamModalComponent>_fixture.debugElement.componentInstance;
        projectModal = <ProjectEntryComponent>_fixture.debugElement.componentInstance;
        projectComponent.projectEntry = mockProjectEntry;
        teamModal = (<any>projectComponent).teamModal;
        clientModal = (<any>projectComponent).clientModal;
        projectModal = (<any>projectComponent).projectModal;

    });

    it('can be instantiated successfully', () => {
        expect(projectComponent).toBeTruthy();
    });

    it('can save project', () => {
        let project = new Project();

        projectComponent.onProjectSaved(project);

        expect(mockProjectEntry.addProject).toHaveBeenCalledWith(project);
    });

    it('can delete project', () => {
        let project = new Project();

        projectComponent.onProjectDeleted(project);

        expect(mockProjectEntry.removeProject).toHaveBeenCalledWith(project);
    });

    it('can display teamMember of a project', () => {
        let project = new Project('1', 'TimeTracking');

        spyOn(teamModal, 'show');
        projectComponent.displayTeam(project);

        expect(teamModal.show).toHaveBeenCalledWith(project);
    });

    it('can display project details', () => {
        let project = new Project('1', 'TimeTracking');

        spyOn(projectModal, 'showById');
        projectComponent.display(project.id);

        expect(projectModal.showById).toHaveBeenCalledWith(project.id);
    });

    it('can display a new project modal', () => {
        spyOn(projectModal, 'show');
        projectComponent.openNewProjectModal();

        expect(projectModal.show).toHaveBeenCalledWith(new Project());
    });

    it('can display client details', () => {
        let client = new Client('1', 'Microsoft');

        spyOn(clientModal, 'showById');
        projectComponent.displayClient(client.id);

        expect(clientModal.showById).toHaveBeenCalledWith(client.id);
    });

});
