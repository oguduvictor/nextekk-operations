import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpinnerService, UserService } from '../../shared';
import { ProjectDataService } from '../common/services/projectData.service';
import { Project } from '../common/models/project.model';
import { ClientModalComponent } from '../common/widgets/clientModalComponent/clientModal.component';
import { ProjectEntryComponent } from '../common/widgets/projectEntryComponent/projectEntry.component';
import { TeamModalComponent } from '../common/widgets/teamMembersModal/teamMemberModal.component';
import { ProjectEntry } from '../common/models/projectEntry.model';
import { ProjectStatusOptions } from '../../projectManagement/common/constants/projectStatusOptions.const';

@Component({
    selector: 'app-projects-component',
    templateUrl: './projects.component.html',
    styleUrls: ['./projects.component.css']
})
export class ProjectComponent implements OnInit {
    projectEntry: ProjectEntry;
    selectedClientId: string;
    selectedStatus: string;
    selectedManagerId: string;
    selectedUserId: string;
    allStatus = [ProjectStatusOptions.notStarted, ProjectStatusOptions.inProgress,
    ProjectStatusOptions.suspended, ProjectStatusOptions.completed];

    @ViewChild(ProjectEntryComponent)
    private projectModal: ProjectEntryComponent;

    @ViewChild(ClientModalComponent)
    private clientModal: ClientModalComponent;

    @ViewChild(TeamModalComponent)
    private teamModal: TeamModalComponent;

    constructor (
        private _dataService: ProjectDataService,
        private _spinnerService: SpinnerService,
        private _route: ActivatedRoute,
        private _userService: UserService
    ) {
       this.selectedClientId = _route.snapshot.params['id'];
    }

    display(projectId: string): void {
        this.projectModal.showById(projectId);
    }

    openNewProjectModal(): void {
    this.projectModal.show(new Project());
    }

    displayTeam(project: Project): void {
        this.teamModal.show(project);
    }

    displayClient(clientId: string): void {
        this.clientModal.showById(clientId);
    }

    onProjectSaved(project: Project): void {
        this.projectEntry.addProject(project);
    }

    onProjectDeleted(project: Project): void {
        this.projectEntry.removeProject(project);
    }

    ngOnInit(): void {
        this.projectEntry = new ProjectEntry(this._dataService, this._spinnerService, this._userService);
    }
}
