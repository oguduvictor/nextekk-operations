import { Angular2SampleCliPage } from './app.po';

describe('angular2-sample-cli App', function() {
  let page: Angular2SampleCliPage;

  beforeEach(() => {
    page = new Angular2SampleCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
