﻿using System;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface ICacheService
    {
        void AddOrUpdate<TObject>(string key, TObject value, TimeSpan cacheDuration = default(TimeSpan));

        TObject Get<TObject>(string key, bool defaultIfNotFound = false);
        
        void Delete(string key);
    }
}
