﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IVacationRepository
    {
        Task<IEnumerable<VacationEntity>> GetAll(int year);

        Task<VacationEntity> Get(Guid userId, int year);

        Task<VacationSetting> GetSetting(Guid userId);

        Task Create(VacationSetting entity);

        Task Update(VacationSetting entity);
    }
}
