﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Enums;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IReportService
    {
        byte[] GetTabularReport(IEnumerable<TimeEntry> timeEntries, FileFormat fileFormat);
    }
}
