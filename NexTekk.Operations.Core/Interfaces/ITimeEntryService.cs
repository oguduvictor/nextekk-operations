﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface ITimeEntryService
    {
        Task<IEnumerable<TimeEntry>> GetTimeEntries(TimeEntrySearchCriteria searchCriteria);

        Task<TimeEntryDetail> GetTimeEntry(Guid id);

        Task<IEnumerable<ActivitySummary>> GetAllActivities(Guid? userId = null);

        Task<TimeEntryDetail> SaveTimeEntry(TimeEntry timeEntry, Guid? id = null);

        Task UpdateTimeEntryStatus(IEnumerable<IdCommentPair> idCommentPairs, TimeEntryStatus status);

        Task DeleteTimeEntry(IEnumerable<Guid> ids);

        Task SendReminderEmail(DateTimeRange dateRange);

        Task<byte[]> GetTabularReport(TimeEntrySearchCriteria searchCriteria, FileFormat fileFormat);
    }
}
