﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<Project>> GetProjects(ProjectSearchCriteria searchCriteria);

        Task<ProjectDetail> GetProject(Guid id);

        Task<Project> SaveProject(Project project);

        Task SaveTeamMembers(IEnumerable<TeamMemberEntity> teamMember);
        
        Task DeleteProject(Guid id);

        Task<IEnumerable<TeamMember>> GetTeamMembers(Guid projectId);
    }
}
