﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IContactService
    {
        Task<IEnumerable<Contact>> GetContacts();

        Task<ContactDetail> GetContact(int id);

        Task<Contact> SaveContact(ContactEntity contact);

        Task DeleteContact(int id);
        Task<IEnumerable<ContactSummary>> GetContactSummaries();
    }
}
