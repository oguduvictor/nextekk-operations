﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IClientRepository
    {
        Task Create(ClientEntity client);

        Task<ClientEntity> Get(Guid id);

        Task<IEnumerable<ClientEntity>> GetAll(bool includeDeleted = false);
        
        Task<IEnumerable<ClientEntity>> GetClientSummaries(bool includeDeleted = false);

        Task SaveContacts(IEnumerable<ClientsContactsEntity> clientContacts);

        Task Update(ClientEntity client);

        Task<bool> HasProjects(Guid id);

        Task<ClientEntity> GetClientEntityAsync(Guid id);
    }
}
