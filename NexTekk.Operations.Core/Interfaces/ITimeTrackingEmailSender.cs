﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface ITimeTrackingEmailSender
    {
        Task SendSubmittedEmails(TimeEntryEntity timeEntry);

        Task SendDeniedEmails(TimeEntryEntity timeEntry);

        Task SendLateSubmissionReminder(IEnumerable<UserEntity> users, DateTimeRange dateRange);
    }
}
