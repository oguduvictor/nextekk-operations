﻿using NexTekk.Operations.Core.Models;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IUserContext
    {
        Guid GetUserId();

        string GetUserEmail();

        IEnumerable<string> GetUserRoles();

        UserDetail GetUserDetail();

        bool IsUserManager();
    }
}
