﻿using System.Collections.Generic;
using NexTekk.Operations.Core.Models.CashFlow;
using System.Threading.Tasks;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IExpenseEmailSender
    {
        Task SendExpenseCreationMailAsync(Expense expense);

        Task SendExpenseDeletionMailAsync(Expense expense);

        Task SendExpenseReimbursedMailAsync(Expense expense);

        Task SendExpenseUpdateMailAsync(Expense expense);
    }
}
