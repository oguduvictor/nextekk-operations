﻿using NexTekk.Operations.Core.Models;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IEmailService
    {
        Task<bool> SendEmail(Email email);
    }
}
