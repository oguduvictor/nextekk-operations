﻿using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IExpenseService
    {
        Task<Expense> Get(Guid id);

        Task<IEnumerable<Expense>> GetAll(ExpenseSearchCriteria searchCriteria); 

        Task<Expense> SaveExpense(Expense entity);

        Task DeleteExpense(Expense expense); 
    }
}
