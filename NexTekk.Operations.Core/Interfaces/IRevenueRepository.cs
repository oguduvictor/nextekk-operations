﻿using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IRevenueRepository
    {
        Task<Revenue> Create(Revenue revenue);

        Task<Revenue> Get(Guid id);

        Task<IList<Revenue>> GetAll(RevenueSearchCriteria searchCriteria);

        Task Update(Revenue revenue);

        Task Update(IEnumerable<Revenue> revenues);
    }
}