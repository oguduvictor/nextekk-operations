﻿using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IExpenseRepository
    {
        Task<IEnumerable<Expense>> Search(ExpenseSearchCriteria searchCriteria);

        Task<Expense> Create(Expense entity);

        Task<IEnumerable<Expense>> Update(IEnumerable<Expense> entities);

        Task Update(Expense expense);

        Task<Expense> Get(Guid id);
    }
}
