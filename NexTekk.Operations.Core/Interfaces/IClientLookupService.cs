﻿using NexTekk.Operations.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IClientLookupService
    {
        Task<ClientSummary> GetClientAsync(Guid clientId);
        Task<IEnumerable<ClientSummary>> GetClientsAsync(IEnumerable<Guid> clientIds);
    }
}