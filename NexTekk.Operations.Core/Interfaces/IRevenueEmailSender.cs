﻿using NexTekk.Operations.Core.Models.CashFlow;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IRevenueEmailSender
    {
        Task SendRevenueAddedEmail(Revenue revenue);
        Task SendRevenueDeletedEmail(Revenue revenue);
        Task SendRevenueUpdatedEmail(Revenue revenue);
    }
}