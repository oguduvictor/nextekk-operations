﻿using NexTekk.Operations.Core.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IContactRepository
    {
        Task<ContactEntity> Get(int id);

        Task<IEnumerable<ContactEntity>> GetAll(bool includeDeleted = false);

        Task Create(ContactEntity entity);

        Task Update(ContactEntity entity);

        Task Delete(int id);

        Task<IEnumerable<ContactEntity>> GetContactSummaries(bool includeDeleted = false);
    }
}
