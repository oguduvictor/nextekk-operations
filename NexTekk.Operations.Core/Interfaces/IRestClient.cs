﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IRestClient
    {
        Task<T> Get<T>(string absoluteUrl, Dictionary<string, string> query);

        Task<T> Post<S, T>(string absoluteUrl, S data);
    }
}
