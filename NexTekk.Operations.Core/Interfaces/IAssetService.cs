﻿using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IAssetService
    {
        Task<IEnumerable<AssetSummary>> GetAssetSummariesAsync(AssetSearchCriteria searchCriteria);

        Task<Asset> Get(Guid id);

        Task<Asset> SaveAssetAsync(Asset entity);

        Task DeleteAsset(Guid id);

        Task<IList<Asset>> GetAssetsAsync(AssetSearchCriteria searchCriteria);

        Task RequestAsset(Guid id);
    }
}
