﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IUserRepository
    {
        Task<UserEntity> Get(Guid id);

        Task<IEnumerable<UserEntity>> GetAll(IEnumerable<Guid> ids = null, bool employeesAndManagersOnly = false);

        Task<IEnumerable<UserEntity>> GetAllNotInIds(IEnumerable<Guid> ids);

        Task<IEnumerable<UserEntity>> GetAllManagers();
    }
}