﻿using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IAssetEmailSender
    {
        Task SendRequestEmail(Asset asset, Guid requesterId);
    }
}
