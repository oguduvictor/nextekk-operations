﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IActivityService
    {
        Task DeleteActivity(Guid activityId);

        Task<IEnumerable<Activity>> GetActivities();

        Task<ActivityDetail> GetActivityById(Guid activityId);

        Task<Activity> SaveActivity(ActivityEntity model);
    }
}
