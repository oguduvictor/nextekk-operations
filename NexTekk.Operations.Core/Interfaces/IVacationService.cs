﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IVacationService
    {
        Task<Vacation> GetVacation(Guid employeeId, int year);

        Task<IEnumerable<Vacation>> GetVacations(int year);

        Task<IEnumerable<TimeEntry>> GetVacationTimeEntries(Guid id, int year);

        Task<decimal> UpdateHoursPerPeriod(Guid employeeId, decimal hoursPerPeriod);

        Task<decimal> UpdateYearBeginBalance(Guid employeeId, decimal yearBeginBalance);

        Task<VacationSetting> SaveVacation(Guid employeeId, decimal? hoursPerPeriod, decimal? yearBeginBalance);
    }
}
