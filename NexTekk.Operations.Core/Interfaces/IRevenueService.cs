﻿using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IRevenueService
    {
        Task<Revenue> GetRevenueAsync(Guid id);

        Task<IList<Revenue>> GetRevenuesAsync(RevenueSearchCriteria searchCriteria);

        Task<Revenue> SaveRevenueAsync(Revenue revenue);

        Task DeleteRevenueAsync(Guid id);
    }
}
