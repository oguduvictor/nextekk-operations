﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IProjectRepository
    {
        Task<IEnumerable<ProjectEntity>> GetProjects(ProjectSearchCriteria searchCriteria);

        Task<ProjectEntity> GetProject(Guid id);

        Task Create(IEnumerable<ProjectEntity> projects);

        Task Update(IEnumerable<ProjectEntity> projects);
        
        Task<IEnumerable<TeamMemberEntity>> GetTeamMembers(Guid projectId);

        Task UpdateTeamMembers(IEnumerable<TeamMemberEntity> teamMembers);

        Task Delete(IEnumerable<Guid> ids);

        Task<IEnumerable<ProjectEntity>> GetProjectSummaries(Guid? userId);

        Task<IEnumerable<ProjectEntity>> GetProjectSummariesById(params Guid[] ids);
    }
}
