﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IClientService
    {
        Task<IEnumerable<Client>> GetClients();

        Task<IEnumerable<ClientSummary>> GetClientSummaries();

        Task<ClientDetail> GetClient(Guid id);

        Task<Client> SaveClient(Client client);

        Task SaveContacts(IEnumerable<ClientsContactsEntity> entities);

        Task DeleteClient(Guid id);
    }
}
