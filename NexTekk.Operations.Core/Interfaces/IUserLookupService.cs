﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IUserLookupService
    {
        Task<User> GetUserAsync(Guid userId);

        Task<IEnumerable<User>> GetUsersAsync(IEnumerable<Guid> userIds);

        Task<IEnumerable<UserEntity>> GetManagersAsync();
    }
}
