﻿using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface ITimeEntryRepository
    {
        Task<IEnumerable<TimeEntryEntity>> GetAll(TimeEntrySearchCriteria searchCriteria);

        Task<IEnumerable<TimeEntryEntity>> GetAll(IEnumerable<Guid> ids);

        Task<TimeEntryEntity> Get(Guid id);

        Task Create(IEnumerable<TimeEntryEntity> entities);

        Task Update(IEnumerable<TimeEntryEntity> entities);

        Task Delete(IEnumerable<Guid> ids);

        Task<IEnumerable<Guid>> GetUsersIdsWithSubmission(DateTimeRange dateRange);
    }
}
