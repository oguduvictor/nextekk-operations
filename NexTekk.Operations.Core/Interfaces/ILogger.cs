﻿using System;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface ILogger
    {
        Guid Trace(string message);

        Guid Debug(string message);

        Guid Info(string message);

        Guid Warn(string message);

        Guid Error(string message);

        Guid Error(Exception exception);

        Guid Fatal(string message);
    }
}
