﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IActivityRepository
    {
        Task<ActivityEntity> Get(Guid id);

        Task<IEnumerable<ActivityEntity>> GetAll(bool includeDeleted = false);

        Task Create(ActivityEntity activity);

        Task Update(ActivityEntity activity);

        Task Delete(Guid id); 
    }
}