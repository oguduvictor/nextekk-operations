﻿using NexTekk.Operations.Core.Models.CashFlow;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IAssetRepository
    {
        Task<IList<Asset>> GetAssetsAsync(AssetSearchCriteria searchCriteria);

        Task<IList<AssetSummary>> GetAssetSummariesAsync(AssetSearchCriteria searchCriteria);

        Task<Asset> Create(Asset entity);

        Task Update(Asset entities);

        Task<Asset> Get(Guid id);
    }
}
