﻿using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Enums;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Interfaces
{
    public interface IAssetReportGenerator
    {
        byte[] GetTabularReport(IEnumerable<AssetSummary> assets, FileFormat fileFormat);
    }
}
