﻿namespace NexTekk.Operations.Core.Helpers
{
    public static class RoleConstants
    {
        public const string UserOnly = "User";

        public const string EmployeeOnly = "Employee";

        public const string ManagerOnly = "Manager";

        public const string EmployeeOrManager = "Employee, Manager";
    }
}
