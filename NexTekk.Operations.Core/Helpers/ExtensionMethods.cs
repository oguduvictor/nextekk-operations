﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NexTekk.Operations.Core.Exceptions;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexTekk.Operations.Core.Helpers
{
    public static class ExtensionMethods
    {
        public static bool IsNull<T>(this T source)
        {
            return source == null;
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || source.Count() == 0;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source.IsNullOrEmpty())
            {
                return;
            }

            foreach (var item in source)
            {
                action(item);
            }
        }

        public static string ToJson<T>(this T data, bool camelCasing = false)
        {
            if (!camelCasing)
            {
                return JsonConvert.SerializeObject(data);
            }

            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return JsonConvert.SerializeObject(data, settings);
        }

        public static T FromJson<T>(this string source)
        {
            return JsonConvert.DeserializeObject<T>(source);
        }

        public static bool IsAnyOf<T>(this T source, params T[] values)
        {
            if (values.IsNullOrEmpty())
            {
                return false;
            }

            return values.Contains(source);
        }

        public static bool IsTrue(this string source)
        {
            if (source.IsNullOrEmpty())
                return false;

            return (source.ToLower() == "true");
        }

        public static string GetCurrencySymbol(this Currency currency)
        {
            switch (currency)
            {
                case Currency.Naira:
                    return "₦";
                case Currency.Dollar:
                    return "$";
                default:
                    return string.Empty;
            }
        }
    }
}
