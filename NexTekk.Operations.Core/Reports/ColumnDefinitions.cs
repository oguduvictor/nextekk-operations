﻿namespace NexTekk.Operations.Core.Reports
{
    public class ColumnDefinitions
    {
        public ColumnDefinitions(string title, string property)
        {
            Title = title;
            Property = property;
        }

        public string Title { get; set; }

        public string Property { get; set; }
    }
}