﻿using NexTekk.Operations.Core.Models;

namespace NexTekk.Operations.Core.Providers
{
    public static class AppSettingsProvider
    {
        public static AppSettings Current { get; private set; }

        public static void Register(AppSettings settings)
        {
            Current = settings;
        }
    }
}
