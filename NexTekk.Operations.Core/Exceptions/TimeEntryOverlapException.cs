﻿using NexTekk.Operations.Core.Models;

namespace NexTekk.Operations.Core.Exceptions
{
    public class TimeEntryOverlapException : OperationException
    {
        public TimeEntryOverlapException(TimeEntry overlappedTimeEntry) 
            : base($"Time entry overlaps with \"{overlappedTimeEntry.Task}\" that already exists in the system.", null)
        {
            OverlappedTimeEntry = overlappedTimeEntry;
        }

        public TimeEntry OverlappedTimeEntry { get; }
    }
}
