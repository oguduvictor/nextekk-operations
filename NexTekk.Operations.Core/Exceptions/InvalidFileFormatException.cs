﻿using System;
using System.Collections.Generic;
using System.Text;
using NexTekk.Operations.Core.Models.Enums;

namespace NexTekk.Operations.Core.Exceptions
{
    public class InvalidFileFormatException : OperationException
    {
        public InvalidFileFormatException(FileFormat fileFormat)
            : base($"Invalid file format for \"{fileFormat}\".", null)
        {

        }
    }
}
