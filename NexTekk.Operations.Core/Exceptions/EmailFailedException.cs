﻿using System;

namespace NexTekk.Operations.Core.Exceptions
{
    public class EmailFailedException : OperationException
    {
        public EmailFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
