﻿using System;

namespace NexTekk.Operations.Core.Exceptions
{
    public class OperationException : Exception
    {
        public OperationException(string message, Exception innerException)
            : base(message, innerException?.GetBaseException())
        {
        }

        public OperationException(string message)
            : base(message)
        {
        }
    }
}
