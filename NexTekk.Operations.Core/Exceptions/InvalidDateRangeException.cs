﻿using NexTekk.Operations.Core.Models;

namespace NexTekk.Operations.Core.Exceptions
{
    public class InvalidDateRangeException : OperationException
    {
        public InvalidDateRangeException(TimeEntry timeEntry) 
            : base($"Invalid time range for \"{timeEntry.Task}\". The end time must be at least 15 minutes greater than the start time.", null)
        {
        }
    }
}
