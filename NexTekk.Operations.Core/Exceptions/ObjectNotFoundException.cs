﻿using System;

namespace NexTekk.Operations.Core.Exceptions
{
    [Serializable]
    public class ObjectNotFoundException : OperationException
    {
        public ObjectNotFoundException(string message)
            : base(message, null)
        {
        }

        public ObjectNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
