﻿using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Core.Models
{
    public class ContactSummary
    {
        public ContactSummary()
        {
        }

        public ContactSummary(ContactEntity entity)
        {
            Id = entity.Id;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
