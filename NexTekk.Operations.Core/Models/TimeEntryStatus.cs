﻿namespace NexTekk.Operations.Core.Models
{
    public enum TimeEntryStatus
    {
        None = 0,
        Saved = 20,
        Submitted = 30,
        Denied = 40,
        Approved = 90
    }
}