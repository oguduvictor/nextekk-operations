﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NexTekk.Operations.Core.Models
{
    public class IdCommentPair
    {
        [Required]
        public Guid Id { get; set; }

        [StringLength(4000)]
        public string Comment { get; set; }
    }
}
