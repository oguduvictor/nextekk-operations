﻿using System;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class Asset : AssetSummary
    {   
        public string Description { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string ServiceTag { get; set; }

        public string ServiceCode { get; set; }
        
        public DateTime Modified { get; set; }

        public DateTime Created { get; set; }

        public DateTime? MarkedBadOn { get; set; }

        public DateTime? LastAssignedDate { get; set; }

        public Guid? LastAssigneeId { get; set; }
        
        public Guid? MarkedBadById { get; set; }

        public Guid CreatedById { get; set; }

        public Guid ModifiedById { get; set; }
        
        public bool Deleted { get; set; }
    }
}
