﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class Revenue
    {
        public Guid Id { get; set; }

        public DateTime PaymentDate { get; set; }

        public Currency Currency { get; set; }

        public decimal Amount { get; set; }

        public string Description { get; set; }

        public Guid ClientId { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime Modified { get; set; }

        public Guid ModifiedById { get; set; }

        public bool Deleted { get; set; }
    }
}
