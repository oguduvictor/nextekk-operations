﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class AssetSummary
    {
        public Guid Id { get; set; }

        public string Item { get; set; }

        public string SerialNo { get; set; }

        public DateTime PurchasedDate { get; set; }

        public DateTime? AssignedDate { get; set; }

        public Condition Condition { get; set; }

        public decimal Cost { get; set; }

        public Currency Currency { get; set; }

        public Guid? AssigneeId { get; set; }

        public Category Category { get; set; }

        public Guid PurchasedById { get; set; }
    }
}
