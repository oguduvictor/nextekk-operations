﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class ExpenseSearchCriteria
    {
        public Guid? UserId { get; set; }

        public Guid? IncurredById { get; set; }

        public Currency? Currency { get; set; }
        
        public DateTimeRange IncurredDateRange { get; set; }

        public int PageNumber { get; set; }

        public bool? Reimbursed { get; set; }

        public string DescriptionContains { get; set; }

        public decimal? MinAmount { get; set; }

        public decimal? MaxAmount { get; set; }
        
        public int? PageSize { get; set; }

        public int? NumberToSkip { get; set; } 
    }
}
