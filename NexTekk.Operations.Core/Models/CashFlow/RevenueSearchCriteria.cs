﻿using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class RevenueSearchCriteria
    {
        public DateTimeRange PaymentDate { get; set; }

        public Guid? EnteredBy { get; set; }

        public Currency? Currency { get; set; }
        
        public IEnumerable<Guid> ClientIds { get; set; }

        public string Description { get; set; }

        public int? MinAmount { get; set; }

        public int? MaxAmount { get; set; }

        public int PageNumber { get; set; }

        public bool IncludeExpenses { get; set; }

        public int? PageSize { get; set; }

        public int? NumberToSkip { get; set; }
    }
}
