﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class Expense
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public Guid IncurredById { get; set; }

        public Guid CreatedById { get; set; }
        
        public Currency Currency { get; set; }

        public decimal Amount { get; set; }

        public string Description { get; set; }

        public string Reason { get; set; }

        public DateTime? Reimbursed { get; set; } 

        public DateTime Modified { get; set; }
         
        public bool Deleted { get; set; }
         
        public Guid ModifiedById { get; set; } 
        
        public DateTime Created { get; set; }
    }
}
