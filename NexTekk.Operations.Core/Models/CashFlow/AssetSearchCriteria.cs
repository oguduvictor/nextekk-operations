﻿using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models.CashFlow
{
    public class AssetSearchCriteria
    {
        public string Item { get; set; }

        public string SerialNo { get; set; }

        public DateTimeRange PurchasedDate { get; set; }

        public int PageNumber { get; set; }

        public Guid? UserId { get; set; }

        public DateTimeRange AssignedDate { get; set; }

        public Condition? Condition { get; set; }

        public IEnumerable<Guid> AssigneeIds { get; set; }

        public IEnumerable<Guid> PurchasedByIds { get; set; }

        public int? PageSize { get; set; }

        public int? NumberToSkip { get; set; }
    }
}
