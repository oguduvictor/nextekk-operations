﻿using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class ClientSummary
    {
        public ClientSummary()
        {
        }

        public ClientSummary(ClientEntity entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }
        
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
