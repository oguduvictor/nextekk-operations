﻿using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class Contact : ContactSummary
    {
        public Contact(ContactEntity entity)
            : base(entity)
        {
            Email = entity.Email;
            PhoneNumber1 = entity.PhoneNumber1;
            PhoneNumber2 = entity.PhoneNumber2;
            Created = entity.Created;
            Edited = entity.Edited;
        }

        public string Email { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public DateTime Created { get; set; }

        public DateTime Edited { get; set; }
    }
}
