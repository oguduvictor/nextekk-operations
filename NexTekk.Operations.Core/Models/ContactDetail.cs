﻿using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Core.Models
{
    public class ContactDetail : Contact
    {
        public ContactDetail(ContactEntity entity)
            : base(entity)
        {
            Creator = entity.Creator == null ? null : new User(entity.Creator);
            Editor = entity.Editor == null ? null : new User(entity.Editor);
        }

        public User Creator { get; set; }

        public User Editor { get; set; }
    }
}
