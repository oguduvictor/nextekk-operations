﻿using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class User
    {
        public User()
        {
        }

        public User(UserEntity entity)
        {
            Id = entity.Id;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
        }

        public User(Guid id, string firstname, string lastname)
        {
            Id = id;
            FirstName = firstname;
            LastName = lastname;
        }

        public Guid Id { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}".Trim();
            }
        }
    }
}
