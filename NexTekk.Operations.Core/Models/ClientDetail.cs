﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models
{
    public class ClientDetail : Client
    {
        public ClientDetail(ClientEntity entity)
            : base(entity)
        {
            Edited = entity.Edited;
            Editor = entity.Editor == null ? null : new User(entity.Editor);
            Projects = new List<ProjectSummary>();
            
            foreach (var project in entity.Projects)
            {
                if (!project.Deleted)
                {
                    Projects.Add(new ProjectSummary(project.Id, project.Title, project.Billable));
                }
            }
        }
        
        public User Editor { get; set; }

        public DateTime Edited { get; set; }

        public ICollection<ProjectSummary> Projects { get; set; }
    }
}
