﻿namespace NexTekk.Operations.Core.Models.Enums
{
    public enum FileFormat
    {
        Excel,

        CSV
    }
}
