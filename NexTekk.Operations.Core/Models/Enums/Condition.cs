﻿namespace NexTekk.Operations.Core.Models.Enums
{
    public enum Condition
    {
        Unknown = 0,
        New = 1,
        Good = 2,
        Fair = 3,
        Bad = 4
    }
}
