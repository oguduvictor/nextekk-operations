﻿namespace NexTekk.Operations.Core.Models.Enums
{
    public enum Currency
    {
        Unknown = 0,
        Naira = 1,
        Dollar = 2
    }
}
