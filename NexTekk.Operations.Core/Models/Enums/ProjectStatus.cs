﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexTekk.Operations.Core.Models.Enums
{
    public enum ProjectStatus
    {
        None = 0,
        NotStarted = 300,
        InProgress = 400,
        Suspended = 600,
        Completed = 900
    }
}
