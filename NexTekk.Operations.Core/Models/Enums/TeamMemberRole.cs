﻿namespace NexTekk.Operations.Core.Models
{
    public enum TeamMemberRole
    {
        None = 0,
        Developer = 1100,
        Architect = 1120,
        QaAnalyst = 1130,
        BusinessAnalyst = 1140,
        ProjectManager = 1150
    }
}
