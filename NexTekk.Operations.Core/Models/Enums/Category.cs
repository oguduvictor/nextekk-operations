﻿namespace NexTekk.Operations.Core.Models.Enums
{
    public enum Category
    {
        Unknown = 0,
        Computer = 1,
        Phone = 2,
        ComputerAccessory = 3,
        Furniture = 4,
        OfficeEquipment = 5,
        OfficeSupply = 6,
        Souvenir = 7,
        Other = 100
    }
}
