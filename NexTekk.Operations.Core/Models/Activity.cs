﻿using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class Activity
    {
        public Activity()
        {
        }

        public Activity(ActivityEntity entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Billable = entity.Billable;
            Description = entity.Description;
            Deleted = entity.Deleted;
            Created = entity.Created;
            Edited = entity.Created;
            IsVacation = entity.IsVacation;
        }

        public Activity(ProjectSummary entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Billable = entity.Billable;
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public bool Billable { get; set; }

        public bool IsVacation { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }

        public DateTime Created { get; set; }

        public DateTime Edited { get; set; }
    }
}
