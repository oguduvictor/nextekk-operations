﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models
{
    public class Client : ClientSummary
    {
        public Client()
        {
            Contacts = new List<ContactSummary>();
        }

        public Client(ClientEntity entity)
            : base(entity)
        {
            Contacts = new List<ContactSummary>();
            Creator = entity.Editor == null ? null : new User(entity.Creator);
            Created = entity.Created;
            
            foreach (var clientContact in entity.ClientsContacts)
            {
                if (!clientContact.Contact.IsNull() && !clientContact.Contact.Deleted)
                {
                    Contacts.Add(new ContactSummary(clientContact.Contact));
                }
            }
        }

        public User Creator { get; set; }

        public DateTime Created { get; set; }

        public ICollection<ContactSummary> Contacts { get; set; }
    }
}
