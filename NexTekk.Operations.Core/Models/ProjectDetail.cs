﻿using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Core.Models
{
    public class ProjectDetail : Project
    {
        public ProjectDetail(ProjectEntity entity)
            : base(entity)
        {
            Creator = entity.Creator == null ? null : new User(entity.Creator);
            Editor = entity.Editor == null ? null : new User(entity.Editor);
        }

        public virtual User Creator { get; set; }

        public virtual User Editor { get; set; }
    }
}
