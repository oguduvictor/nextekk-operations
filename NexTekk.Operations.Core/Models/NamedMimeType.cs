﻿namespace NexTekk.Operations.Core.Models
{
    public class NamedMIMEType
    {
        public NamedMIMEType(string name, string mimeType)
        {
            Name = name;
            MIMEType = mimeType;
        }

        public string Name { get; set; }

        public string MIMEType { get; set; }
    }
}
