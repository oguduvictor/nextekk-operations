﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class TimeEntryDetail : TimeEntry
    {
        public TimeEntryDetail()
        {
        }

        public TimeEntryDetail(TimeEntryEntity entity)
            : base(entity)
        {
            if (entity.IsNull())
            {
                throw new ArgumentNullException(nameof(entity));
            }

            Comment = entity.Comment;
            ManagerComment = entity.ManagerComment;
            Edited = entity.Edited;
            Editor = entity.Editor == null ? null : new User(entity.Editor);
            Approved = entity.Approved;
            Approver = entity.Approver == null ? null : new User(entity.Approver);
            Denied = entity.Denied;
            Denier = entity.Denier == null ? null : new User(entity.Denier);
        }

        public DateTime Edited { get; set; }

        public User Editor { get; set; }

        public DateTime? Approved { get; set; }

        public User Approver { get; set; }

        public DateTime? Denied { get; set; }

        public User Denier { get; set; }
    }
}
