﻿using NexTekk.Operations.Core.Models.Entities;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models
{
    public class ProjectSummary
    {
        public ProjectSummary()
        {
            TeamMembers = new List<TeamMemberEntity>();
        }

        public ProjectSummary(Guid id, string title, bool billable)
        {
            Id = id;
            Title = title;
            Billable = billable;
            TeamMembers = new List<TeamMemberEntity>();
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public bool Billable { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<TeamMemberEntity> TeamMembers { get; set; }
    }
}