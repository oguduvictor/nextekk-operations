﻿using System;

namespace NexTekk.Operations.Core.Models
{
    public class TimeEntrySearchCriteria
    {
        public DateTime? BeginDateTime { get; set; }

        public DateTime? EndDateTime { get; set; }

        public Guid? CreatorId { get; set; }

        public Guid? ApproverId { get; set; }

        public Guid? DenierId { get; set; }

        public Guid? ProjectId { get; set; }

        public Guid? ProjectManagerId { get; set; }

        public Guid? ActivityId { get; set; }

        public Guid? ProjectOrActivityId { get; set; }

        public bool ActivitiesOnly { get; set; }

        public bool ProjectsOnly { get; set; }

        public TimeEntryStatus? Status { get; set; }

        public bool? IsVacation { get; set; }
    }
}
