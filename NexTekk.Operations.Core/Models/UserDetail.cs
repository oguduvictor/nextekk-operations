﻿using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models
{
    public class UserDetail : User
    {
        public UserDetail(Guid id, string firstname, string lastname, string email, IEnumerable<string> roles)
            : base(id, firstname, lastname)
        {
            Email = email;
            Roles = roles;
        }

        public string Email { get; private set; }

        public IEnumerable<string> Roles { get; private set; }
    }
}
