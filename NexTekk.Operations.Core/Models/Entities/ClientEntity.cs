﻿using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class ClientEntity
    {
        public ClientEntity()
        {
            ClientsContacts = new List<ClientsContactsEntity>();
            Projects = new List<ProjectEntity>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid EditorId { get; set; }

        public DateTime Edited { get; set; }

        public bool Deleted { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatorId { get; set; }

        public virtual UserEntity Creator { get; set; }

        public virtual UserEntity Editor { get; set; }

        public virtual ICollection<ClientsContactsEntity> ClientsContacts { get; set; }

        public virtual ICollection<ProjectEntity> Projects { get; set; }
    }
}
