﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class ActivityEntity
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public bool Billable { get; set; }

        public bool IsVacation { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatorId { get; set; }

        public virtual UserEntity Creator { get; set; }

        public DateTime Edited { get; set; }

        public Guid EditorId { get; set; }

        public virtual UserEntity Editor { get; set; }
    }
}
