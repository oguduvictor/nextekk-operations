﻿namespace NexTekk.Operations.Core.Models.Entities
{
    public class RoleEntity
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
