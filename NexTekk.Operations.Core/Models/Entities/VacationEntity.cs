﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class VacationEntity
    {
        public Guid UserId { get; set; }

        public virtual UserEntity User { get; set; }

        public decimal HoursPerPeriod { get; set; }

        public decimal? TotalHoursTaken { get; private set; }

        public decimal YearBeginBalance { get; set; }

        public int Year { get; private set; }

        public DateTime Created { get; set; }

        public Guid CreatorId { get; set; }

        public DateTime Edited { get; set; }

        public Guid EditorId { get; set; }
    }
}
