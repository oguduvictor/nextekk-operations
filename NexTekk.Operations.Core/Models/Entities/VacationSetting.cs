﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class VacationSetting
    {
        public Guid UserId { get; set; }

        public decimal HoursPerPeriod { get; set; }

        public decimal YearBeginBalance { get; set; }

        public Guid CreatorId { get; set; }

        public DateTime Created { get; set; }

        public Guid EditorId { get; set; }

        public DateTime Edited { get; set; }
    }
}
