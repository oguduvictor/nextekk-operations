﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class UserRoleEntity
    {
        public Guid UserId { get; private set; }

        public virtual UserEntity User { get; private set; }

        public string RoleId { get; private set; }

        public virtual RoleEntity Role { get; private set; }
    }
}
