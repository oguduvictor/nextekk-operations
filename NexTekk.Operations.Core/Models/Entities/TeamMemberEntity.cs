﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class TeamMemberEntity
    {
        public Guid ProjectId { get; set; }

        public Guid UserId { get; set; }

        public UserEntity User { get; set; }

        public TeamMemberRole Role { get; set; }
    }
}
