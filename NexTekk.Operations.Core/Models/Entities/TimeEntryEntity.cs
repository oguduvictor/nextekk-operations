﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class TimeEntryEntity
    {
        public Guid Id { get; set; }

        public string Task { get; set; }

        public DateTime BeginDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public string Comment { get; set; }

        public string ManagerComment { get; set; }

        public Guid? ProjectId { get; set; }

        public Guid? ActivityId { get; set; }

        public virtual ActivityEntity Activity { get; set; }

        public virtual ProjectSummary Project { get; set; }

        public TimeEntryStatus Status { get; set; }

        public bool Billable { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatorId { get; set; }

        public virtual UserEntity Creator { get; set; }

        public DateTime Edited { get; set; }

        public Guid EditorId { get; set; }

        public virtual UserEntity Editor { get; set; }

        public DateTime? Approved { get; set; }

        public Guid? ApproverId { get; set; }

        public virtual UserEntity Approver { get; set; }

        public DateTime? Denied { get; set; }

        public Guid? DenierId { get; set; }

        public virtual UserEntity Denier { get; set; }
    }
}
