﻿using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class ContactEntity
    {
        public ContactEntity()
        {
        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public DateTime Created { get; set; }

        public bool Deleted { get; set; }

        public Guid CreatorId { get; set; }

        public Guid EditorId { get; set; }

        public virtual UserEntity Creator { get; set; }

        public virtual UserEntity Editor { get; set; }

        public DateTime Edited { get; set; }
    }
}
