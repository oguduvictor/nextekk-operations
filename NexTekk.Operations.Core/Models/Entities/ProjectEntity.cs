﻿using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class ProjectEntity
    {
        public ProjectEntity()
        {
            TeamMembers = new List<TeamMemberEntity>();
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public bool Billable { get; set; }

        public Guid ClientId { get; set; }

        public virtual ClientEntity Client { get; set; }
        
        public DateTime Created { get; set; }

        public Guid CreatorId { get; set; }

        public DateTime Edited { get; set; }

        public Guid EditorId { get; set; }
        
        public virtual UserEntity Creator { get; set; }

        public virtual UserEntity Editor { get; set; }

        public bool Deleted { get; set; }

        public ProjectStatus Status { get; set; }

        public virtual ICollection<TeamMemberEntity> TeamMembers { get; set; }
    }
}
