﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class ClientsContactsEntity
    {
        public Guid ClientId { get; set; }

        public virtual ClientEntity Client { get; set; }

        public int ContactId { get; set; }

        public virtual ContactEntity Contact { get; set; }

        public DateTime Created { get; set; }
    }
}
