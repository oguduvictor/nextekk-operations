﻿using System;

namespace NexTekk.Operations.Core.Models.Entities
{
    public class UserEntity
    {
        public UserEntity()
        {
        }

        public UserEntity(Guid id, string firstName = null, string lastName = null)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }

        public UserEntity(Guid id, string firstName = null, string lastName = null, string email = null)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }


        public Guid Id { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Email { get; private set; }
    }
}