﻿using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models
{
    public class Email
    {
        public string Subject { get; set; }

        public string Body { get; set; }

        public string Sender { get; set; }

        public IEnumerable<string> Recipients { get; set; }

        public IEnumerable<string> Bcc { get; set; }

        public IEnumerable<string> CC { get; set; }
    }
}
