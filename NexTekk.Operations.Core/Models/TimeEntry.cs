﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class TimeEntry : TimeEntryDateRange
    {
        public TimeEntry()
        {
        }

        public TimeEntry(TimeEntryEntity entity)
        {
            Id = entity.Id;
            Task = entity.Task;
            BeginDateTime = entity.BeginDateTime;
            EndDateTime = entity.EndDateTime;
            Billable = entity.Billable;
            Comment = entity.Comment;
            Status = entity.Status;
            ManagerComment = entity.ManagerComment;
            Created = entity.Created;
            Creator = entity.Creator == null ? null : new User(entity.Creator);

            if (!entity.Activity.IsNull())
            {
                Activity = new ActivitySummary(entity.Activity);
            }
            else if (!entity.Project.IsNull())
            {
                Activity = new ActivitySummary(entity.Project);
            }
        }

        public Guid Id { get; set; }

        public string Task { get; set; }

        public bool Billable { get; set; }

        public ActivitySummary Activity { get; set; }

        public TimeEntryStatus Status { get; set; }

        public string Comment { get; set; }

        public string ManagerComment { get; set; }

        public DateTime Created { get; set; }

        public User Creator { get; set; }
    }
}
