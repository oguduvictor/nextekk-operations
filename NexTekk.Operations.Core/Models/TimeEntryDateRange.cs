﻿namespace NexTekk.Operations.Core.Models
{
    public class TimeEntryDateRange : DateTimeRange
    {
        public override bool IsValid()
        {
            var minAllowedMinutes = 15;

            return base.IsValid()
                && BeginDateTime.Date.Equals(EndDateTime.Date) // Begin and End dates must be on the same day
                && EndDateTime.Subtract(BeginDateTime).TotalMinutes >= minAllowedMinutes;
        }
    }
}
