﻿using NexTekk.Operations.Core.Helpers;
using NexTekk.Operations.Core.Models.Entities;
using NexTekk.Operations.Core.Models.Enums;
using System;
using System.Collections.Generic;

namespace NexTekk.Operations.Core.Models
{
    public class Project
    {
        public Project()
        {
            TeamMembers = new List<TeamMember>();
        }

        public Project(ProjectEntity entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Billable = entity.Billable;
            Created = entity.Created;
            Status = entity.Status;
            Edited = entity.Edited;
            Deleted = entity.Deleted;
            TeamMembers = new List<TeamMember>();

            if (!entity.Client.IsNull() && !entity.Client.Deleted)
            {
                Client = new ClientSummary(entity.Client);
            }
            else
            {
                throw new Exception("This client associated with this project does not exist or has been deleted");
            }
            
            foreach (var member in entity.TeamMembers)
            {
                TeamMembers.Add(new TeamMember(member));
            }
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public bool Billable { get; set; }
        
        public virtual ClientSummary Client { get; set; }

        public ProjectStatus Status { get; set; }

        public DateTime Edited { get; set; }

        public DateTime Created { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<TeamMember> TeamMembers { get; set; }
    }
}
