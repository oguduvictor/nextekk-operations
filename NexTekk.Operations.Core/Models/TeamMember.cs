﻿using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Core.Models
{
    public class TeamMember
    {
        public TeamMember()
        {
        }

        public TeamMember(TeamMemberEntity entity)
        {
            User = new User(entity.User);
            Role = entity.Role;
        }
        
        public User User { get; set; }

        public TeamMemberRole Role { get; set; }
    }
}
