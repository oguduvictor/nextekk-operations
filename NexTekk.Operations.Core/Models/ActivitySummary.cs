﻿using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class ActivitySummary
    {
        public ActivitySummary()
        {
        }

        public ActivitySummary(ActivityEntity entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Billable = entity.Billable;
        }

        public ActivitySummary(ProjectSummary entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Billable = entity.Billable;
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public bool Billable { get; set; }
    }
}
