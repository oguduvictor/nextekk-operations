﻿using NexTekk.Operations.Core.Models.Enums;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class ProjectSearchCriteria
    {
        public Guid? ProjectManagerId { get; set; }

        public Guid? CreatorId { get; set; }

        public Guid? ClientId { get; set; }

        public ProjectStatus? Status { get; set; }
    }
}
