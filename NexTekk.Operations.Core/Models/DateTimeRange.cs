﻿using System;

namespace NexTekk.Operations.Core.Models
{
    public class DateTimeRange
    {
        public DateTime BeginDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public bool Overlaps(DateTimeRange dateTimeRange)
        {
            var condition1 = EndDateTime <= dateTimeRange.BeginDateTime;
            var condition2 = BeginDateTime >= dateTimeRange.EndDateTime;

            return !(condition1 || condition2);
        }

        public virtual bool IsValid()
        {
            var earliestAllowableYear = 1900;

            return BeginDateTime.Year > earliestAllowableYear
                && EndDateTime.Year > earliestAllowableYear
                && EndDateTime > BeginDateTime;
        }
    }
}
