﻿using Microsoft.Extensions.Configuration;
using System;
using NexTekk.Operations.Core.Helpers;

namespace NexTekk.Operations.Core.Models
{
    public class AppSettings
    {
        private readonly IConfiguration _configuration;

        public AppSettings(IConfiguration configuration)
        {
            _configuration = configuration;

            SenderEmail = configuration["SenderEmail"];
            EnableStatusChangeEmail = configuration["EnableStatusChangeEmail"].IsTrue();
            DefaultLogLevel = configuration["Logging:LogLevel:Default"] ?? "Debug";
            LoggingDbConnectionString = configuration.GetConnectionString("LoggingDBConnection");
            LoggingTargets = (configuration["Logging:LogLevel:Targets"] ?? string.Empty)
                .Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public string SenderEmail { get; private set; }

        public bool EnableStatusChangeEmail { get; private set; }

        public string DefaultLogLevel { get; private set; }

        public string LoggingDbConnectionString { get; private set; }

        public string[] LoggingTargets { get; private set; }

        public string GetValue(string key, string defaultValue = null)
        {
            return _configuration[key] ?? defaultValue;
        }

        public string GetConnectionString(string name)
        {
            return _configuration.GetConnectionString(name);
        }
    }
}
