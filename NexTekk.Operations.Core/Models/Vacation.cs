﻿using NexTekk.Operations.Core.Models.Entities;
using System;

namespace NexTekk.Operations.Core.Models
{
    public class Vacation
    {
        private const int PAY_PERIODS_PER_MONTH = 2;
        private readonly int _payPeriodsCount;

        public Vacation()
        {
            _payPeriodsCount = GetPayPeriodsCount();
        }

        public Vacation(VacationEntity entity)
            : this()
        {
            Id = entity.UserId;
            Employee = new User(entity.User);
            HoursPerPeriod = entity.HoursPerPeriod;
            YearBeginBalance = entity.YearBeginBalance;
            TotalHoursTaken = entity.TotalHoursTaken ?? 0;
            Year = entity.Year;
            Edited = entity.Edited;
        }

        public Guid Id { get; set; }

        public virtual User Employee { get; set; }

        public decimal HoursPerPeriod { get; set; }

        public decimal TotalHoursTaken { get; set; }

        public decimal TotalHoursAvailable
        {
            get
            {
                return TotalHoursAccrued - TotalHoursTaken;
            }
        }

        public decimal TotalHoursAccrued
        {
            get
            {
                return (_payPeriodsCount * HoursPerPeriod) + YearBeginBalance;
            }
        }

        public decimal YearBeginBalance { get; set; }

        public int Year { get; set; }

        public DateTime Edited { get; set; }

        private int GetPayPeriodsCount()
        {
            var daysInMonth = 30m;
            var currentMonth = DateTime.Now.Month;
            var currentDay = DateTime.Now.Day;
            var payPeriods = (PAY_PERIODS_PER_MONTH * (currentMonth - 1))  + 
                                ((int)Math.Floor(currentDay / (daysInMonth / PAY_PERIODS_PER_MONTH)));

            return payPeriods;
        }
    }
}
