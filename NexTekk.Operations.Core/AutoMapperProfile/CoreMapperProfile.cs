﻿using AutoMapper;
using NexTekk.Operations.Core.Models;
using NexTekk.Operations.Core.Models.CashFlow;
using NexTekk.Operations.Core.Models.Entities;

namespace NexTekk.Operations.Core.AutoMapperProfile
{
    public class CoreMapperProfile : Profile
    {
        public CoreMapperProfile()
        {
            CreateMap<UserEntity, User>()
                .ForMember(x => x.FullName, y => y.Ignore());
            CreateMap<ClientEntity, ClientSummary>();
            CreateMap<Expense, Revenue>(MemberList.None)
                .ForMember(x => x.PaymentDate, y => y.MapFrom(z => z.Date))
                .ForMember(x => x.Amount, y => y.MapFrom(z => -z.Amount))
                .ForMember(x => x.CreatedById, y => y.MapFrom(z => z.CreatedById))
                .ForMember(x => x.ModifiedById, y => y.MapFrom(z => z.ModifiedById));
        }
    }
}
